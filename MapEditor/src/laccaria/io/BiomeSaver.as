package laccaria.io 
{
	import com.adobe.images.JPGEncoder;
	import com.adobe.images.PNGEncoder;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Point;
	import flash.utils.ByteArray;
	import laccaria.model.Biome;
	import laccaria.model.gridspace.gridspace_object.GridspaceObjectManager;
	import laccaria.model.gridspace.gridspace_object.type.GridspaceObjectType;
	
	public class BiomeSaver 
	{
		public static const BACKGROUND_SLICE_DIRECTORY	:String = "images\\background";
		public static const SPRITE_SHEET_DIRECTORY		:String = "images\\spriteSheets";
		public static const ICON_DIRECTORY				:String = "images\\icons";
		
		public static const BACKGROUND_SLICE_EXTENSION	:String = ".jpg";
		public static const ICON_EXTENSION				:String = ".png";
		public static const SPRITE_SHEET_EXTENSION		:String = ".png";
		public static const BIOME_XML_EXTENSION			:String = ".txt";
		
		private var _biome:Biome;
		private var _gridspaceObjectManager:GridspaceObjectManager
		private var _saveDirectory:File;
		private var _jpgEncoder:JPGEncoder;
		
		private var _uniqueTypes:Array;
		
		public function BiomeSaver() 
		{
			init();
		}
		
		private function init():void
		{
			_jpgEncoder = new JPGEncoder( 100 );
		}
		
		public function saveBiome( biome:Biome, gridspaceObjectManager:GridspaceObjectManager ):void
		{
			_biome = biome;
			_gridspaceObjectManager = gridspaceObjectManager;
			
			//Prompt the user for a save directory
			_saveDirectory = new File();
			_saveDirectory.addEventListener( Event.SELECT, saveDirectorySelectedHandler );
			_saveDirectory.addEventListener( Event.CANCEL, saveDirectoryCanceledHandler );
			_saveDirectory.browseForDirectory( "Select save location" );
		}
		
		private function createBackgroundFiles( _backgroundSlices:Array ):void
		{
			
		}
		
		private function saveDirectoryCanceledHandler( e:Event ):void
		{
			_saveDirectory.removeEventListener( Event.SELECT, saveDirectorySelectedHandler );
			_saveDirectory.removeEventListener( Event.CANCEL, saveDirectoryCanceledHandler );
		}
		
		private function saveDirectorySelectedHandler( e:Event ):void
		{
			_saveDirectory.removeEventListener( Event.SELECT, saveDirectorySelectedHandler );
			_saveDirectory.removeEventListener( Event.CANCEL, saveDirectoryCanceledHandler );
			
			//Add a directory in the selected save directory with the same name as the biome's name
			_saveDirectory = _saveDirectory.resolvePath( _biome.name );
			_saveDirectory.createDirectory();
			
			saveBackgroundSlices();
			saveObjectSprites();
			saveXML();
		}
		
		private function saveBackgroundSlices():void
		{
			var imagesSaveDirectory:File = _saveDirectory.resolvePath( BACKGROUND_SLICE_DIRECTORY );
			var backgroundSlices:Array = _biome.backgroundSlices;			
			
			for ( var i:int = 0; i < backgroundSlices.length; i++ )
			{
				for ( var j:int = 0; j < backgroundSlices[ i ].length; j++ )
				{
					//Get the current background slice
					var curBackgroundSlice:Bitmap = backgroundSlices[ i ][ j ];
					
					//Convert to a jpg byte array
					var backgroundSliceByteArray:ByteArray = _jpgEncoder.encode( curBackgroundSlice.bitmapData );
					
					//Create a file in the save directory
					var curBackgroundName:String = _biome.getBackgroundName( new Point( j, i ) );
					var curFile:File = imagesSaveDirectory.resolvePath( curBackgroundName );
					
					//Write the file
					writeFile( curFile, backgroundSliceByteArray );
				}
			}
		}
		
		private function saveObjectSprites():void
		{
			var spriteSheetDirectory:File 	= _saveDirectory.resolvePath( SPRITE_SHEET_DIRECTORY );
			var iconDirectory		:File 	= _saveDirectory.resolvePath( ICON_DIRECTORY );			
			
			//Get unique gridspace object types in the biome
			var uniqueIdTypes:Array 	= _biome.getUniqueIdTypes();		
			_uniqueTypes = new Array();
			
			for each ( var idUniqueType:int in uniqueIdTypes )
			{
				_uniqueTypes.push( _gridspaceObjectManager.findType( idUniqueType ) );
			}
			
			//Iterate through them
			for each ( var uniqueType:GridspaceObjectType in _uniqueTypes )
			{
				//Save the unique sprite sheets
				if ( uniqueType.spriteSheet )
				{
					//Convert to a png byte array
					var spriteByteArray:ByteArray = PNGEncoder.encode( uniqueType.spriteSheet.bitmapData );
					
					//Create a file in the save directory
					var fileName:String = uniqueType.fileName + SPRITE_SHEET_EXTENSION;
					var file:File = spriteSheetDirectory.resolvePath( fileName );
					
					//Write the file
					writeFile( file, spriteByteArray );
				}
				
				//Save the unique sprite icons
				if ( uniqueType.sprite )
				{
					//Convert to a png byte array
					spriteByteArray = PNGEncoder.encode( uniqueType.sprite.bitmapData );
					
					//Create a file in the save directory
					fileName = uniqueType.fileName + ICON_EXTENSION;
					file = iconDirectory.resolvePath( fileName );
					
					//Write the file
					writeFile( file, spriteByteArray );
				}
			}
		}
		
		private function writeFile( file:File, byteArray:ByteArray ):void
		{
			//Create a file stream to save the file
			var fileStream:FileStream = new FileStream();
			
			try
			{
				//Open the file stream in write mode
				fileStream.open( file, FileMode.WRITE );
				
				//Write the file
				fileStream.writeBytes( byteArray );
				
				//Close the file
				fileStream.close();
			}
			catch ( e:Error )
			{
				trace( "Error writing file in SaveManager: " + e.message );
			}
		}
		
		private function saveXML():void
		{
			var xmlString:String = "<biome>\n\t";
			xmlString += _gridspaceObjectManager.customTypesToXML( _uniqueTypes ) + "\n\t";
			xmlString += _biome.toXMLString() + "\n</biome>";
			
			var xml:XML = new XML( xmlString );
			var biomeXMLFile:File = _saveDirectory.resolvePath( "biome" + BIOME_XML_EXTENSION );
			writeXML( biomeXMLFile, xml );
		}
		
		private function writeXML( file:File, xml:XML ):void
		{
			//Create a file stream to save the file
			var fileStream:FileStream = new FileStream();
			
			try
			{
				//Open the file stream in write mode
				fileStream.open( file, FileMode.WRITE );
				
				//Write the file
				fileStream.writeUTFBytes( xml );
				
				//Close the file
				fileStream.close();
			}
			catch ( e:Error )
			{
				trace( "Error writing file in SaveManager: " + e.message );
			}
		}
	}
}