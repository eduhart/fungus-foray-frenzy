package laccaria.io 
{
	import laccaria.model.Biome;
	import laccaria.model.gridspace.gridspace_object.GridspaceObjectManager;
	
	public class IOManager 
	{
		private var _biomeSaver			:BiomeSaver;
		private var _biomeLoader		:BiomeLoader;
		private var _backgroundLoader	:ImageLoader;
		
		public function IOManager() 
		{
			init();
		}
		
		private function init():void
		{
			_biomeSaver = new BiomeSaver();
			_biomeLoader = new BiomeLoader();
			_backgroundLoader = new ImageLoader();
		}
		
		public function saveBiome( biome:Biome, gridspaceObjectManager:GridspaceObjectManager ):void
		{
			_biomeSaver.saveBiome( biome, gridspaceObjectManager );
		}
		
		public function loadBiome( caller:Object, callback:Function, gridspaceObjectManager:GridspaceObjectManager ):void
		{
 			_biomeLoader.loadBiome( caller, callback, gridspaceObjectManager );
		}
		
		public function loadBackground( caller:Object, callback:Function ):void
		{
			_backgroundLoader.loadImage( caller, callback );
		}
	}
}