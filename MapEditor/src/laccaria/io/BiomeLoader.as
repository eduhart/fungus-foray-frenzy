package laccaria.io 
{
	import laccaria.events.EventManager;
	import laccaria.events.gridspace_object.GridspaceObjectManagerEvent;
	import laccaria.events.gridspace_object.GridspaceObjectTypeEvent;
	import laccaria.model.gridspace.Gridspace;
	import laccaria.model.gridspace.Direction;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.FileFilter;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import laccaria.model.Biome;
	import laccaria.model.gridspace.gridspace_object.category.GridspaceObjectCategory;
	import laccaria.model.gridspace.gridspace_object.GridspaceObjectManager;
	import laccaria.model.gridspace.gridspace_object.type.GridspaceObjectType;
	import laccaria.utility.MushroomUtil;
	
	public class BiomeLoader 
	{
		private var _loadBiomeCaller		:Object;
		private var _loadBiomeCallback		:Function;		
		private var _biomeXMLFile			:File;
		private var _backgroundSlicesFile	:File;
		private var _gridspaceObjectManager	:GridspaceObjectManager;
		
		private var _biomeXML				:XML;
		
		private var _numSlicesX				:int;
		private var _numSlicesY				:int;
		private var _backgroundSlices		:Array;
		private var _backroundSlicesFile	:File;
		private var _backgroundSlicesLoaders:Array;
		private var _backgroundSlicesLoaded	:int;
		
		private var _numTypesToInitialize	:int;
		private var _numTypesInitialized	:int;
		
		public function BiomeLoader() 
		{
		}
		
		public function loadBiome( caller:Object, callback:Function, gridspaceObjectManager:GridspaceObjectManager ):void
		{			
			_biomeXMLFile = new File();
			_backgroundSlicesFile = new File();
			_backgroundSlices = new Array();
			_backgroundSlicesLoaders = new Array();
			_backgroundSlicesLoaded = 0;
			_gridspaceObjectManager = gridspaceObjectManager;
			
			_loadBiomeCaller = caller;
			_loadBiomeCallback = callback;
			
			var fileFilter:FileFilter = new FileFilter( "Text", "*.txt;*.xml" );
			var filters:Array = [ fileFilter ];
			
			_biomeXMLFile.addEventListener( Event.SELECT, fileSelectHandler );
			_biomeXMLFile.addEventListener( Event.CANCEL, loadCanceledHandler );
			
			//Prompt the user to select the biome's xml file
			_biomeXMLFile.browse( filters );
		}
		
		//Called if the user cancels the prompt to load the biome
		private function loadCanceledHandler( e:Event ):void
		{
			_biomeXMLFile.removeEventListener( Event.SELECT, fileSelectHandler );
			_biomeXMLFile.removeEventListener( Event.CANCEL, loadCanceledHandler );
		}
		
		//Called if the user selects a file when prompted to load a biome
		private function fileSelectHandler( e:Event ):void
		{
			_biomeXMLFile.removeEventListener( Event.SELECT, fileSelectHandler );
			_biomeXMLFile.removeEventListener( Event.CANCEL, loadCanceledHandler );
			
			_backgroundSlicesFile = _biomeXMLFile.parent.resolvePath( BiomeSaver.BACKGROUND_SLICE_DIRECTORY );
			_biomeXMLFile.addEventListener( Event.COMPLETE, xmlLoadedHandler );
			_biomeXMLFile.load();
		}
		
		private function xmlLoadedHandler( e:Event ):void
		{
			var xmlFileByteArray:ByteArray = _biomeXMLFile.data;
			var xmlFileString:String = xmlFileByteArray.toString();
			_biomeXML = new XML( xmlFileString );
			loadBackgroundSlices();
		}
		
		private function loadBackgroundSlices():void
		{
			_numSlicesX = _biomeXML.max_width[ 0 ];
			_numSlicesY = _biomeXML.max_height[ 0 ];
			var curIndex:int = 0;
			
			for ( var i:int = 0; i < _numSlicesY; i++ )
			{
				_backgroundSlices.push( new Array() );
				
				for ( var j:int = 0; j < _numSlicesX; j++ )
				{
					var curImageName:String = "background_" + curIndex + BiomeSaver.BACKGROUND_SLICE_EXTENSION;
					var curURL:String = _backgroundSlicesFile.url + "/" + curImageName;
					var curLoader:Loader = new Loader();
					_backgroundSlicesLoaders.push( curLoader );
					
					trace( curURL );
					
					curLoader.contentLoaderInfo.addEventListener( Event.COMPLETE, backgroundSliceLoaded );
					curLoader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, backgroundSliceFailed );
					curLoader.load( new URLRequest( curURL ) );
					
					curIndex++;
				}
			}
		}
		
		private function backgroundSliceLoaded( e:Event ):void
		{
			var loader:Loader = ( e.target as LoaderInfo ).loader;
			var indexOfLoader:int = _backgroundSlicesLoaders.indexOf( loader );
			var rowOfLoader:int = indexOfLoader / _numSlicesX;
			var colOfLoader:int = indexOfLoader % _numSlicesX;
			
			//Create the bitmap
			var bitmapData:BitmapData = new BitmapData( loader.width, loader.height );
			bitmapData.draw( loader );
			var bitmap:Bitmap = new Bitmap( bitmapData );
			_backgroundSlices[ rowOfLoader ][ colOfLoader ] = bitmap;
			
			_backgroundSlicesLoaded++;
			
			//If all the slices are loaded, init the gridspace object manager
			if ( _backgroundSlicesLoaded == _numSlicesX * _numSlicesY )
			{
				initGridspaceObjectManager();
			}
		}
		
		private function backgroundSliceFailed( e:IOErrorEvent ):void
		{
			var loaderInfo:LoaderInfo = e.target as LoaderInfo;
			var url:String = loaderInfo.url
			trace( "Loading background slice " + url + " failed!" );
		}
		
		private function initGridspaceObjectManager():void
		{
			_numTypesToInitialize = 0;
			
			//Load the custom gridspace objects
			var customGridspaceObjectXMLList:XMLList = _biomeXML.gridspaceObjectType;
			
			for each ( var customGridspaceObjectXML:XML in customGridspaceObjectXMLList )
			{
				_numTypesToInitialize++;
				
				var idType		:int 	= customGridspaceObjectXML.idType		[ 0 ];
				var idCategory	:int 	= customGridspaceObjectXML.idCategory	[ 0 ];
				var spritePath	:String = customGridspaceObjectXML.spritePath	[ 0 ];
				var displayName	:String = customGridspaceObjectXML.displayName	[ 0 ];
				var fileName	:String = customGridspaceObjectXML.fileName		[ 0 ];
				
				spritePath = MushroomUtil.strReplace( spritePath, "\\\\", "\\" );				
				spritePath = _biomeXMLFile.parent.url + "/" + spritePath;
				
				//Figure out if the custom type is background or foreground
				if ( idCategory == GridspaceObjectCategory.BACKGROUND_MISC )
				{
					_gridspaceObjectManager.addBackgroundMiscType( idType, displayName, fileName, spritePath );
				}
				else if ( idCategory == GridspaceObjectCategory.FOREGROUND_MISC )
				{
					_gridspaceObjectManager.addForegroundMiscType( idType, displayName, fileName, spritePath );
				}
			}
			
			if ( _numTypesToInitialize > 0 )
			{
				//Wait until all of the custom types are initialized before creating the biome
				EventManager.AddGlobalEventListener( GridspaceObjectTypeEvent.INITIALIZED, gridspaceObjectTypeInitializedHandler );
			}
			else
			{
				//Or, if there are no custom types, create the biome now!
				createBiome();
			}
		}
		
		private function gridspaceObjectTypeInitializedHandler( e:GridspaceObjectTypeEvent ):void
		{
			_numTypesInitialized++;
			
			if ( _numTypesInitialized >= _numTypesToInitialize )
			{
				EventManager.RemoveGlobalEventListener( GridspaceObjectTypeEvent.INITIALIZED, gridspaceObjectTypeInitializedHandler );
				createBiome();
			}			
		}
		
		private function createBiome():void
		{
			//Get the biome name
			var biomeName:String = _biomeXML.name[ 0 ];
			
			//Get the biome background
			var biomeBackground:Bitmap = createBackground();
			
			//Get the gridspaces
			var gridspaces:Array = loadGridspaces();			
			
			//Create the biome!
			var biome:Biome = new Biome( biomeName, gridspaces );
			biome.background = biomeBackground;
			biome.backgroundSlices = _backgroundSlices;
			
			//Call the callback!
			_loadBiomeCallback.call( _loadBiomeCaller, biome );
		}
		
		private function loadGridspaces():Array 
		{
			var gridspaces:Array = new Array();
			
			//Get the biome gridspaces
			var gridspacesList:XMLList = _biomeXML.gridspace;
			
			for each ( var gridspaceXML:XML in gridspacesList )
			{
				//Get gridspace id
				var idGridspace:int = gridspaceXML.idNum[ 0 ];
				
				//Get gridspace coordinates
				var gridspaceCoordinatesX:int = gridspaceXML.coordinate_x[ 0 ];
				var gridspaceCoordinatesY:int = gridspaceXML.coordinate_y[ 0 ];
				var gridspaceCoordinates:Point = new Point( gridspaceCoordinatesX, gridspaceCoordinatesY );
				
				//Get gridspace background file name
				var gridspaceBackgroundDirectory:String = gridspaceXML.bkgSource[ 0 ];
				var stringToRemove:String = MushroomUtil.strReplace( BiomeSaver.BACKGROUND_SLICE_DIRECTORY, "\\", "\\\\" ) + "\\\\";
				var gridspaceBackgroundFileName:String = gridspaceBackgroundDirectory.replace( stringToRemove, "" );
				var gridspaceBackground:Bitmap = _backgroundSlices[ gridspaceCoordinatesY ][ gridspaceCoordinatesX ];
				
				//Create the gridspace
				var gridspace:Gridspace = new Gridspace( idGridspace, gridspaceCoordinates, gridspaceBackgroundFileName, gridspaceBackground );
				gridspaces.push( gridspace );
				
				//Add the gridspace's connections
				var connections:XMLList = gridspaceXML.connection;
				setGridspaceConnections( gridspace, connections );
				
				addGridspaceObjects( gridspace, gridspaceXML );
			}
			
			return gridspaces;
		}
		
		private function setGridspaceConnections( gridspace:Gridspace, connectionsXMLList:XMLList ):void
		{				
			for each ( var connectionXML:XML in connectionsXMLList )
			{
				var idGridspaceTo:int = connectionXML.to[ 0 ];
				var directionString:String = connectionXML.type[ 0 ];
				var direction:int = Direction.StringToDirection( directionString );
				
				gridspace.setConnection( direction, idGridspaceTo );
			}
		}
		
		private function addGridspaceObjects( gridspace:Gridspace, gridspaceXML:XML ):void
		{
			var npcXMLList		:XMLList = gridspaceXML.npc;				//Get npcs
			var mushroomXMLList	:XMLList = gridspaceXML.mushroomSpawner;	//Get mushrooms
			var itemChestXMLList:XMLList = gridspaceXML.itemChest;			//Get item chests
			var bkgObjectXMLList:XMLList = gridspaceXML.bkgObject;			//Get background objects
			var cowObjectXMLList:XMLList = gridspaceXML.cow;
			
			var gridspaceObjectXMLLists:Array = [ npcXMLList, mushroomXMLList, itemChestXMLList, bkgObjectXMLList, cowObjectXMLList ];
			
			for each ( var gridspaceObjectXMLList:XMLList in gridspaceObjectXMLLists )			
			{
				for each ( var gridspaceObjectXML:XML in gridspaceObjectXMLList )
				{
					var idType		:int 					= gridspaceObjectXML		.idType[ 0 ];
					var type		:GridspaceObjectType 	= _gridspaceObjectManager	.findType( idType );
					
					var idObject	:int 					= gridspaceObjectXML		.idObject[ 0 ];
					var xCoord		:int 					= gridspaceObjectXML		.xCoord[ 0 ];
					var yCoord		:int 					= gridspaceObjectXML		.yCoord[ 0 ];
					var coordinates	:Point 					= new Point( xCoord, yCoord );
					
					gridspace.addGridspaceObject( idObject, coordinates, type );
				}
			}
		}
		
		private function createBackground():Bitmap
		{
			var backgroundWidth:Number = Gridspace.WIDTH * _numSlicesX;
			var backgroundHeight:Number = Gridspace.HEIGHT * _numSlicesY;
			var backgroundBitmapData:BitmapData = new BitmapData( backgroundWidth, backgroundHeight );
			
			var curDestination:Point = new Point( 0, 0 );
			var sourceRect:Rectangle = new Rectangle( 0, 0, Gridspace.WIDTH, Gridspace.HEIGHT );
			
			for ( var i:int = 0; i < _backgroundSlices.length; i++ )
			{
				for ( var j:int = 0; j < _backgroundSlices[ i ].length; j++ )
				{
					var curBackgroundSlice:Bitmap = _backgroundSlices[ i ][ j ];
					backgroundBitmapData.copyPixels( curBackgroundSlice.bitmapData, sourceRect, curDestination );
					curDestination.x += Gridspace.WIDTH;
				}
				
				curDestination.x = 0;
				curDestination.y += Gridspace.HEIGHT;
			}
			
			return new Bitmap( backgroundBitmapData );
		}
	}
}