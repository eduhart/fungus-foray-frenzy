package laccaria.io 
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.utils.ByteArray;
	
	public class ImageLoader 
	{
		//------Construction
		
		private var _file			:File;
		
		private var _caller			:Object;
		private var _callback		:Function;
		private var _imageLoader	:Loader;
		
		//------Getters/Setters
		
		public function get imagePath():String { return _file.url; }
		
		//------Construction
		
		public function ImageLoader() 
		{
			init();
		}
		
		private function init():void
		{
			initVars();
		}
		
		private function initVars():void
		{
			_file = new File();
			_imageLoader = new Loader();
		}
		
		//------Event Handlers
		
		
		
		//------Methods
		
		/**
		 * Prompt the user to browse for a background image, then load the selected image
		 * @param	caller		The object that called this function
		 * @param	callback	The function to call when the background is loaded
		 */
		public function loadImage( caller:Object, callback:Function ):void
		{
			_caller = caller;
			_callback = callback;
			
			var fileFilter:FileFilter = new FileFilter( "Images", "*.png;*.jpeg;*.jpg;*.gif;*.bmp" );
			var filters:Array = [ fileFilter ];
			
			_file.addEventListener( Event.SELECT, backgroundSpriteSelectedHandler );			
			_file.addEventListener( Event.CANCEL, backgroundSpriteCanceledHandler );
			
			_file.browse( filters );
		}
		
		//Called if the user cancels the prompt to load the background
		private function backgroundSpriteCanceledHandler( e:Event ):void
		{
			_file.removeEventListener( Event.SELECT, backgroundSpriteSelectedHandler );
			_file.removeEventListener( Event.CANCEL, backgroundSpriteCanceledHandler );
		}
		
		//Called if the user selects a file when prompted to load a background
		private function backgroundSpriteSelectedHandler( e:Event ):void
		{
			_file.removeEventListener( Event.SELECT, backgroundSpriteSelectedHandler );
			_file.removeEventListener( Event.CANCEL, backgroundSpriteCanceledHandler );
			
			_file.addEventListener( Event.COMPLETE, backgroundByteArrayLoadedHandler );
			_file.load();
		}
		
		//Called when the background byte array gets loaded
		private function backgroundByteArrayLoadedHandler( e:Event ):void
		{
			_file.removeEventListener( Event.COMPLETE, backgroundByteArrayLoadedHandler );
			
			//Get the byte array of the background
			var byteArray:ByteArray = _file.data;
			
			//Load the byte array as a bitmap
			_imageLoader.contentLoaderInfo.addEventListener( Event.COMPLETE, backgroundLoadedHandler );
			_imageLoader.loadBytes( byteArray );
		}
		
		private function backgroundLoadedHandler( e:Event ):void
		{
			_imageLoader.contentLoaderInfo.removeEventListener( Event.COMPLETE, backgroundLoadedHandler );
			
			//The background has been loaded. Woo!
			var background:Bitmap = _imageLoader.content as Bitmap;			
			_callback.call( _caller, background );
		}
	}
}