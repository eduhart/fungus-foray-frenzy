package laccaria.utility 
{
	import com.bit101.components.PushButton;
	import com.bit101.components.Text;
	import com.bit101.components.Window;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * A message box. Removes itself from the parent once the ok button is clicked.
	 */
	public class MessageBox extends Sprite
	{
		private static const MESSAGE_X				:int = 5;
		private static const MESSAGE_Y				:int = 10;
		private static const BUTTON_X				:int = 5;
		private static const BUTTON_Y_FROM_BOTTOM	:int = 5;
		
		private var _window:Window;
		
		public function MessageBox( title:String, message:String ) 
		{
			init( title, message );
		}
		
		private function init( title:String, message:String ):void
		{
			createWindow( title, message );
		}
		
		private function createWindow( title:String, message:String ):void
		{
			_window = new Window( this, 0, 0, title );
			
			//Add the message to the window
			var tMessage:Text = new Text( _window.content, 0, 0, message );
			
			var bOk:PushButton = new PushButton( null, 0, 0, "= [", okButtonPushedHandler );
			bOk.x = .5 * ( _window.width - bOk.width );
			bOk.y = .5 * ( _window.height - bOk.height - BUTTON_Y_FROM_BOTTOM );
			_window.content.addChild( bOk );
			
			addChild( _window );
		}
		
		private function okButtonPushedHandler( e:Event ):void
		{
			if ( parent )
			{
				parent.removeChild( this );
			}
		}
	}
}