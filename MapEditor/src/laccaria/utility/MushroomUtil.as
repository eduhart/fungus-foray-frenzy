package laccaria.utility 
{
	import flash.display.DisplayObject;
	import flash.geom.Point;
	
	public class MushroomUtil 
	{
		
		public function MushroomUtil() 
		{
			
		}
		
		public static function randomRange( min:Number, max:Number ):Number
		{
			return Math.random() * ( max - min ) + min;
		}
		
		/**
		 * Get the object's x and y position in the caller's coordinate system
		 * @param	caller	The display object whose coordinate system will be converted to
		 * @param	object	The object that will have its position converted
		 * @return	The location of the object in the caller's coordinate system
		 */
		public static function convertCoordinates( caller:DisplayObject, object:DisplayObject ):Point
		{
			var stageLoc:Point = object.localToGlobal( new Point( 0, 0 ) );
			return caller.globalToLocal( stageLoc );
		}
		
		public static function strReplace( str:String, search:String, replace:String ):String
		{
			return str.split( search ).join( replace );
		}
	}
}