package laccaria.model.gridspace 
{
	
	public class Direction 
	{
		public static const NORTH	:int = 0;
		public static const EAST	:int = 1;
		public static const SOUTH	:int = 2;
		public static const WEST	:int = 3;
		
		public function Direction() 
		{
			
		}
		
		/**
		 * Converts an int-form of a direction to a string-form of the direction.
		 * @param	direction	The int-form of the direction.
		 * @return	The string-form of the direction, or "unknown" if the int-form is
		 * 			not a direction.
		 */
		public static function DirectionToString( direction:int ):String
		{
			var directionString:String = "unknown";
			
			switch( direction )
			{
				case NORTH:
					directionString = "north";
					break;
				case EAST:
					directionString = "east";
					break;
				case SOUTH:
					directionString = "south";
					break;
				case WEST:
					directionString = "west";
					break;
			}
			
			return directionString;
		}
		
		/**
		 * Converts a string to its proper direction int
		 * @param	directionString	The string to convert to an int (must match the constant's name, not
		 * 							case sensitive.
		 * @return	The int version of the given direction string, or -1 if not able to convert.
		 */
		public static function StringToDirection( directionString:String ):int
		{
			var formatted:String = directionString.toLowerCase();
			var directionToReturn:int = -1;
			
			switch ( formatted )
			{
				case "north"	: directionToReturn = NORTH; 	break;
				case "east"		: directionToReturn = EAST;		break;
				case "south"	: directionToReturn = SOUTH;	break;
				case "west"		: directionToReturn = WEST;		break;
			}
			
			return directionToReturn;
		}
	}
}