package laccaria.model.gridspace.gridspace_object.type 
{
	import flash.display.Sprite;
	
	public class InvisibleType extends BackgroundMiscType
	{		
		public function InvisibleType( idType:int, displayName:String, fileName:String ) 
		{
			super( idType, displayName, fileName, "" );
			
			init();
		}
		
		private function init():void
		{
			_sprite = new InvisibleTypeBackground();
			_tileWidth = InvisibleTypeBackground.TILE_WIDTH;
			_tileHeight = InvisibleTypeBackground.TILE_HEIGHT;
		}
	}
}