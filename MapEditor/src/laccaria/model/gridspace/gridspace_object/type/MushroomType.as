package laccaria.model.gridspace.gridspace_object.type 
{
	import flash.display.Sprite;
	import laccaria.model.gridspace.gridspace_object.category.GridspaceObjectCategory;
	import laccaria.model.gridspace.gridspace_object.category.MushroomGridspaceObjectCategory;
	
	public class MushroomType extends AnimatedGridspaceObjectType
	{	
		//------Constants
		
		public	static const UBIQUITOUS				:int = 0;
		public	static const EDIBLE					:int = 1;
		public	static const POISONOUS				:int = 2;
		public	static const GLOWING				:int = 3;
		public	static const TRIPPY					:int = 4;
		
		private static const UBIQUITOUS_XML_NAME	:String = "littleBrown";
		private static const EDIBLE_XML_NAME		:String = "edible";
		private static const POISONOUS_XML_NAME		:String = "poisonous";
		private static const GLOWING_XML_NAME		:String = "glowing";
		private static const TRIPPY_XML_NAME		:String = "psychoactive";
		
		private static const SPAWN_CHANCE			:int 	= 35;
		
		//------Variables
		
		private var _idMushroom:int;
		
		//------Getters/Setters
		
		public function get idMushroom():int { return _idMushroom; }
		
		//------Construction		
		
		public function MushroomType( idType:int, displayName:String, fileName:String,
			spritePath:String, sheetPath:String, animCols:int, animFrames:int, idMushroom:int ) 
		{
			var category:MushroomGridspaceObjectCategory = new MushroomGridspaceObjectCategory();
			
			super( idType, displayName, fileName, category, spritePath, sheetPath, animCols, animFrames );
			
			init ( idMushroom );
		}
		
		private function init( idMushroom:int ):void
		{
			initVars( idMushroom );
		}
		
		private function initVars( idMushroom:int ):void
		{
			_idMushroom = idMushroom;
		}
		
		//------Methods
		
		public static function convertMushroomIdToXmlString( idType:int ):String
		{
			var toReturn:String = "";
			
			switch( idType )
			{
				case UBIQUITOUS:
					toReturn = UBIQUITOUS_XML_NAME;
					break;
				case EDIBLE:
					toReturn = EDIBLE_XML_NAME;
					break;
				case POISONOUS:
					toReturn = POISONOUS_XML_NAME;
					break;
				case GLOWING:
					toReturn = GLOWING_XML_NAME;
					break;
				case TRIPPY:
					toReturn = TRIPPY_XML_NAME;
					break;
			}
			
			return toReturn;
		}
		
		public override function xmlStringHook():String
		{
			return 	"<mushType>" 	+ convertMushroomIdToXmlString( _idType ) 	+ "</mushType>\n\t" +
					"<spawnChance>" + SPAWN_CHANCE 								+ "</spawnChance>";
		}
	}

}