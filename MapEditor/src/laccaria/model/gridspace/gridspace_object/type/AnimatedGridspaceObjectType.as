package laccaria.model.gridspace.gridspace_object.type 
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import laccaria.io.BiomeSaver;
	import laccaria.model.gridspace.gridspace_object.category.GridspaceObjectCategory;
	import laccaria.utility.MushroomUtil;
	
	public class AnimatedGridspaceObjectType extends GridspaceObjectType
	{		
		public function AnimatedGridspaceObjectType( idType:int, displayName:String, fileName:String,
			category:GridspaceObjectCategory, spritePath:String, sheetPath:String, animCols:int, animFrames:int ) 
		{
			super( idType, displayName, fileName, category, spritePath );
			
			init( sheetPath, animCols, animFrames );
		}
		
		private function init( sheetPath:String, animCols:int, animFrames:int ):void
		{
			initVars( animCols, animFrames );
			
			if ( sheetPath != "" )
			{
				loadSheet( sheetPath );
			}
		}
		
		private function initVars( animCols:int, animFrames:int ):void
		{
			_sheetPath 	= BiomeSaver.SPRITE_SHEET_DIRECTORY + "\\" + _fileName + BiomeSaver.SPRITE_SHEET_EXTENSION;			
			_sheetPath 	= MushroomUtil.strReplace( _sheetPath, "\\", "\\\\" );
			
			_animCols 	= animCols;
			_animFrames = animFrames;
		}		
		
		private function loadSheet( sheetPath:String ):void
		{
			var loader:Loader = new Loader();
			
			loader.contentLoaderInfo.addEventListener( Event		.COMPLETE, 	spriteSheetLoadHandler 	);
			loader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, 	ioErrorHandler 			);
			
			loader.load( new URLRequest( sheetPath ) );
		}		
		
		private function spriteSheetLoadHandler( e:Event ):void
		{
			_spriteSheet = ( e.target as LoaderInfo ).content as Bitmap;
		}
	}
}