package laccaria.model.gridspace.gridspace_object.type 
{
	import flash.display.Sprite;
	import laccaria.model.gridspace.gridspace_object.category.GridspaceObjectCategory;
	import laccaria.model.gridspace.gridspace_object.category.ItemChestGridspaceObjectCategory;
	
	public class ItemChestType extends GridspaceObjectType
	{
		//------Constants
		
		public	static const SHOVEL				:int = 0;
		public	static const BUG_SPRAY			:int = 1;
		public	static const COW_FEED			:int = 2;
		public	static const FLASHLIGHT			:int = 3;
		
		private static const SHOVEL_XML_NAME	:String = "shovel";
		private static const BUG_SPRAY_XML_NAME	:String = "bugSpray";
		private static const COW_FEED_XML_NAME	:String = "cowFeed";
		private static const FLASHLIGHT_XML_NAME:String = "lantern";
		
		//------Variables
		
		private var _idItem:int;
		
		//------Getters/Setters
		
		public function get idItem():int { return _idItem; }
		
		//------Construction
		
		public function ItemChestType( idType:int, displayName:String, fileName:String, spritePath:String, idItem:int ) 
		{
			var category:ItemChestGridspaceObjectCategory = new ItemChestGridspaceObjectCategory();
			
			super( idType, displayName, fileName, category, spritePath );
			
			init( idItem );
		}
		
		private function init( idItem:int ):void
		{
			initVars( idItem );
		}
		
		private function initVars( idItem:int ):void
		{
			_idItem = idItem;
		}
		
		//------Methods
		
		public static function convertIdItemToXMLString( idItem:int ):String
		{
			var xmlName:String = "";
			
			switch( idItem )
			{
				case SHOVEL:		xmlName = SHOVEL_XML_NAME; 		break;
				case BUG_SPRAY:		xmlName = BUG_SPRAY_XML_NAME;	break;
				case COW_FEED:		xmlName = COW_FEED_XML_NAME;	break;
				case FLASHLIGHT:	xmlName = FLASHLIGHT_XML_NAME;	break;
			}
			
			return xmlName;
		}
		
		public override function xmlStringHook():String
		{
			return "<initialItem>" + convertIdItemToXMLString( _idItem ) + "</initialItem>";
		}
	}
}