package laccaria.model.gridspace.gridspace_object.type 
{
	import laccaria.model.gridspace.gridspace_object.category.GridspaceObjectCategory;
	import laccaria.model.gridspace.gridspace_object.category.NPCGridspaceObjectCategory;
	
	public class NPCType extends AnimatedGridspaceObjectType
	{
		private var _idDesiredMush:int;
		
		public function NPCType( idType:int, displayName:String, fileName:String,
			spritePath:String, sheetPath:String, animCols:int, animFrames:int, idDesiredMush:int ) 
		{
			var category:NPCGridspaceObjectCategory = new NPCGridspaceObjectCategory();
			
			super( idType, displayName, fileName, category, spritePath, sheetPath, animCols, animFrames );
			
			init( idDesiredMush );
		}
		
		private function init( idDesiredMush:int ):void
		{
			initVars( idDesiredMush );
		}
		
		private function initVars( idDesiredMush:int ):void
		{
			_idDesiredMush = idDesiredMush;
		}
		
		public override function xmlStringHook():String
		{
			return "<desiredMush>" + MushroomType.convertMushroomIdToXmlString( _idDesiredMush ) + "</desiredMush>";
		}
	}

}