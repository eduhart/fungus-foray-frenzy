package laccaria.model.gridspace.gridspace_object.type 
{
	import laccaria.model.gridspace.gridspace_object.category.ForegroundMiscGridspaceObjectCategory;
	import laccaria.model.gridspace.gridspace_object.category.GridspaceObjectCategory;
	
	public class ForegroundMiscType extends GridspaceObjectType
	{
		
		public function ForegroundMiscType( idType:int, displayName:String, fileName:String, spritePath:String ) 
		{
			var category:ForegroundMiscGridspaceObjectCategory = new ForegroundMiscGridspaceObjectCategory();
			
			super( idType, displayName, fileName, category, spritePath );
		}		
	}
}