package laccaria.model.gridspace.gridspace_object.type 
{
	import laccaria.model.gridspace.gridspace_object.category.BackgroundMiscGridspaceObjectCategory;
	import laccaria.model.gridspace.gridspace_object.category.GridspaceObjectCategory;
	
	public class BackgroundMiscType extends GridspaceObjectType
	{
		
		public function BackgroundMiscType( idType:int, displayName:String, fileName:String, spritePath:String ) 
		{
			var category:BackgroundMiscGridspaceObjectCategory = new BackgroundMiscGridspaceObjectCategory();
			
			super( idType, displayName, fileName, category, spritePath );
		}		
	}
}