package laccaria.model.gridspace.gridspace_object.type 
{
	import laccaria.model.gridspace.gridspace_object.category.ForegroundMiscGridspaceObjectCategory;
	
	public class MosquitoType extends AnimatedGridspaceObjectType
	{
		
		public function MosquitoType( idType:int, displayName:String, fileName:String,
			spritePath:String, sheetPath:String, animCols:int, animFrames:int ) 
		{
			var category:ForegroundMiscGridspaceObjectCategory = new ForegroundMiscGridspaceObjectCategory();
			
			super( idType, displayName, fileName, category, spritePath, sheetPath, animCols, animFrames );
		}
		
	}

}