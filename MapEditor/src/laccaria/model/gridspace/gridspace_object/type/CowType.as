package laccaria.model.gridspace.gridspace_object.type 
{
	import laccaria.model.gridspace.gridspace_object.category.CowGridspaceObjectCategory;
	
	public class CowType extends AnimatedGridspaceObjectType
	{
		
		public function CowType( idType:int, displayName:String, fileName:String,
			spritePath:String, sheetPath:String, animCols:int, animFrames:int )
		{
			var category:CowGridspaceObjectCategory = new CowGridspaceObjectCategory();

			super( idType, displayName, fileName, category, spritePath, sheetPath, animCols, animFrames );
		}
	}
}