package laccaria.model.gridspace.gridspace_object.type 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import laccaria.events.EventManager;
	import laccaria.events.gridspace_object.GridspaceObjectTypeEvent;
	import laccaria.io.BiomeSaver;
	import laccaria.model.gridspace.Gridspace;
	import laccaria.model.gridspace.gridspace_object.category.GridspaceObjectCategory;
	import laccaria.utility.MushroomUtil;
	
	public class GridspaceObjectType 
	{
		//------Variables
		
		protected var _idType		:int;
		protected var _category		:GridspaceObjectCategory;
		
		protected var _displayName	:String;
		protected var _fileName		:String;
		
		protected var _tileWidth	:int;						//Width of the type in number of tile widths
		protected var _tileHeight	:int;						//Width of the type in number of tile heights
		
		protected var _spritePath	:String;
		protected var _sprite		:Bitmap;
		protected var _sheetPath	:String;
		protected var _spriteSheet	:Bitmap;
		
		protected var _animCols		:int;
		protected var _animFrames	:int;
		
		//------Getters/Setters
		
		public function get idType()			:int 	{ return _idType; 				}
		public function get spritePath()		:String { return _spritePath; 			}
		public function get sprite()			:Bitmap { return _sprite;				}
		public function get displayName()		:String { return _displayName; 			}
		public function get fileName()			:String { return _fileName;				}
		public function get tileWidth()			:int	{ return _tileWidth;			}
		public function get tileHeight()		:int	{ return _tileHeight;			}		
		public function get idCategory()		:int	{ return _category.idCategory; 	}
		public function get idLayer()			:int	{ return _category.idLayer;		}
		public function get xmlNameCategory()	:String	{ return _category.xmlName;		}
		
		public function get animCols()			:int	{ return _animCols;				}
		public function get animFrames()		:int	{ return _animFrames;			}
		public function get sheetPath()			:String	{ return _sheetPath;			}
		public function get spriteSheet()		:Bitmap	{ return _spriteSheet;			}
		
		//------Construction
		
		public function GridspaceObjectType( idType:int, displayName:String, fileName:String, category:GridspaceObjectCategory,
			spritePath:String )
		{
			init( idType, displayName, fileName, category, spritePath );
		}
		
		private function init( idType:int, displayName:String, fileName:String, category:GridspaceObjectCategory,
			spritePath:String ):void
		{
			initVars( idType, displayName, fileName, category )
			
			if ( spritePath != "" )
			{
				loadSprite( spritePath );
			}
			else
			{
				var gridspaceObjectTypeEvent:GridspaceObjectTypeEvent = new GridspaceObjectTypeEvent( GridspaceObjectTypeEvent.INITIALIZED );
				EventManager.DispatchGlobal( gridspaceObjectTypeEvent );
			}
		}
		
		private function initVars( idType:int, displayName:String, fileName:String, category:GridspaceObjectCategory ):void
		{
			_idType 		= idType;
			_displayName 	= displayName;
			_fileName		= fileName;
			_category = category;
			
			_animCols 	= 0;
			_animFrames = 0;
			
			if ( _fileName == "invisibleTile" )
			{
				_spritePath = "";
				_sheetPath = "";
			}
			else
			{
				_spritePath = BiomeSaver.ICON_DIRECTORY 		+ "\\" + _fileName + BiomeSaver.ICON_EXTENSION;
				_sheetPath 	= BiomeSaver.SPRITE_SHEET_DIRECTORY + "\\" + _fileName + BiomeSaver.SPRITE_SHEET_EXTENSION;
				
				_spritePath = MushroomUtil.strReplace( _spritePath, "\\", "\\\\" );
				_sheetPath = MushroomUtil.strReplace( _sheetPath, "\\", "\\\\" );
			}
		}
		
		private function loadSprite( spritePath:String ):void
		{
			var loader:Loader = new Loader();
			
			loader.contentLoaderInfo.addEventListener( Event		.COMPLETE, 	spriteLoadedHandler );
			loader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, 	ioErrorHandler 		);
			
			loader.load( new URLRequest( spritePath ) );
		}
		
		//------Event Handlers
		
		private function spriteLoadedHandler( e:Event ):void
		{
			_sprite = ( e.target as LoaderInfo ).content as Bitmap;
			
			if ( ! this is AnimatedGridspaceObjectType )
			{
				_spriteSheet = getSpriteCopy();
			}
			
			//Get the width and height in number of tiles
			_tileWidth = int( Math.round( _sprite.width / Gridspace.TILE_WIDTH ) );
			_tileHeight = int( Math.round( _sprite.height / Gridspace.TILE_HEIGHT ) );
			
			var gridspaceObjectTypeEvent:GridspaceObjectTypeEvent = new GridspaceObjectTypeEvent( GridspaceObjectTypeEvent.INITIALIZED );
			EventManager.DispatchGlobal( gridspaceObjectTypeEvent );
		}
		
		protected function ioErrorHandler( e:IOErrorEvent ):void
		{
			trace( "Trouble loading image at path: " + _spritePath );
		}		
		
		//------Methods
		
		public function getSpriteCopy():Bitmap
		{
			var bitmapDataClone:BitmapData = _sprite.bitmapData.clone();
			return new Bitmap( bitmapDataClone );
		}
		
		public function equals( idType:int ):Boolean
		{
			return _idType == idType;
		}
		
		//Meant to be overridden by subclasses
		public function xmlStringHook():String
		{
			return "";
		}
	}
}