package laccaria.model.gridspace.gridspace_object.type 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.geom.Rectangle;
	import laccaria.model.gridspace.Gridspace;
	
	public class InvisibleTypeBackground extends Bitmap
	{
		public static const TILE_WIDTH	:int = 1;
		public static const TILE_HEIGHT	:int = 1;
		
		private static const BACKGROUND_COLOR:uint 		= 0x66FFFFFF;
		
		public function InvisibleTypeBackground()
		{
			super();
			
			init();
		}
		
		private function init():void
		{			
			var rect:Rectangle = new Rectangle( 0, 0, Gridspace.TILE_WIDTH, Gridspace.TILE_HEIGHT );
			bitmapData = new BitmapData( rect.width, rect.height, true, BACKGROUND_COLOR );
		}		
	}
}