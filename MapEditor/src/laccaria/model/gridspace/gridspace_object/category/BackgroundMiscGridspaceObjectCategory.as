package laccaria.model.gridspace.gridspace_object.category 
{
	import laccaria.model.gridspace.Gridspace;
	
	public class BackgroundMiscGridspaceObjectCategory extends GridspaceObjectCategory
	{
		public	static const XML_NAME		:String = "bkgObject";
		public	static const LAYER			:int 	= Gridspace.COLLIDABLE_LAYER;
		private static const DISPLAY_NAME	:String = "Background Misc";
		
		public function BackgroundMiscGridspaceObjectCategory() 
		{
			super( GridspaceObjectCategory.BACKGROUND_MISC, LAYER, XML_NAME, DISPLAY_NAME );
		}		
	}
}