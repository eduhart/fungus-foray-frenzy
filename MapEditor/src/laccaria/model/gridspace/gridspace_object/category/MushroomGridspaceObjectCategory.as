package laccaria.model.gridspace.gridspace_object.category 
{
	import laccaria.model.gridspace.Gridspace;
	/**
	 * For all the homies repin' mushrooms
	 */
	public class MushroomGridspaceObjectCategory extends GridspaceObjectCategory
	{
		//------Constants
		
		public	static const XML_NAME				:String = "mushroomSpawner";
		public	static const LAYER					:int	= Gridspace.COLLIDABLE_LAYER;
		public	static const DISPLAY_NAME			:String	= "Mushrooms";
		
		//------Construction
		
		public function MushroomGridspaceObjectCategory() 
		{
			super( GridspaceObjectCategory.MUSHROOM, LAYER, XML_NAME, DISPLAY_NAME );
		}
	}
}