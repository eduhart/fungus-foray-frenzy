package laccaria.model.gridspace.gridspace_object.category 
{
	import laccaria.model.gridspace.Gridspace;
	
	public class ForegroundMiscGridspaceObjectCategory extends GridspaceObjectCategory
	{
		public	static const XML_NAME		:String = "bkgObject";
		public	static const LAYER			:int 	= Gridspace.FOREGROUND_LAYER;
		private static const DISPLAY_NAME	:String = "Foreground Misc";
		
		public function ForegroundMiscGridspaceObjectCategory() 
		{
			super( GridspaceObjectCategory.FOREGROUND_MISC, LAYER, XML_NAME, DISPLAY_NAME );
		}		
	}
}