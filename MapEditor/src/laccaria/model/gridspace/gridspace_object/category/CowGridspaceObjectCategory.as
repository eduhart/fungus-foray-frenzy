package laccaria.model.gridspace.gridspace_object.category 
{
	import laccaria.model.gridspace.Gridspace;
	
	public class CowGridspaceObjectCategory extends GridspaceObjectCategory
	{
		//------Constants
		
		public	static const XML_NAME			:String = "cow";
		public	static const LAYER				:int 	= Gridspace.COLLIDABLE_LAYER;
		private static const DISPLAY_NAME		:String = "Cow";
		
		public function CowGridspaceObjectCategory()
		{
			super( GridspaceObjectCategory.COW, LAYER, XML_NAME, DISPLAY_NAME ); 
		}		
	}
}