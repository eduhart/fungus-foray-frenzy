package laccaria.model.gridspace.gridspace_object.category 
{
	import laccaria.model.gridspace.Gridspace;
	
	public class NPCGridspaceObjectCategory extends GridspaceObjectCategory
	{
		public	static const XML_NAME		:String = "npc";
		public	static const LAYER			:int 	= Gridspace.COLLIDABLE_LAYER;
		private static const DISPLAY_NAME	:String = "NPCs";
		
		public function NPCGridspaceObjectCategory() 
		{
			super( GridspaceObjectCategory.NPC, LAYER, XML_NAME, DISPLAY_NAME );
		}		
	}
}