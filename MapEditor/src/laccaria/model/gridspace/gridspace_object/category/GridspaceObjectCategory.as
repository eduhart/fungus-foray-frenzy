package laccaria.model.gridspace.gridspace_object.category 
{
	
	public class GridspaceObjectCategory 
	{
		//------Constants
		
		public static const NPC					:int = 0;
		public static const MUSHROOM			:int = 1;
		public static const ITEM_CHEST			:int = 2;
		public static const BACKGROUND_MISC		:int = 3;
		public static const FOREGROUND_MISC		:int = 4;
		public static const COW					:int = 5;
		
		public static const BACKGROUND_LAYER	:int = 1;
		public static const FOREGROUND_LAYER	:int = 3;
		
		//------Variables
		
		private var _idCategory	:int;
		private var _idLayer	:int;
		private var _displayName:String;
		private var _xmlName	:String;
		
		//------Getters/Setters
		
		public function get idCategory()	:int 	{ return _idCategory; 	}
		public function get idLayer()		:int 	{ return _idLayer;		}
		public function get displayName()	:String { return _displayName;	}
		public function get xmlName()		:String { return _xmlName;		}
		
		//------Construction
		
		public function GridspaceObjectCategory( idCategory:int, idLayer:int, xmlName:String, displayName:String ) 
		{
			init( idCategory, idLayer, xmlName, displayName );
		}
		
		private function init( idCategory:int, idLayer:int, xmlName:String, displayName:String ):void
		{
			initVars( idCategory, idLayer, xmlName, displayName );
		}
		
		private function initVars( idCategory:int, idLayer:int, xmlName:String, displayName:String ):void
		{
			_idCategory		= idCategory;
			_idLayer		 	= idLayer;
			_xmlName 		= xmlName;
			_displayName 	= displayName;
		}
	}
}