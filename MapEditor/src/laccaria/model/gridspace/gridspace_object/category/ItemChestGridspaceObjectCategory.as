package laccaria.model.gridspace.gridspace_object.category 
{
	import laccaria.model.gridspace.Gridspace;
	
	public class ItemChestGridspaceObjectCategory extends GridspaceObjectCategory
	{
		//------Constants
		
		public	static const XML_NAME			:String = "itemChest";
		public	static const LAYER				:int 	= Gridspace.COLLIDABLE_LAYER;
		private static const DISPLAY_NAME		:String = "Item Chest";
		
		//------Construction
		
		public function ItemChestGridspaceObjectCategory()
		{
			super( GridspaceObjectCategory.ITEM_CHEST, LAYER, XML_NAME, DISPLAY_NAME );
		}
	}
}