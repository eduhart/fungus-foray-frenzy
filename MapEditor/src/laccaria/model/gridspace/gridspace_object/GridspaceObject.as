package laccaria.model.gridspace.gridspace_object 
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import laccaria.model.gridspace.gridspace_object.type.GridspaceObjectType;
	
	public class GridspaceObject 
	{		
		//------Variables		
		
		private var _idObject		:int;
		private var _coordinates	:Point;					//The tile coordinates of the object
		private var _type			:GridspaceObjectType;
		private var _background		:Bitmap;
		
		//------Getters/Setters
		
		public function get idObject()		:int	{ return _idObject; 		}
		public function get coordinates()	:Point 	{ return _coordinates; 		}
		public function get idType()		:int	{ return _type.idType;		}
		public function get tileWidth()		:int 	{ return _type.tileWidth;	}
		public function get tileHeight()	:int	{ return _type.tileHeight;	}
		public function get background()	:Bitmap	{ return _background;		}
		
		public function set coordinates( coordinates:Point ):void { _coordinates = coordinates; }
		
		//------Construction
		
		public function GridspaceObject( idObject:int, coordinates:Point, background:Bitmap, type:GridspaceObjectType )
		{
			init( idObject, coordinates, background, type );
		}
		
		private function init( idObject:int, coordinates:Point, background:Bitmap, type:GridspaceObjectType ):void
		{
			initVars( idObject, coordinates, background, type );
		}
		
		private function initVars( idObject:int, coordinates:Point, background:Bitmap, type:GridspaceObjectType ):void
		{
			_idObject 		= idObject;
			_coordinates 	= coordinates;
			_background 	= background;
			_type 			= type;
		}
		
		//------Methods
		
		public function toString():String
		{
			return "idObject: " + _idObject + ", coordinates: (" + _coordinates.x + "," + _coordinates.y + "), idType: " + idType;
		}
		
		public function toXMLString():String
		{
			var xmlOutput:String = "";
			
			xmlOutput += 	"<" + _type.xmlNameCategory + ">\n\t\t\t" +
								"<idObject>" 			+ _idObject 		+ "</idObject>\n\t\t\t" 	+
								"<idType>"				+ _type.idType 		+ "</idType>\n\t\t\t"		+
								"<idCategory>"			+ _type.idCategory	+ "</idCategory>\n\t\t\t" 	+
								"<layer>"				+ _type.idLayer		+ "</layer>\n\t\t\t" 		+
								"<xCoord>"				+ _coordinates.x	+ "</xCoord>\n\t\t\t"		+
								"<yCoord>"				+ _coordinates.y	+ "</yCoord>\n\t\t\t"		+
								"<width>"				+ _type.tileWidth	+ "</width>\n\t\t\t"		+
								"<height>"				+ _type.tileHeight	+ "</height>\n\t\t\t"		+
								"<animCols>"			+ _type.animCols	+ "</animCols>\n\t\t\t"		+
								"<animFrames>"			+ _type.animFrames	+ "</animFrames>\n\t\t\t"	+
								"<spriteSource>"		+ _type.sheetPath 	+ "</spriteSource>\n\t\t\t"	+
								"<iconSource>"			+ _type.spritePath 	+ "</iconSource>\n\t\t\t"	+
								xmlStringHook()			+ "\n\t\t\t"									+
								_type.xmlStringHook() 	+ "\n\t\t"										+
							"</" + _type.xmlNameCategory + ">";
			
			return xmlOutput;
		}
		
		//Meant to be overridden by any subclasses
		protected function xmlStringHook():String
		{
			return "";
		}
	}
}