package laccaria.model.gridspace.gridspace_object 
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import laccaria.events.EventManager;
	import laccaria.events.gridspace_object.GridspaceObjectManagerEvent;
	import laccaria.events.gridspace_object.GridspaceObjectTypeEvent;
	import laccaria.model.gridspace.gridspace_object.category.GridspaceObjectCategory;
	import laccaria.model.gridspace.gridspace_object.type.BackgroundMiscType;
	import laccaria.model.gridspace.gridspace_object.type.CowType;
	import laccaria.model.gridspace.gridspace_object.type.ForegroundMiscType;
	import laccaria.model.gridspace.gridspace_object.type.GridspaceObjectType;
	import laccaria.model.gridspace.gridspace_object.type.InvisibleType;
	import laccaria.model.gridspace.gridspace_object.type.ItemChestType;
	import laccaria.model.gridspace.gridspace_object.type.MushroomType;
	import laccaria.model.gridspace.gridspace_object.type.NPCType;
	
	public class GridspaceObjectManager 
	{
		//------Constants
		
		private static const STANDARD_CATEGORIES_XML_PATH	:String = "xml/StandardGridspaceObjectCategories.xml";
		private static const STANDARD_TYPES_XML_PATH		:String = "xml/StandardGridspaceObjectTypes.xml";
		
		//------Variables
		
		private var _categories						:Array;
		private var _types							:Array;
		private var _nextIdType						:int;
		private var _nextIdObject					:int;
		
		private var _standardCategoriesInitialized	:Boolean;
		private var _standardTypesInitialized		:Boolean;
		private var _numTypesToInitialize			:int;
		private var _numTypesInitialized			:int;
		
		//------Getters/Setters
		
		public function get categories():Array { return _categories; 	}
		public function get types()		:Array { return _types; 		}
		
		//------Construction
		
		public function GridspaceObjectManager() 
		{
			init();
		}
		
		private function init():void
		{
			initVars();
			loadStandardCategories();
			loadStandardTypes();
			addEventListeners();
		}
		
		private function initVars():void
		{
			_categories 					= new Array();
			_types 							= new Array();			
			_nextIdType						= 0;
			_nextIdObject					= 0;
			
			_standardCategoriesInitialized	= false;
			_standardTypesInitialized 		= false;
			_numTypesToInitialize			= 0;
			_numTypesInitialized			= 0;
		}
		
		private function loadStandardCategories():void
		{
			var xmlLoader:URLLoader = new URLLoader();
			xmlLoader.addEventListener( Event.COMPLETE, standardCategoriesXMLLoadedHandler );
			xmlLoader.load( new URLRequest( STANDARD_CATEGORIES_XML_PATH ) );
		}
		
		private function loadStandardTypes():void
		{
			var xmlLoader:URLLoader = new URLLoader();
			xmlLoader.addEventListener( Event.COMPLETE, standardTypesXMLLoadedHandler );
			xmlLoader.load( new URLRequest( STANDARD_TYPES_XML_PATH ) );
		}
		
		private function addEventListeners():void
		{
			EventManager.AddGlobalEventListener( GridspaceObjectTypeEvent.INITIALIZED, gridspaceObjectTypeInitializedHandler );
		}
		
		//------Event Handlers
		
		private function standardCategoriesXMLLoadedHandler( e:Event ):void
		{
			var standardCategoriesXML:XML = new XML( e.target.data );			
			var categories:XMLList = standardCategoriesXML.gridspaceObjectCategory;
			
			for each ( var categoryXML:XML in categories )
			{
				//Extract!
				var idCategory	:int 	= categoryXML.idCategory	[ 0 ];
				var layer		:int 	= categoryXML.layer			[ 0 ];
				var xmlName		:String = categoryXML.xmlName		[ 0 ];
				var displayName	:String = categoryXML.displayName	[ 0 ];
				
				addCategory( idCategory, layer, xmlName, displayName );
			}
			
			_standardCategoriesInitialized = true;
			
			checkIfInitialized();
		}
		
		private function standardTypesXMLLoadedHandler( e:Event ):void
		{			
			var standardTypesXML:XML = new XML( e.target.data );
			var types:XMLList = standardTypesXML.gridspaceObjectType;
			
			for each ( var typeXML:XML in types )
			{
				var idType		:int	= typeXML.idType		[ 0 ];
				var idCategory	:int 	= typeXML.idCategory	[ 0 ];
				var spritePath	:String	= typeXML.spritePath	[ 0 ];
				var displayName	:String = typeXML.displayName	[ 0 ];
				var fileName	:String = typeXML.fileName		[ 0 ];
				
				//Update the next automatically assigned idType if the current idType is greater than it
				if ( idType >= _nextIdType ) { _nextIdType = idType + 1; }
				
				switch( idCategory )
				{
					case GridspaceObjectCategory.NPC:
						var sheetPath		:String = typeXML.sheetPath		[ 0 ];
						var animCols		:int 	= typeXML.animCols		[ 0 ];
						var animFrames		:int	= typeXML.animFrames	[ 0 ];
						var idDesiredMush	:int 	= typeXML.idDesiredMush	[ 0 ];
						addNPCType( idType, displayName, fileName, spritePath, sheetPath, animCols, animFrames, idDesiredMush );
						_numTypesToInitialize++;
						break;
					case GridspaceObjectCategory.MUSHROOM:
						sheetPath				= typeXML.sheetPath	[ 0 ];
						animCols		 		= typeXML.animCols	[ 0 ];
						animFrames				= typeXML.animFrames[ 0 ];
						var idMushroom	:int 	= typeXML.idMushroom[ 0 ];
						addMushroomType( idType, displayName, fileName, spritePath, sheetPath, animCols, animFrames, idMushroom );
						_numTypesToInitialize++;
						break;
					case GridspaceObjectCategory.ITEM_CHEST:
						var idItem:int = typeXML.idItem[ 0 ];
						addItemChestType( idType, displayName, fileName, spritePath, idItem );
						_numTypesToInitialize++;
						break;
					case GridspaceObjectCategory.BACKGROUND_MISC:
						addBackgroundMiscType( idType, displayName, fileName, spritePath );
						_numTypesToInitialize++;
						break;
					case GridspaceObjectCategory.FOREGROUND_MISC:
						addForegroundMiscType( idType, displayName, fileName, spritePath );
						_numTypesToInitialize++;
						break;
					case GridspaceObjectCategory.COW:
						sheetPath	= typeXML.sheetPath	[ 0 ];
						animCols 	= typeXML.animCols	[ 0 ];
						animFrames	= typeXML.animFrames[ 0 ];
						addCowType( idType, displayName, fileName, spritePath, sheetPath, animCols, animFrames );
						_numTypesToInitialize++;
						break;
				}
			}			
		}
		
		private function gridspaceObjectTypeInitializedHandler( e:GridspaceObjectTypeEvent ):void
		{
			_numTypesInitialized++;
			
			if ( _numTypesInitialized >= _numTypesToInitialize )
			{
				_standardTypesInitialized = true;
				EventManager.RemoveGlobalEventListener( GridspaceObjectTypeEvent.INITIALIZED, gridspaceObjectTypeInitializedHandler );
				checkIfInitialized();
			}
		}
		
		//------Methods
		
		public function addCategory( idCategory:int, layer:int, xmlName:String, displayName:String ):void
		{
			var category:GridspaceObjectCategory = new GridspaceObjectCategory( idCategory, layer, xmlName, displayName );
			_categories[ idCategory ] = category;
		}
		
		public function addNPCType( idType:int, displayName:String, fileName:String, spritePath:String, sheetPath:String, animCols:int, animFrames:int, idDesiredMush:int ):void
		{			
			var npcType:NPCType = new NPCType( idType, displayName, fileName, spritePath, sheetPath, animCols, animFrames, idDesiredMush )
			_types.push( npcType );
		}
		
		public function addMushroomType( idType:int, displayName:String, fileName:String, spritePath:String, sheetPath:String, animCols:int, animFrames:int, idMushroomType:int ):void
		{
			var mushroomType:MushroomType = new MushroomType( idType, displayName, fileName, spritePath,  sheetPath, animCols, animFrames, idMushroomType );
			_types.push( mushroomType );
		}
		
		public function addItemChestType( idType:int, displayName:String, fileName:String, spritePath:String, idItem:int ):void
		{
			var itemChestType:ItemChestType = new ItemChestType( idType, displayName, fileName, spritePath, idItem );
			_types.push( itemChestType );
		}
		
		public function addBackgroundMiscType( idType:int, displayName:String, fileName:String, spritePath:String ):void
		{			
			var backgroundMiscType:BackgroundMiscType;
			
			if ( spritePath == "" )
			{
				backgroundMiscType = new InvisibleType( idType, displayName, fileName );
			}
			else
			{
				backgroundMiscType = new BackgroundMiscType( idType, displayName, fileName, spritePath );
			}
			
			_types.push( backgroundMiscType );
		}
		
		public function addForegroundMiscType( idType:int, displayName:String, fileName:String, spritePath:String ):void
		{
			var foregroundMiscType:ForegroundMiscType = new ForegroundMiscType( idType, displayName, fileName, spritePath );
			_types.push( foregroundMiscType );
		}
		
		public function addCowType( idType:int, displayName:String, fileName:String, spritePath:String, sheetPath:String, animCols:int, animFrames:int ):void
		{
			var cowType:CowType = new CowType( idType, displayName, fileName, spritePath, sheetPath, animCols, animFrames );
			_types.push( cowType );
		}
		
		public function removeType( idType:int ):void
		{
			var type:GridspaceObjectType = findType( idType );
			
			if ( type )
			{
				var indexType:int = _types.indexOf( type );
				_types.splice( indexType, 1 );
			}
		}
		
		public function getAllCategoriesWithLayer( idLayer:int ):Array
		{
			var categories:Array = new Array()
			
			for each ( var category:GridspaceObjectCategory in _categories )
			{
				if ( category.idLayer == idLayer )
				{
					categories.push( category );
				}
			}
			
			return categories;
		}
		
		public function getAllTypesWithCategory( idCategory:int ):Array
		{
			var types:Array = new Array();
			
			for each ( var type:GridspaceObjectType in _types )
			{
				if ( type.idCategory == idCategory )
				{
					types.push( type );
				}
			}
			
			return types;
		}
		
		public function findType( idType:int ):GridspaceObjectType		
		{
			for each ( var type:GridspaceObjectType in _types )
			{
				if ( type.equals( idType ) )
				{
					return type;
				}
			}
			
			return null;
		}
		
		public function getNextIdType():int
		{
			return _nextIdType++;
		}
		
		public function getNextIdObject():int
		{
			return _nextIdObject++;
		}
		
		//Give the types that are used in the gridspace. This function will
		//sort through them and find the custom types, generating xml for them.
		public function customTypesToXML( types:Array ):String
		{
			var xmlString:String = "";
			
			//Find the custom types
			for each ( var type:GridspaceObjectType in types )
			{
				if ( isCustomType( type ) )
				{
					var idType:int = type.idType;
					var idCategory:int = type.idCategory;
					var spritePath:String = type.spritePath;
					var displayName:String = type.displayName;
					var fileName:String = type.fileName;
					
					xmlString += "<gridspaceObjectType>\n\t\t";
					xmlString += 	"<idType>" 		+ idType 		+ "</idType>\n\t\t";
					xmlString +=	"<idCategory>"	+ idCategory 	+ "</idCategory>\n\t\t";
					xmlString +=	"<spritePath>"	+ spritePath 	+ "</spritePath>\n\t\t";
					xmlString +=	"<displayName>"	+ displayName 	+ "</displayName>\n\t\t";
					xmlString +=	"<fileName>"	+ fileName 		+ "</fileName>\n\t";
					xmlString += "</gridspaceObjectType>\n\t";
				}
			}
			
			return xmlString;
		}
		
		private function isCustomType( type:GridspaceObjectType ):Boolean
		{
			if
				(
					type is ForegroundMiscType ||
					(
						type is BackgroundMiscType && ! ( type is InvisibleType )
					)
				)
			{
				return true;
			}
			
			return false;	
		}
		
		private function checkIfInitialized():void
		{
			if ( _standardCategoriesInitialized && _standardTypesInitialized )
			{
				var gridspaceObjectManagerEvent:GridspaceObjectManagerEvent = new GridspaceObjectManagerEvent(
					GridspaceObjectManagerEvent.INITIALIZED );				
				EventManager.DispatchGlobal( gridspaceObjectManagerEvent );
			}
		}
	}
}