package laccaria.model.gridspace 
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import laccaria.io.BiomeSaver;
	import laccaria.model.gridspace.gridspace_object.GridspaceObject;
	import laccaria.model.gridspace.gridspace_object.GridspaceObjectManager;
	import laccaria.model.gridspace.gridspace_object.type.GridspaceObjectType;
	import laccaria.utility.MushroomUtil;
	
	/**
	 * A gridspace in the biome, defined by an index, and it's two-dimensional gridspace
	 * coordinates (where each unit = the width/height of a gridspace).
	 */
	public class Gridspace 
	{
		//------Constants
		
		public static const WIDTH		:Number = 512;
		public static const HEIGHT		:Number = 384;
		
		public static const TILE_WIDTH	:Number = 16;
		public static const TILE_HEIGHT	:Number = 16;
		
		public static const COLLIDABLE_LAYER	:int = 1;
		public static const FOREGROUND_LAYER	:int = 3;
		
		//------Variables
		
		//The properties of the gridspace
		private var _index					:int;
		private var _coordinates			:Point;
		private var _backgroundFileName		:String;
		private var _background				:Bitmap;
		
		/**
		 * An array of 4 connections.
		 * Index = direction.
		 * Value = id of the gridspace, or -1 if there are no connections in that direction.
		 */
		private var _connections			:Array;
		
		private var _gridspaceObjects		:Array;
		
		//------Getters/Setters
		
		public function get index()				:int 	{ return _index; 			}
		public function get coordinates()		:Point 	{ return _coordinates; 		}
		public function get connections()		:Array	{ return _connections;		}
		public function get background()		:Bitmap { return _background;		}
		public function get gridspaceObjects()	:Array 	{ return _gridspaceObjects	}
		
		public function set index		( index:int 		):void { _index = index; 				}
		public function set coordinates	( coordinates:Point ):void { _coordinates = coordinates; 	}
		
		//------Construction
		
		public function Gridspace( index:int, coordinates:Point, backgroundFileName:String, background:Bitmap )
		{
			_index 				= index
			_coordinates 		= coordinates;
			_backgroundFileName = backgroundFileName;
			_background = background;
			_connections 		= [ -1, -1, -1, -1 ];
			_gridspaceObjects 	= new Array();
		}
		
		//------Methods
		
		/**
		 * Overwrites the connection from the given direction to the given gridspace.
		 * @param	direction	The direction from this gridspace that the connection comes from.
		 * @param	idGridspace	The id of the gridspace that the connection leads to.
		 */
		public function setConnection( direction:int, idGridspace:int ):void
		{
			if ( direction < _connections.length && direction >= 0 )
			{
				_connections[ direction ] = idGridspace;
			}
		}
		
		/**
		 * Removes the connection from the given direction (sets it to -1).
		 * @param	direction	The direction from this gridspace that the connection went to.
		 */
		public function removeConnection( direction:int ):void
		{
			if ( direction < _connections.length && direction >= 0 )
			{
				_connections[ direction ] = -1;
			}
		}
		
		public function addGridspaceObject( idGridspaceObject:int, coordinates:Point, type:GridspaceObjectType ):GridspaceObject
		{
			var background:Bitmap = type.getSpriteCopy();
			
			var gridspaceObject:GridspaceObject = new GridspaceObject(
				idGridspaceObject, coordinates, background, type );
			_gridspaceObjects.push( gridspaceObject );
			
			return gridspaceObject;
		}
		
		public function removeGridspaceObject( idGridspaceObject:int ):void
		{
			var gridspaceObject:GridspaceObject = findGridspaceObject( idGridspaceObject );
			
			if ( gridspaceObject )
			{
				var indexGridspaceObject:int = _gridspaceObjects.indexOf( gridspaceObject );
				_gridspaceObjects.splice( indexGridspaceObject, 1 );
			}
		}
		
		public function moveGridspaceObject( idGridspaceObject:int, newXCoord:int, newYCoord:int ):void
		{
			var gridspaceObject:GridspaceObject = findGridspaceObject( idGridspaceObject );
			
			if ( gridspaceObject )
			{
				gridspaceObject.coordinates.x = newXCoord;
				gridspaceObject.coordinates.y = newYCoord;
			}
		}
		
		private function findGridspaceObject( idGridspaceObject:int ):GridspaceObject
		{
			for each ( var gridspaceObject:GridspaceObject in _gridspaceObjects )
			{
				if ( gridspaceObject.idObject == idGridspaceObject )
				{
					return gridspaceObject;
				}
			}
			
			return null;
		}
		
		public function toXMLString( biomeName:String ):String
		{
			var backgroundDirectory:String = BiomeSaver.BACKGROUND_SLICE_DIRECTORY + "\\" + _backgroundFileName;
			backgroundDirectory = MushroomUtil.strReplace( backgroundDirectory, "\\", "\\\\" );
			
			var xmlString:String =
							"<gridspace>\n\t\t";
			xmlString +=		"<idNum>" 			+ _index 				+ "</idNum>\n\t\t";
			xmlString +=		"<coordinate_x>" 	+ _coordinates.x 		+ "</coordinate_x>\n\t\t";
			xmlString +=		"<coordinate_y>" 	+ _coordinates.y 		+ "</coordinate_y>\n\t\t";
			xmlString +=		"<bkgSource>" 		+ backgroundDirectory 	+ "</bkgSource>\n\t\t";
			xmlString +=		connectionsToXMLString( biomeName );
			xmlString +=		objectsToXMLString()						+ "\n\t";
			
			
			xmlString +=	"</gridspace>";
			
			return xmlString;
		}
		
		private function connectionsToXMLString( biomeName:String ):String
		{
			var xmlString:String = "";
			
			for ( var i:int = 0; i < _connections.length; i++ )
			{
				var idGridspace:int = _connections[ i ];
				
				if ( idGridspace != -1 )
				{
					xmlString +=	"<connection>\n\t\t\t";
					xmlString +=		"<to>" 			+ idGridspace 						+ "</to>\n\t\t\t";
					xmlString +=		"<type>" 		+ Direction.DirectionToString( i ) 	+ "</type>\n\t\t\t";
					xmlString +=		"<biomeType>"	+ biomeName							+ "</biomeType>\n\t\t";
					xmlString +=	"</connection>\n\t\t";
				}
			}
			
			return xmlString;
		}
		
		private function objectsToXMLString():String
		{
			var xmlString:String = "";
			for each ( var gridspaceObject:GridspaceObject in _gridspaceObjects )
			{
				xmlString += "\n\t\t" + gridspaceObject.toXMLString();
			}
			
			return xmlString;
		}
		
		public function printObjects():void
		{
			var toPrint:String = "Objects for gridspace #" + _index + ":\n\t";
			
			for each ( var object:GridspaceObject in _gridspaceObjects )
			{
				toPrint += object.toString() + "\n\t";
			}
			
			trace( toPrint );
		}
		
		public function toString():String
		{
			return _index + " : (" + _coordinates.x + "," + _coordinates.y + ")";
		}
	}
}