package laccaria.model 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import laccaria.events.BiomeEvent;
	import laccaria.events.EventManager;
	import laccaria.events.GridspaceEvent;
	import laccaria.io.BiomeSaver;
	import laccaria.model.gridspace.Direction;
	import laccaria.model.gridspace.Gridspace;
	import laccaria.model.gridspace.gridspace_object.GridspaceObject;
	import laccaria.model.gridspace.gridspace_object.type.GridspaceObjectType;
	
	public class Biome
	{
		//------Variables
		
		private var _name					:String;
		private var _gridspaces				:Array;
		
		private var _background				:Bitmap;
		private var _backgroundSlices		:Array;
		
		//------Getters/Setters
		
		public function get name()						:String					{ return _name;						}
		public function get gridspaces()				:Array					{ return _gridspaces;				}
		public function get background()				:Bitmap					{ return _background;				}
		public function get backgroundSlices()			:Array					{ return _backgroundSlices;			}
		
		public function set name			( name				:String )	:void	{ _name = name;							}
		public function set background		( background		:Bitmap )	:void	{ _background = background;				}
		public function set backgroundSlices( backgroundSlices	:Array	)	:void	{ _backgroundSlices = backgroundSlices;	}
		public function set gridspaces		( gridspaces		:Array 	)	:void	{ _gridspaces = gridspaces;				}
		
		//------Construction
		
		public function Biome( name:String="", gridspaces:Array = null )
		{
			init( name, gridspaces );
		}
		
		private function init( name:String, gridspaces:Array ):void
		{
			initVars( name, gridspaces );
			addEventListeners();
		}
		
		private function initVars( name:String, gridspaces:Array ):void
		{
			_name 		= name;	
			_gridspaces	= ( gridspaces != null ) ? gridspaces : new Array();
		}
		
		private function addEventListeners():void
		{
		}
		
		//------Methods
		
		/**
		 * Creates a gridspace based on the given biome position.
		 * @param	coordinates The coordinates of the top left corner of the gridspace
		 * 			relative to the biome (with each unit being the width/height of the gridspace)
		 * @return	Gridspace that was created, otherwise null if no gridspace was created
		 */
		public function createGridspace( coordinates:Point ):Gridspace
		{
			if ( ! hasGridspaceWithCoordinates( coordinates ) )
			{
				var nextIndex			:int 		= _gridspaces.length;
				var backgroundFileName	:String 	= getBackgroundName( coordinates );
				var background			:Bitmap 	= getBackground( coordinates );
				var gridspace			:Gridspace 	= new Gridspace( nextIndex, coordinates, backgroundFileName, background );
				
				_gridspaces.push( gridspace );				
				return gridspace;
			}
			
			return null;
		}
		
		/**
		 * Removes a gridspace
		 * @param	gridspace The gridspace to remove
		 * @return	Gridspace that was removed, otherwise null if no gridspace was removed
		 */
		public function removeGridspace( index:int ):Gridspace
		{
			var gridspaceToRemove:Gridspace = findGridspaceByIndex( index );
			
			if ( gridspaceToRemove != null )
			{				
				//Remove all connections to the removed gridspace
				for each ( var gridspace:Gridspace in gridspaces )
				{
					for ( var i:int = 0; i < gridspace.connections.length; i++ )
					{
						var connection:int = gridspace.connections[ i ];
						
						if ( connection == index )
						{
							gridspace.removeConnection( i );
						}
					}
				}
				
				//Decrement the indices of all gridspaces that were after the removed gridspace
				for ( i = index+1; i < _gridspaces.length; i++ )
				{
					var curGridspace:Gridspace = _gridspaces[ i ];
					
					//Decrement the connection values that point to the curGridspace
					for ( var j:int = 0; j < curGridspace.connections.length; j++ )
					{
						connection = curGridspace.connections[ j ];
						
						if ( connection == curGridspace.index )
						{
							gridspace.setConnection( j, curGridspace.index - 1 );
						}
					}
					
					//Decrement the index if the current gridspace
					curGridspace.index--;
					
					//Tell interested parties that the index of the current gridspace has changed
					var gridspaceEvent:GridspaceEvent = new GridspaceEvent(
						GridspaceEvent.CHANGE_INDEX, curGridspace.index, curGridspace.coordinates );
					EventManager.DispatchGlobal( gridspaceEvent );
				}
				
				//Remove from array				
				_gridspaces.splice( index, 1 );
			}
			
			return gridspaceToRemove;
		}
		
		/**
		 * Adds a connections from one gridspace to another
		 * @param	indexFrom		The index of the gridspace that the connection comes from
		 * @param	indexTo			The index of the gridspace that the connection goes to
		 * @param	directionFrom	The direction that the connection is coming from (N, S, E, W)
		 */
		public function addConnection( indexFrom:int, indexTo:int, directionFrom:int ):void
		{
			var gridspaceFrom:Gridspace = findGridspaceByIndex( indexFrom );
			gridspaceFrom.setConnection( directionFrom, indexTo );
		}
		
		/**
		 * Removes a connection from one gridspace to another
		 * @param	indexFrom		The index of the gridspace that the connection comes from
		 * @param	directionFrom	The direction from the gridspace that the connection comes from
		 */
		public function removeConnection( indexFrom:int, directionFrom:int ):void
		{
			var gridspaceFrom:Gridspace = findGridspaceByIndex( indexFrom );
			gridspaceFrom.removeConnection( directionFrom );
		}
		
		public function addGridspaceObject( idGridspace:int, idGridspaceObject:int, xCoord:int, yCoord:int, type:GridspaceObjectType ):GridspaceObject
		{
			//Find the matching gridspace
			var gridspace:Gridspace = findGridspaceByIndex( idGridspace );
			var coordinates	:Point = new Point( xCoord, yCoord );
			
			return gridspace.addGridspaceObject( idGridspaceObject, coordinates, type );
		}
		
		public function giveBackground( background:Bitmap ):void
		{
			if ( background )
			{
				//Set member variable
				_background = background;
				
				//Initialize the backgroundSlices
				_backgroundSlices = new Array();
				
				//Figure out how many gridspaces can fit across the image
				var numGridspacesWidth	:int = Math.floor( _background.width 	/ Gridspace.WIDTH 	);
				var numGridspacesHeight	:int = Math.floor( _background.height 	/ Gridspace.HEIGHT 	);
				
				//The rectangle that defines where to copy the pixels from the background
				var sourceRectangle:Rectangle = new Rectangle( 0, 0, Gridspace.WIDTH, Gridspace.HEIGHT );
				
				//Where to put the copied pixels on the background slice
				var destPoint:Point = new Point();
				
				for ( var i:int = 0; i < numGridspacesHeight; i++ )
				{
					_backgroundSlices.push( new Array() );
					
					for ( var j:int = 0; j < numGridspacesWidth; j++ )
					{
						var backgroundSliceData:BitmapData = new BitmapData( Gridspace.WIDTH, Gridspace.HEIGHT );
						backgroundSliceData.copyPixels( background.bitmapData, sourceRectangle, destPoint );
						var backgroundSlice:Bitmap = new Bitmap( backgroundSliceData );
						
						sourceRectangle.x += Gridspace.WIDTH;
						_backgroundSlices[ i ].push( backgroundSlice );
					}					
					
					sourceRectangle.x = 0;
					sourceRectangle.y += Gridspace.HEIGHT;
				}
			}
		}
		
		/**
		 * Figure out if a gridspace exists with the given biome coordinates
		 * @param	biomePosition The position of the gridspace that may or may not exist
		 * @return	True if the gridspace exists, otherwise false
		 */
		private function hasGridspaceWithCoordinates( coordinates:Point ):Boolean
		{
			for each ( var gridspace:Gridspace in _gridspaces )
			{
				if ( gridspace.coordinates.equals( coordinates ) )
				{
					return true;
				}
			}
			
			return false;
		}
		
		public function findGridspaceByIndex( index:int ):Gridspace
		{
			for each ( var gridspace:Gridspace in _gridspaces )
			{
				if ( gridspace.index == index )
				{
					return gridspace;
				}
			}
			
			return null;
		}
		
		public function getBackgroundName( coordinates:Point ):String
		{
			var numBackgroundSlicesX:Number = 
				( _backgroundSlices.length > 0 ) ? _backgroundSlices[ 0 ].length : 0;
			return "background_" + ( coordinates.y * numBackgroundSlicesX + coordinates.x ) + BiomeSaver.BACKGROUND_SLICE_EXTENSION;
		}
		
		public function getUniqueIdTypes():Array
		{
			var uniqueTypes:Array = new Array();
			
			for each ( var gridspace:Gridspace in _gridspaces )
			{
				for each ( var gridspaceObject:GridspaceObject in gridspace.gridspaceObjects )
				{
					var idType:int = gridspaceObject.idType;					
					var indexOfType:int = uniqueTypes.indexOf( idType );
					
					if ( indexOfType == -1 )
					{
						uniqueTypes.push( idType );
					}
				}
			}
			
			return uniqueTypes;
		}
		
		private function getBackground( coordinates:Point ):Bitmap
		{
			var background:Bitmap;
			
			//Make sure we don't get an index out of bounds error
			if ( coordinates.y < _backgroundSlices.length && coordinates.x < _backgroundSlices[ coordinates.y ].length )
			{
				background = _backgroundSlices[ coordinates.y ][ coordinates.x ];
			}
			
			return background;
		}
		
		public function print():void
		{
			var output:String = _name + " Biome:\nGridspaces:\n";
			
			for each ( var gridspace:Gridspace in _gridspaces )
			{
				output += gridspace + "\n";
			}
			
			trace( output + "\n" );
		}
		
		public function printConnections():void
		{
			var toPrint:String = "Connections:";
			for each ( var gridspace:Gridspace in _gridspaces )
			{
				var connections:Array = gridspace.connections;
				
				for ( var i:int = 0; i < connections.length; i++ )
				{
					if ( connections[ i ] != -1 )
					{
						toPrint += "\n\tFrom " + gridspace.index + " " + Direction.DirectionToString( i ) + 
							" To " + connections[ i ];
					}
				}
			}
			
			trace( toPrint );
		}
		
		public function toXMLString():String
		{
			var maxWidth:int = ( _backgroundSlices.length > 0 ) ? _backgroundSlices[ 0 ].length : 0;
			var maxHeight:int = _backgroundSlices.length;
			
			var xmlString:String = "";
			xmlString +=		"<name>" 		+ _name 	+ "</name>\n\t";
			xmlString +=		"<max_width>" 	+ maxWidth 	+ "</max_width>\n\t";
			xmlString +=		"<max_height>" 	+ maxHeight + "</max_height>\n\t";
			
			for each ( var gridspace:Gridspace in gridspaces )
			{
				xmlString += "\n\t" + gridspace.toXMLString( _name );
			}
			
			return xmlString;
		}
	}
}