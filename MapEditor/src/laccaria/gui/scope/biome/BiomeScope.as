package laccaria.gui.scope.biome 
{
	import laccaria.model.gridspace.Gridspace;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import laccaria.events.ConnectionEvent;
	import laccaria.events.EventManager;
	import laccaria.events.GridspaceRegionEvent;
	import laccaria.events.GridspaceViewEvent;
	import laccaria.gui.MainWindow;
	import laccaria.gui.scope.biome.layer.background.BackgroundLayer;
	import laccaria.gui.scope.biome.layer.BiomeLayer;
	import laccaria.gui.scope.biome.layer.connection.ConnectionLayer;
	import laccaria.gui.scope.biome.layer.connection.GridspaceRegion;
	import laccaria.gui.scope.biome.layer.gridspace.BiomeGridspaceLayer;
	import laccaria.gui.scope.biome.layer.gridspace.GridspaceView;
	import laccaria.gui.scope.Scope;
	
	public class BiomeScope extends Scope
	{		
		//------Variables
		
		// Layers
		private var _backgroundLayer:BackgroundLayer;
		private var _gridspaceLayer	:BiomeGridspaceLayer;
		private var _connectionLayer:ConnectionLayer;
		
		private var _scale			:Number;
		private var _offset			:Point;
		
		//------Construction
		
		public function BiomeScope() 
		{
			super( Scope.BIOME );
			init();
		}
		
		private function init():void
		{
			initVars();
			addEventListeners();
			setLayer( 0 );
		}
		
		private function initVars():void
		{
			_backgroundLayer = new BackgroundLayer();
			_gridspaceLayer = new BiomeGridspaceLayer();
			_connectionLayer = new ConnectionLayer();
			_layers = [ _backgroundLayer, _gridspaceLayer, _connectionLayer ];
			
			_scale = 1;
			_offset = new Point( 0, 0 );
		}
		
		private function addEventListeners():void
		{
			EventManager.AddGlobalEventListener( GridspaceRegionEvent	.START_ADD_CONNECTION, 	startAddConnectionHandler 	);			
			EventManager.AddGlobalEventListener( GridspaceRegionEvent	.CANCEL_ADD_CONNECTION, cancelAddConnectionHandler 	);
			EventManager.AddGlobalEventListener( GridspaceViewEvent		.COMPLETE_CONNECTION, 	completeConnectionHandler 	);
		}
		
		//------Event Handlers
		
		private function startAddConnectionHandler( e:GridspaceRegionEvent ):void
		{
			_connectionLayer.startConnection( e.gridspaceRegion );
			_gridspaceLayer.setAddedAddingConnectionMode();
		}
		
		private function cancelAddConnectionHandler( e:GridspaceRegionEvent ):void
		{
			_gridspaceLayer.setAllNoMode();
			_connectionLayer.cancelConnection();
		}
		
		private function completeConnectionHandler( e:GridspaceViewEvent ):void
		{
			var gridspaceViewTo:GridspaceView = e.gridspaceView;
			var gridspaceRegionFrom:GridspaceRegion = _connectionLayer.gridspaceRegionAdding;
			
			_gridspaceLayer.setAllNoMode();
			_connectionLayer.completeConnection( gridspaceViewTo );
			
			var connectionEvent:ConnectionEvent = new ConnectionEvent(
				ConnectionEvent.ADD_CONNECTION, gridspaceRegionFrom.gridspaceIndex, gridspaceRegionFrom.region, gridspaceViewTo.index );
			EventManager.DispatchGlobal( connectionEvent );
		}
		
		//------Methods
		
		public override function setLayer( layerNum:int ):void
		{
			if ( _curLayer == BiomeLayer.GRIDSPACE && layerNum != BiomeLayer.GRIDSPACE )
			{
				_gridspaceLayer.initialize();
			}
			if ( layerNum == BiomeLayer.CONNECTION && _curLayer != BiomeLayer.CONNECTION )
			{
				_connectionLayer.initialize( _gridspaceLayer.gridspaceViews );
			}
			
			super.setLayer( layerNum );
		}
		
		/**
		 * Set the GridspaceView with the given coordinates to the given index
		 * @param	coordinates	The coordinates of the GridspaceView
		 * @param	index		The index the GridspaceView will be set to
		 */
		public function setGridspaceViewIndex( coordinates:Point, index:int ):void
		{
			_gridspaceLayer.setGridspaceViewIndex( coordinates, index );
		}
		
		public function setGridspaceAsRemoved( idGridspace:int, coordinates:Point ):void
		{
			_gridspaceLayer.setGridspaceAsRemoved( coordinates );
			_connectionLayer.removeAllConnectionViewsToGridspace( idGridspace );
			_connectionLayer.removeAllConnectionViewsFromGridspace( idGridspace );
		}
		
		/**
		 * Add a connection automatically, without the user's gui manipulation
		 * @param	indexFrom		The index of the gridspace where the connection is coming from
		 * @param	directionFrom	The direction from the gridspace where the connection is coming from
		 * @param	indexTo			The index of the gridspace where the connection is going to
		 */
		public function addConnection( indexFrom:int, directionFrom:int, indexTo:int ):void
		{
			var gridspaceViewFrom	:GridspaceView 		= _gridspaceLayer	.findGridspaceViewByIndex	( indexFrom 	);
			var gridspaceRegionFrom	:GridspaceRegion 	= gridspaceViewFrom	.getGridspaceRegion			( directionFrom );
			var gridspaceViewTo		:GridspaceView 		= _gridspaceLayer	.findGridspaceViewByIndex	( indexTo 		);
			
			_connectionLayer.addConnection( gridspaceRegionFrom, gridspaceViewTo );
		}
		
		public function giveBackground( background:Bitmap ):void
		{
			if ( background )
			{				
				_backgroundLayer.addBackground( background );
				buildMaxGridspaces( background.width, background.height );
				fitBackgroundToWindow();
			}
		}
		
		private function fitBackgroundToWindow():void
		{
			fitBackgroundLayerToWindow();
			fitGridspaceLayerToWindow();
		}
		
		private function fitBackgroundLayerToWindow():void
		{
			var widthDifference:Number = _backgroundLayer.width - MainWindow.WIDTH;
			var heightDifference:Number = _backgroundLayer.height - MainWindow.HEIGHT;
			
			// Is the width too large and is the amount it is too large by greater than
			// the amount that the height is too large by?
			if ( widthDifference > 0 && widthDifference > heightDifference )
			{
				_scale = MainWindow.WIDTH / _backgroundLayer.width;
				scale( _scale, _scale );
				
				//Calculate the new height difference
				heightDifference = _container.height - MainWindow.HEIGHT;
				
				//Position the background so that it is vertically centered on the screen
				_offset = new Point( 0, Math.abs( .5 * heightDifference ) );
			}
			
			// Is the height too large?
			else if ( heightDifference > 0 )
			{
				_scale = MainWindow.HEIGHT / _backgroundLayer.height;
				scale( _scale, _scale );
				
				//Calculate the new height difference
				widthDifference = _container.width - MainWindow.WIDTH;
				
				//Position the background so that it is horizontally centered on the screen
				_offset = new Point( Math.abs( .5 * widthDifference ), 0 );
			}
			
			move( _offset.x, _offset.y );
		}
		
		private function fitGridspaceLayerToWindow():void
		{
			//_gridspaceLayer.scale( _scale, _scale );
			//_gridspaceLayer.move( _offset.x, _offset.y );
		}
		
		public function buildMaxGridspaces( backgroundWidth:Number, backgroundHeight:Number ):void
		{			
			var numGridspacesX:int = backgroundWidth / Gridspace.WIDTH;
			var numGridspacesY:int = backgroundHeight / Gridspace.HEIGHT;
			var curX:Number = 0;
			var curY:Number = 0;
			
			//Gridspaces
			for ( var i:int = 0; i < numGridspacesY; i++ )
			{
				for ( var j:int = 0; j < numGridspacesX; j++ )
				{
					var position:Point = new Point( curX, curY );
					_gridspaceLayer.addGridspaceView( new Point( j, i ), position );
					
					curX += Gridspace.WIDTH * 1 / _scale;
				}
				
				curX = 0;
				curY += Gridspace.HEIGHT * 1 / _scale;
			}
		}
	}
}