package laccaria.gui.scope.biome.layer.connection
{
	import laccaria.model.gridspace.Gridspace;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import laccaria.events.EventManager;
	import laccaria.events.GridspaceEvent;
	import laccaria.events.GridspaceRegionEvent;
	
	public class GridspaceRegion extends Sprite
	{
		//------Constants
		
		private static const BACKGROUND_COLOR_DEFAULT			:uint 	= 0xF5B5CF;
		private static const BACKGROUND_COLOR_ADD				:uint 	= 0x00FF00;
		private static const BACKGROUND_COLOR_REMOVE			:uint 	= 0xFF0000;
		private static const BACKGROUND_ALPHA					:Number = .7;
		
		private static const LINE_COLOR							:uint 	= 0x000000;
		private static const LINE_THICKNESS						:Number = 1;
		private static const LINE_ALPHA							:Number = 1;
		
		private static const NO_MODE							:int 	= 0;
		private static const HIDE_MODE							:int 	= 1;
		private static const ADD_CONNECTION_MODE				:int 	= 2;
		private static const ADDING_CONNECTION_MODE				:int 	= 3;
		private static const REMOVE_CONNECTION_MODE				:int 	= 4;
		private static const REMOVING_CONNECTION_MODE			:int 	= 5;
		
		//------Member Variables
		
		private var _gridspaceIndex		:int;
		private var _region				:int;
		
		private var _width				:Number;
		private var _height				:Number;
		private var _gridspacePosition	:Point;
		
		private var _mode:int = NO_MODE;
		
		private var _isConnected		:Boolean;
		
		private var _background			:Shape;
		private var _curBackgroundColor	:uint;
		
		//------Getters/Setters
		
		public function get gridspaceIndex()	:int 		{ return _gridspaceIndex; 		}
		public function get region()			:int 		{ return _region; 				}
		public function get gridspacePosition()	:Point		{ return _gridspacePosition;	}
		public function get isConnected()		:Boolean 	{ return _isConnected; 			}
		
		public function set gridspaceIndex	( gridspaceIndex:int 		):void { _gridspaceIndex 	= gridspaceIndex;	}
		public function set isConnected		( isConnected	:Boolean 	):void { _isConnected 		= isConnected; 		}
		
		//------Construction
		
		public function GridspaceRegion( gridspaceIndex:int, region:int, gridspacePosition:Point )
		{
			init( gridspaceIndex, region, gridspacePosition );
		}
		
		private function init( gridspaceIndex:int, region:int, gridspacePosition:Point ):void
		{
			initVars( gridspaceIndex, region, gridspacePosition );
			drawBackground();
			addEventListeners();
		}
		
		private function initVars( gridspaceIndex:int, region:int, gridspacePosition:Point ):void
		{
			_gridspaceIndex = gridspaceIndex;
			_region = region;
			_gridspacePosition = gridspacePosition;
			_width = Gridspace.WIDTH / 3;
			_height = Gridspace.HEIGHT / 3;
			_isConnected = false;
			_background = new Shape();
			_curBackgroundColor = BACKGROUND_COLOR_DEFAULT;
			
			addChild( _background );
		}
		
		private function addEventListeners():void
		{
			addEventListener( MouseEvent.ROLL_OVER, rollOverHandler );
			addEventListener( MouseEvent.MOUSE_DOWN, mouseDownHandler );
			addEventListener( MouseEvent.ROLL_OUT, rollOutHandler );
		}
		
		//------Event Handlers
		
		private function rollOverHandler( e:MouseEvent ):void
		{
			if ( _mode == ADD_CONNECTION_MODE && !_isConnected )
			{
				_curBackgroundColor = BACKGROUND_COLOR_ADD;
				drawBackground();
			}
			else if ( _mode == REMOVE_CONNECTION_MODE && _isConnected )
			{
				_curBackgroundColor = BACKGROUND_COLOR_REMOVE;
				drawBackground();
			}
			else if ( _mode == REMOVING_CONNECTION_MODE && _isConnected )
			{
				_curBackgroundColor = BACKGROUND_COLOR_DEFAULT;
				drawBackground();
				
				var gridspaceRegionEvent:GridspaceRegionEvent = new GridspaceRegionEvent(
					GridspaceRegionEvent.REMOVE_CONNECTION, this );
				EventManager.DispatchGlobal( gridspaceRegionEvent );
			}
		}
		
		private function mouseDownHandler( e:MouseEvent ):void
		{
			if ( _mode == ADD_CONNECTION_MODE && !_isConnected )
			{
				if ( stage )
				{
					stage.addEventListener( MouseEvent.MOUSE_UP, stageMouseUpHandler );
				}
				
				var gridspaceRegionEvent:GridspaceRegionEvent = new GridspaceRegionEvent(
					GridspaceRegionEvent.START_ADD_CONNECTION, this );
				EventManager.DispatchGlobal( gridspaceRegionEvent );
			}
			else if ( _mode == REMOVE_CONNECTION_MODE && _isConnected )
			{
				if ( stage )
				{
					stage.addEventListener( MouseEvent.MOUSE_UP, stageMouseUpHandler );
				}
				
				gridspaceRegionEvent = new GridspaceRegionEvent(
					GridspaceRegionEvent.REMOVE_CONNECTION, this );
				EventManager.DispatchGlobal( gridspaceRegionEvent );
				
				gridspaceRegionEvent = new GridspaceRegionEvent(
					GridspaceRegionEvent.START_REMOVE_CONNECTION, this );
				EventManager.DispatchGlobal( gridspaceRegionEvent );
				
				_curBackgroundColor = BACKGROUND_COLOR_DEFAULT;
				drawBackground();
			}
		}
		
		private function rollOutHandler( e:MouseEvent ):void
		{
			if 
			( 
				( _mode == ADD_CONNECTION_MODE && !_isConnected )
				||
				( _mode == REMOVE_CONNECTION_MODE && _isConnected )
			)
			{
				_curBackgroundColor = ( _isConnected ) ? BACKGROUND_COLOR_ADD : BACKGROUND_COLOR_DEFAULT;
				drawBackground();
			}
		}
		
		private function stageMouseUpHandler( e:MouseEvent ):void
		{
			if ( stage )
			{
				stage.removeEventListener( MouseEvent.MOUSE_UP, stageMouseUpHandler );
			}
			
			if ( _mode == ADDING_CONNECTION_MODE )
			{
				var gridspaceRegionEvent:GridspaceRegionEvent = new GridspaceRegionEvent(
					GridspaceRegionEvent.CANCEL_ADD_CONNECTION, this );
				EventManager.DispatchGlobal( gridspaceRegionEvent );
			}
			else if ( _mode == REMOVING_CONNECTION_MODE )
			{
				gridspaceRegionEvent = new GridspaceRegionEvent(
					GridspaceRegionEvent.END_REMOVE_CONNECTION, this );
				EventManager.DispatchGlobal( gridspaceRegionEvent );
			}			
		}
		
		//------Static Methods
		
			
		//------Member Methods
			
		public function setNoMode():void
		{
			_mode = NO_MODE;
			setDefaultBackground();			
			showBackground();
		}
		
		public function setHideMode():void
		{
			_mode = HIDE_MODE;
			setDefaultBackground();
			hideBackground();
		}
		
		public function setAddConnectionMode():void
		{
			_mode = ADD_CONNECTION_MODE;
			setDefaultBackground();
			showBackground();
		}
		
		public function setAddingConnectionMode():void
		{
			_mode = ADDING_CONNECTION_MODE;
			setDefaultBackground();
			showBackground();
		}
		
		public function setRemoveConnectionMode():void
		{
			_mode = REMOVE_CONNECTION_MODE;
			setDefaultBackground();
			showBackground();
		}
		
		public function setRemovingConnectionMode():void
		{
			_mode = REMOVING_CONNECTION_MODE;
			setDefaultBackground();
			showBackground();
		}
		
		private function setDefaultBackground():void
		{
			_curBackgroundColor = ( _isConnected ) ? BACKGROUND_COLOR_ADD : BACKGROUND_COLOR_DEFAULT;
			drawBackground();
		}
		
		private function showBackground():void
		{
			if ( _background )
			{
				addChild( _background );
			}
		}
		
		private function hideBackground():void
		{
			if ( _background && contains( _background ) )
			{
				removeChild( _background );
			}
		}
		
		private function drawBackground():void
		{
			_background.graphics.clear();
			_background.graphics.lineStyle( LINE_THICKNESS, LINE_COLOR );
			_background.graphics.beginFill( _curBackgroundColor, BACKGROUND_ALPHA );
			_background.graphics.drawRect( 0, 0, _width, _height );
			_background.graphics.endFill();
		}
		
		public function equals( other:GridspaceRegion ):Boolean
		{
			return _gridspaceIndex == other.gridspaceIndex && _region == other.region;
		}
	}
}