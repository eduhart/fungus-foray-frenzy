package laccaria.gui.scope.biome.layer.connection 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Point;
	import laccaria.gui.scope.biome.layer.connection.GridspaceRegion;
	import laccaria.gui.scope.biome.layer.gridspace.GridspaceView;
	
	public class ConnectionView extends Sprite 
	{
		//------Constants
		
		private static const ANCHOR_BACKGROUND_COLOR_NORMAL	:uint	= 0xFFFFFF;
		private static const ANCHOR_BACKGROUND_COLOR_REMOVE	:uint	= 0xFF0000;
		private static const ANCHOR_BACKGROUND_ALPHA		:Number	= .5;
		private static const ANCHOR_RADIUS					:Number = 5;
		
		private static const LINE_THICKNESS					:Number	= 2;
		private static const LINE_COLOR						:uint = 0xFF00FF;
		
		//------Variables
		
		private var _regionFrom		:GridspaceRegion;
		private var _gridspaceTo	:GridspaceView;
		
		private var _anchor			:Shape;
		private var _curAnchorColor	:uint;	
		
		private var _from		:Point;
		private var _to			:Point;
		
		//------Getters/Setters
		
		public function get regionFrom()	:GridspaceRegion 	{ return _regionFrom; 			}
		public function get idGridspaceTo()	:int				{ return _gridspaceTo.index;	}
		
		//------Construction
		
		public function ConnectionView( regionFrom:GridspaceRegion, gridspaceTo:GridspaceView ) 
		{
			init( regionFrom, gridspaceTo );
		}
		
		private function init( regionFrom:GridspaceRegion, gridspaceTo:GridspaceView ):void
		{
			initVars( regionFrom, gridspaceTo );
			createAnchor();
			resetPosition();
			//addEventListeners();
		}
		
		private function initVars( regionFrom:GridspaceRegion, gridspaceTo:GridspaceView ):void
		{
			_regionFrom = regionFrom;
			_gridspaceTo = gridspaceTo;	
			mouseEnabled = false;
		}
		
		private function createAnchor():void
		{
			_anchor = new Shape();	
			addChild( _anchor );
			
			_curAnchorColor = ANCHOR_BACKGROUND_COLOR_NORMAL;
		}
		
		//------Methods
		
		public function resetPosition():void
		{			
			var centerX:Number = _regionFrom.x + .5 * _regionFrom.width;
			var centerY:Number = _regionFrom.y + .5 * _regionFrom.height;
			_from = new Point( centerX, centerY );
			
			centerX = _gridspaceTo.x + .5 * _gridspaceTo.width;
			centerY = _gridspaceTo.y + .5 * _gridspaceTo.height;
			_to = new Point( centerX, centerY );
			
			draw();
		}
		
		private function drawAchor():void
		{			
			with ( _anchor.graphics )
			{
				clear();
				beginFill( _curAnchorColor );
				drawCircle( _from.x, _from.y, ANCHOR_RADIUS );
				endFill();
			}
		}
		
		public function draw():void
		{
			with ( graphics )
			{
				clear();
				lineStyle( LINE_THICKNESS, LINE_COLOR );
				moveTo( _from.x, _from.y );
				lineTo( _to.x, _to.y );
			}
			
			drawAchor();
		}
		
		public function equals( regionFrom:GridspaceRegion ):Boolean
		{
			return _regionFrom.equals( regionFrom );
		}
	}
}