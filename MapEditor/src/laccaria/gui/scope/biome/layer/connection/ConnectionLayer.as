package laccaria.gui.scope.biome.layer.connection 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import laccaria.events.ButtonEvent;
	import laccaria.events.ConnectionEvent;
	import laccaria.events.EventManager;
	import laccaria.events.GridspaceRegionEvent;
	import laccaria.gui.scope.biome.layer.BiomeLayer;
	import laccaria.gui.scope.biome.layer.connection.GridspaceRegion;
	import laccaria.gui.scope.biome.layer.gridspace.GridspaceView;
	import laccaria.gui.scope.Layer;
	
	public class ConnectionLayer extends BiomeLayer
	{
		//------Constants
		
		private static const LINE_THICKNES		:Number = 2;
		private static const LINE_COLOR			:uint	= 0xFF00FF;
		private static const LINE_ALPHA			:Number = 1;
		private static const CONNECTION_OFFSET	:Point = new Point( -4, -3 );
		
		//------Variables
		
		private var _gridspaceRegions		:Array;
		//The gridspace region that is having a connection added from it
		private var _gridspaceRegionAdding	:GridspaceRegion;
		
		private var _connectionViews			:Array;
		private var _connectionIndicator	:Sprite;
		
		//------Getters/Setters
		
		public function get gridspaceRegionAdding():GridspaceRegion { return _gridspaceRegionAdding; }
		
		//------Construction
		
		public function ConnectionLayer()
		{
			super( BiomeLayer.CONNECTION );
			init();
		}
		
		private function init():void
		{
			initVars();
			addEventListeners();
			mouseEnabled = false;
		}
		
		private function initVars():void
		{
			_gridspaceRegions 		= new Array();
			_connectionViews			= new Array();
			
			_connectionIndicator 	= new Sprite();			
			_connectionIndicator.mouseEnabled = false;
			
			_container.mouseEnabled = false;
			mouseEnabled = false;
		}
		
		private function addEventListeners():void
		{
			//Button events (to change gridspace region mode)
			
			EventManager.AddGlobalEventListener( ButtonEvent.SELECT_ADD_CONNECTION,
				selectAddConnectionHandler );
			EventManager.AddGlobalEventListener( ButtonEvent.SELECT_REMOVE_CONNECTION,
				selectRemoveConnectionHandler );
			
			//Adding/Removing connection related events
			
			EventManager.AddGlobalEventListener( GridspaceRegionEvent.START_REMOVE_CONNECTION,
				startRemoveConnectionHandler );
			EventManager.AddGlobalEventListener( GridspaceRegionEvent.REMOVE_CONNECTION,
				removeConnectionHandler );
			EventManager.AddGlobalEventListener( GridspaceRegionEvent.END_REMOVE_CONNECTION,
				endRemoveConnectionHandler );
		}
		
		//------Event Handlers
		
		private function selectAddConnectionHandler( e:ButtonEvent ):void
		{
			setAllAddConnectionMode();
		}
		
		private function selectRemoveConnectionHandler( e:ButtonEvent ):void
		{
			setAllRemoveConnectionMode();
		}
		
		private function startRemoveConnectionHandler( e:GridspaceRegionEvent ):void
		{
			setAllRemovingConnectionMode();
		}
		
		private function removeConnectionHandler( e:GridspaceRegionEvent ):void
		{
			//Get the gridspace region that the connection came from and set is as not connected
			var gridspaceRegionFrom:GridspaceRegion = e.gridspaceRegion;
			gridspaceRegionFrom.isConnected = false;
			
			//Find the connection view attached to the gridspace region and remove it
			var connectionView:ConnectionView = findConnection( gridspaceRegionFrom );
			removeConnectionView( connectionView );
			
			//Send a remove connection event so that the connection can be removed from the model
			var idGridspaceFrom	:int = gridspaceRegionFrom	.gridspaceIndex;
			var directionFrom	:int = gridspaceRegionFrom	.region;
			var idGridspaceTo	:int = connectionView		.idGridspaceTo;
			
			var connectionEvent:ConnectionEvent = new ConnectionEvent(
				ConnectionEvent.REMOVE_CONNECTION, idGridspaceFrom, directionFrom, idGridspaceTo );
			EventManager.DispatchGlobal( connectionEvent );
		}
		
		private function endRemoveConnectionHandler( e:GridspaceRegionEvent ):void
		{
			setAllRemoveConnectionMode();
		}
		
		private function connectionUpdateHandler( e:Event ):void
		{
			var regionCenter:Point = new Point
			(
				_gridspaceRegionAdding.x + .5 * _gridspaceRegionAdding.width 	+ CONNECTION_OFFSET.x,
				_gridspaceRegionAdding.y + .5 * _gridspaceRegionAdding.height 	+ CONNECTION_OFFSET.y
			);
			
			_connectionIndicator.graphics.clear();
			_connectionIndicator.graphics.lineStyle( 1, 0 );
			_connectionIndicator.graphics.moveTo( regionCenter.x, regionCenter.y );
			_connectionIndicator.graphics.lineTo( mouseX, mouseY );
		}
		
		//------Methods
		
		public function initialize( gridspaces:Array ):void
		{
			_gridspaceRegions = new Array();
			
			removeChild( _container );
			_container = new Sprite();
			addChild( _container );
			_container.mouseEnabled = false;
			
			for each ( var gridspaceView:GridspaceView in gridspaces )
			{
				if ( gridspaceView.isAdded() )
				{
					for each ( var gridspaceRegion:GridspaceRegion in gridspaceView.regions )
					{
						gridspaceRegion.setNoMode();
						
						gridspaceRegion.x = gridspaceRegion.gridspacePosition.x + gridspaceView.x;
						gridspaceRegion.y = gridspaceRegion.gridspacePosition.y + gridspaceView.y;
						
						_gridspaceRegions.push( gridspaceRegion );
						_container.addChild( gridspaceRegion );
					}
				}
			}
			
			for each ( var connectionView:ConnectionView in _connectionViews )
			{
				_container.addChild( connectionView );
				connectionView.resetPosition();
			}
		}
		
		public function startConnection( gridspaceRegionFrom:GridspaceRegion ):void
		{
			_gridspaceRegionAdding = gridspaceRegionFrom;
			
			setAllHideMode();
			_gridspaceRegionAdding.setAddingConnectionMode();
			
			_container.addChild( _connectionIndicator );			
			addEventListener( Event.ENTER_FRAME, connectionUpdateHandler );
		}
		
		public function completeConnection( gridspaceViewTo:GridspaceView ):void
		{
			addConnection( _gridspaceRegionAdding, gridspaceViewTo );
			setAllAddConnectionMode();	
			
			//Remove and stop updating the connection indicator
			_container.removeChild( _connectionIndicator );
			removeEventListener( Event.ENTER_FRAME, connectionUpdateHandler );
		
			_gridspaceRegionAdding = null;
		}
		
		public function cancelConnection():void
		{
			setAllAddConnectionMode();
			
			//Remove and stop updating the connection indicator
			_container.removeChild( _connectionIndicator );
			removeEventListener( Event.ENTER_FRAME, connectionUpdateHandler );	
		
			_gridspaceRegionAdding = null;
		}
		
		public function addConnection( gridspaceRegionFrom:GridspaceRegion, gridspaceViewTo:GridspaceView ):void
		{
			//Set as connected
			gridspaceRegionFrom.isConnected = true;
			
			//Create the connection view
			var connectionView:ConnectionView = new ConnectionView( gridspaceRegionFrom, gridspaceViewTo );
			_connectionViews.push( connectionView );
			_container.addChild( connectionView );
		}
		
		public function removeAllConnectionViewsFromGridspace( idGridspace:int ):void
		{
			for ( var i:int = 0; i < _connectionViews.length; i++ )
			{
				var connectionView:ConnectionView = _connectionViews[ i ];
				var region:GridspaceRegion = connectionView.regionFrom;
				
				if ( region.gridspaceIndex == idGridspace )
				{
					region.isConnected = false;					
					removeConnectionView( connectionView );
					
					i--;
				}
			}
		}
		
		public function removeAllConnectionViewsToGridspace( idGridspace:int ):void
		{
			for ( var i:int = 0; i < _connectionViews.length; i++ )
			{
				var connectionView:ConnectionView = _connectionViews[ i ];
				
				if ( connectionView.idGridspaceTo == idGridspace )
				{
					var gridspaceRegionFrom:GridspaceRegion = connectionView.regionFrom;
					gridspaceRegionFrom.isConnected = false;
					
					removeConnectionView( connectionView );
					
					i--;
				}
			}
		}
		
		private function removeConnectionView( connectionView:ConnectionView ):void
		{
			if ( connectionView )
			{				
				var indexOfConnection:int = _connectionViews.indexOf( connectionView );
				_connectionViews.splice( indexOfConnection, 1 );
				
				if ( _container.contains( connectionView ) )
				{
					_container.removeChild( connectionView );
				}
			}
		}
		
		private function findConnection( fromRegion:GridspaceRegion ):ConnectionView
		{
			for each ( var connection:ConnectionView in _connectionViews )
			{
				if ( connection.equals( fromRegion ) )
				{
					return connection;
				}
			}
			
			return null;
		}
		
		public function setAllNoMode():void
		{
			for each ( var gridspaceRegion:GridspaceRegion in _gridspaceRegions )
			{
				gridspaceRegion.setNoMode();
			}
		}
		
		public function setAllHideMode():void
		{
			for each ( var gridspaceRegion:GridspaceRegion in _gridspaceRegions )
			{
				gridspaceRegion.setHideMode();
			}
		}
		
		public function setAllAddConnectionMode():void
		{
			for each ( var gridspaceRegion:GridspaceRegion in _gridspaceRegions )
			{
				gridspaceRegion.setAddConnectionMode();
			}
		}
		
		public function setAllAddingConnectionMode():void
		{
			for each ( var gridspaceRegion:GridspaceRegion in _gridspaceRegions )
			{
				gridspaceRegion.setAddingConnectionMode();
			}
		}
		
		public function setAllRemoveConnectionMode():void
		{
			for each ( var gridspaceRegion:GridspaceRegion in _gridspaceRegions )
			{
				gridspaceRegion.setRemoveConnectionMode();
			}
		}
		
		public function setAllRemovingConnectionMode():void
		{
			for each ( var gridspaceRegion:GridspaceRegion in _gridspaceRegions )
			{
				gridspaceRegion.setRemovingConnectionMode();
			}
		}
	}
}