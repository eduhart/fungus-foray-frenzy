package laccaria.gui.scope.biome.layer.gridspace 
{
	import laccaria.model.gridspace.Direction;
	import laccaria.model.gridspace.Gridspace;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;	
	import laccaria.events.GridspaceViewEvent;
	import laccaria.gui.scope.biome.layer.connection.GridspaceRegion
	import laccaria.events.EventManager;
	import laccaria.events.GridspaceEvent;
	
	public class GridspaceView extends Sprite
	{
		
		//------Constants
		
		private static const BACKGROUND_COLOR_HIDDEN	:uint 	= 0xFFFFFF;
		private static const BACKGROUND_COLOR_FILLED	:uint 	= 0xFFFF00;
		private static const BACKGROUND_COLOR_ADD		:uint 	= 0x00FF00;
		private static const BACKGROUND_COLOR_REMOVE	:uint	= 0xFF0000;
		
		private static const LINE_COLOR_DEFAULT			:uint	= 0x000000;
		private static const LINE_COLOR_HIGHLIGHT		:uint	= 0xFFFFFF;
		
		private static const ADD_CONNECTION_COLOR		:uint	= 0x00FF00;
		private static const REMOVE_CONNECTION_COLOR	:uint	= 0xFF0000;
		private static const BACKGROUND_ALPHA			:Number	= 0.5;
		private static const LINE_THICKNESS				:Number = 1;
		private static const LINE_COLOR					:uint 	= 0x000000;
		
		private static const NO_MODE					:int = 0;
		private static const ENTER_MODE					:int = 1;
		private static const ADD_MODE					:int = 2;
		private static const ADDING_MODE				:int = 3;
		private static const REMOVE_MODE				:int = 4;
		private static const REMOVING_MODE				:int = 5;
		private static const ADDING_CONNECTION_MODE		:int = 6;
		
		//------Member Variables
		
		private var _index				:int;
		private var _coordinates		:Point;
		
		private var _curBackgroundColor	:uint;
		private var _curLineColor		:uint;
		private var _gridspaceRegions	:Array;
		
		private var _mode				:int;
		
		//------Getters/Setters
		
		public function get index()			:int	{ return _index;			}
		public function get coordinates()	:Point	{ return _coordinates		}
		public function get regions()		:Array	{ return _gridspaceRegions;	}
		
		//------Construction
		
		public function GridspaceView( coordinates:Point ) 
		{
			init( coordinates );
		}
		
		private function init( coordinates:Point ):void
		{
			initVars( coordinates );
			drawBackground();
			createGridspaceRegions();
			addEventListeners();
		}
		
		private function initVars( coordinates:Point ):void
		{
			_index = -1;
			_coordinates = coordinates;
			_curBackgroundColor = BACKGROUND_COLOR_HIDDEN;
			_curLineColor = LINE_COLOR_DEFAULT;
			_gridspaceRegions = new Array();
			_mode = NO_MODE;
		}
		
		private function createGridspaceRegions():void
		{
			var northRegionPosition	:Point = new Point( ( 1 / 3 ) * Gridspace.WIDTH	, 0 							);
			var eastRegionPosition	:Point = new Point( ( 2 / 3 ) * Gridspace.WIDTH	, ( 1 / 3 ) * Gridspace.HEIGHT	);
			var southRegionPosition	:Point = new Point( ( 1 / 3 ) * Gridspace.WIDTH	, ( 2 / 3 ) * Gridspace.HEIGHT 	);
			var westRegionPosition	:Point = new Point( 0							, ( 1 / 3 ) * Gridspace.HEIGHT	);
			
			var northRegion	:GridspaceRegion = new GridspaceRegion( _index, Direction.NORTH,	northRegionPosition	);
			var eastRegion	:GridspaceRegion = new GridspaceRegion( _index, Direction.EAST,		eastRegionPosition 	);
			var southRegion	:GridspaceRegion = new GridspaceRegion( _index, Direction.SOUTH,	southRegionPosition );
			var westRegion	:GridspaceRegion = new GridspaceRegion( _index, Direction.WEST,		westRegionPosition 	);
			
			_gridspaceRegions = [ northRegion, eastRegion, southRegion, westRegion ];
		}
		
		private function addEventListeners():void
		{
			addEventListener( MouseEvent.ROLL_OVER, 	rollOverHandler 	);
			addEventListener( MouseEvent.MOUSE_DOWN, 	mouseDownHandler 	);
			addEventListener( MouseEvent.ROLL_OUT, 		rollOutHandler 		);
			addEventListener( MouseEvent.MOUSE_UP, 		mouseUpHandler 		);
		}
		
		//------Event Handlers
		
		private function rollOverHandler( e:MouseEvent ):void
		{
			if ( _mode == ENTER_MODE && isAdded() )
			{
				_curLineColor = LINE_COLOR_HIGHLIGHT;
				drawBackground();
			}
			if ( _mode == ADD_MODE && !isAdded() )
			{
				_curBackgroundColor = BACKGROUND_COLOR_ADD;
				drawBackground();
			}
			else if ( _mode == ADDING_MODE && !isAdded() )
			{
				var gridspaceEvent:GridspaceEvent = new GridspaceEvent( GridspaceEvent.ADD, _index, _coordinates );
				EventManager.DispatchGlobal( gridspaceEvent );
			}
			else if ( _mode == REMOVE_MODE && isAdded() )
			{
				_curBackgroundColor = BACKGROUND_COLOR_REMOVE;
				drawBackground();
			}
			else if ( _mode == REMOVING_MODE && isAdded() )
			{
				gridspaceEvent = new GridspaceEvent( GridspaceEvent.REMOVE, _index, _coordinates );
				EventManager.DispatchGlobal( gridspaceEvent );
			}
			else if ( _mode == ADDING_CONNECTION_MODE && isAdded() )
			{
				_curBackgroundColor = BACKGROUND_COLOR_ADD;
				drawBackground();
			}
		}
		
		private function mouseDownHandler( e:MouseEvent ):void
		{
			if ( _mode == ADD_MODE && _index == -1 )
			{
				var gridspaceViewEvent:GridspaceViewEvent = new GridspaceViewEvent( GridspaceViewEvent.START_ADD, this );
				EventManager.DispatchGlobal( gridspaceViewEvent );
				
				if ( !isAdded() )
				{				
					var gridspaceEvent:GridspaceEvent = new GridspaceEvent( GridspaceEvent.ADD, _index, _coordinates );
					EventManager.DispatchGlobal( gridspaceEvent );
				}
				
				if ( stage )
				{
					stage.addEventListener( MouseEvent.MOUSE_UP, stageMouseUpHandler );
				}
			}
			else if ( _mode == REMOVE_MODE && _index != -1 )
			{
				gridspaceViewEvent = new GridspaceViewEvent( GridspaceViewEvent.START_REMOVE, this );
				EventManager.DispatchGlobal( gridspaceViewEvent );
				
				if ( isAdded() )
				{
					gridspaceEvent = new GridspaceEvent( GridspaceEvent.REMOVE, _index, _coordinates );
					EventManager.DispatchGlobal( gridspaceEvent );
				}
				
				if ( stage )
				{
					stage.addEventListener( MouseEvent.MOUSE_UP, stageMouseUpHandler );
				}
			}
		}
		
		private function mouseUpHandler( e:MouseEvent ):void
		{
			if ( _mode == ENTER_MODE && isAdded() )
			{
				var gridspaceViewEvent:GridspaceViewEvent = new GridspaceViewEvent( GridspaceViewEvent.ENTER, this );
				EventManager.DispatchGlobal( gridspaceViewEvent );
			}
			else if ( _mode == ADDING_CONNECTION_MODE )
			{
				gridspaceViewEvent = new GridspaceViewEvent( GridspaceViewEvent.COMPLETE_CONNECTION, this );
				EventManager.DispatchGlobal( gridspaceViewEvent );
			}
		}
		
		private function rollOutHandler( e:MouseEvent ):void
		{
			if ( _mode == ADD_MODE || _mode == REMOVE_MODE || _mode == ADDING_CONNECTION_MODE )
			{
				_curBackgroundColor = _index != -1 ?  BACKGROUND_COLOR_FILLED : BACKGROUND_COLOR_HIDDEN;
				drawBackground();
			}
			if ( _mode == ENTER_MODE && isAdded() )
			{
				_curLineColor = LINE_COLOR_DEFAULT;
				drawBackground();
			}
		}
		
		private function stageMouseUpHandler( e:MouseEvent ):void
		{
			stage.removeEventListener( MouseEvent.MOUSE_UP, stageMouseUpHandler );
			
			if ( _mode == ADDING_MODE )
			{
				var gridspaceViewEvent:GridspaceViewEvent = new GridspaceViewEvent( GridspaceViewEvent.STOP_ADD, this );
				EventManager.DispatchGlobal( gridspaceViewEvent );
			}
			else if ( _mode == REMOVING_MODE )
			{
				gridspaceViewEvent = new GridspaceViewEvent( GridspaceViewEvent.STOP_REMOVE, this );
				EventManager.DispatchGlobal( gridspaceViewEvent );
			}
		}
		
		//------Member Methods
		
		public function getGridspaceRegion( direction:int ):GridspaceRegion
		{
			var regionToReturn:GridspaceRegion;
			
			if ( direction >= 0 && direction < _gridspaceRegions.length )
			{
				regionToReturn = _gridspaceRegions[ direction ];
			}
			
			return regionToReturn;
		}
		
		public function setNoMode():void
		{
			_mode = NO_MODE;
			setDefaultState();
		}
		
		public function setEnterMode():void
		{
			_mode = ENTER_MODE;
			setDefaultState();
		}
		
		public function setAddMode():void
		{
			_mode = ADD_MODE
			setDefaultState();
		}
		
		public function setAddingMode():void
		{
			_mode = ADDING_MODE;
			setDefaultState();
		}
		
		public function setRemoveMode():void
		{
			_mode = REMOVE_MODE;
			setDefaultState();
		}
		
		public function setRemovingMode():void
		{
			_mode = REMOVING_MODE;
			setDefaultState();
		}
		
		public function setAddingConnectionMode():void
		{
			_mode = ADDING_CONNECTION_MODE;
			setDefaultState();
		}
		
		private function setDefaultState():void
		{
			_curBackgroundColor = _index != -1 ?  BACKGROUND_COLOR_FILLED : BACKGROUND_COLOR_HIDDEN;
			_curLineColor = LINE_COLOR_DEFAULT;
			drawBackground();
		}
		
		public function setAdded( index:int ):void
		{
			_index = index;
			
			for each ( var gridspaceRegion:GridspaceRegion in _gridspaceRegions )
			{
				gridspaceRegion.gridspaceIndex = index;
			}
			
			_curBackgroundColor = BACKGROUND_COLOR_FILLED;
			drawBackground();
		}
		
		public function setRemoved():void
		{
			_index = -1;
			_curBackgroundColor = BACKGROUND_COLOR_HIDDEN;
			drawBackground();
			
			for each ( var gridspaceRegion:GridspaceRegion in _gridspaceRegions )
			{
				gridspaceRegion.isConnected = false;
			}
		}
		
		public function isAdded():Boolean
		{
			return _index != -1;
		}
		
		private function drawBackground():void
		{
			graphics.clear();
			graphics.lineStyle( LINE_THICKNESS, _curLineColor );
			graphics.beginFill( _curBackgroundColor, BACKGROUND_ALPHA );
			graphics.drawRect( 0, 0, Gridspace.WIDTH - LINE_THICKNESS, Gridspace.HEIGHT - LINE_THICKNESS );
			graphics.endFill();
		}
	}
}