package laccaria.gui.scope.biome.layer.gridspace 
{
	import flash.geom.Point;
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	import laccaria.events.GridspaceViewEvent;
	import laccaria.gui.scope.biome.layer.BiomeLayer;
	
	public class BiomeGridspaceLayer extends BiomeLayer
	{
		//------Variables
		
		private var _gridspaceViews		:Array;
		private var _backgroundWidth	:Number;
		private var _backgroundHeight	:Number;
		private var _initialized		:Boolean;
		
		//------Getters/Setters
		
		public function get gridspaceViews():Array { return _gridspaceViews; }
		
		//------Construction
		
		public function BiomeGridspaceLayer() 
		{
			super( BiomeLayer.GRIDSPACE );
			init();
		}
		
		private function init():void
		{
			initVars();
			addEventListeners();
		}
		
		private function initVars():void
		{
			_gridspaceViews = new Array();
			_initialized = false;
		}
		
		private function addEventListeners():void
		{
			EventManager.AddGlobalEventListener( ButtonEvent.SELECT_ENTER_GRIDSPACE,	selectEnterGridspaceHandler		);
			EventManager.AddGlobalEventListener( ButtonEvent.SELECT_ADD_GRIDSPACE, 		selectAddGridspaceHandler 		);
			EventManager.AddGlobalEventListener( ButtonEvent.SELECT_REMOVE_GRIDSPACE, 	selectRemoveGridspaceHandler 	);
			
			EventManager.AddGlobalEventListener( GridspaceViewEvent.START_ADD, 		startAddHandler 	);
			EventManager.AddGlobalEventListener( GridspaceViewEvent.STOP_ADD, 		stopAddHandler 		);
			EventManager.AddGlobalEventListener( GridspaceViewEvent.START_REMOVE, 	startRemoveHandler 	);
			EventManager.AddGlobalEventListener( GridspaceViewEvent.STOP_REMOVE, 	stopRemoveHandler 	);
		}
		
		//------Event Handlers
		
		private function selectEnterGridspaceHandler( e:ButtonEvent ):void
		{
			setAllEnterMode();
		}
		
		private function selectAddGridspaceHandler( e:ButtonEvent ):void
		{
			setAllAddMode();
		}
		
		private function selectRemoveGridspaceHandler( e:ButtonEvent ):void
		{
			setAllRemoveMode();
		}
		
		private function startAddHandler( e:GridspaceViewEvent ):void
		{
			setAllAddingMode();
		}
		
		private function stopAddHandler( e:GridspaceViewEvent ):void
		{
			setAllAddMode();
		}
		
		private function startRemoveHandler( e:GridspaceViewEvent ):void
		{
			setAllRemovingMode();
		}
		
		private function stopRemoveHandler( e:GridspaceViewEvent ):void
		{
			setAllRemoveMode();
		}
		
		//------Methods
		
		public function addGridspaceView( coordinates:Point, position:Point ):void
		{
			//If the given coordinates are not null and we don't already have a gridspaceView
			//with the given coordinates
			if ( coordinates && ! hasGridspaceView( coordinates ) )
			{
				var gridspaceView:GridspaceView = new GridspaceView( coordinates );
				gridspaceView.x = position.x;
				gridspaceView.y = position.y;
				
				_gridspaceViews.push( gridspaceView );
				_container.addChild( gridspaceView );
			}
		}
		
		public function setGridspaceAsRemoved( coordinates:Point ):void
		{
			var gridspaceView:GridspaceView = findGridspaceViewByCoordinates( coordinates );
			
			if ( gridspaceView )
			{
				gridspaceView.setRemoved();
			}
		}
		
		/**
		 * Set the GridspaceView with the given coordinates to the given index
		 * @param	coordinates	The coordinates of the GridspaceView
		 * @param	index		The index the GridspaceView will be set to
		 */
		public function setGridspaceViewIndex( coordinates:Point, index:int ):void
		{
			var gridspaceView:GridspaceView = findGridspaceViewByCoordinates( coordinates );
			
			if ( gridspaceView )
			{
				gridspaceView.setAdded( index );
			}
		}
		
		public function initialize():void
		{
			setAllNoMode();
		}
		
		public function findGridspaceViewByIndex( index:int ):GridspaceView
		{
			for each ( var gridspaceView:GridspaceView in _gridspaceViews )
			{
				if ( gridspaceView.index == index )
				{
					return gridspaceView;
				}
			}
			
			return null;
		}
		
		public function findGridspaceViewByCoordinates( coordinates:Point ):GridspaceView
		{
			for each ( var gridspaceView:GridspaceView in _gridspaceViews )
			{
				if ( gridspaceView.coordinates.equals( coordinates ) )
				{
					return gridspaceView;
				}
			}
			
			return null;
		}
		
		private function hasGridspaceView( coordinates:Point ):Boolean
		{
			for each ( var gridspaceView:GridspaceView in _gridspaceViews )
			{
				if ( gridspaceView.coordinates.equals( coordinates ) )
				{
					return true;
				}
			}
			
			return false;
		}
		
		public function setAllNoMode():void
		{
			for each ( var gridspaceView:GridspaceView in _gridspaceViews )
			{
				gridspaceView.setNoMode();
			}
		}
		
		public function setAllEnterMode():void
		{
			for each ( var gridspaceView:GridspaceView in _gridspaceViews )
			{
				gridspaceView.setEnterMode();
			}
		}
		
		public function setAllAddMode():void
		{
			for each ( var gridspaceView:GridspaceView in _gridspaceViews )
			{
				gridspaceView.setAddMode();
			}
		}
		
		public function setAllAddingMode():void
		{
			for each ( var gridspaceView:GridspaceView in _gridspaceViews )
			{
				gridspaceView.setAddingMode();
			}
		}
		
		public function setAllRemoveMode():void
		{
			for each ( var gridspaceView:GridspaceView in _gridspaceViews )
			{
				gridspaceView.setRemoveMode();
			}
		}
		
		public function setAllRemovingMode():void
		{
			for each ( var gridspaceView:GridspaceView in _gridspaceViews )
			{
				gridspaceView.setRemovingMode();
			}
		}
		
		/**
		 * Set the gridspace mode to ADDING_CONNECTION mode *if* the gridspace is added
		 */
		public function setAddedAddingConnectionMode():void
		{
			for each ( var gridspaceView:GridspaceView in _gridspaceViews )
			{
				if ( gridspaceView.isAdded() )
				{
					gridspaceView.setAddingConnectionMode();
				}
			}
		}
	}
}