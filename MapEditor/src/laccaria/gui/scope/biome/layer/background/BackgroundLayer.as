package laccaria.gui.scope.biome.layer.background 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import laccaria.gui.scope.biome.layer.BiomeLayer;
	
	public class BackgroundLayer extends BiomeLayer
	{		
		//------Variables
		
		private var _background:DisplayObject;
		
		//------Construction
		
		public function BackgroundLayer() 
		{
			super( BiomeLayer.BACKGROUND );
		}
		
		//------Event Handlers
		
		//------Methods
		
		public function addBackground( background:DisplayObject ):void
		{			
			// Remove the background if one is already there
			if ( _background && _container.contains( _background ) )
			{
				_container.removeChild( _background );
			}
			
			// Add the new background to the window - welcome to the family background!
			_background = background;			
			_container.addChild( background );
		}
	}
}