package laccaria.gui.scope.biome.layer 
{
	import laccaria.gui.scope.Layer;
	import laccaria.gui.scope.Scope;
	
	public class BiomeLayer extends Layer
	{
		//------Constants
		
		//Layer types
		public static const BACKGROUND	:int = 0;
		public static const GRIDSPACE	:int = 1;
		public static const CONNECTION	:int = 2;
		public static const ALL			:int = 3;
		
		//------Construction
		
		public function BiomeLayer( layerNum:int ) 
		{
			super( Scope.BIOME, layerNum );
		}
	}
}