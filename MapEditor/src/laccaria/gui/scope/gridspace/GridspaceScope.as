package laccaria.gui.scope.gridspace
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	import laccaria.events.gridspace_object.AddGridspaceObjectEvent;
	import laccaria.events.gridspace_object.GridspaceObjectTypeEvent;
	import laccaria.events.gridspace_object.GridspaceObjectViewEvent;
	import laccaria.events.gridspace_object.MoveGridspaceObjectEvent;
	import laccaria.events.gridspace_object.RemoveGridspaceObjectEvent;
	import laccaria.gui.scope.Scope;
	import laccaria.model.gridspace.Gridspace;
	import laccaria.model.gridspace.gridspace_object.GridspaceObject;
	import laccaria.model.gridspace.gridspace_object.type.GridspaceObjectType;
	
	public class GridspaceScope extends Scope
	{
		//------Constants
		
		private static const NO_MODE	:int = 0;
		private static const ADD_MODE	:int = 1;
		private static const MOVE_MODE	:int = 2;
		private static const REMOVE_MODE:int = 3;
		
		//------Variables
		
		private static var _isMouseDown		:Boolean = false;;
		
		private var _idGridspace			:int;
		
		private var _gridspaceObjectViews	:Array;
		private var _selectedType			:GridspaceObjectType;
		
		private var _viewMoving				:GridspaceObjectView;
		private var _viewMovingStartX		:Number;
		private var _viewMovingStartY		:Number;
		
		private var _placementObject		:GridspaceObjectView;
		private var _placementGrid			:Array;					//2D array of Booleans. True = object placed.
		
		private var _mode					:int;
		
		private var _backgroundContainer	:Sprite;
		
		//------Getters/Setters
		
		public function get idGridspace():int { return _idGridspace; }
		
		//------Construction
		
		public function GridspaceScope() 
		{
			super( Scope.GRIDSPACE );
			
			init();
		}
		
		private function init():void
		{
			addEventListeners();
			initVars();
		}
		
		private function initVars():void
		{
			createPlacementGrid();
			
			_gridspaceObjectViews = new Array();
			
			_backgroundContainer = new Sprite();
			addChild( _backgroundContainer );
		}
		
		private function createPlacementGrid():void
		{
			_placementGrid = new Array();
			var numGridspaceTilesX:int = int( Gridspace.WIDTH / Gridspace.TILE_WIDTH );
			var numGridspaceTilesY:int = int( Gridspace.HEIGHT / Gridspace.TILE_HEIGHT );
			
			for ( var i:int = 0; i < numGridspaceTilesX; i++ )
			{
				_placementGrid.push( new Array() );
				
				for ( var j:int = 0; j < numGridspaceTilesY; j++ )
				{
					_placementGrid[ i ].push( false );
				}
			}
		}
		
		private function addEventListeners():void
		{
			EventManager.AddGlobalEventListener( GridspaceObjectTypeEvent	.SELECT_TYPE, 			selectTypeHandler 					);
			EventManager.AddGlobalEventListener( GridspaceObjectTypeEvent	.UNSELECT_TYPE,			unselectTypeHandler					);
			
			EventManager.AddGlobalEventListener( ButtonEvent				.SELECT_ADD_OBJECT,		selectAddButtonHandler				);
			EventManager.AddGlobalEventListener( ButtonEvent				.SELECT_REMOVE_OBJECT,	selectRemoveButtonHandler			);
			EventManager.AddGlobalEventListener( ButtonEvent				.SELECT_MOVE_OBJECT,	selectMoveButtonHandler				);
			
			EventManager.AddGlobalEventListener( GridspaceObjectViewEvent	.REMOVE,				removeGridspaceObjectViewHandler	);
			EventManager.AddGlobalEventListener( GridspaceObjectViewEvent	.START_MOVE,			startMoveGridspaceObjectViewHandler	);
			EventManager.AddGlobalEventListener( GridspaceObjectViewEvent	.STOP_MOVE,				stopMoveGridspaceObjectViewHandler	);
			
			addEventListener( MouseEvent.MOUSE_DOWN,	mouseDownHandler	);
			addEventListener( MouseEvent.MOUSE_MOVE, 	mouseMoveHandler 	);
			addEventListener( MouseEvent.MOUSE_UP, 		mouseUpHandler 	  	);
		}
		
		//------Event Handlers
		
		private function selectTypeHandler( e:GridspaceObjectTypeEvent ):void
		{
			_selectedType = e.gridspaceObjectType;
			
			if ( _mode == ADD_MODE )
			{
				removePlacementObject();
				addPlacementObject();
			}
		}
		
		private function unselectTypeHandler( e:GridspaceObjectTypeEvent ):void
		{
			_selectedType = null;
			removePlacementObject();
		}
		
		private function selectAddButtonHandler( e:ButtonEvent ):void
		{
			setAddMode();
		}
		
		private function selectRemoveButtonHandler( e:ButtonEvent ):void
		{
			setRemoveMode();
		}
		
		private function selectMoveButtonHandler( e:ButtonEvent ):void
		{
			setMoveMode();
		}
		
		private function removeGridspaceObjectViewHandler( e:GridspaceObjectViewEvent ):void
		{
			var objectToRemove:GridspaceObjectView = e.gridspaceObjectView;
			var idObject:int = objectToRemove.idObject;
			
			//If the object was successfully removed
			if ( removeGridspaceObjectView( objectToRemove ) )
			{
				var event:RemoveGridspaceObjectEvent = new RemoveGridspaceObjectEvent( idObject );
				EventManager.DispatchGlobal( event );
			}
		}
		
		private function startMoveGridspaceObjectViewHandler( e:GridspaceObjectViewEvent ):void
		{
			_viewMoving = e.gridspaceObjectView;
			_viewMovingStartX = _viewMoving.x;
			_viewMovingStartY = _viewMoving.y;
			_mode = MOVE_MODE;
			
			var placementRect:Rectangle = _viewMoving.areaRect;
			setPlacementGridArea( placementRect, false );
			
			setAllObjectsMoveMode();
		}
		
		private function stopMoveGridspaceObjectViewHandler( e:GridspaceObjectViewEvent ):void
		{
			//If the view moving was placed on an existing object, bring it back to where it started
			var areaRect:Rectangle = _viewMoving.areaRect;
			if ( ! isCoordinateAreaClear( areaRect ) )
			{
				_viewMoving.move( _viewMovingStartX, _viewMovingStartY );
			}
			else
			{
				var idObject	:int = _viewMoving.idObject;
				var newXCoord	:int = _viewMoving.xCoord;
				var newYCoord	:int = _viewMoving.yCoord;
				
				//Dispatch an event saying that the gridspace object has moved
				var event:MoveGridspaceObjectEvent = new MoveGridspaceObjectEvent( idObject, newXCoord, newYCoord );
				EventManager.DispatchGlobal( event );
			}
			
			areaRect = _viewMoving.areaRect;
			setPlacementGridArea( areaRect, true );
			
			_viewMoving.setOverlayNone();
			setAllObjectsMoveMode();
			
			_viewMoving = null;
			_viewMovingStartX = -1;
			_viewMovingStartY = -1;
			
			_mode = NO_MODE;
		}
		
		private function mouseDownHandler( e:MouseEvent ):void
		{
			_isMouseDown = true;
			
			if ( stage )
			{
				stage.addEventListener( MouseEvent.MOUSE_UP, stageMouseUpHandler );
			}
			
			if ( _mode == ADD_MODE && _placementObject )
			{
				attemptAddPlacementObject();
			}
		}
		
		private function mouseMoveHandler( e:MouseEvent ):void
		{
			if ( _mode == ADD_MODE && _placementObject )
			{
				movePlacementObject();
				
				if ( _isMouseDown )
				{
					attemptAddPlacementObject();
				}
			}
			else if ( _mode == MOVE_MODE && _viewMoving )
			{
				moveSelectedGridspaceObject();
			}
		}
		
		private function mouseUpHandler( e:MouseEvent ):void
		{
			
		}
		
		private function stageMouseUpHandler( e:MouseEvent ):void
		{
			_isMouseDown = false;
			
			if ( stage )
			{
				stage.removeEventListener( MouseEvent.MOUSE_UP, stageMouseUpHandler );
			}
		}
		
		//------Methods
		
		public override function setLayer( layerNum:int ):void
		{
			super.setLayer( layerNum );
		}
		
		public function initialize( idGridspace:int, gridspaceBackground:Bitmap, gridspaceObjects:Array ):void
		{
			_mode = NO_MODE;
			
			_idGridspace = idGridspace;
			
			if ( _backgroundContainer && contains( _backgroundContainer ) )
			{
				removeChild( _backgroundContainer );
			}
			
			_backgroundContainer = new Sprite();
			addChild( _backgroundContainer );
			
			_idGridspace = idGridspace;
			_backgroundContainer.addChild( gridspaceBackground );
			
			createPlacementGrid();
			
			for each ( var gridspaceObject:GridspaceObject in gridspaceObjects )
			{
				addGridspaceObjectView( gridspaceObject );
			}
			
			if ( _placementObject && contains( _placementObject ) )
			{
				removeChild( _placementObject );
			}
		}
		
		public function addGridspaceObjectView( gridspaceObject:GridspaceObject ):void
		{
			var xCoord:int = gridspaceObject.coordinates.x;
			var yCoord:int = gridspaceObject.coordinates.y;
			
			//If a gridspace view is not already placed there
			if ( ! _placementGrid[ xCoord ][ yCoord ] )
			{
				var idObject	:int 	= gridspaceObject.idObject;
				var tileWidth	:int 	= gridspaceObject.tileWidth;
				var tileHeight	:int 	= gridspaceObject.tileHeight;
				var background	:Bitmap	= gridspaceObject.background;
				
				var newGridspaceObjectView:GridspaceObjectView = new GridspaceObjectView( background, tileWidth, tileHeight, idObject, xCoord, yCoord );
				newGridspaceObjectView.x = xCoord * Gridspace.TILE_WIDTH;
				newGridspaceObjectView.y = yCoord * Gridspace.TILE_HEIGHT;
				
				var area:Rectangle = newGridspaceObjectView.areaRect;
				setPlacementGridArea( area, true );
				
				addChild( newGridspaceObjectView );
				_gridspaceObjectViews.push( newGridspaceObjectView );
				
				if ( _placementObject && contains( _placementObject ) )
				{
					//Add the placement object to the front of the stage
					addChild( _placementObject );
				}
			}
		}
		
		private function attemptAddPlacementObject():void
		{
			var areaRect:Rectangle = _placementObject.areaRect;
				
			//If the placement object is not on another object
			if ( isCoordinateAreaClear( areaRect ) )
			{
				//Tell something to add the gridspace object to the gridspace
				var event:AddGridspaceObjectEvent = new AddGridspaceObjectEvent
				(
					_selectedType.idType,
					_placementObject.xCoord,
					_placementObject.yCoord
				);
				
				EventManager.DispatchGlobal( event );
			}
		}
		
		/**
		 * Removes the gridspace object view from the gridspace scope
		 * @param	gridspaceObjectView	The gridspace object view to remove
		 * @return	True if successfully removed, otherwise false
		 */
		private function removeGridspaceObjectView( gridspaceObjectView:GridspaceObjectView ):Boolean
		{			
			if ( gridspaceObjectView )
			{
				var index:int = _gridspaceObjectViews.indexOf( gridspaceObjectView )
				
				if ( index != -1 )
				{
					_gridspaceObjectViews.splice( index, 1 );
					
					if ( contains( gridspaceObjectView ) )
					{
						removeChild( gridspaceObjectView );
					}
					
					//Mark the area that the gridspace object view took up as not taken					
					var gridArea:Rectangle = gridspaceObjectView.areaRect;
					setPlacementGridArea( gridArea, false );
					
					return true;
				}
			}
			
			return false;
		}
		
		private function setAddMode():void
		{
			if ( _mode != ADD_MODE )
			{				
				if ( _selectedType )
				{
					addPlacementObject();
				}
				
				_mode = ADD_MODE;
				
				setAllObjectsNoMode();
			}
		}
		
		private function setRemoveMode():void
		{
			if ( _mode == ADD_MODE )
			{
				removePlacementObject();
			}
			
			_mode = REMOVE_MODE;
			setAlLObjectsRemoveMode();
		}
		
		private function setMoveMode():void
		{
			if ( _mode == ADD_MODE )
			{
				removePlacementObject();
			}
			
			setAllObjectsMoveMode();
		}
		
		private function addPlacementObject():void
		{
			if ( _selectedType )
			{
				var background	:Bitmap = _selectedType.getSpriteCopy();
				var tileWidth	:int 	= _selectedType.tileWidth;
				var tileHeight	:int 	= _selectedType.tileHeight;
				
				_placementObject = new GridspaceObjectView( background, tileWidth, tileHeight );
				_placementObject.mouseEnabled = false;
				addChild( _placementObject );
			}
		}
		
		private function removePlacementObject():void
		{
			if ( _placementObject && contains( _placementObject ) )
			{
				removeChild( _placementObject );
			}
			
			_placementObject = null;
		}
		
		private function setPlacementGridArea( rect:Rectangle, value:Boolean ):void
		{			
			for ( var i:int = rect.x; i < rect.x + rect.width; i++ )
			{
				if ( i < _placementGrid.length )
				{
					for ( var j:int = rect.y; j < rect.y + rect.height; j++ )
					{
						if ( j < _placementGrid[ i ].length )
						{
							_placementGrid[ i ][ j ] = value;
						}
					}
				}
			}
		}
		
		private function movePlacementObject():void
		{
			moveGridspaceObjectView( _placementObject );
		}
		
		private function moveSelectedGridspaceObject():void
		{
			moveGridspaceObjectView( _viewMoving );
		}
		
		//Moves the given gridspace object view.
		//If the new coordinates are on an existing object, then the given object view's overlay will be error,
		//otherwise the overlay will be placement.
		private function moveGridspaceObjectView( gridspaceObjectView:GridspaceObjectView ):void
		{
			//Snap the placement marker's position to the top left of the tile it is in
			var newXLoc:int = int( mouseX / Gridspace.TILE_WIDTH ) * Gridspace.TILE_WIDTH;
			var newYLoc:int = int( mouseY / Gridspace.TILE_HEIGHT ) * Gridspace.TILE_HEIGHT;
			
			//If the new placement is within the gridspace
			if ( newXLoc >= 0 && newXLoc < Gridspace.WIDTH && newYLoc >= 0 && newYLoc < Gridspace.HEIGHT )
			{
				gridspaceObjectView.move( newXLoc, newYLoc );				
				var areaRect:Rectangle = gridspaceObjectView.areaRect;
				
				//If a gridspace object is already on the tile
				if ( ! isCoordinateAreaClear( areaRect ) )
				{
					gridspaceObjectView.setOverlayError();
				}
				else
				{
					gridspaceObjectView.setOverlayPlacement();
				}
			}
		}
		
		private function isCoordinateAreaClear( areaRect:Rectangle ):Boolean
		{
			for ( var i:int = areaRect.x; i < areaRect.x + areaRect.width; i++ )
			{
				for ( var j:int = areaRect.y; j < areaRect.y + areaRect.height; j++ )
				{
					//If i and j is not within the bounds of the placement grid or the placement grid at i and j is true...
					if
					(
						i < 0 || i >= _placementGrid.length ||
						j < 0 || j >= _placementGrid[ i ].length ||
						_placementGrid[ i ][ j ]
					)
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		private function setAllObjectsNoMode():void
		{
			for each ( var gridspaceObjectView:GridspaceObjectView in _gridspaceObjectViews )
			{
				gridspaceObjectView.setNoMode();
			}
		}
		
		private function setAlLObjectsRemoveMode():void
		{
			for each ( var gridspaceObjectView:GridspaceObjectView in _gridspaceObjectViews )
			{
				gridspaceObjectView.setRemoveMode();
			}
		}
		
		private function setAllObjectsMoveMode():void
		{
			for each ( var gridspaceObjectView:GridspaceObjectView in _gridspaceObjectViews )
			{
				gridspaceObjectView.setMoveMode();
			}
		}
	}
}