package laccaria.gui.scope.gridspace 
{
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import laccaria.events.EventManager;
	import laccaria.events.gridspace_object.GridspaceObjectViewEvent;
	import laccaria.events.gridspace_object.MoveGridspaceObjectEvent;
	import laccaria.model.gridspace.Gridspace;
	
	public class GridspaceObjectView extends Sprite
	{
		//------Constants
		
		private static const ERROR_COLOR	:uint = 0xFF0000;
		private static const PLACE_COLOR	:uint = 0x00FF00;
		private static const SELECT_COLOR	:uint = 0xFFFFFF;
		private static const REMOVE_COLOR	:uint = 0xFF0000;
		private static const OVERLAY_ALPHA	:Number = 0.5;
		
		private static const NO_MODE		:int = 0;
		private static const REMOVE_MODE	:int = 1;
		private static const MOVE_MODE		:int = 2;
		
		//------Variables
		
		private static var _isMouseDown	:Boolean = false;
		
		private var _idObject		:int;
		private var _tileWidth		:int;
		private var _tileHeight		:int;
		private var _background		:Bitmap;
		private var _xCoord			:int;
		private var _yCoord			:int;
		private var _areaRect		:Rectangle;
		
		private var _overlay		:Shape;
		private var _curOverlayColor:uint;
		
		private var _mode			:int;
		
		//------Getters/Setters
		
		public function get idObject()	:int 		{ return _idObject;		}
		public function get tileWidth()	:int 		{ return _tileWidth; 	}
		public function get tileHeight():int 		{ return _tileHeight; 	}		
		public function get xCoord()	:int 		{ return _xCoord; 		}
		public function get yCoord()	:int 		{ return _yCoord; 		}
		public function get areaRect()	:Rectangle 	{ return _areaRect; 	}
		
		//------Construction
		
		public function GridspaceObjectView( background:Bitmap, tileWidth:int, tileHeight:int, idObject:int=-1, xCoord:int=-1, yCoord:int=-1 ) 
		{
			init( background, tileWidth, tileHeight, idObject, xCoord, yCoord );
		}
		
		private function init( background:Bitmap, tileWidth:int, tileHeight:int, idObject:int, xCoord:int, yCoord:int ):void
		{
			initVars( background, tileWidth, tileHeight, idObject, xCoord, yCoord );
			addEventListeners();
		}
		
		private function initVars( background:Bitmap, tileWidth:int, tileHeight:int, idObject:int, xCoord:int, yCoord:int ):void
		{
			_idObject = idObject;
			_tileWidth = tileWidth;
			_tileHeight = tileHeight;
			_xCoord = xCoord;
			_yCoord = yCoord;
			_areaRect = new Rectangle( _xCoord, _yCoord, _tileWidth, _tileHeight );
			
			_background = background;
			addChild( background );
			
			_overlay = new Shape();
		}
		
		private function addEventListeners():void
		{
			addEventListener( MouseEvent.ROLL_OVER, rollOverHandler 	);
			addEventListener( MouseEvent.MOUSE_DOWN, mouseDownHandler 	);
			addEventListener( MouseEvent.ROLL_OUT,	rollOutHandler		);
		}
		
		//------Event Handlers
		
		private function rollOverHandler( e:MouseEvent ):void
		{
			switch( _mode )
			{
				case REMOVE_MODE:
					if ( _isMouseDown )
					{
						//Dispatch an event telling the GridspaceScope to remove this
						var event:GridspaceObjectViewEvent = new GridspaceObjectViewEvent(
							GridspaceObjectViewEvent.REMOVE, this );
						EventManager.DispatchGlobal( event );
					}
					else
					{
						setOverlayRemove();
					}
					break;
			}
		}
		
		private function mouseDownHandler( e:MouseEvent ):void
		{
			_isMouseDown = true;
			
			if ( stage )
			{
				stage.addEventListener( MouseEvent.MOUSE_UP, stageMouseUpHandler );
			}
			
			switch( _mode )
			{
				case MOVE_MODE:				
					//Dispatch an event telling the GridspaceScope to start moving this
					var event:GridspaceObjectViewEvent = new GridspaceObjectViewEvent(
						GridspaceObjectViewEvent.START_MOVE, this );
					EventManager.DispatchGlobal( event );
					break;
				case REMOVE_MODE:
					//Dispatch an event telling the GridspaceScope to remove this
					event = new GridspaceObjectViewEvent( GridspaceObjectViewEvent.REMOVE, this );
					EventManager.DispatchGlobal( event );
					break;
			}		
		}
		
		private function rollOutHandler( e:MouseEvent ):void
		{
			switch( _mode )
			{
				case REMOVE_MODE:
					setOverlayNone();
					break;
			}
		}
		
		private function stageMouseUpHandler( e:MouseEvent ):void
		{
			switch( _mode )
			{
				case MOVE_MODE:					
					//Dispatch an event telling the GridspaceScope to stop moving this
					var event:GridspaceObjectViewEvent = new GridspaceObjectViewEvent(
						GridspaceObjectViewEvent.STOP_MOVE, this );
					EventManager.DispatchGlobal( event );
					break;
			}
			
			_isMouseDown = false;
			trace( _isMouseDown );
			
			if ( stage )
			{
				stage.removeEventListener( MouseEvent.MOUSE_UP, stageMouseUpHandler );
			}
		}
		
		//------Methods
		
		public function move( x:Number, y:Number ):void
		{
			_xCoord = int( x / Gridspace.TILE_WIDTH );
			_yCoord = int( y / Gridspace.TILE_HEIGHT );
			
			_areaRect.x = _xCoord;
			_areaRect.y = _yCoord;
			
			this.x = x;
			this.y = y;
		}
		
		public function setNoMode():void
		{
			_mode = NO_MODE;
			setOverlayNone();
		}
		
		public function setRemoveMode():void
		{
			_mode = REMOVE_MODE;
			setOverlayNone();
		}
		
		public function setMoveMode():void
		{
			_mode = MOVE_MODE;
			setOverlayNone();
		}
		
		public function setOverlayError():void
		{
			_curOverlayColor = ERROR_COLOR;
			drawOverlay();
			
			if ( _overlay && ! contains( _overlay ) )
			{
				addChild( _overlay );
			}
		}
		
		public function setOverlayPlacement():void
		{
			_curOverlayColor = PLACE_COLOR;
			drawOverlay();
			
			if ( _overlay && ! contains( _overlay ) )
			{
				addChild( _overlay );
			}
		}
		
		public function setOverlaySelect():void
		{
			_curOverlayColor = SELECT_COLOR;
			drawOverlay();
			
			if ( _overlay && ! contains( _overlay ) )
			{
				addChild( _overlay );
			}
		}
		
		public function setOverlayNone():void
		{
			if ( _overlay && contains( _overlay ) )
			{
				removeChild( _overlay );
			}
		}
		
		private function setOverlayRemove():void
		{
			_curOverlayColor = REMOVE_COLOR;
			drawOverlay();
			
			if ( _overlay && ! contains( _overlay ) )
			{
				addChild( _overlay );
			}
		}
		
		private function drawOverlay():void
		{
			_overlay.graphics.clear();
			_overlay.graphics.beginFill( _curOverlayColor, OVERLAY_ALPHA );
			_overlay.graphics.drawRect( 0, 0, width, height );
			_overlay.graphics.endFill();
		}
	}
}