package laccaria.gui.scope.gridspace 
{
	import laccaria.gui.scope.Layer;
	import laccaria.gui.scope.Scope;
	
	public class GridspaceLayer extends Layer
	{
		//------Constants
		
		//Layer types
		public static const BACKGROUND	:int = 0;
		public static const FOREGROUND	:int = 1;
		public static const ALL			:int = 2;
		
		//------Construction
		
		public function GridspaceLayer( layerNum:int ) 
		{
			super( Scope.GRIDSPACE, layerNum );
		}		
	}
}