package laccaria.gui.scope 
{
	import flash.display.Sprite;
	
	public class Layer extends Sprite
	{
		//------Constants
		
		public static const ALL	:int = 9001;
		
		//------Variables
		
		protected var _container:Sprite;
		protected var _scope	:int;
		protected var _layerNum	:int;
		
		//------Construction
		
		public function Layer( scope:int, layerNum:int ) 
		{
			init( scope, layerNum );
		}
		
		private function init( scope:int, layerNum:int ):void
		{
			initVars( scope, layerNum );
		}
		
		private function initVars( scope:int, layerNum:int ):void
		{
			_scope = scope;
			_layerNum = layerNum;
			
			_container = new Sprite();
			addChild( _container );
		}
		
		//------Methods
		
		public function scale( scaleX:Number, scaleY:Number ):void
		{
			_container.scaleX = scaleX;
			_container.scaleY = scaleY;
		}
		
		public function move( xLoc:Number, yLoc:Number ):void
		{
			_container.x = xLoc;
			_container.y = yLoc;
		}
		
		public function applies( scope:int, layerNum:int ):Boolean
		{
			return _scope == scope && _layerNum <= layerNum;
		}
	}

}