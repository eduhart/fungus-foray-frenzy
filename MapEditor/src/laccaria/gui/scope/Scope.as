package laccaria.gui.scope 
{
	import flash.display.Sprite;
	
	public class Scope extends Sprite
	{
		public static const BIOME		:int = 0;
		public static const GRIDSPACE	:int = 1;
		
		protected var _scopeNum			:int;
		protected var _container	:Sprite;
		protected var _layers			:Array;
		
		protected var _curLayer			:int;
		
		public function Scope( scopeNum:int ) 
		{
			init( scopeNum );
		}
		
		private function init( scopeNum:int ):void
		{
			initVars( scopeNum );
		}
		
		private function initVars( scopeNum:int ):void
		{
			_scopeNum = scopeNum;
			
			_container = new Sprite();
			addChild( _container );
			
			_layers = new Array();
		}
		
		public function setLayer( layerNum:int ):void
		{
			_curLayer = layerNum;
			
			for each ( var layer:Layer in _layers )
			{
				if ( layer.applies( _scopeNum, _curLayer ) )
				{
					_container.addChild( layer );
				}
			}
		}
		
		public function scale( scaleX:Number, scaleY:Number ):void
		{
			_container.scaleX = scaleX;
			_container.scaleY = scaleY;
		}
		
		public function move( xLoc:Number, yLoc:Number ):void
		{
			_container.x = xLoc;
			_container.y = yLoc;
		}
		
		public function applies( scopeNum:int ):Boolean
		{
			return _scopeNum == scopeNum;
		}
	}
}