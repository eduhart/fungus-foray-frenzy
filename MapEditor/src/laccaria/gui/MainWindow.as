package laccaria.gui 
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import laccaria.events.EventManager;
	import laccaria.events.GridspaceViewEvent;
	import laccaria.gui.scope.biome.BiomeScope;
	import laccaria.gui.scope.gridspace.GridspaceScope;
	import laccaria.gui.scope.Scope;
	import laccaria.model.gridspace.Gridspace;
	
	public class MainWindow extends Sprite
	{
		//------Constants
		
		public static const WIDTH				:Number = 600;
		public static const HEIGHT				:Number = 450;
		
		private static const LINE_THICKNESS		:Number = 2;
		private static const LINE_COLOR			:uint 	= 0x2A2A2A;
		private static const BACKGROUND_COLOR	:uint 	= 0x000000;
		
		//------Variables
		
		private var _activeScope:Scope;
		private var _biomeScope:BiomeScope;
		private var _gridspaceScope:GridspaceScope;
		
		//------Getters/Setters
		
		public function get biomeScope()	:BiomeScope 	{ return _biomeScope; 		}
		public function get gridspaceScope():GridspaceScope	{ return _gridspaceScope; 	}
		
		//------Construction
		
		public function MainWindow() 
		{
			init();
		}
		
		private function init():void
		{
			initVars();
			drawBackground();
		}
		
		private function initVars():void
		{
			_biomeScope 	= new BiomeScope();
			_gridspaceScope = new GridspaceScope();
		}
		
		private function drawBackground():void
		{
			with ( graphics )
			{
				lineStyle( LINE_THICKNESS, LINE_COLOR );
				beginFill( BACKGROUND_COLOR );
				drawRect( 0, 0, WIDTH, HEIGHT );
				endFill();
			}
		}
		
		//------Methods
		
		public function setBiomeScope():void
		{
			if ( _activeScope && contains( _activeScope ) )
			{
				removeChild( _activeScope );
			}
			
			_activeScope = _biomeScope;
			addChild( _activeScope );
		}
		
		public function setGridspaceScope( gridspaceIndex:int, gridspaceBackground:Bitmap, gridspaceObjects:Array ):void
		{
			if ( _activeScope && contains( _activeScope ) )
			{
				removeChild( _activeScope );
			}
			
			_gridspaceScope.initialize( gridspaceIndex, gridspaceBackground, gridspaceObjects );
			_activeScope = _gridspaceScope;			
			addChild( _gridspaceScope );
		}
		
		public function setLayer( layerNum:int ):void
		{
			if ( _activeScope )
			{
				_activeScope.setLayer( layerNum );
			}
		}
	}
}