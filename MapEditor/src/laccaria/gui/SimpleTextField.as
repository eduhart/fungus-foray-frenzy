package laccaria.gui 
{
	import flash.display.BlendMode;
	import flash.filters.BitmapFilter;
	import flash.filters.ColorMatrixFilter;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.getQualifiedClassName;
	
	/**
	 * Same functionality as a text field, but less work intializing.
	 */
	public class SimpleTextField extends TextField
	{
		private static const FONT		:String 	= "Arial";
		private static const TEXT_ALIGN	:String 	= TextFormatAlign.LEFT;
		private static const FONT_COLOR	:uint 		= 0X000000;
		private static const AUTO_SIZE	:String 	= TextFieldAutoSize.LEFT;
		
		public function SimpleTextField ( text:String, fontSize:Number, width:Number=-1, height:Number=-1 ) 
		{
			mouseEnabled = false;
			selectable = false;
			textColor = FONT_COLOR;
			blendMode = BlendMode.LAYER;
			autoSize = AUTO_SIZE;
			
			if ( width > 0 )
			{
				this.width = width;
			}
			if ( height > 0 )
			{
				this.height = height;
			}
			
			var format:TextFormat = new TextFormat();
			format.font = FONT;
			format.size = fontSize;
			format.align = TEXT_ALIGN;
			format.color = FONT_COLOR;
			defaultTextFormat = format;
			
			this.text = text;
		}
		
		public function addFilter( filter:BitmapFilter ):void
		{
			var filters:Array = new Array();
			
			for each ( var currentFilter:BitmapFilter in filters )
			{
				if ( getQualifiedClassName( currentFilter ) == getQualifiedClassName( filter ) )
				{
					return;
				}
				
				filters.push ( currentFilter );
			}
			
			filters.push( filter );			
			this.filters = filters;
		}
		
		public function removeFilter( filterType:Class ):void
		{
			var otherFilters:Array = new Array();
			
			for ( var i:int = 0; i < filters.length; i++ )
			{
				if ( getQualifiedClassName( filters[i] ) != getQualifiedClassName( filterType ) )
				{
					otherFilters.push ( filters[ i ] );
				}
			}
			
			filters = otherFilters;
		}
		
		public function setTextAlpha( alpha:Number ):void
		{
			var matrix:Array = new Array();
			matrix = matrix.concat( [ 1, 0, 0, 0, 		0 ] );
			matrix = matrix.concat( [ 0, 1, 0, 0, 		0 ] );
			matrix = matrix.concat( [ 0, 0, 1, 0, 		0 ] );
			matrix = matrix.concat( [ 0, 0, 0, alpha, 	0 ] );
			
			var colorFilter:ColorMatrixFilter = new ColorMatrixFilter( matrix );
			addFilter( colorFilter );
		}
	}
}