package laccaria.gui.components.button 
{
	import flash.display.DisplayObject;
	import flash.ui.Keyboard;
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	
	public class RemoveObjectButton extends SelectableButton
	{
		private static const WIDTH		:Number = 45;
		private static const HEIGHT		:Number = 45;
		
		private static const HOT_KEY	:int	= Keyboard.E;
		
		[Embed(source="../../../../../bin/icons/Remove.png")]
		private var RemoveObjectIcon:Class;
		
		public function RemoveObjectButton() 
		{
			super( WIDTH, HEIGHT, HOT_KEY );
			init();
		}
		
		private function init():void
		{
			var icon:DisplayObject = new RemoveObjectIcon();
			addIcon( icon );
		}
		
		protected override function buttonPressed():void
		{
			super.buttonPressed();
			
			var buttonEvent:ButtonEvent = new ButtonEvent( ButtonEvent.SELECT_REMOVE_OBJECT, this );
			EventManager.DispatchGlobal( buttonEvent );
		}
	}

}