package laccaria.gui.components.button 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class Button extends Sprite
	{
		//------Constants
		
		protected static const BACKGROUND_COLOR_NORMAL		:uint 	= 0xC8C8C8;
		protected static const BACKGROUND_COLOR_ROLLOVER	:uint 	= 0xF2F2F2;
		protected static const BACKGROUND_COLOR_MOUSE_DOWN	:uint 	= 0x959595;
		
		protected static const LINE_THICKNESS				:Number = 2;
		protected static const LINE_COLOR_NORMAL			:uint 	= 0x2A2A2A;
		
		//------Variables
		
		protected var _icon:DisplayObject;
		
		protected var _curBackgroundColor:uint;
		protected var _curLineColor:uint;
		
		protected var _isMouseDown:Boolean;
		
		protected var _backgroundWidth:Number;
		protected var _backgroundHeight:Number;
		
		//------Construction
		
		public function Button( width:Number, height:Number )
		{
			init( width, height );
		}
		
		private function init( width:Number, height:Number ):void
		{
			initVars( width, height );
			drawBackground();
			addEventListeners();
		}
		
		private function initVars( width:Number, height:Number ):void
		{
			_curBackgroundColor = BACKGROUND_COLOR_NORMAL;
			_curLineColor = LINE_COLOR_NORMAL;
			_isMouseDown = false;
			_backgroundWidth = width;
			_backgroundHeight = height;
		}
		
		//------Event Handlers
		
		private function rollOverHandler( e:MouseEvent ):void
		{
			if ( ! _isMouseDown )
			{
				_curBackgroundColor = BACKGROUND_COLOR_ROLLOVER;
				drawBackground();
			}
		}
		
		private function mouseDownHandler( e:MouseEvent ):void
		{
			_isMouseDown = true;
			_curBackgroundColor = BACKGROUND_COLOR_MOUSE_DOWN;
			drawBackground();
			
			if ( stage )
			{
				stage.addEventListener( MouseEvent.MOUSE_UP, mouseUpStageHandler );
			}
		}
		
		private function mouseUpHandler( e:MouseEvent ):void
		{
			if ( _isMouseDown )
			{
				if ( stage )
				{
					stage.removeEventListener( MouseEvent.MOUSE_UP, mouseUpStageHandler );
				}
				
				_isMouseDown = false;
				_curBackgroundColor = BACKGROUND_COLOR_NORMAL;
				drawBackground();
				
				buttonPressed();
			}
		}
		
		private function mouseUpStageHandler( e:MouseEvent ):void
		{
			if ( stage )
			{
				stage.removeEventListener( MouseEvent.MOUSE_UP, mouseUpStageHandler );
			}
			
			_isMouseDown = false;
			_curBackgroundColor = BACKGROUND_COLOR_NORMAL;
			drawBackground();
		}
		
		private function rollOutHandler( e:MouseEvent ):void
		{
			if ( ! _isMouseDown )
			{
				_curBackgroundColor = BACKGROUND_COLOR_NORMAL;
				drawBackground();
			}
		}
		
		//------Methods
		
		//Meant to be overriden		
		protected function buttonPressed():void
		{
			
		}
		
		public function addIcon( icon:DisplayObject ):void
		{
			if ( _icon && contains( _icon ) )
			{
				removeChild( _icon );
			}
			
			_icon = icon;
			_icon.x = .5 * ( width - _icon.width );
			_icon.y = .5 * ( height - _icon.height );
			addChild( _icon );
		}
		
		protected function addEventListeners():void
		{
			addEventListener( MouseEvent.ROLL_OVER, rollOverHandler );
			addEventListener( MouseEvent.MOUSE_DOWN, mouseDownHandler );
			addEventListener( MouseEvent.MOUSE_UP, mouseUpHandler );
			addEventListener( MouseEvent.ROLL_OUT, rollOutHandler );
		}
		
		protected function removeEventListeners():void
		{
			removeEventListener( MouseEvent.ROLL_OVER, rollOverHandler );
			removeEventListener( MouseEvent.MOUSE_DOWN, mouseDownHandler );
			removeEventListener( MouseEvent.MOUSE_UP, mouseUpHandler );
			removeEventListener( MouseEvent.ROLL_OUT, rollOutHandler );
			
			if ( stage )
			{
				stage.removeEventListener( MouseEvent.MOUSE_UP, mouseUpStageHandler );
			}
		}
		
		protected function drawBackground():void
		{
			with ( graphics )
			{
				clear();
				lineStyle( LINE_THICKNESS, _curLineColor );
				beginFill( _curBackgroundColor );
				drawRect( 0, 0, _backgroundWidth, _backgroundHeight );
				endFill();
			}
		}
	}
}