package laccaria.gui.components.button 
{
	import flash.display.DisplayObject;
	import flash.ui.Keyboard;
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	
	public class MoveObjectButton extends SelectableButton
	{
		private static const WIDTH:Number = 45;
		private static const HEIGHT:Number = 45;
		
		private static const HOT_KEY	:int	= Keyboard.R;
		
		[Embed(source="../../../../../bin/icons/Move.png")]
		private var MoveObjectIcon:Class;
		
		public function MoveObjectButton()
		{
			super( WIDTH, HEIGHT, HOT_KEY );
			init();
		}
		
		private function init():void
		{
			var icon:DisplayObject = new MoveObjectIcon();
			addIcon(icon);
		}
		
		protected override function buttonPressed():void
		{
			super.buttonPressed();
			
			var buttonEvent:ButtonEvent = new ButtonEvent( ButtonEvent.SELECT_MOVE_OBJECT, this );
			EventManager.DispatchGlobal( buttonEvent );
		}
	}

}