package laccaria.gui.components.button 
{
	import flash.display.DisplayObject;
	import flash.ui.Keyboard;
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	
	public class AddObjectButton extends SelectableButton
	{
		private static const WIDTH		:Number = 45;
		private static const HEIGHT		:Number = 45;
		
		private static const HOT_KEY	:int	= Keyboard.W;
		
		[Embed(source="../../../../../bin/icons/Add.png")]
		private var AddObjectIcon:Class;
		
		public function AddObjectButton() 
		{
			super( WIDTH, HEIGHT, HOT_KEY );
			init();
		}
		
		private function init():void
		{
			var icon:DisplayObject = new AddObjectIcon();
			addIcon( icon );
		}
		
		protected override function buttonPressed():void
		{
			super.buttonPressed();
			
			var buttonEvent:ButtonEvent = new ButtonEvent( ButtonEvent.SELECT_ADD_OBJECT, this );
			EventManager.DispatchGlobal( buttonEvent );
		}
	}
}