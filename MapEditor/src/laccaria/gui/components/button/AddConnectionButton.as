package laccaria.gui.components.button 
{
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.ui.Keyboard;
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	
	public class AddConnectionButton extends SelectableButton
	{
		private static const WIDTH		:Number = 45;
		private static const HEIGHT		:Number = 45;
		
		private static const HOT_KEY	:int	= Keyboard.Q;
		
		[Embed(source = "../../../../../bin/icons/AddConnection.png")]
		private var AddConnectionIcon:Class;
		
		public function AddConnectionButton() 
		{
			super( WIDTH, HEIGHT, HOT_KEY );
			init();
		}
		
		private function init():void
		{
			var icon:DisplayObject = new AddConnectionIcon();
			addIcon( icon );
		}
		
		protected override function buttonPressed():void
		{
			super.buttonPressed();
			
			var buttonEvent:ButtonEvent = new ButtonEvent( ButtonEvent.SELECT_ADD_CONNECTION, this );
			EventManager.DispatchGlobal( buttonEvent );
		}
	}
}