package laccaria.gui.components.button
{
	import flash.display.DisplayObject;
	import flash.ui.Keyboard;
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	
	public class ExitGridspaceButton extends SelectableButton
	{
		private static const WIDTH:Number = 45;
		private static const HEIGHT:Number = 45;
		
		private static const HOT_KEY	:int	= Keyboard.Q;
		
		[Embed(source="../../../../../bin/icons/ExitGridspace.png")]
		private var ExitGridspaceIcon:Class;
		
		public function ExitGridspaceButton()
		{
			super( WIDTH, HEIGHT, HOT_KEY );
			init();
		}
		
		private function init():void
		{
			var icon:DisplayObject = new ExitGridspaceIcon();
			addIcon(icon);
		}
		
		protected override function buttonPressed():void
		{
			super.buttonPressed();
			
			var buttonEvent:ButtonEvent = new ButtonEvent( ButtonEvent.SELECT_EXIT_GRIDSPACE, this );
			EventManager.DispatchGlobal( buttonEvent );
		}
	}
}