package laccaria.gui.components.button 
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	
	/**
	 *Button that work like radio buttons - one can be selected in a group
	 */
	public class SelectableButton extends Button
	{
		//------Constants
		
		private static const LINE_COLOR_SELECTED			:uint 	= 0xF4F4F4;
		
		//------Variables
		
		private var _hotKey:int;
		
		//------Construction
		
		public function SelectableButton( width:Number, height:Number, hotKey:int=-1 )
		{
			super( width, height );
			
			init( hotKey );
		}
		
		private function init( hotKey:int ):void
		{
			initVars( hotKey );
			addStageEventListeners();
		}
		
		private function initVars( hotKey:int ):void
		{
			_hotKey = hotKey;
		}
		
		private function addStageEventListeners():void
		{
			addEventListener( Event.ADDED_TO_STAGE, 	addedToStageHandler 	);
			addEventListener( Event.REMOVED_FROM_STAGE, removedFromStageHandler );
		}
		
		//------Event Handlers
		
		private function addedToStageHandler( e:Event ):void
		{
			if ( _hotKey != -1 )
			{
				stage.addEventListener( KeyboardEvent.KEY_DOWN, keyPressedHandler );
			}
		}
		
		private function removedFromStageHandler( e:Event ):void
		{
			stage.removeEventListener( KeyboardEvent.KEY_DOWN, keyPressedHandler );
		}
		
		private function keyPressedHandler( e:KeyboardEvent ):void
		{
			if ( e.keyCode == _hotKey )
			{
				//buttonPressed();
			}
		}
		
		//------Methods
		
		public function select():void
		{
			removeEventListeners();
			_curLineColor = LINE_COLOR_SELECTED;
			_curBackgroundColor = BACKGROUND_COLOR_NORMAL;
			drawBackground();
		}
		
		public function unselect():void
		{
			addEventListeners();
			_curLineColor = LINE_COLOR_NORMAL;
			drawBackground();
		}
		
		protected override function buttonPressed():void
		{
			//Send a button select event
			var buttonSelectEvent:ButtonEvent = new ButtonEvent( ButtonEvent.SELECT, this );
			EventManager.DispatchGlobal( buttonSelectEvent );
		}
	}
}