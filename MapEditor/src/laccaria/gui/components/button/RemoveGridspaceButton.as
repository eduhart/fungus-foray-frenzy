package laccaria.gui.components.button 
{
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.ui.Keyboard;
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	
	public class RemoveGridspaceButton extends SelectableButton
	{
		private static const WIDTH		:Number = 45;
		private static const HEIGHT		:Number = 45;
		
		private static const HOT_KEY	:int	= Keyboard.E;
		
		[Embed(source = "../../../../../bin/icons/RemoveGridspace.png")]
		private var RemoveGridspaceIcon:Class;
		
		public function RemoveGridspaceButton() 
		{
			super( WIDTH, HEIGHT, HOT_KEY );
			init();
		}
		
		private function init():void
		{
			var icon:DisplayObject = new RemoveGridspaceIcon();
			addIcon( icon );
		}
		
		protected override function buttonPressed():void
		{
			super.buttonPressed();
			
			var buttonEvent:ButtonEvent = new ButtonEvent( ButtonEvent.SELECT_REMOVE_GRIDSPACE, this );
			EventManager.DispatchGlobal( buttonEvent );
		}
	}
}