package laccaria.gui 
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	import laccaria.events.LayerEvent;
	import laccaria.gui.scope.biome.layer.BiomeLayer;
	import laccaria.gui.scope.Scope;
	import laccaria.gui.toolbar.ActionToolbar;
	import laccaria.gui.toolbar.ButtonToolbar;
	import laccaria.gui.toolbar.layer.LayerToolbar;
	import laccaria.gui.toolbar.MetaToolbar;
	import laccaria.model.gridspace.Gridspace;
	import laccaria.model.gridspace.gridspace_object.GridspaceObjectManager;
	
	public class GUI extends Sprite
	{
		//------Constants
		
		private static const MARGIN_LEFT					:Number = 10;
		private static const MARGIN_TOP						:Number = 10;
		
		private static const META_TOOLBAR_MARGIN_BOTTOM		:Number = 10;
		private static const BUTTON_TOOLBAR_MARGIN_RIGHT	:Number = 10;	
		private static const MAIN_WINDOW_MARGIN_RIGHT		:Number = 10;
		private static const MAIN_WINDOW_MARGIN_BOTTOM		:Number = 10;
		
		private static const START_SCOPE					:int = Scope.BIOME;
		private static const START_LAYER					:int = BiomeLayer.BACKGROUND;
		
		//------Variables
		
		private var _scope				:int;
		private var _layer				:int;
		private var _lastBiomeLayer		:int;
		
		private var _childContainer		:Sprite;
		
		private var _mainWindow			:MainWindow;
		
		private var _metaToolbar		:MetaToolbar;
		private var _buttonToolbar		:ButtonToolbar;
		private var _layerToolbar		:LayerToolbar;
		private var _actionToolbar		:ActionToolbar;
		
		//------Getters/Setters
		
		public function get mainWindow()	:MainWindow 	{ return _mainWindow; 		}
		public function get scope()			:int			{ return _scope;			}
		public function get layer()			:int			{ return _layer;			}
		
		public function get metaToolbar()	:MetaToolbar	{ return _metaToolbar;		}
		public function get buttonToolbar()	:ButtonToolbar	{ return _buttonToolbar;	}
		public function get layerToolbar()	:LayerToolbar	{ return _layerToolbar;		}
		public function get actionToolbar()	:ActionToolbar	{ return _actionToolbar;	}
		
		//------Construction
		
		public function GUI( gridspaceObjectManager:GridspaceObjectManager ) 
		{
			init( gridspaceObjectManager );
		}
		
		private function init( gridspaceObjectManager:GridspaceObjectManager ):void
		{
			initVars();
			createDisplay( gridspaceObjectManager );
			addEventListeners();
			
			setBiomeScope();	
			setLayer( START_LAYER );
		}
		
		private function initVars():void
		{
			_lastBiomeLayer 	= -1;
		}
		
		private function createDisplay( gridspaceObjectManager:GridspaceObjectManager ):void
		{
			createChildContainer();
			createMetaToolbar();
			createButtonToolbar();
			createMainWindow();
			createLayerToolbar( gridspaceObjectManager );
			createActionToolbar();
		}
		
		private function createChildContainer():void
		{
			_childContainer = new Sprite();
			_childContainer.x = MARGIN_LEFT;
			_childContainer.y = MARGIN_TOP;
			addChild( _childContainer );
		}
		
		private function createMetaToolbar():void
		{
			_metaToolbar = new MetaToolbar();
			_metaToolbar.x = ButtonToolbar.WIDTH + BUTTON_TOOLBAR_MARGIN_RIGHT + 2;
			_childContainer.addChild( _metaToolbar );
		}
		
		private function createButtonToolbar():void
		{
			_buttonToolbar = new ButtonToolbar();
			_buttonToolbar.y = _metaToolbar.y + _metaToolbar.height + META_TOOLBAR_MARGIN_BOTTOM;
			_childContainer.addChild( _buttonToolbar );
		}
		
		private function createMainWindow():void
		{
			_mainWindow = new MainWindow();
			_mainWindow.x = _buttonToolbar.x + _buttonToolbar.width + BUTTON_TOOLBAR_MARGIN_RIGHT;
			_mainWindow.y = _buttonToolbar.y;
			_childContainer.addChild( _mainWindow );
		}
		
		private function createLayerToolbar( gridspaceObjectManager:GridspaceObjectManager ):void
		{
			_layerToolbar = new LayerToolbar( gridspaceObjectManager );
			_layerToolbar.x = _mainWindow.x + _mainWindow.width + MAIN_WINDOW_MARGIN_RIGHT;
			_layerToolbar.y = _buttonToolbar.y;
			_childContainer.addChild( _layerToolbar );
		}
		
		private function createActionToolbar():void
		{
			_actionToolbar = new ActionToolbar();
			_actionToolbar.x = _mainWindow.x;
			_actionToolbar.y = _mainWindow.y + _mainWindow.height + MAIN_WINDOW_MARGIN_BOTTOM;
			_childContainer.addChild( _actionToolbar );
		}
		
		private function addEventListeners():void
		{
			EventManager.AddGlobalEventListener( ButtonEvent	.SELECT_EXIT_GRIDSPACE, exitGridspaceHandler );
			EventManager.AddGlobalEventListener( LayerEvent		.CHANGE, 				layerChangeHandler );
		}
		
		//------Event Handlers
		
		private function exitGridspaceHandler( e:ButtonEvent ):void
		{
			setBiomeScope();
		}
		
		private function layerChangeHandler( e:LayerEvent ):void
		{
			setLayer( e.layer );
		}
		
		//------Methods
		
		public function setBiomeScope():void
		{
			_scope = Scope.BIOME;
			
			_metaToolbar	.setScope( _scope );
			_buttonToolbar	.setScope( _scope );
			_mainWindow		.setBiomeScope();
			_layerToolbar	.setScope( _scope );
			_actionToolbar	.setScope( _scope );
			
			if ( _lastBiomeLayer != -1 )
			{
				_metaToolbar	.setLayer( _lastBiomeLayer );
				_buttonToolbar	.setLayer( _lastBiomeLayer );
				_mainWindow		.setLayer( _lastBiomeLayer );
				_layerToolbar	.setLayer( _lastBiomeLayer );
				_actionToolbar	.setLayer( _lastBiomeLayer );
			}
		}
		
		public function setGridspaceScope( indexGridspace:int, gridspaceCoordinates:Point, gridspaceBackground:Bitmap,
			gridspaceObjects:Array ):void
		{
			_scope = Scope.GRIDSPACE;
			
			_metaToolbar	.setScope( _scope );
			_buttonToolbar	.setScope( _scope );
			_mainWindow		.setGridspaceScope( indexGridspace, gridspaceBackground, gridspaceObjects );
			_layerToolbar	.setScope( _scope );
			_actionToolbar	.setScope( _scope );			
			_metaToolbar	.setGridspaceProperties( indexGridspace, gridspaceCoordinates );
		}
		
		public function setLayer( layer:int ):void
		{
			_layer = layer;
			
			if ( _scope == Scope.BIOME )
			{
				_lastBiomeLayer = layer;
			}
			
			_metaToolbar	.setLayer( layer );
			_buttonToolbar	.setLayer( layer );
			_mainWindow		.setLayer( layer );
			_layerToolbar	.setLayer( layer );
			_actionToolbar	.setLayer( layer );
		}
	}
}