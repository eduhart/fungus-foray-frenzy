package laccaria.gui.toolbar 
{
	import laccaria.events.ButtonEvent;
	import laccaria.events.EventManager;
	import laccaria.gui.components.button.AddConnectionButton;
	import laccaria.gui.components.button.AddGridspaceButton;
	import laccaria.gui.components.button.AddObjectButton;
	import laccaria.gui.components.button.EnterGridspaceButton;
	import laccaria.gui.components.button.ExitGridspaceButton;
	import laccaria.gui.components.button.MoveObjectButton;
	import laccaria.gui.components.button.RemoveConnectionButton;
	import laccaria.gui.components.button.RemoveGridspaceButton;
	import laccaria.gui.components.button.RemoveObjectButton;
	import laccaria.gui.components.button.SelectableButton;
	import laccaria.gui.scope.biome.layer.BiomeLayer;
	import laccaria.gui.scope.Layer;
	import laccaria.gui.scope.Scope;
	
	public class ButtonToolbar extends Toolbar
	{
		//------Constants
		
		public  static const WIDTH						:Number = 60;
		private static const HEIGHT						:Number = 450;
		
		private static const COMPONENT_MARGIN_BOTTOM	:Number = 5;
		
		//------Variables
		
		private var _selectedButton:SelectableButton;
		
		//------Construction
		
		public function ButtonToolbar() 
		{
			super( WIDTH, HEIGHT );
			init();
		}
		
		private function init():void
		{
			addEventListeners();
		}
		
		private function addEventListeners():void
		{
			EventManager.AddGlobalEventListener( ButtonEvent.SELECT, buttonSelectHandler );
		}
		
		protected override function createComponentSets():void
		{
			createGridspaceSet();
			createConnectionSet();
			
			createGridspaceScopeSet();
		}
		
		private function createGridspaceSet():void
		{
			var enterGridspaceButton	:EnterGridspaceButton 	= new EnterGridspaceButton();
			var addGridspaceButton		:AddGridspaceButton 	= new AddGridspaceButton();
			var removeGridspaceButton	:RemoveGridspaceButton 	= new RemoveGridspaceButton();
			
			var components:Array = [ enterGridspaceButton, addGridspaceButton, removeGridspaceButton ];
			var componentSet:ComponentSet = new ComponentSet( Scope.BIOME, BiomeLayer.GRIDSPACE, COMPONENT_MARGIN_BOTTOM, components );
			_componentSets.push( componentSet );
		}
		
		private function createConnectionSet():void
		{
			var addConnectionButton		:AddConnectionButton 	= new AddConnectionButton();
			var removeConnectionButton	:RemoveConnectionButton = new RemoveConnectionButton();
			
			var components:Array = [ addConnectionButton, removeConnectionButton ];
			var componentSet:ComponentSet = new ComponentSet( Scope.BIOME, BiomeLayer.CONNECTION, COMPONENT_MARGIN_BOTTOM, components );
			_componentSets.push( componentSet );
		}
		
		private function createGridspaceScopeSet():void
		{
			//For Gridspace scope background layer
			var exitGridspaceButton	:ExitGridspaceButton	= new ExitGridspaceButton();
			var moveObjectButton	:MoveObjectButton		= new MoveObjectButton();
			var addObjectButton		:AddObjectButton		= new AddObjectButton();
			var removeObjectButton	:RemoveObjectButton		= new RemoveObjectButton();
			
			var components:Array = [ exitGridspaceButton, addObjectButton, removeObjectButton, moveObjectButton ];
			var componentSet:ComponentSet = new ComponentSet( Scope.GRIDSPACE, Layer.ALL, COMPONENT_MARGIN_BOTTOM, components );
			_componentSets.push( componentSet );
		}
		
		//------Event Handlers
		
		private function buttonSelectHandler( e:ButtonEvent ):void
		{
			if ( _selectedButton )
			{
				_selectedButton.unselect();
			}
			
			_selectedButton = e.button;
			_selectedButton.select();
		}
		
		//------Methods
		
		public override function setScope( scope:int ):void
		{
			super.setScope( scope );
			
			if ( _selectedButton )
			{
				_selectedButton.unselect();
			}
		}
		
		public override function setLayer( layer:int ):void
		{
			super.setLayer( layer );
			
			if ( _selectedButton )
			{
				_selectedButton.unselect();
			}
		}
	}
}