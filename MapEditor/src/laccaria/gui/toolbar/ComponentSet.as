package laccaria.gui.toolbar 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import laccaria.gui.scope.Layer;
	
	public class ComponentSet extends Sprite
	{
		private var _scope:int;
		private var _layer:int;
		private var _components:Array;
		
		public function get scope()			:int 	{ return _scope; 		}
		public function get layer()			:int	{ return _layer;		}
		public function get components()	:Array	{ return _components;	}
		
		public function ComponentSet( scope:int, layer:int, componentMarginBottom:Number, components:Array ) 
		{
			init( scope, layer, componentMarginBottom, components );
		}
		
		private function init( scope:int, layer:int, componentMarginBottom:Number, components:Array ):void
		{
			initVars( scope, layer, components );
			addComponents( componentMarginBottom );
		}
		
		private function initVars( scope:int, layer:int, components:Array ):void
		{
			_scope = scope;
			_layer = layer;
			_components = components;
		}
		
		private function addComponents( componentMarginBottom:Number ):void
		{
			var curY:Number = 0;
			
			for ( var i:int = 0; i < _components.length; i++ )
			{
				if ( ! _components[ i ] is DisplayObject )
				{
					continue;
				}
				
				var curComponent:DisplayObject = _components[ i ];
				curComponent.y = curY;
				addChild( curComponent );
				
				curY += curComponent.height + componentMarginBottom;
			}
		}
		
		public function applies( scope:int, layer:int ):Boolean
		{
			return _scope == scope && ( _layer == Layer.ALL || _layer == layer );
		}
	}
}