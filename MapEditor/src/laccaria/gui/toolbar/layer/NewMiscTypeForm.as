package laccaria.gui.toolbar.layer 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import laccaria.events.EventManager;
	import laccaria.events.NewMiscTypeFormEvent;
	import laccaria.gui.components.button.Button;
	import laccaria.gui.InputTextField;
	import laccaria.gui.SimpleTextField;
	import laccaria.io.ImageLoader;
	import laccaria.utility.MessageBox;
	
	/**
	 * The form that allows the user to insert a custom misc gridspace object type.
	 * The user needs to provide three things:
		 *The display name for the type
		 *The file name for the type
		 *The path to the icon for the type
	 */
	public class NewMiscTypeForm extends Sprite
	{
		//------Constants
		
		private static const WIDTH					:Number = 200;
		
		private static const MARGIN_LEFT			:Number = 3;
		private static const MARGIN_TOP				:Number = 3;
		private static const MARGIN_RIGHT			:Number = 6;
		private static const COMPONENT_MARGIN_RIGHT	:Number = 4;
		private static const COMPONENT_MARGIN_BOTTOM:Number = 4;
		
		private static const LABEL_WIDTH			:Number = 70;
		private static const INPUT_TEXT_WIDTH		:Number = 95;
		private static const COMPONENT_HEIGHT		:Number = 20;
		
		private static const B_ADD_NEW_OFFSET_X		:Number = 2;
		
		private static const FONT_SIZE				:Number = 12;
		
		private static const DISPLAY_NAME_TEXT		:String = "Display Name:";
		private static const FILE_NAME_TEXT			:String = "File Name:";
		private static const LOAD_ICON_TEXT			:String = "Load Icon";
		private static const ADD_NEW_TEXT			:String = "Add New";
		
		private static const ERROR_BOX_TITLE		:String = "You have done something wrong!";
		private static const NO_DISPLAY_NAME_MESSAGE:String = "You should indulge in a display name.";
		private static const NO_FILE_NAME_MESSAGE	:String = "You should indulge in a file name.";
		private static const NO_ICON_MESSAGE		:String = "You should indulge in an icon";
		private static const SUCCESS_MESSAGE		:String = "You have succeeded!";
		
		//------Variables
		
		private var _lDisplayName	:SimpleTextField;
		private var _tDisplayName	:InputTextField;
		
		private var _lFileName		:SimpleTextField;
		private var _tFileName		:InputTextField;
		
		private var _lLoadIcon		:SimpleTextField;
		private var _bLoadIcon		:Button;
		private var _tIconPath		:SimpleTextField;
		
		private var _lAddNew		:SimpleTextField;
		private var _bAddNew		:Button;
		
		private var _messageBox		:MessageBox;
		
		private var _imageLoader	:ImageLoader;
		private var _iconPath		:String;
		
		//------Getters/Setters
		
		public function get displayName()	:String { return _tDisplayName.text; 	}
		public function get fileName()		:String { return _tFileName.text;		}
		public function get iconPath()		:String { return _iconPath;				}
		
		//------Construction
		
		public function NewMiscTypeForm() 
		{
			init();
		}
		
		private function init():void
		{
			initVars();
			createComponents();
			addEventListeners();
		}
		
		private function initVars():void
		{
			_imageLoader 	= new ImageLoader();
			_iconPath 		= "";
		}
		
		private function createComponents():void
		{
			var displayNameRow	:Sprite = createDisplayNameRow();
			var fileNameRow		:Sprite = createFileNameRow();
			var loadIconRow		:Sprite = createLoadIconRow();
			var addNewRow		:Sprite = createAddNewRow();
			
			displayNameRow	.y = MARGIN_TOP;
			fileNameRow		.y = displayNameRow.y 	+ displayNameRow.height + COMPONENT_MARGIN_BOTTOM;
			loadIconRow		.y = fileNameRow.y 		+ fileNameRow.height 	+ COMPONENT_MARGIN_BOTTOM;
			addNewRow		.y = loadIconRow.y 		+ loadIconRow.height 	+ COMPONENT_MARGIN_BOTTOM;
			
			addChild( displayNameRow );
			addChild( fileNameRow	);
			addChild( loadIconRow	);
			addChild( addNewRow		);
		}
		
		private function createDisplayNameRow():Sprite
		{
			_lDisplayName = new SimpleTextField( DISPLAY_NAME_TEXT, FONT_SIZE, LABEL_WIDTH, COMPONENT_HEIGHT );
			_tDisplayName = new InputTextField( INPUT_TEXT_WIDTH, COMPONENT_HEIGHT );
			
			_lDisplayName.x = MARGIN_LEFT;
			_tDisplayName.x = WIDTH - _tDisplayName.width - MARGIN_RIGHT;
			
			var row:Sprite = new Sprite();
			row.addChild( _lDisplayName );
			row.addChild( _tDisplayName );
			
			return row;
		}
		
		private function createFileNameRow():Sprite
		{
			_lFileName = new SimpleTextField( FILE_NAME_TEXT, FONT_SIZE, LABEL_WIDTH, COMPONENT_HEIGHT );
			_tFileName = new InputTextField( INPUT_TEXT_WIDTH, COMPONENT_HEIGHT );
			
			_lFileName.x = MARGIN_LEFT;
			_tFileName.x = WIDTH - _tFileName.width - MARGIN_RIGHT;
			
			var row:Sprite = new Sprite();
			row.addChild( _lFileName );
			row.addChild( _tFileName );
			
			return row;
		}
		
		private function createLoadIconRow():Sprite
		{
			_lLoadIcon = new SimpleTextField( LOAD_ICON_TEXT, FONT_SIZE );
			_bLoadIcon = new Button( LABEL_WIDTH, COMPONENT_HEIGHT );
			_bLoadIcon.addIcon( _lLoadIcon );
			
			_tIconPath = new SimpleTextField( "", FONT_SIZE, INPUT_TEXT_WIDTH, COMPONENT_HEIGHT );
			
			_bLoadIcon.x = MARGIN_LEFT;
			_tIconPath.x = WIDTH - _tIconPath.width - MARGIN_RIGHT;
			
			var row:Sprite = new Sprite();
			row.addChild( _bLoadIcon );
			row.addChild( _tIconPath );
			
			return row;
		}
		
		private function createAddNewRow():Sprite
		{
			_lAddNew = new SimpleTextField( ADD_NEW_TEXT, FONT_SIZE );
			_bAddNew = new Button( INPUT_TEXT_WIDTH, COMPONENT_HEIGHT );
			_bAddNew.addIcon( _lAddNew );
			
			_bAddNew.x = WIDTH - _bAddNew.width - MARGIN_RIGHT + B_ADD_NEW_OFFSET_X;
			
			var row:Sprite = new Sprite();
			row.addChild( _bAddNew );
			return row;
		}
		
		private function addEventListeners():void
		{
			_bLoadIcon	.addEventListener( MouseEvent.MOUSE_UP, loadIconSelectHandler 	);
			_bAddNew	.addEventListener( MouseEvent.MOUSE_UP, addNewSelectHandler 	);
		}
		
		//------Event Handlers
		
		private function loadIconSelectHandler( e:MouseEvent ):void
		{
			_imageLoader.loadImage( this, imageLoadedHandler );
		}
		
		private function imageLoadedHandler( icon:Bitmap ):void
		{
			_iconPath = _imageLoader.imagePath;
		}
		
		private function addNewSelectHandler( e:Event ):void
		{
			var displayName	:String = _tDisplayName.text;
			var fileName	:String = _tFileName.text;
			
			if ( displayName == "" )
			{
				showMessageBox( NO_DISPLAY_NAME_MESSAGE );
			}
			else if ( fileName == "" )
			{
				showMessageBox( NO_FILE_NAME_MESSAGE );
			}
			else if ( _iconPath == "" )
			{
				showMessageBox( NO_ICON_MESSAGE );
			}
			else
			{
				var event:NewMiscTypeFormEvent = new NewMiscTypeFormEvent( NewMiscTypeFormEvent.ADD_NEW_TYPE,
					displayName, fileName, iconPath );
				EventManager.DispatchGlobal( event );
				
				showMessageBox( SUCCESS_MESSAGE );
				
				_tDisplayName.text 	= "";
				_tFileName.text 	= "";
				_iconPath 			= "";
			}
		}
		
		//------Methods
		
		private function showMessageBox( message:String ):void
		{
			//If we already have a message box on the screen, ignore the user's annoyance
			if ( _messageBox && contains( _messageBox ) )
			{
				return;
			}
			
			if ( stage )
			{
				_messageBox = new MessageBox( ERROR_BOX_TITLE, message );
				_messageBox.x = .5 * ( stage.stageWidth - _messageBox.width );
				_messageBox.y = .5 * ( stage.stageHeight - _messageBox.height );
				stage.addChild( _messageBox );
			}
		}
	}
}