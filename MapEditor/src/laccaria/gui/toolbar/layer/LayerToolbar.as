package laccaria.gui.toolbar.layer 
{
	import com.bit101.components.ComboBox;
	import com.bit101.components.RadioButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import laccaria.events.EventManager;
	import laccaria.events.gridspace_object.GridspaceObjectTypeEvent;
	import laccaria.events.LayerEvent;
	import laccaria.events.NewMiscTypeFormEvent;
	import laccaria.gui.scope.biome.layer.BiomeLayer;
	import laccaria.gui.scope.gridspace.GridspaceLayer;
	import laccaria.gui.scope.Layer;
	import laccaria.gui.scope.Scope;
	import laccaria.gui.SimpleTextField;
	import laccaria.gui.toolbar.ComponentSet;
	import laccaria.gui.toolbar.Toolbar;
	import laccaria.model.gridspace.gridspace_object.category.GridspaceObjectCategory;
	import laccaria.model.gridspace.gridspace_object.GridspaceObjectManager;
	import laccaria.model.gridspace.gridspace_object.type.GridspaceObjectType;

	public class LayerToolbar extends Toolbar
	{
		//------Constants
		
		private static const MARGIN_LEFT			:Number = 3;
		private static const MARGIN_TOP				:Number = 3;
		private static const COMPONENT_MARGIN_RIGHT	:Number = 3;
		private static const COMPONENT_MARGIN_BOTTOM:Number = 3;
		
		private static const WIDTH					:Number = 200;
		private static const HEIGHT					:Number = 450;
		
		private static const NEW_MISC_FORM_Y		:Number = 180;
		
		private static const COMBO_BOX_WIDTH		:Number = 150;
		
		//------Variables
		
		private var _rbBackground				:RadioButton;
		private var _rbGridspace				:RadioButton;
		private var _rbConnection				:RadioButton;
		
		private var _rbGridspaceBackground		:RadioButton;		
		private var _rbGridspaceForeground		:RadioButton;
		
		private var _cbBackgroundCategories		:ComboBox;
		private var _cbBackgroundTypes			:ComboBox;
		private var _cbForegroundCategories		:ComboBox;
		private var _cbForegroundTypes			:ComboBox;
		
		private var _gridspaceObjectManager		:GridspaceObjectManager;
		private var _idSelectedCategory			:int;
		private var _idSelectedType				:int;
		
		private var _newMiscForm				:NewMiscTypeForm;
		
		//------Construction
		
		public function LayerToolbar( gridspaceObjectManager:GridspaceObjectManager ) 
		{
			super( WIDTH, HEIGHT );
			init( gridspaceObjectManager );
		}
		
		private function init( gridspaceObjectManager:GridspaceObjectManager ):void
		{
			initVars( gridspaceObjectManager );
			addEventListeners();
		}
		
		private function initVars( gridspaceObjectManager:GridspaceObjectManager ):void
		{
			_gridspaceObjectManager = gridspaceObjectManager;
			_newMiscForm = new NewMiscTypeForm();
			_newMiscForm.y = NEW_MISC_FORM_Y;
		}
		
		private function addEventListeners():void
		{
			//Biome scope radio buttons
			_rbBackground			.addEventListener( Event.SELECT, backgroundLayerSelectedHandler 		);
			_rbGridspace			.addEventListener( Event.SELECT, gridspaceLayerSelectedHandler 			);
			_rbConnection			.addEventListener( Event.SELECT, connectionLayerSelectedHandler 		);
			
			//Gridspace scope radio buttons
			_rbGridspaceBackground	.addEventListener( Event.SELECT, gridspaceBackgroundLayerSelectHandler 	);
			_rbGridspaceForeground	.addEventListener( Event.SELECT, gridspaceForegroundLayerSelectHandler 	);
			
			//New type form
			EventManager.AddGlobalEventListener( NewMiscTypeFormEvent.ADD_NEW_TYPE, addNewTypeHandler );
		}
		
		protected override function createComponentSets():void
		{
			createBiomeSet();
			createGridspaceSets();
		}
		
		private function createBiomeSet():void
		{
			var row1:Sprite = createBiomeSetFirstRow();
			var row2:Sprite = createBiomeSetSecondRow();
			var row3:Sprite = createBiomeSetThirdRow()
			
			var components:Array = [ row1, row2, row3 ];
			var componentSet:ComponentSet = new ComponentSet( Scope.BIOME, Layer.ALL, COMPONENT_MARGIN_BOTTOM, components );
			_componentSets.push( componentSet );
		}
		
		private function createBiomeSetFirstRow():Sprite
		{
			_rbBackground = new RadioButton( null, 0, 0, "", true );
			_rbBackground.groupName = "layer";			
			var rbBackgroundLabel:SimpleTextField = new SimpleTextField( "Background", 12 );
			
			var row1:Sprite = new Sprite();
			row1.addChild( _rbBackground );
			row1.addChild( rbBackgroundLabel );
			rbBackgroundLabel.x = _rbBackground.x + _rbBackground.width + COMPONENT_MARGIN_RIGHT;
			
			return row1;
		}
		
		private function createBiomeSetSecondRow():Sprite
		{
			_rbGridspace = new RadioButton();
			_rbGridspace.groupName = "layer";
			var rbGridspaceLabel:SimpleTextField = new SimpleTextField( "Gridspace", 12 );
			
			var row2:Sprite = new Sprite();
			row2.addChild( _rbGridspace );
			row2.addChild( rbGridspaceLabel );
			rbGridspaceLabel.x = _rbGridspace.x + _rbGridspace.width + COMPONENT_MARGIN_RIGHT;
			
			return row2;
		}
		
		private function createBiomeSetThirdRow():Sprite
		{
			_rbConnection = new RadioButton();
			_rbConnection.groupName = "layer";
			var rbConnectionLabel:SimpleTextField = new SimpleTextField( "Connection", 12 );
			
			var row3:Sprite = new Sprite();
			row3.addChild( _rbConnection );
			row3.addChild( rbConnectionLabel );
			rbConnectionLabel.x = _rbConnection.x + _rbConnection.width + COMPONENT_MARGIN_RIGHT;
			
			return row3;
		}
		
		private function createGridspaceSets():void
		{
			createGridspaceAllSet();			
			createGridspaceBackgroundSet();
			createGridspaceForegroundSet();
		}
		
		private function createGridspaceAllSet():void
		{
			var row1:Sprite = createGridspaceAllSetFirstRow();
			var row2:Sprite = createGridspaceAllSetSecondRow();
			
			var components:Array = [ row1, row2 ];
			var componentSet:ComponentSet = new ComponentSet( Scope.GRIDSPACE, Layer.ALL, COMPONENT_MARGIN_BOTTOM, components );
			_componentSets.push( componentSet );
		}
		
		private function createGridspaceAllSetFirstRow():Sprite
		{
			//Gridspace background radio button and label
			_rbGridspaceBackground = new RadioButton( null, 0, 0, "", true );
			_rbGridspaceBackground.groupName = "gridspaceLayer";			
			
			var _gridspaceBackgroundLabel:SimpleTextField = new SimpleTextField( "Background", 12 );			
			_gridspaceBackgroundLabel.x = _rbGridspaceBackground.x + _rbGridspaceBackground.width + COMPONENT_MARGIN_RIGHT;
			
			var row1:Sprite = new Sprite();
			row1.addChild( _rbGridspaceBackground );
			row1.addChild( _gridspaceBackgroundLabel );
			
			return row1;
		}
		
		private function createGridspaceAllSetSecondRow():Sprite
		{
			//Gridspace foreground radio button and label
			_rbGridspaceForeground = new RadioButton();
			_rbGridspaceForeground.groupName = "gridspaceLayer";
			
			var gridspaceForegroundLabel:SimpleTextField = new SimpleTextField( "Foreground", 12 );
			gridspaceForegroundLabel.x = _rbGridspaceForeground.x + _rbGridspaceForeground.width + COMPONENT_MARGIN_RIGHT;
			
			var row2:Sprite = new Sprite();
			row2.addChild( _rbGridspaceForeground );
			row2.addChild( gridspaceForegroundLabel );
			
			return row2;
		}
		
		private function createGridspaceBackgroundSet():void
		{
			var row1:Sprite = createGridspaceBackgroundSetFirstRow();
			var row2:Sprite = createGridspaceBackgroundSetSecondRow();
			
			var components:Array = [ row1, row2 ];
			var componentSet:ComponentSet = new ComponentSet( Scope.GRIDSPACE, GridspaceLayer.BACKGROUND, COMPONENT_MARGIN_BOTTOM, components );
			_componentSets.push( componentSet );
		}
		
		private function createGridspaceBackgroundSetFirstRow():Sprite
		{
			var cbBackgroundCategoryLabel:SimpleTextField = new SimpleTextField( "Background Categories", 12 );			
			_cbBackgroundCategories = new ComboBox();
			_cbBackgroundCategories.setSize( COMBO_BOX_WIDTH, _cbBackgroundCategories.height );
			_cbBackgroundCategories.addEventListener( Event.SELECT, backgroundCategorySelectHandler );
			
			var row1:Sprite = new Sprite();
			row1.addChild( cbBackgroundCategoryLabel );
			row1.addChild( _cbBackgroundCategories );
			_cbBackgroundCategories.y = cbBackgroundCategoryLabel.y + cbBackgroundCategoryLabel.height + COMPONENT_MARGIN_BOTTOM;
			
			return row1;
		}
		
		private function createGridspaceBackgroundSetSecondRow():Sprite
		{
			var cbBackgroundTypeLabel:SimpleTextField = new SimpleTextField( "Background Types", 12 );			
			_cbBackgroundTypes = new ComboBox();
			_cbBackgroundTypes.setSize( COMBO_BOX_WIDTH, _cbBackgroundTypes.height );
			_cbBackgroundTypes.addEventListener( Event.SELECT, backgroundTypeSelectHandler );
			
			var row2:Sprite = new Sprite();
			row2.addChild( cbBackgroundTypeLabel );
			row2.addChild( _cbBackgroundTypes );
			_cbBackgroundTypes.y = cbBackgroundTypeLabel.y + cbBackgroundTypeLabel.height + COMPONENT_MARGIN_BOTTOM;
			
			return row2;
		}
		
		public function createGridspaceForegroundSet():void
		{
			var row1:Sprite = createGridspaceForegroundSetFirstRow();
			var row2:Sprite = createGridspaceForegroundSetSecondRow();
			
			var components:Array = [ row1, row2 ];
			var componentSet:ComponentSet = new ComponentSet( Scope.GRIDSPACE, GridspaceLayer.FOREGROUND, COMPONENT_MARGIN_BOTTOM, components );
			_componentSets.push( componentSet );
		}
		
		private function createGridspaceForegroundSetFirstRow():Sprite
		{
			var cbForegroundCategoryLabel:SimpleTextField = new SimpleTextField( "Foreground Categories", 12 );			
			_cbForegroundCategories = new ComboBox();
			_cbForegroundCategories.setSize( COMBO_BOX_WIDTH, _cbForegroundCategories.height );
			_cbForegroundCategories.addEventListener( Event.SELECT, foregroundCategorySelectHandler );
			
			var row1:Sprite = new Sprite();
			row1.addChild( cbForegroundCategoryLabel );
			row1.addChild( _cbForegroundCategories );
			_cbForegroundCategories.y = cbForegroundCategoryLabel.y + cbForegroundCategoryLabel.height + COMPONENT_MARGIN_BOTTOM;
			
			return row1;
		}
		
		private function createGridspaceForegroundSetSecondRow():Sprite
		{
			var cbForegroundLabel:SimpleTextField = new SimpleTextField( "Foreground Type", 12 );			
			_cbForegroundTypes = new ComboBox();
			_cbForegroundTypes.setSize( COMBO_BOX_WIDTH, _cbForegroundTypes.height );
			_cbForegroundTypes.addEventListener( Event.SELECT, foregroundTypeSelectHandler );
			
			var row2:Sprite = new Sprite();
			row2.addChild( cbForegroundLabel );
			row2.addChild( _cbForegroundTypes );
			_cbForegroundTypes.y = cbForegroundLabel.y + cbForegroundLabel.height + COMPONENT_MARGIN_BOTTOM;
			
			return row2;
		}
		
		//------Event Handlers
		
		private function backgroundLayerSelectedHandler( e:Event ):void
		{
			var layerEvent:LayerEvent = new LayerEvent( LayerEvent.CHANGE, BiomeLayer.BACKGROUND );
			EventManager.DispatchGlobal( layerEvent );
		}
		
		private function gridspaceLayerSelectedHandler( e:Event ):void
		{
			var layerEvent:LayerEvent = new LayerEvent( LayerEvent.CHANGE, BiomeLayer.GRIDSPACE );
			EventManager.DispatchGlobal( layerEvent ); 
		}
		
		private function connectionLayerSelectedHandler( e:Event ):void
		{
			var layerEvent:LayerEvent = new LayerEvent( LayerEvent.CHANGE, BiomeLayer.CONNECTION );
			EventManager.DispatchGlobal( layerEvent );
		}
		
		private function gridspaceBackgroundLayerSelectHandler( e:Event ):void
		{
			var layerEvent:LayerEvent = new LayerEvent( LayerEvent.CHANGE, GridspaceLayer.BACKGROUND );
			EventManager.DispatchGlobal( layerEvent );
			
			var gridspaceObjectTypeEvent:GridspaceObjectTypeEvent = new GridspaceObjectTypeEvent( GridspaceObjectTypeEvent.UNSELECT_TYPE );
			EventManager.DispatchGlobal( gridspaceObjectTypeEvent );
		}
		
		private function gridspaceForegroundLayerSelectHandler( e:Event ):void
		{
			var layerEvent:LayerEvent = new LayerEvent( LayerEvent.CHANGE, GridspaceLayer.FOREGROUND );
			EventManager.DispatchGlobal( layerEvent );
			
			var gridspaceObjectTypeEvent:GridspaceObjectTypeEvent = new GridspaceObjectTypeEvent( GridspaceObjectTypeEvent.UNSELECT_TYPE );
			EventManager.DispatchGlobal( gridspaceObjectTypeEvent );
		}
		
		private function backgroundCategorySelectHandler( e:Event ):void
		{
			var selectedCategoryItem:GridspaceObjectListItem = _cbBackgroundCategories.selectedItem as GridspaceObjectListItem;
			
			if (  selectedCategoryItem && selectedCategoryItem.id != _idSelectedCategory )
			{
				//If the new category is the background misc category
				if ( selectedCategoryItem.id == GridspaceObjectCategory.BACKGROUND_MISC )
				{
					addChild( _newMiscForm );
				}
				//If the old category was the background misc category
				else if ( _idSelectedCategory == GridspaceObjectCategory.BACKGROUND_MISC )
				{
					removeChild( _newMiscForm );
				}
				
				_idSelectedCategory = selectedCategoryItem.id;
				setBackgroundTypes();
				
				var gridspaceObjectTypeEvent:GridspaceObjectTypeEvent = new GridspaceObjectTypeEvent( GridspaceObjectTypeEvent.UNSELECT_TYPE );
				EventManager.DispatchGlobal( gridspaceObjectTypeEvent );
			}			
		}
		
		private function foregroundCategorySelectHandler( e:Event ):void
		{
			var selectedCategoryItem:GridspaceObjectListItem = _cbForegroundCategories.selectedItem as GridspaceObjectListItem;
			
			if ( selectedCategoryItem && selectedCategoryItem.id != _idSelectedCategory )
			{
				//If the new category is the background misc category
				if ( selectedCategoryItem.id == GridspaceObjectCategory.FOREGROUND_MISC )
				{
					addChild( _newMiscForm );
				}
				//If the old category was the background misc category
				else if ( _idSelectedCategory == GridspaceObjectCategory.FOREGROUND_MISC )
				{
					removeChild( _newMiscForm );
				}
				
				_idSelectedCategory = selectedCategoryItem.id;
				setForegroundTypes();
				
				var gridspaceObjectTypeEvent:GridspaceObjectTypeEvent = new GridspaceObjectTypeEvent( GridspaceObjectTypeEvent.UNSELECT_TYPE );
				EventManager.DispatchGlobal( gridspaceObjectTypeEvent );
			}
		}
		
		private function backgroundTypeSelectHandler( e:Event ):void
		{
			var selectedTypeItem:GridspaceObjectListItem = _cbBackgroundTypes.selectedItem as GridspaceObjectListItem;
			if ( selectedTypeItem )
			{
				var idSelectedType:int = selectedTypeItem.id;
				selectType( idSelectedType );
			}
		}
		
		private function foregroundTypeSelectHandler( e:Event ):void
		{
			var selectedTypeItem:GridspaceObjectListItem = _cbForegroundTypes.selectedItem as GridspaceObjectListItem;
			if ( selectedTypeItem )
			{
				var idSelectedType:int = selectedTypeItem.id;
				selectType( idSelectedType );
			}
		}
		
		private function addNewTypeHandler( e:NewMiscTypeFormEvent ):void
		{
			var displayName	:String = e.displayName;
			var fileName	:String = e.fileName;
			var iconPath	:String = e.iconPath;
			var idType		:int	= _gridspaceObjectManager.getNextIdType();
			
			if ( _curLayer == GridspaceLayer.BACKGROUND )
			{
				//Add a new background misc type
				_gridspaceObjectManager.addBackgroundMiscType( idType, displayName, fileName, iconPath );
				
				//Add to combo box
				setBackgroundTypes();
			}
			else if ( _curLayer == GridspaceLayer.FOREGROUND )
			{
				//Add a new foreground misc type
				_gridspaceObjectManager.addForegroundMiscType( idType, displayName, fileName, iconPath );
				setForegroundTypes();
			}
		}
		
		//------Methods
		
		public override function setLayer( layer:int ):void
		{
			super.setLayer( layer );
			
			if ( _curScope == Scope.BIOME )
			{
				switch( layer )
				{					
					case BiomeLayer		.BACKGROUND:	_rbBackground.selected 			= true; break;
					case BiomeLayer		.GRIDSPACE:		_rbGridspace.selected 			= true;	break;
					case BiomeLayer		.CONNECTION:	_rbConnection.selected 			= true;	break;
				}
			}
			else if ( _curScope == Scope.GRIDSPACE )
			{
				_idSelectedCategory = -1;
				_idSelectedType = -1;
				
				switch( layer )
				{
					case GridspaceLayer	.BACKGROUND:
						_rbGridspaceBackground.selected = true;
						setBackgroundCategories();
						setBackgroundTypes();
						break;
					case GridspaceLayer	.FOREGROUND:
						_rbGridspaceForeground.selected = true;
						setForegroundCategories();
						setForegroundTypes();
						break;
				}
				
				if ( _newMiscForm && contains( _newMiscForm ) )
				{
					removeChild( _newMiscForm );
				}
			}
		}
		
		public override function setScope( scope:int ):void
		{
			super.setScope( scope );
			
			if ( _newMiscForm && contains( _newMiscForm ) )
			{
				removeChild( _newMiscForm );
			}
		}
		
		private function setBackgroundCategories():void
		{
			_cbBackgroundCategories.removeAll();
			
			var matchingCategories:Array = _gridspaceObjectManager.getAllCategoriesWithLayer( GridspaceObjectCategory.BACKGROUND_LAYER );
			
			for each ( var matchingCategory:GridspaceObjectCategory in matchingCategories )
			{
				var displayName:String = matchingCategory.displayName;
				var idCategory:int = matchingCategory.idCategory;
				var listItem:GridspaceObjectListItem = new GridspaceObjectListItem( idCategory, displayName );
				
				_cbBackgroundCategories.addItem( listItem );
			}
			
			_cbBackgroundCategories.selectedIndex = _idSelectedCategory;
		}
		
		private function setBackgroundTypes():void
		{
			_cbBackgroundTypes.removeAll();
			
			if ( _idSelectedCategory >= 0 )
			{				
				var matchingTypes:Array = _gridspaceObjectManager.getAllTypesWithCategory( _idSelectedCategory );
				
				for each ( var matchingType:GridspaceObjectType in matchingTypes )
				{
					var displayName:String = matchingType.displayName;
					var idType:int = matchingType.idType;
					var listItem:GridspaceObjectListItem = new GridspaceObjectListItem( idType, displayName );
					
					_cbBackgroundTypes.addItem( listItem );
				}
			}
			
			_cbBackgroundTypes.selectedIndex = -1;
		}
		
		private function setForegroundCategories():void
		{
			_cbForegroundCategories.removeAll();
			
			var matchingCategories:Array = _gridspaceObjectManager.getAllCategoriesWithLayer( GridspaceObjectCategory.FOREGROUND_LAYER );
			
			for each ( var matchingCategory:GridspaceObjectCategory in matchingCategories )
			{
				var displayName:String = matchingCategory.displayName;
				var idCategory:int = matchingCategory.idCategory;
				var listItem:GridspaceObjectListItem = new GridspaceObjectListItem( idCategory, displayName );
				
				_cbForegroundCategories.addItem( listItem );
			}
			
			_cbForegroundCategories.selectedIndex = _idSelectedCategory;
		}
		
		private function setForegroundTypes():void
		{
			_cbForegroundTypes.removeAll();
			
			if ( _idSelectedCategory >= 0 )
			{				
				var matchingTypes:Array = _gridspaceObjectManager.getAllTypesWithCategory( _idSelectedCategory );
				
				for each ( var matchingType:GridspaceObjectType in matchingTypes )
				{
					var displayName:String = matchingType.displayName;
					var idType:int = matchingType.idType;
					var listItem:GridspaceObjectListItem = new GridspaceObjectListItem( idType, displayName );
					
					_cbForegroundTypes.addItem( listItem );
				}
			}
			
			_cbForegroundTypes.selectedIndex = -1;
		}
		
		private function selectType( idType:int ):void
		{
			if ( idType != _idSelectedType )
			{
				_idSelectedType = idType;
				var selectedType:GridspaceObjectType = _gridspaceObjectManager.findType( _idSelectedType );
				
				var gridspaceObjectTypeEvent:GridspaceObjectTypeEvent = new GridspaceObjectTypeEvent( GridspaceObjectTypeEvent.SELECT_TYPE, selectedType );
				EventManager.DispatchGlobal( gridspaceObjectTypeEvent );
			}
		}
		
		protected override function positionComponentSets():void
		{
			for each ( var componentSet:ComponentSet in _componentSets )
			{
				componentSet.x = MARGIN_LEFT;
				componentSet.y = MARGIN_TOP;
			}
		}
	}
}