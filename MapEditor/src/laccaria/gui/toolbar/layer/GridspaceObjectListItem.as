package laccaria.gui.toolbar.layer 
{
	public class GridspaceObjectListItem 
	{
		private var _id		:int;
		private var _label	:String;
		
		public function get id()	:int	{ return _id;		}
		public function get label()	:String	{ return _label;	}
		
		public function GridspaceObjectListItem( id:int, label:String ) 
		{
			_id = id;
			_label = label;
		}		
	}
}