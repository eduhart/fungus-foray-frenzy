package laccaria.gui.toolbar 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import laccaria.events.BiomeEvent;
	import laccaria.events.EventManager;
	import laccaria.gui.components.button.Button;
	import laccaria.gui.scope.Layer;
	import laccaria.gui.scope.Scope;
	import laccaria.gui.SimpleTextField;
	
	public class ActionToolbar extends Toolbar
	{
		//------Constants
		
		public  static const WIDTH						:Number = 600;
		private static const HEIGHT						:Number = 100;
		
		private static const SAVE_BUTTON_WIDTH			:Number = 75;
		private static const SAVE_BUTTON_HEIGHT			:Number = 25;	
		
		private static const LOAD_BUTTON_WIDTH			:Number = 75;
		private static const LOAD_BUTTON_HEIGHT			:Number = 25;
		
		private static const LABEL_FONT_SIZE			:Number = 12;
		
		private static const COMPONENT_MARGIN_RIGHT		:Number = 5;
		private static const COMPONENT_MARGIN_BOTTOM	:Number = 5;
		
		//------Construction
		
		public function ActionToolbar() 
		{
			super( WIDTH, HEIGHT );
		}
		
		protected override function createComponentSets():void
		{
			createBiomeSet();
		}
		
		private function createBiomeSet():void
		{
			var row:Sprite = new Sprite();
			
			//Save Button
			var saveButton:Button = new Button( SAVE_BUTTON_WIDTH, SAVE_BUTTON_HEIGHT );
			var saveButtonLabel:SimpleTextField = new SimpleTextField( "Save", LABEL_FONT_SIZE );
			saveButton.addIcon( saveButtonLabel );
			saveButton.addEventListener( MouseEvent.MOUSE_UP, saveHandler );
			saveButton.x = COMPONENT_MARGIN_RIGHT;
			
			//Load Button
			var loadButton:Button = new Button( LOAD_BUTTON_WIDTH, LOAD_BUTTON_HEIGHT );
			var loadButtonLabel:SimpleTextField = new SimpleTextField( "Load", LABEL_FONT_SIZE );
			loadButton.addIcon( loadButtonLabel );
			loadButton.addEventListener( MouseEvent.MOUSE_UP, loadHandler );
			loadButton.x = saveButton.x + saveButton.width + COMPONENT_MARGIN_RIGHT;
			
			row.addChild( saveButton );
			row.addChild( loadButton );
			
			//Create the components array
			var components:Array = [ row ];
			
			//Create the component set
			var componentSet:ComponentSet = new ComponentSet( Scope.BIOME, Layer.ALL, COMPONENT_MARGIN_BOTTOM, components );
			_componentSets.push( componentSet );
		}
		
		protected override function positionComponentSets():void
		{
			for each ( var componentSet:ComponentSet in _componentSets )
			{
				componentSet.x = 3;
				componentSet.y = MARGIN_TOP;
			}
		}
		
		//------Event Handlers
		
		private function saveHandler( e:MouseEvent ):void
		{
			var saveEvent:BiomeEvent = new BiomeEvent( BiomeEvent.SAVE );
			EventManager.DispatchGlobal( saveEvent );
		}
		
		private function loadHandler( e:MouseEvent ):void
		{
			var loadEvent:BiomeEvent = new BiomeEvent( BiomeEvent.LOAD );
			EventManager.DispatchGlobal( loadEvent );
		}	
	}
}