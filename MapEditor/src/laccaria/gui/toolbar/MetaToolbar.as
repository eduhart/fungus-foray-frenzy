package laccaria.gui.toolbar 
{
	import flash.display.Sprite;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import laccaria.events.BiomeEvent;
	import laccaria.events.EventManager;
	import laccaria.gui.components.button.Button;
	import laccaria.gui.InputTextField;
	import laccaria.gui.scope.Layer;
	import laccaria.gui.scope.Scope;
	import laccaria.gui.SimpleTextField;
	
	public class MetaToolbar extends Toolbar
	{
		//------Constants
		
		private static const MARGIN_LEFT				:Number = 5;
		private static const COMPONENT_MARGIN_BOTTOM	:Number = 5;
		
		private static const WIDTH						:Number = 600;
		private static const HEIGHT						:Number = 80;
		
		private static const LOAD_BUTTON_WIDTH			:Number = 120;
		private static const LOAD_BUTTON_HEIGHT			:Number = 20;
		
		private static const LABEL_FONT_SIZE			:Number = 12;
		private static const COMPONENT_MARGIN_RIGHT		:Number = 5;
		
		//------Variables
		
		private var _nameInput:InputTextField;
		private var _gridspaceLabel:SimpleTextField;
		
		//------Construction
		
		public function MetaToolbar() 
		{
			super( WIDTH, HEIGHT );
		}
		
		protected override function createComponentSets():void
		{
			createBiomeComponentSet();
			createGridspaceComponentSet();
		}
		
		private function createBiomeComponentSet():void
		{
			var nameRow			:Sprite = createNameRow();
			var backgroundRow	:Sprite = createBackgroundRow();
			
			var components:Array = [ nameRow, backgroundRow ];
			var componentSet:ComponentSet = new ComponentSet( Scope.BIOME, Layer.ALL, COMPONENT_MARGIN_BOTTOM, components );
			_componentSets.push( componentSet );
		}
		
		private function createNameRow():Sprite
		{
			var nameLabel:SimpleTextField = new SimpleTextField( "Biome Name: ", LABEL_FONT_SIZE );
			_nameInput = new InputTextField();
			_nameInput.addEventListener( FocusEvent.FOCUS_OUT, nameLoseFocusHandler );
			
			var nameRow:Sprite = new Sprite();
			_nameInput.x = nameLabel.x + nameLabel.width + COMPONENT_MARGIN_RIGHT;
			nameRow.addChild( _nameInput );
			nameRow.addChild( nameLabel );
			
			return nameRow;
		}
		
		private function createBackgroundRow():Sprite
		{
			var loadButton:Button = new Button( LOAD_BUTTON_WIDTH, LOAD_BUTTON_HEIGHT );
			var loadButtonLabel:SimpleTextField = new SimpleTextField( "Load Background", LABEL_FONT_SIZE );
			loadButton.addIcon( loadButtonLabel );
			loadButton.addEventListener( MouseEvent.MOUSE_UP, loadBackgroundSpriteHandler );			
			
			var backgroundRow:Sprite = new Sprite();
			backgroundRow.addChild( loadButton );
			
			return backgroundRow;
		}
		
		private function createGridspaceComponentSet():void
		{
			_gridspaceLabel = new SimpleTextField( "Gridspace #", LABEL_FONT_SIZE );
			var components:Array = [ _gridspaceLabel ];
			var componentSet:ComponentSet = new ComponentSet( Scope.GRIDSPACE, Layer.ALL, 0, components );
			_componentSets.push( componentSet );
		}
		
		protected override function positionComponentSets():void
		{
			for each ( var componentSet:ComponentSet in _componentSets )
			{
				componentSet.x = MARGIN_LEFT;
				componentSet.y = MARGIN_TOP;
			}
		}
		
		//------Event Handlers
		
		private function loadBackgroundSpriteHandler( e:MouseEvent ):void
		{
			var biomeEvent:BiomeEvent = new BiomeEvent( BiomeEvent.LOAD_BACKGROUND );
			EventManager.DispatchGlobal( biomeEvent );
		}
		
		private function nameLoseFocusHandler( e:FocusEvent ):void
		{
			var biomeName:String = _nameInput.text;
			var biomeEvent:BiomeEvent = new BiomeEvent( BiomeEvent.CHANGE_NAME, biomeName );
			EventManager.DispatchGlobal( biomeEvent );
		}
		
		//------Methods
		
		public function setBiomeName( biomeName:String ):void
		{
			_nameInput.text = biomeName;
		}
		
		public function setGridspaceProperties( gridspaceIndex:int, gridspaceCoordinates:Point ):void
		{
			_gridspaceLabel.text = "Gridspace #" + gridspaceIndex + " (" + gridspaceCoordinates.x + ", " + gridspaceCoordinates.y + " )";
		}
	}
}