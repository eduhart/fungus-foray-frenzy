package laccaria.gui.toolbar 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	public class Toolbar extends Sprite
	{
		protected 	static const MARGIN_TOP						:Number = 10;
		private 	static const COMPONENT_SET_MARGIN_BOTTOM	:Number = 6;
		
		private 	static const LINE_THICKNESS					:Number	= 2;
		private 	static const LINE_COLOR						:uint	= 0x2A2A2A;
		private 	static const BACKGROUND_COLOR				:uint 	= 0x808080;		
		
		protected var _componentSets:Array;
		protected var _curComponentSets:Array;
		
		protected var _curScope:int;
		protected var _curLayer:int;
		
		public function Toolbar( width:Number, height:Number ) 
		{
			init( width, height );
		}
		
		private function init( width:Number, height:Number ):void
		{
			initVars();
			drawBackground( width, height );
			createComponentSets();
			positionComponentSets();
		}
		
		private function initVars():void
		{			
			_componentSets 		= new Array();
			_curComponentSets 	= new Array();
			_curScope		 	= -1;
			_curLayer		 	= -1;
		}
		
		private function drawBackground( width:Number, height:Number ):void
		{
			with ( graphics )
			{
				lineStyle( LINE_THICKNESS, LINE_COLOR );
				beginFill( BACKGROUND_COLOR );
				drawRect( 0, 0, width, height );
				endFill();
			}
		}
		
		//Meant to be overridden
		protected function createComponentSets():void
		{
		}
		
		public function setScope( scope:int ):void
		{
			_curScope = scope;
			
			if ( _curLayer != -1 )
			{
				changeComponent();
			}
		}
		
		public function setLayer( layer:int ):void
		{
			_curLayer = layer;
			
			if ( _curScope != -1 )
			{
				changeComponent();
			}
		}
		
		private function changeComponent():void
		{
			//Remove the current component sets
			for each ( var componentSet:ComponentSet in _componentSets )
			{
				if ( componentSet && contains( componentSet ) )
				{
					removeChild( componentSet );
				}
			}
			
			//Find the new component sets
			var nextComponentSets:Array = getCurrentComponentSets();
			
			//If one was found
			if ( nextComponentSets.length > 0 )
			{
				//Add them
				_curComponentSets = nextComponentSets;
				
				var curY:int = MARGIN_TOP;
				for each ( var curComponentSet:ComponentSet in _curComponentSets )
				{
					curComponentSet.y = curY;
					curY += curComponentSet.y + curComponentSet.height + COMPONENT_SET_MARGIN_BOTTOM;
					
					addChild( curComponentSet );
				}
			}
		}
		
		protected function positionComponentSets():void
		{
			for each ( var componentSet:ComponentSet in _componentSets )
			{
				componentSet.x = .5 * ( width - componentSet.width );
				componentSet.y = MARGIN_TOP;
			}
		}
		
		private function getCurrentComponentSets():Array
		{
			var currentComponentSets:Array = new Array();
			
			for each ( var componentSet:ComponentSet in _componentSets )
			{
				if ( componentSet.applies( _curScope, _curLayer ) )
				{
					currentComponentSets.push( componentSet );
				}
			}
			
			return currentComponentSets;
		}
	}
}