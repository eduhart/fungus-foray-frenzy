package laccaria.gui 
{
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	public class InputTextField extends TextField
	{
		private static const FONT				:String = "Arial";
		private static const FONT_SIZE			:Number = 12;
		private static const FONT_COLOR			:uint 	= 0x000000;
		private static const BACKGROUND_COLOR	:uint 	= 0xFFFFFF;
		private static const WIDTH				:Number = 150;
		private static const HEIGHT				:Number = 20;
		private static const TEXT_ALIGN			:String = TextFormatAlign.LEFT;
		
		public function InputTextField( width:Number=-1, height:Number=-1 )
		{
			super();
			
			init( width, height );
		}
		
		private function init( width:Number, height:Number ):void
		{
			type = TextFieldType.INPUT;
			border = true;
			this.width = ( width > 0 ) ? width : WIDTH;
			this.height = ( height > 0 ) ? height : HEIGHT;
			background = true;
			backgroundColor = BACKGROUND_COLOR;
			
			var format:TextFormat = new TextFormat();
			format.size = FONT_SIZE;
			format.font = FONT;
			format.align = TEXT_ALIGN;
			format.color = FONT_COLOR;
			defaultTextFormat = format;
		}
	}

}