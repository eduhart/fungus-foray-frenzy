package laccaria.events 
{
	import flash.events.Event;
	
	public class ConnectionEvent extends Event
	{
		public static const ADD_CONNECTION		:String = "add_connection_connection_event";
		public static const REMOVE_CONNECTION	:String = "remove_connection_connection_event";
		
		private var _idGridspaceFrom		:int;
		private var _directionGridspaceFrom	:int;
		private var _idGridspaceTo			:int;
		
		public function get idGridspacefrom()		:int { return _idGridspaceFrom; 		}
		public function get directionGridspaceFrom():int { return _directionGridspaceFrom; 	}	
		public function get idGridspaceTo()			:int { return _idGridspaceTo;			}
		
		public function ConnectionEvent( type:String, idGridspaceFrom:int, directionGridspaceFrom:int,
			idGridspaceTo:int ) 
		{
			super( type );
			_idGridspaceFrom = idGridspaceFrom;
			_directionGridspaceFrom = directionGridspaceFrom;
			_idGridspaceTo = idGridspaceTo;
		}		
	}
}