﻿package laccaria.events
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	//Handles event dispatching that doesn't have to do with the display list.
	//Use the static methods for global events, or instantiate for local events.
	public class EventManager
	{
		//-------Static
		
		private static var globalDispatcher:EventDispatcher = new EventDispatcher();		
		
		public static function AddGlobalEventListener( type:String, callback:Function ):void
		{
			globalDispatcher.addEventListener( type, callback );
		}
		
		public static function RemoveGlobalEventListener( type:String, callback:Function ):void
		{
			globalDispatcher.removeEventListener( type, callback );
		}
		
		public static function DispatchGlobal( event:Event ):void
		{
			globalDispatcher.dispatchEvent( event );
		}
		
		//Flush the global listeners to the murky waters of the great unknown
		public static function FlushListeners():void
		{
			globalDispatcher = new EventDispatcher();
		}
		
		//---------Local
		
		private var dispatcher:EventDispatcher;
		
		public function EventManager()
		{
			dispatcher = new EventDispatcher();
		}
		
		public function addEventListener( type:String, callback:Function ):void
		{
			dispatcher.addEventListener( type, callback );
		}
		
		public function removeEventListener( type:String, callback:Function ):void
		{
			dispatcher.removeEventListener( type, callback );
		}
		
		public function dispatch( event:Event ):void
		{
			dispatcher.dispatchEvent( event );
		}
	}
}