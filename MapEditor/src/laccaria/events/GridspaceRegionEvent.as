package laccaria.events 
{
	import flash.events.Event;
	import laccaria.gui.scope.biome.layer.connection.GridspaceRegion;
	
	public class GridspaceRegionEvent extends Event
	{
		public static const START_ADD_CONNECTION	:String = "start_add_connection_gr_event";
		public static const CANCEL_ADD_CONNECTION	:String = "cancel_add_connection_gr_event";
		public static const START_REMOVE_CONNECTION	:String = "start_remove_connection_gr_event";
		public static const END_REMOVE_CONNECTION	:String = "end_remove_connection_gr_event";
		public static const REMOVE_CONNECTION		:String = "remove_connection_gr_event";
		
		private var _gridspaceRegion:GridspaceRegion;
		
		public function get gridspaceRegion():GridspaceRegion { return _gridspaceRegion; }
		
		public function GridspaceRegionEvent( type:String, gridspaceRegion:GridspaceRegion ) 
		{
			super( type );
			_gridspaceRegion = gridspaceRegion;
		}
	}
}