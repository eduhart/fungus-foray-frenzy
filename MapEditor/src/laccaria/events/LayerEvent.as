package laccaria.events 
{
	import flash.events.Event;
	
	public class LayerEvent extends Event
	{
		public static const CHANGE:String = "layer_change_event";
		
		private var _layer:int;
		
		public function get layer():int { return _layer; }
		
		public function LayerEvent( type:String, layer:int )
		{
			super( type );
			
			_layer = layer;
		}		
	}
}