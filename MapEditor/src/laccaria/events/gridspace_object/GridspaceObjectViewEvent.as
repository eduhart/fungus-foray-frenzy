package laccaria.events.gridspace_object 
{
	import flash.events.Event;
	import laccaria.gui.scope.gridspace.GridspaceObjectView;
	
	public class GridspaceObjectViewEvent extends Event
	{
		public static const REMOVE		:String = "remove_gridspace_object_view_event";
		public static const START_MOVE	:String = "start_move_gridspace_object_view_event";
		public static const STOP_MOVE	:String = "stop_move_gridspace_object_view_event";
		
		private var _gridspaceObjectView:GridspaceObjectView;
		
		public function get gridspaceObjectView():GridspaceObjectView { return _gridspaceObjectView; }
		
		public function GridspaceObjectViewEvent( type:String, gridspaceObjectView:GridspaceObjectView ) 
		{
			super( type );
			
			_gridspaceObjectView = gridspaceObjectView;
		}
		
	}

}