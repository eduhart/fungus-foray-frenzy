package laccaria.events.gridspace_object 
{
	import flash.events.Event;
	
	public class AddGridspaceObjectEvent extends Event
	{
		public static const ADD_GRIDSPACE_OBJECT:String = "add_gridspace_object_event";
		
		private var _idGridspaceObjectType	:int;
		private var _xCoord					:int;
		private var _yCoord					:int;
		
		public function get idGridspaceObjectType()	:int { return _idGridspaceObjectType; 	}
		public function get xCoord()				:int { return _xCoord;					}
		public function get yCoord()				:int { return _yCoord;					}
		
		public function AddGridspaceObjectEvent( idGridspaceObjectType:int, xCoord:int, yCoord:int )
		{
			super( ADD_GRIDSPACE_OBJECT );
			
			_idGridspaceObjectType = idGridspaceObjectType;
			_xCoord = xCoord;
			_yCoord = yCoord;
		}		
	}
}