package laccaria.events.gridspace_object 
{
	import flash.events.Event;
	
	public class GridspaceObjectCategoryEvent extends Event
	{
		public static const SELECT_CATEGORY	:String = "select_category_gridspace_object_category_event";
		
		private var _category:int;
		
		public function get category():int { return _category; }
		
		public function GridspaceObjectCategoryEvent( type:String, category:int ) 
		{
			super( type );
			
			_category = category;
		}		
	}
}