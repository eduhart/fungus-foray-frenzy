package laccaria.events.gridspace_object 
{
	import flash.events.Event;
	import laccaria.model.gridspace.gridspace_object.type.GridspaceObjectType;
	
	public class GridspaceObjectTypeEvent extends Event 
	{
		public static const INITIALIZED		:String = "initialized_gridspace_object_type_event";
		public static const SELECT_TYPE		:String = "select_type_gridspace_object_type_event";
		public static const UNSELECT_TYPE	:String = "unselect_type_gridspace_object_type_event";
		
		private var _gridspaceObjectType:GridspaceObjectType;
		
		public function get gridspaceObjectType():GridspaceObjectType { return _gridspaceObjectType; }
		
		public function GridspaceObjectTypeEvent( eventType:String, gridspaceObjectType:GridspaceObjectType=null ) 
		{
			super( eventType );
			
			_gridspaceObjectType = gridspaceObjectType;
		}		
	}
}