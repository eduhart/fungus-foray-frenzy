package laccaria.events.gridspace_object 
{
	import flash.events.Event;
	
	public class GridspaceObjectManagerEvent extends Event
	{
		public static const INITIALIZED	:String = "initialized_gridspace_object_event";
		
		public function GridspaceObjectManagerEvent( type:String )
		{
			super( type );
		}		
	}
}