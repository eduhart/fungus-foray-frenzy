package laccaria.events.gridspace_object 
{
	import flash.events.Event;
	
	public class MoveGridspaceObjectEvent extends Event
	{
		public static const MOVE_GRIDSPACE_OBJECT:String = "move_gridspace_object_event";
		
		private var _idGridspaceObject	:int;
		private var _newXCoord			:int;
		private var _newYCoord			:int;
		
		
		public function get idGridspaceObject()	:int { return _idGridspaceObject; 	}
		public function get newXCoord()			:int { return _newXCoord;			}
		public function get newYCoord()			:int { return _newYCoord;			}
		
		public function MoveGridspaceObjectEvent( idGridspaceObject:int, newXCoord:int, newYCoord:int )
		{
			super( MOVE_GRIDSPACE_OBJECT );
			
			_idGridspaceObject = idGridspaceObject;
			_newXCoord = newXCoord;
			_newYCoord = newYCoord;
		}	
	}

}