package laccaria.events.gridspace_object 
{
	import flash.events.Event;
	
	public class RemoveGridspaceObjectEvent extends Event
	{
		public static const REMOVE_GRIDSPACE_OBJECT:String = "remove_gridspace_object_event";
		
		private var _idGridspaceObject:int;
		
		public function get idGridspaceObject():int { return _idGridspaceObject; }
		
		public function RemoveGridspaceObjectEvent( idGridspaceObject:int )
		{
			super( REMOVE_GRIDSPACE_OBJECT );
			
			_idGridspaceObject = idGridspaceObject;
		}	
	}

}