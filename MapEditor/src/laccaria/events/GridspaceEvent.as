package laccaria.events 
{
	import flash.events.Event;
	import flash.geom.Point;

	
	public class GridspaceEvent extends Event
	{
		public static const ADD					:String = "add_gridspace_event";		
		public static const REMOVE				:String = "remove_gridspace_event";		
		public static const CHANGE_INDEX		:String = "change_index_gridspace_event";
		
		private var _index		:int;
		private var _coordinates:Point;
		
		public function get index()			:int 	{ return _index; 		}
		public function get coordinates()	:Point 	{ return _coordinates; 	}
		
		public function GridspaceEvent( type:String, index:int, coordinates:Point ) 
		{
			super( type );
			
			_index = index;
			_coordinates = coordinates;
		}		
	}

}