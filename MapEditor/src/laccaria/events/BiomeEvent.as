package laccaria.events 
{
	import flash.events.Event;
	
	public class BiomeEvent extends Event
	{
		public static const INITIALIZED		:String = "initialized_biome_event";
		public static const SAVE			:String = "save_biome_event";
		public static const LOAD			:String = "load_biome_event";
		public static const LOAD_BACKGROUND	:String = "load_background_biome_event";
		public static const CHANGE_NAME		:String = "change_name_biome_event";
		
		private var _args:Array;
		
		public function get args():Array { return _args; }
		
		public function BiomeEvent( type:String, ... args ) 
		{
			super( type );
			
			_args = args;
		}		
	}
}