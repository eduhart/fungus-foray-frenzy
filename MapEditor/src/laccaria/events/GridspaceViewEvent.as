package laccaria.events 
{
	import flash.events.Event;
	import laccaria.gui.scope.biome.layer.gridspace.GridspaceView;
	
	public class GridspaceViewEvent extends Event
	{
		public static const ENTER				:String = "enter_gridspace_view_event";
		public static const START_ADD			:String = "start_add_gridspace_view_event";
		public static const STOP_ADD			:String = "stop_add_gridspace_view_event";
		public static const START_REMOVE		:String = "start_remove_gridspace_view_event";
		public static const STOP_REMOVE			:String = "stop_remove_gridspace_view_event";
		public static const COMPLETE_CONNECTION:String = "complete_connection_gridspace_view_event";
		
		private var _gridspaceView:GridspaceView;
		
		public function get gridspaceView():GridspaceView { return _gridspaceView; }
		
		public function GridspaceViewEvent( type:String, gridspaceView:GridspaceView )
		{
			super( type );
			
			_gridspaceView = gridspaceView;
		}	
	}
}