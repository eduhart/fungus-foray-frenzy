package laccaria.events 
{
	import flash.events.Event;
	import laccaria.gui.components.button.SelectableButton;
	
	public class ButtonEvent extends Event
	{
		public static const SELECT						:String = "select_button_event";
		public static const SELECT_ADD_CONNECTION		:String = "select_add_connection_button_event";
		public static const SELECT_ADD_GRIDSPACE		:String = "select_add_gridspace_button_event";
		public static const SELECT_ADD_OBJECT			:String = "select_add_object_button_event"
		public static const SELECT_ENTER_GRIDSPACE		:String = "select_enter_gridspace_button_event";
		public static const SELECT_EXIT_GRIDSPACE		:String = "select_exit_gridspace_button_event";
		public static const SELECT_MOVE_OBJECT			:String = "select_move_object_button_event";
		public static const SELECT_REMOVE_CONNECTION	:String = "select_remove_connection_button_event";
		public static const SELECT_REMOVE_GRIDSPACE		:String = "select_remove_gridspace_button_event";
		public static const SELECT_REMOVE_OBJECT		:String = "select_remove_object_button_event";
		
		private var _button:SelectableButton;
		
		public function get button():SelectableButton { return _button; }
		
		public function ButtonEvent( type:String, button:SelectableButton ) 
		{
			super( type );
			_button = button;
		}		
	}
}