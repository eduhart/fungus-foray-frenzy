package laccaria.events 
{
	import flash.events.Event;
	
	public class NewMiscTypeFormEvent extends Event
	{
		public static const ADD_NEW_TYPE:String = "add_new_type_new_misc_type_form_event";
		
		private var _displayName:String;
		private var _fileName	:String;
		private var _iconPath	:String;
		
		public function get displayName()	:String { return _displayName; 	}
		public function get fileName()		:String { return _fileName;		}
		public function get iconPath()		:String { return _iconPath;	}
		
		public function NewMiscTypeFormEvent( type:String, displayName:String, fileName:String, iconPath:String )
		{
			super( type );
			
			init( displayName, fileName, iconPath );
		}
		
		private function init( displayName:String, fileName:String, iconPath:String ):void
		{
			_displayName 	= displayName;
			_fileName 		= fileName;
			_iconPath 		= iconPath;
		}
	}
}