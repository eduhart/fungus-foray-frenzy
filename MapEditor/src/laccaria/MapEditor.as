package laccaria 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Point;
	import laccaria.events.BiomeEvent;
	import laccaria.events.ConnectionEvent;
	import laccaria.events.EventManager;
	import laccaria.events.gridspace_object.AddGridspaceObjectEvent;
	import laccaria.events.gridspace_object.GridspaceObjectManagerEvent;
	import laccaria.events.gridspace_object.MoveGridspaceObjectEvent;
	import laccaria.events.gridspace_object.RemoveGridspaceObjectEvent;
	import laccaria.events.GridspaceEvent;
	import laccaria.events.GridspaceViewEvent;
	import laccaria.gui.GUI;
	import laccaria.gui.scope.biome.BiomeScope;
	import laccaria.gui.scope.biome.layer.BiomeLayer;
	import laccaria.gui.scope.gridspace.GridspaceLayer;
	import laccaria.gui.scope.gridspace.GridspaceScope;
	import laccaria.io.IOManager;
	import laccaria.model.Biome;
	import laccaria.model.gridspace.Gridspace;
	import laccaria.model.gridspace.gridspace_object.GridspaceObject;
	import laccaria.model.gridspace.gridspace_object.GridspaceObjectManager;
	import laccaria.model.gridspace.gridspace_object.type.GridspaceObjectType;
	
	//Controller
	public class MapEditor extends Sprite
	{
		//------Variables
		
		private var _biome					:Biome;						//Model
		private var	_gridspaceObjectManager	:GridspaceObjectManager;	//Model
		
		private var _gui					:GUI;						//View
		private var _biomeScope				:BiomeScope;				//View
		private var _gridspaceScope			:GridspaceScope;			//View
		
		private var _ioManager				:IOManager;					//IO
		
		//------Construction
		
		public function MapEditor() 
		{
			init();
		}
		
		private function init():void
		{
			initVars();
			addEventListeners();
		}
		
		private function initVars():void
		{
			_gridspaceObjectManager = new GridspaceObjectManager();
		}
		
		private function addEventListeners():void
		{
			EventManager.AddGlobalEventListener( GridspaceObjectManagerEvent.INITIALIZED, 				gridspaceObjectManagerInitializedHandler 	);
			
			EventManager.AddGlobalEventListener( GridspaceEvent				.ADD, 						addGridspaceHandler 						);
			EventManager.AddGlobalEventListener( GridspaceEvent				.REMOVE, 					removeGridspaceHandler 						);
			EventManager.AddGlobalEventListener( GridspaceEvent				.CHANGE_INDEX, 				changeGridspaceIndexHandler					);
			
			EventManager.AddGlobalEventListener( ConnectionEvent			.ADD_CONNECTION,			addConnectionHandler						);
			EventManager.AddGlobalEventListener( ConnectionEvent			.REMOVE_CONNECTION, 		removeConnectionHandler						);
			
			EventManager.AddGlobalEventListener( BiomeEvent					.SAVE,						saveBiomeHandler							);
			EventManager.AddGlobalEventListener( BiomeEvent					.LOAD,						loadBiomeHandler							);
			EventManager.AddGlobalEventListener( BiomeEvent					.LOAD_BACKGROUND,			loadBackgroundHandler						);
			EventManager.AddGlobalEventListener( BiomeEvent					.CHANGE_NAME,				changeBiomeNameHandler						);
			
			EventManager.AddGlobalEventListener( AddGridspaceObjectEvent	.ADD_GRIDSPACE_OBJECT,		addGridspaceObjectHandler					);
			EventManager.AddGlobalEventListener( RemoveGridspaceObjectEvent	.REMOVE_GRIDSPACE_OBJECT,	removeGridspaceObjectHandler				);
			EventManager.AddGlobalEventListener( MoveGridspaceObjectEvent	.MOVE_GRIDSPACE_OBJECT,		moveGridspaceObjectHandler					);
			
			EventManager.AddGlobalEventListener( GridspaceViewEvent			.ENTER, 					gridspaceViewEnterHandler 					);
		}
		
		//------Event Handlers
		
		private function gridspaceObjectManagerInitializedHandler( e:GridspaceObjectManagerEvent ):void
		{
			_biome = new Biome();
			
			_gui = new GUI( _gridspaceObjectManager );
			addChild( _gui );			
			
			_biomeScope = _gui.mainWindow.biomeScope;
			_gridspaceScope = _gui.mainWindow.gridspaceScope;
			
			_ioManager = new IOManager();
		}
		
		private function addGridspaceHandler( e:GridspaceEvent ):void
		{
			var gridspaceCoordinates:Point = e.coordinates;			
			var newGridspace:Gridspace = _biome.createGridspace( gridspaceCoordinates );
			
			if ( newGridspace )
			{
				_biomeScope.setGridspaceViewIndex( gridspaceCoordinates, newGridspace.index );
			}
		}
		
		private function removeGridspaceHandler( e:GridspaceEvent ):void
		{
			var gridspaceIndex:int = e.index;
			var removedGridspace:Gridspace = _biome.removeGridspace( gridspaceIndex );
			
			if ( removedGridspace )
			{
				_biomeScope.setGridspaceAsRemoved( removedGridspace.index, removedGridspace.coordinates );
			}
		}
		
		private function changeGridspaceIndexHandler( e:GridspaceEvent ):void
		{
			var gridspaceCoordinates:Point = e.coordinates;
			var indexToSetTo:int = e.index;
			
			_biomeScope.setGridspaceViewIndex( gridspaceCoordinates, indexToSetTo );
		}
		
		private function addConnectionHandler( e:ConnectionEvent ):void
		{
			_biome.addConnection( e.idGridspacefrom, e.idGridspaceTo, e.directionGridspaceFrom );
		}
		
		private function removeConnectionHandler( e:ConnectionEvent ):void
		{
			_biome.removeConnection( e.idGridspacefrom, e.directionGridspaceFrom );
		}
		
		private function saveBiomeHandler( e:BiomeEvent ):void
		{
			_ioManager.saveBiome( _biome, _gridspaceObjectManager );
		}
		
		private function loadBiomeHandler( e:BiomeEvent ):void
		{
			_ioManager.loadBiome( this, biomeLoadedHandler, _gridspaceObjectManager );
		}
		
		private function biomeLoadedHandler( biome:Biome ):void
		{
			_gui.setBiomeScope();
			_gui.setLayer( BiomeLayer.BACKGROUND );
			
			_biome = biome;
			
			_biomeScope.giveBackground( _biome.background );
			_gui.metaToolbar.setBiomeName( _biome.name );
			
			//Set gridspace views
			for each ( var gridspace:Gridspace in _biome.gridspaces )
			{
				_biomeScope.setGridspaceViewIndex( gridspace.coordinates, gridspace.index );
			}
			
			//Add visual connections
			for each ( gridspace in _biome.gridspaces )
			{
				for ( var i:int = 0; i < gridspace.connections.length; i++ )
				{
					var curConnection:int = gridspace.connections[ i ];
					
					if ( curConnection != -1 )
					{
						_biomeScope.addConnection( gridspace.index, i, curConnection );
					}
				}
			}
		}
		
		private function loadBackgroundHandler( e:BiomeEvent ):void
		{
			_ioManager.loadBackground( this, backgroundLoadedHandler );
		}
		
		private function backgroundLoadedHandler( background:Bitmap ):void
		{
			//Give the model the background
			_biome.giveBackground( background );
			
			//Give the view the background
			_biomeScope.giveBackground( background );
		}
		
		private function changeBiomeNameHandler( e:BiomeEvent ):void
		{
			if ( e.args && e.args.length > 0  && e.args[ 0 ] is String )
			{
				var name:String = e.args[ 0 ];
				_biome.name = name;
			}
		}
		
		private function gridspaceViewEnterHandler( e:GridspaceViewEvent ):void
		{
			var gridspaceIndex:int = e.gridspaceView.index;
			var matchingGridspace:Gridspace = _biome.findGridspaceByIndex( gridspaceIndex );
			
			if ( matchingGridspace )
			{
				_gui.setGridspaceScope( matchingGridspace.index, matchingGridspace.coordinates,
					matchingGridspace.background, matchingGridspace.gridspaceObjects );
				_gui.setLayer( GridspaceLayer.BACKGROUND );
			}
		}
		
		private function addGridspaceObjectHandler( e:AddGridspaceObjectEvent ):void
		{
			var idGridspace				:int 					= _gridspaceScope.idGridspace;
			var xCoord					:int 					= e.xCoord;
			var yCoord					:int 					= e.yCoord;	
			var idGridspaceObjectType	:int 					= e.idGridspaceObjectType;			
			var gridspaceObjectType		:GridspaceObjectType 	= _gridspaceObjectManager.findType( idGridspaceObjectType );
			var idObject				:int 					= _gridspaceObjectManager.getNextIdObject();
			var gridspaceObject			:GridspaceObject 		= _biome.addGridspaceObject( idGridspace, idObject, xCoord, yCoord, gridspaceObjectType );
			
			
			_gridspaceScope.addGridspaceObjectView( gridspaceObject );
		}
		
		private function removeGridspaceObjectHandler( e:RemoveGridspaceObjectEvent ):void
		{
			var idGridspace	:int = _gridspaceScope.idGridspace;			
			var gridspace:Gridspace = _biome.findGridspaceByIndex( idGridspace );
			
			if ( gridspace )
			{
				var idObject:int = e.idGridspaceObject;
				gridspace.removeGridspaceObject( idObject );
			}
		}
		
		private function moveGridspaceObjectHandler( e:MoveGridspaceObjectEvent ):void
		{
			var idGridspace:int = _gridspaceScope.idGridspace;
			
			var gridspace:Gridspace = _biome.findGridspaceByIndex( idGridspace );
			
			if ( gridspace )
			{
				var idObject	:int = e.idGridspaceObject;
				var newXCoord	:int = e.newXCoord;
				var newYCoord	:int = e.newYCoord;
				gridspace.moveGridspaceObject( idObject, newXCoord, newYCoord );
			}
		}
	}
}