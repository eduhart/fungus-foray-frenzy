// Partially Taken from: Programming 2D Games
// Copyright (c) 2011 by: Charles Kelly


#ifndef _SHROOMGAME_H             // prevent multiple definitions
#define _SHROOMGAME_H             
#define WIN32_LEAN_AND_MEAN

#include "game.h"

#include "textureManager.h"
#include "image.h"

#include "Input Controller\ActionNode.h"
#include "Interface\Map.h"
#include "Interface\Camera.h"
#include "GameObjects\player.h"
#include "GameObjects\GameObject.h"
#include "GameObjects\MushroomSpawner.h"
#include "SoundVault.h"

#include <vector>
#include <ctime>

//=============================================================================
// Create game class
//=============================================================================
class ShroomGame : public Game
{
public:
    ShroomGame();
    virtual ~ShroomGame();

	//To whom it may concern: this is an annoying little workaround struct. 
	//because we can't make textures outside of a class that inherits from 'Game',
	//we have to keep track of all of them in here, not inside gameObject...gah
	struct ScreenObject
	{
		GameObject* m_obj;
		Image m_image;
		TextureManager m_texManager;
		int m_layer;

		ScreenObject(GameObject* obj, Image img, TextureManager man, int layer = 0)
			: m_obj(obj)
			, m_image(img)
			, m_texManager(man)
			, m_layer(layer)
		{}
		ScreenObject()
		: m_obj()
		, m_image()
		, m_texManager()
		, m_layer(0)
		{}

		void recalculateAnim()
		{
			m_image.setFrames(m_obj->getFirstFrame(), m_obj->getLastFrame());
			m_image.setCurrentFrame(m_obj->getFirstFrame());
			m_image.setLoop(m_obj->getTexture().m_loops);
		}
	};

	struct HiddenGridspace
	{
		std::vector<ScreenObject> m_oldObjs;
		Image m_oldBkg;
		TextureManager m_oldTexManager;
		HiddenGridspace( std::vector<ScreenObject> objs, Image bkg, TextureManager man)
			: m_oldObjs(objs)
			, m_oldBkg(bkg)
			, m_oldTexManager(man)
		{}
	};

    void initialize(HWND hwnd);
	void gotoStartState();
	void gotoMainState();
	void gotoEndState();
    void update();     
	void updateCameraNormal(int camId); //different camera states
	void updateCameraSwitchScreen(int camId);//there may be more of these
	void updateCameraFadeBlack(int camId);//----------------
	void updateStartState();
	void updateMainState();
	void updateEndState();
    void render();     
	void renderStartState();
	void renderMainState();
	void renderEndState();
    void releaseAll();
    void resetAll();

	void setGridToCamera(int camId, BiomeType biomeId, int gridId, CardinalDirection dir = CardinalDirection::NUM_DIRECTIONS);
	void setBiomeToCamera(int camId, BiomeType biomeId, int gridId, CardinalDirection dir = CardinalDirection::NUM_DIRECTIONS);
	void addSecondaryPlayerToCam(int camId, int playerId);
	void checkForScreenChange(int camId);

	//conversion functions
	Point screenToGridCoords(int x, int y, int camId);
	Point screenToGridCoords(Point screenPoint, int camId);
	Point gridToScreenCoords(int x, int y, int camId);
	Point gridToScreenCoords(Point screenPoint, int camId);
	int getCameraLoc(int x, int y); //tells which camera the current pixel is in
	inline int getCameraLoc(Point pos) {return getCameraLoc(pos.m_x, pos.m_y);};

	//==========================================================================
	//call initialize on all the textures in a group and set our defaults
	//must be called any time alterations are made to a render list because pointers get messed up
	void reInitializeTextures(int camId);
	void reInitializeGui(int camId);
	void reInitializeHiddens(int camId);
	//=====================================================================

	bool canPlayerMove(int camId, CardinalDirection dir);
	void playerUseKey(int camId);
	std::vector<Point> getTriggeredTile(int playerId);
	void addMushToPlayer(int camId, int mushX, int mushY);
	void toggleLantern(bool isOn, int playerId);
	void toggleBugSpray(bool isOn, int playerId);

private:

	Map theMap;
	std::vector<Player> m_players;

	std::vector<Camera> m_cameras;
	std::vector<Image> m_cameraBkgs;
	std::vector<TextureManager> m_cameraTextures;

	std::vector<std::vector<ScreenObject>> m_screenObjects;
	std::vector<HiddenGridspace> m_hiddenSpaces; //the images that 'linger' during the transition to a new gridspace
	std::vector<std::vector<ScreenObject>> m_guiObjects;
	std::vector<std::vector<ScreenObject>> m_filterObjects;

	Image m_cameraDivider;
	TextureManager m_dividerMan;
	Image m_splashScreen;
	TextureManager m_splashMan;
	Image m_endPlayer;
	TextureManager m_ePlayerMan;

	std::time_t m_startTime;
	SoundVault soundVault;

	bool signal; //temporary for debugging

	GameState m_gameState;

	void setupMovesets();
	void handleMessage(int camId, string msg);

};

#endif
