#include "ShroomGame.h"

//=============================================================================
// Constructor
//=============================================================================
ShroomGame::ShroomGame()
	: theMap(1)
	, m_players()
	, m_cameras()
	, m_screenObjects()
	, m_hiddenSpaces()
	, m_gameState(GameState::START)
{
}

//=============================================================================
// Destructor
//=============================================================================
ShroomGame::~ShroomGame()
{
    releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void ShroomGame::initialize(HWND hwnd)
{
    Game::initialize(hwnd); // throws GameError

	gotoStartState();

    return;
}

void ShroomGame::gotoStartState()
{
	m_gameState = GameState::START;
	
	//set the splash screen
	m_splashMan.initialize(graphics, "Art\\GUI\\startScreen.png");
	m_splashScreen.initialize(graphics, 1024, 768, 0, &(m_splashMan));
	m_splashScreen.setX(0);
	m_splashScreen.setY(0);
}

void ShroomGame::gotoMainState()
{
	m_gameState = GameState::MAIN;

	theMap.load();

	//make some players with locations and textures
	Player p1(PlayerType::PLAYER_ZERO);
	p1.setPosition(gridToScreenCoords(9, 12, CameraType::TOP_LEFT));
	p1.setTexture("Art\\CharacterSprites\\PlayerBlue.png",p1.getWidth()*TILE_SIZE,p1.getHeight()*TILE_SIZE,5,2);
	Player p2(PlayerType::PLAYER_ONE);
	p2.setPosition(gridToScreenCoords(12, 12, CameraType::TOP_RIGHT));
	p2.setTexture("Art\\CharacterSprites\\PlayerRed.png",p2.getWidth()*TILE_SIZE,p2.getHeight()*TILE_SIZE,5,2);
	Player p3(PlayerType::PLAYER_TWO);
	p3.setPosition(gridToScreenCoords(15, 12, CameraType::BOTTOM_LEFT));
	p3.setTexture("Art\\CharacterSprites\\PlayerGreen.png",p3.getWidth()*TILE_SIZE,p3.getHeight()*TILE_SIZE,5,2);
	Player p4(PlayerType::PLAYER_THREE);
	p4.setPosition(gridToScreenCoords(18, 12, CameraType::BOTTOM_RIGHT));
	p4.setTexture("Art\\CharacterSprites\\PlayerYellow.png",p4.getWidth()*TILE_SIZE,p4.getHeight()*TILE_SIZE,5,2);

	m_players.push_back(p1);
	m_players.push_back(p2);
	m_players.push_back(p3);
	m_players.push_back(p4);

	//make some cameras with players and dimensions
	Camera camera0(0,0,0,384,512, &(m_players[0]));
	Camera camera1(1,512,0,384,512, &(m_players[1]));
	Camera camera2(1,0,384,384,512, &(m_players[2]));
	Camera camera3(1,512,384,384,512, &(m_players[3]));

	m_cameras.push_back(camera0);
	m_cameras.push_back(camera1);
	m_cameras.push_back(camera2);
	m_cameras.push_back(camera3);
 
	for(int i = 0; i < m_cameras.size(); ++i)
	{
		//add empty background textures
		TextureManager tempTex;
		m_cameraTextures.push_back(tempTex);
		Image tempImg;
		m_cameraBkgs.push_back(tempImg);

		//add an empty render list
		std::vector<ScreenObject> objs;
		m_screenObjects.push_back(objs);

		//pull all the objects off the camera's gui, add them to their own render list
		std::vector<ScreenObject> guiObjs;
		m_cameras[i].getGui()->setItem(m_players[i].getItem());
		m_cameras[i].getGui()->setBiome(BiomeType::FOREST);
		m_cameras[i].getGui()->setPlayer(&(m_players[i]));
		std::vector<GameObject*> stuffOnGui = m_cameras[i].getGui()->getAllObjects();
		for(int k = 0; k < stuffOnGui.size(); ++k)
		{
			TextureManager guiTex;
			Image guiImage;
			ScreenObject curGuiObj(stuffOnGui[k], guiImage, guiTex);
			guiObjs.push_back(curGuiObj);
		}
		m_guiObjects.push_back(guiObjs);

		//add empty textures and render lists to the 'transition' tiles
		TextureManager hiddenTex;
		Image hiddenImage;
		std::vector<ScreenObject> hiddenObjs;
		HiddenGridspace h(hiddenObjs, hiddenImage, hiddenTex);
		m_hiddenSpaces.push_back(h);
	}
	
	for(int i = 0; i < m_cameras.size(); ++i)
	{
		reInitializeGui(i);
	}

	//set initial gridspaces to cameras
	setGridToCamera(CameraType::TOP_LEFT, BiomeType::FOREST, 0);
	setGridToCamera(CameraType::TOP_RIGHT, BiomeType::FOREST, 0);
	setGridToCamera(CameraType::BOTTOM_LEFT, BiomeType::FOREST, 0);
	setGridToCamera(CameraType::BOTTOM_RIGHT, BiomeType::FOREST, 0);

	//set non-controlled players to cameras
	addSecondaryPlayerToCam(0,1);
	addSecondaryPlayerToCam(0,2);
	addSecondaryPlayerToCam(0,3);
	reInitializeTextures(0);
	addSecondaryPlayerToCam(1,0);
	addSecondaryPlayerToCam(1,2);
	addSecondaryPlayerToCam(1,3);
	reInitializeTextures(1);
	addSecondaryPlayerToCam(2,0);
	addSecondaryPlayerToCam(2,1);
	addSecondaryPlayerToCam(2,3);
	reInitializeTextures(2);
	addSecondaryPlayerToCam(3,0);
	addSecondaryPlayerToCam(3,1);
	addSecondaryPlayerToCam(3,2);
	reInitializeTextures(3);

	//add that horrible black cross to the screen
	m_dividerMan.initialize(graphics, "Art\\GUI\\divider.png");
	m_cameraDivider.initialize(graphics,0,0,0, &m_dividerMan);
	
	//start the clock
	m_startTime = std::time(0); //set to now

	setupMovesets();	//sets up the controls and movesets for each player


	//play the forest theme	
	soundVault.playBackgroundSong( BiomeType::CAVE );
    return;
}

void ShroomGame::gotoEndState()
{
	m_gameState = GameState::END;
	m_cameras.clear();

	//add up all the little browns that everyone got
	for(int i = 0; i < m_players.size(); ++i)
	{
		m_players[i].cashInMushrooms(MushroomType::LITTLE_BROWN);
	}

	//calculate the winner
	int max = -1;
	int winner = -1;
	for(int i = 0; i < m_players.size(); ++i)
	{
		if(m_players[i].getScore() > max)
		{
			max = m_players[i].getScore();
			winner = i;
		}
	}

	//put an image of the winner on screen
	m_players[winner].setToWalkAnimation(CardinalDirection::SOUTH);
	TexInfo pTex = m_players[winner].getTexture();
	m_ePlayerMan.initialize(graphics, pTex.m_resource);
	m_endPlayer.initialize(graphics, pTex.m_frameWidth, pTex.m_frameHeight, pTex.m_frameColumns, &(m_ePlayerMan));
	m_endPlayer.setX(343);
	m_endPlayer.setY(100);
	m_endPlayer.setFrameDelay(FRAME_DELAY);
	m_endPlayer.setLoop(true);
	m_endPlayer.setFrames(m_players[winner].getFirstFrame(),m_players[winner].getLastFrame());
	m_endPlayer.setCurrentFrame(m_endPlayer.getStartFrame());
	
	//put the end background on screen
	m_splashMan.initialize(graphics, "Art\\GUI\\winScreen.png");
	m_splashScreen.initialize(graphics, 1024, 768, 0, &(m_splashMan));
	m_splashScreen.setX(0);
	m_splashScreen.setY(0);
}

//=============================================================================
// Set up move sets
//=============================================================================

void ShroomGame::setupMovesets()
{
	//set player names
		m_players[0].setName("player 1");
		m_players[1].setName("player 2");
		m_players[2].setName("player 3");
		m_players[3].setName("player 4");

	//Create movesets
		m_players[0].BaseMoveSet();
		m_players[1].BaseMoveSet();
		m_players[2].BaseMoveSet();
		m_players[3].BaseMoveSet();

	//give the movesets to the input controller
		inputController.moveSet[0]=m_players[0].moveSet;
		inputController.moveSet[1]=m_players[1].moveSet;
		inputController.moveSet[2]=m_players[1].moveSet;
		inputController.moveSet[3]=m_players[1].moveSet;

}

//=============================================================================
// Update all game items
//=============================================================================
void ShroomGame::update()
{
	//check controllers
		inputController.checkControllers();		
		inputController.ControllerAction();

	//Variables for examining input messages
		ActionNode* action=inputController.getNextAction();
		string* info=NULL;
		string chr="";
		string act="";

	//message counter
		int counter=0;

		do
		{
			if(action!=NULL)
			{
				info=(*action).getInfo();
				chr=(*info);
				act=*(info+1);
				
				//increment counter
				counter++;
		
				for(int i = 0; i < m_cameras.size(); ++i)
				{
					if(chr==m_players[i].getName() && m_cameras[i].getState() == Camera::NORMAL)
					{
						handleMessage(i,act);
						break;
					}
				}

				//get next action
				action=inputController.getNextAction();
			}
		}
		while(action!=NULL && counter<1);

	//State Machine for Cameras, call a different update function
	//depending on what each camera is doing
	
	for(int i = 0; i < m_cameras.size(); ++i)
	{
	}

	switch(m_gameState)
	{
	case GameState::START: updateStartState();
		break;
	case GameState::MAIN: updateMainState();
		break;
	case GameState::END: updateEndState();
		break;
	default:break;
	}
}


void ShroomGame::handleMessage(int camId, string msg)
{
	//Handle Input ========================================================================================
	
		bool isIdle = true;

		if(msg=="move left hold")//move the player if the key is being held
		{
			if(canPlayerMove(camId, CardinalDirection::WEST))
				m_players[camId].setPosition(m_players[camId].getPosition().m_x - m_players[camId].getSpeed(), m_players[camId].getPosition().m_y);
 			isIdle = false;
		}
			if(msg=="move left press")//input->wasKeyPressed(VK_LEFT)) //switch to the proper animation on press
			{
				m_players[camId].setToWalkAnimation(CardinalDirection::WEST);
				isIdle = false;
			}
		if(msg=="move right hold")
		{
			if(canPlayerMove(camId, CardinalDirection::EAST))
				m_players[camId].setPosition(m_players[camId].getPosition().m_x + m_players[camId].getSpeed(), m_players[camId].getPosition().m_y);
 			isIdle = false;
		}
			if(msg=="move right press")//input->wasKeyPressed(VK_RIGHT))
			{
				m_players[camId].setToWalkAnimation(CardinalDirection::EAST);
				isIdle = false;
			}
		if(msg=="move up hold")
		{
			if(canPlayerMove(camId, CardinalDirection::NORTH))
				m_players[camId].setPosition(m_players[camId].getPosition().m_x, m_players[camId].getPosition().m_y - m_players[camId].getSpeed());
 			isIdle = false;
		}
			if(msg=="move up press")//input->wasKeyPressed(VK_UP))
			{
				m_players[camId].setToWalkAnimation(CardinalDirection::NORTH);
				isIdle = false;
			}
		if(msg=="move down hold")
		{
			if(canPlayerMove(camId, CardinalDirection::SOUTH))
				m_players[camId].setPosition(m_players[camId].getPosition().m_x, m_players[camId].getPosition().m_y + m_players[camId].getSpeed());
 			isIdle = false;
		}
			if(msg=="move down press")//input->wasKeyPressed(VK_DOWN))
			{
				m_players[camId].setToWalkAnimation(CardinalDirection::SOUTH);
				isIdle = false;
			}
		//'use key' don't switch back to idle animation if we're holding the button down
		if(msg=="use hold")//input->isKeyDown(VK_SPACE))
		{
			isIdle = false;
		}
		else 
		{
			toggleLantern(false, camId);
		}
		if(msg=="use press")//input->wasKeyPressed(VK_SPACE))
		{
			playerUseKey(camId); //check for stuff to interact with
			isIdle = false;
		}

		if(isIdle)
		{
			m_players[camId].setToIdleAnimation();
		}		
	}

void ShroomGame::updateMainState()
{
	//check controllers
		inputController.checkControllers();		
		inputController.ControllerAction();

	//Variables for examining input messages
		ActionNode* action=inputController.getNextAction();
		string* info=NULL;
		string chr="";
		string act="";

		do
		{
			if(action!=NULL)
			{
				info=(*action).getInfo();
				chr=(*info);
				act=*(info+1);
			}
		
			for(int i = 0; i < m_cameras.size(); ++i)
			{
				if(chr==m_players[i].getName() && m_cameras[i].getState() == Camera::NORMAL)
				{
					handleMessage(i,act);
					break;
				}
			}

			if(action!=NULL)
			{
				action=inputController.getNextAction();
			}

		}while(action!=NULL);

	//State Machine for Cameras, call a different update function
	//depending on what each camera is doing
	
	for(int i = 0; i < m_cameras.size(); ++i)
	{
		switch(m_cameras[i].getState())
		{
			case Camera::NORMAL: updateCameraNormal(i);
				break;
			case Camera::NEW_GRIDSPACE: updateCameraSwitchScreen(i);
				break;
			case Camera::FADE_BLACK: updateCameraFadeBlack(i);
				break;
			default: break;
		}
	}
	
}

void ShroomGame::updateStartState()
{
	if(input->wasKeyPressed(VK_RETURN))
	{
		gotoMainState();
	}
}

void ShroomGame::updateEndState()
{
	m_endPlayer.update(frameTime);
}

void ShroomGame::updateCameraNormal(int camId)
{	

	if(camId == CameraType::TOP_LEFT)
	{
 		bool isIdle = true;
 
		//move the player if the key is being held
		if(input->isKeyDown(VK_LEFT))
 		{
			if(canPlayerMove(camId, CardinalDirection::WEST))
				m_players[camId].setPosition(m_players[camId].getPosition().m_x - m_players[camId].getSpeed(), m_players[camId].getPosition().m_y);
 			isIdle = false;
 		}
			if(input->wasKeyPressed(VK_LEFT)) //switch to the proper animation on press
 			{
 				m_players[camId].setToWalkAnimation(CardinalDirection::WEST);
 				isIdle = false;
 			}
		if(input->isKeyDown(VK_RIGHT))
 		{
 			if(canPlayerMove(camId, CardinalDirection::EAST))
				m_players[camId].setPosition(m_players[camId].getPosition().m_x + m_players[camId].getSpeed(), m_players[camId].getPosition().m_y);
 			isIdle = false;
 		}
			if(input->wasKeyPressed(VK_RIGHT))
 			{
 				m_players[camId].setToWalkAnimation(CardinalDirection::EAST);
 				isIdle = false;
 			}
		if(input->isKeyDown(VK_UP))
 		{
 			if(canPlayerMove(camId, CardinalDirection::NORTH))
				m_players[camId].setPosition(m_players[camId].getPosition().m_x, m_players[camId].getPosition().m_y - m_players[camId].getSpeed());
 			isIdle = false;
 		}
			if(input->wasKeyPressed(VK_UP))
 			{
 				m_players[camId].setToWalkAnimation(CardinalDirection::NORTH);
 				isIdle = false;
 			}
		if(input->isKeyDown(VK_DOWN))
 		{
 			if(canPlayerMove(camId, CardinalDirection::SOUTH))
				m_players[camId].setPosition(m_players[camId].getPosition().m_x, m_players[camId].getPosition().m_y + m_players[camId].getSpeed());
 			isIdle = false;
 		}
			if(input->wasKeyPressed(VK_DOWN))
 			{
 				m_players[camId].setToWalkAnimation(CardinalDirection::SOUTH);
 				isIdle = false;
 			}
 		//'use key' don't switch back to idle animation if we're holding the button down
		if(input->isKeyDown(VK_SPACE))
 		{
 			isIdle = false;
 		}
 		else 
 		{
 			toggleLantern(false, camId);
			toggleBugSpray(false, camId);
 		}
			if(input->wasKeyPressed(VK_SPACE))
			{
				playerUseKey(camId); //check for stuff to interact with
				isIdle = false;
			}

 
 		if(isIdle)
 		{
			m_players[camId].setToIdleAnimation();
		}
	
 	}
	

	//=====================================================================================================

	//Handle Movement Updates =============================================================================
	for(int k = 0; k < m_screenObjects[camId].size(); ++k)
	{
		//get the position of the game object
		int x = m_screenObjects[camId][k].m_obj->getPosition().m_x;
		int y = m_screenObjects[camId][k].m_obj->getPosition().m_y;
		Point coords(x,y);
		if( k != 0 && m_screenObjects[camId][k].m_layer == Layer::PLAYER) //if there is a foreign player on the screen
		{
			int playerOrigCam = getCameraLoc(x,y);
			coords = screenToGridCoords(coords, playerOrigCam); //convert our coords to the coords on the other screen
		}
		if(k != 0)
			coords = gridToScreenCoords(coords, camId);
		//update the image position to match
		m_screenObjects[camId][k].m_image.setX(coords.m_x);
		m_screenObjects[camId][k].m_image.setY(coords.m_y);
	}
	//update the 'darkness' over the player
	m_guiObjects[camId][0].m_image.setX(m_guiObjects[camId][0].m_obj->getPosition().m_x);
	m_guiObjects[camId][0].m_image.setY(m_guiObjects[camId][0].m_obj->getPosition().m_y);
	//=====================================================================================================

	//Handle Animation Updates=============================================================================
	for(int k = 0; k < m_screenObjects[camId].size(); ++k)
	{
		//check if the object has switched animations
		if(m_screenObjects[camId][k].m_image.getStartFrame() != m_screenObjects[camId][k].m_obj->getFirstFrame())
		{
			m_screenObjects[camId][k].recalculateAnim();
		}
		//progress the animation
		m_screenObjects[camId][k].m_image.update(frameTime);
		
		//check for the end of non-looping animation
		if(!m_screenObjects[camId][k].m_obj->isLooping())
		{
			if(m_screenObjects[camId][k].m_image.getCurrentFrame() == m_screenObjects[camId][k].m_obj->getLastFrame())
			{
				//return to idle
				m_screenObjects[camId][k].m_obj->setToIdleAnimation();
			}
		}
	}
	//duplicate for stuff on the gui
	for(int k = 0; k < m_guiObjects[camId].size(); ++k)
	{
		if(m_guiObjects[camId][k].m_image.getStartFrame() != m_guiObjects[camId][k].m_obj->getFirstFrame() ||
			m_guiObjects[camId][k].m_image.getEndFrame() != m_guiObjects[camId][k].m_obj->getLastFrame())
		{
			m_guiObjects[camId][k].recalculateAnim();
		}
		m_guiObjects[camId][k].m_image.update(frameTime);
		if(!m_guiObjects[camId][k].m_obj->isLooping())
		{
			if(m_guiObjects[camId][k].m_image.getCurrentFrame() == m_guiObjects[camId][k].m_obj->getLastFrame())
			{
				//return to idle
				m_guiObjects[camId][k].m_obj->setToIdleAnimation();
			}
		}
	}

	m_cameras[camId].getGui()->setDarkness(screenToGridCoords(m_players[camId].getPosition(), camId));
	//=====================================================================================================

	checkForScreenChange(camId);
}

//play the animation of one gridspace sliding over the other
void ShroomGame::updateCameraSwitchScreen(int camId)
{
	int aniSpeed = 5; //move at 5 pixels per frame
	int objX = m_screenObjects[camId][0].m_obj->getPosition().m_x;
	int objY = m_screenObjects[camId][0].m_obj->getPosition().m_y;
	int screenX = m_screenObjects[camId][0].m_image.getX();
	int screenY = m_screenObjects[camId][0].m_image.getY();

	//if the image positions do not match those of their game objects
	//note: assumes the player is always first on the object list
	if(screenX != objX || screenY != objY)
	{
		if(screenX > objX) //if the imageX doesn't match the objectX
		{
			//move all the new objects 
			for(int i = 0; i < m_screenObjects[camId].size(); ++i)
			{
				int oldX = m_screenObjects[camId][i].m_image.getX();
				m_screenObjects[camId][i].m_image.setX(oldX - aniSpeed);
			}
			//move all the old objects
			for(int i = 0; i < m_hiddenSpaces[camId].m_oldObjs.size(); ++i)
			{
				int oldX = m_hiddenSpaces[camId].m_oldObjs[i].m_image.getX();
				m_hiddenSpaces[camId].m_oldObjs[i].m_image.setX(oldX - aniSpeed);
			}
			//move the backgrounds
			m_cameraBkgs[camId].setX(m_cameraBkgs[camId].getX() - aniSpeed);
			m_hiddenSpaces[camId].m_oldBkg.setX(m_hiddenSpaces[camId].m_oldBkg.getX() - aniSpeed);
		}
		if(screenX < objX)
		{
			for(int i = 0; i < m_screenObjects[camId].size(); ++i)
			{
				int oldX = m_screenObjects[camId][i].m_image.getX();
				m_screenObjects[camId][i].m_image.setX(oldX + aniSpeed);
			}
			for(int i = 0; i < m_hiddenSpaces[camId].m_oldObjs.size(); ++i)
			{
				int oldX = m_hiddenSpaces[camId].m_oldObjs[i].m_image.getX();
				m_hiddenSpaces[camId].m_oldObjs[i].m_image.setX(oldX + aniSpeed);
			}
			m_cameraBkgs[camId].setX(m_cameraBkgs[camId].getX() + aniSpeed);
			m_hiddenSpaces[camId].m_oldBkg.setX(m_hiddenSpaces[camId].m_oldBkg.getX() + aniSpeed);
		}
		if(screenY > objY)
		{
			for(int i = 0; i < m_screenObjects[camId].size(); ++i)
			{
				int oldY = m_screenObjects[camId][i].m_image.getY();
				m_screenObjects[camId][i].m_image.setY(oldY - aniSpeed);
			}
			for(int i = 0; i < m_hiddenSpaces[camId].m_oldObjs.size(); ++i)
			{
				int oldY = m_hiddenSpaces[camId].m_oldObjs[i].m_image.getY();
				m_hiddenSpaces[camId].m_oldObjs[i].m_image.setY(oldY - aniSpeed);
			}
			m_cameraBkgs[camId].setY(m_cameraBkgs[camId].getY() - aniSpeed);
			m_hiddenSpaces[camId].m_oldBkg.setY(m_hiddenSpaces[camId].m_oldBkg.getY() - aniSpeed);
		}
		if(screenY < objY)
		{
			for(int i = 0; i < m_screenObjects[camId].size(); ++i)
			{
				int oldY = m_screenObjects[camId][i].m_image.getY();
				m_screenObjects[camId][i].m_image.setY(oldY + aniSpeed);
			}
			for(int i = 0; i < m_hiddenSpaces[camId].m_oldObjs.size(); ++i)
			{
				int oldY = m_hiddenSpaces[camId].m_oldObjs[i].m_image.getY();
				m_hiddenSpaces[camId].m_oldObjs[i].m_image.setY(oldY + aniSpeed);
			}
			m_cameraBkgs[camId].setY(m_cameraBkgs[camId].getY() + aniSpeed);
			m_hiddenSpaces[camId].m_oldBkg.setY(m_hiddenSpaces[camId].m_oldBkg.getY() + aniSpeed);
		}

		//if the positions are closer than the animation speed
		//snap the images to the object postitions
		if(abs(screenX - objX) <= aniSpeed)
		{
			for(int i = 0; i < m_screenObjects[camId].size(); ++i)
			{
				Point newPos = m_screenObjects[camId][i].m_obj->getPosition();
				if(i != 0)
					newPos = gridToScreenCoords(newPos, camId);
				m_screenObjects[camId][i].m_image.setX(newPos.m_x);
			}
			for(int i = 0; i < m_hiddenSpaces[camId].m_oldObjs.size(); ++i)
			{
				Point newPos = m_hiddenSpaces[camId].m_oldObjs[i].m_obj->getPosition();
				if(i != 0)
					newPos = gridToScreenCoords(newPos, camId);
				m_hiddenSpaces[camId].m_oldObjs[i].m_image.setX(newPos.m_x);
			}
			m_cameraBkgs[camId].setX(m_cameras[camId].getX());
		}
		if(abs(screenY - objY) <= aniSpeed)
		{
			for(int i = 0; i < m_screenObjects[camId].size(); ++i)
			{
				Point newPos = m_screenObjects[camId][i].m_obj->getPosition();
				if(i != 0)
					newPos = gridToScreenCoords(newPos, camId);
				m_screenObjects[camId][i].m_image.setY(newPos.m_y);
			}
			for(int i = 0; i < m_hiddenSpaces[camId].m_oldObjs.size(); ++i)
			{
				Point newPos = m_hiddenSpaces[camId].m_oldObjs[i].m_obj->getPosition();
				if(i != 0)
					newPos = gridToScreenCoords(newPos, camId);
				m_hiddenSpaces[camId].m_oldObjs[i].m_image.setY(newPos.m_y);
			}
			m_cameraBkgs[camId].setY(m_cameras[camId].getY());
		}
		
	}
	else //everything has made it to its end position
	{
		m_cameras[camId].setState(Camera::NORMAL); //return camera to normal
		m_hiddenSpaces[camId].m_oldTexManager.onLostDevice(); //clear the old background
	}
	
	for(int k = 0; k < m_guiObjects[camId].size(); ++k)
	{
		if(m_guiObjects[camId][k].m_image.getStartFrame() != m_guiObjects[camId][k].m_obj->getFirstFrame() ||
			m_guiObjects[camId][k].m_image.getEndFrame() != m_guiObjects[camId][k].m_obj->getLastFrame())
		{
			m_guiObjects[camId][k].recalculateAnim();
		}
		m_guiObjects[camId][k].m_image.update(frameTime);
	}
	//if(m_cameras[camId].getGui()->getPlayerDarkness()->getFirstFrame() != 0)
	{
		
		m_cameras[camId].getGui()->setDarkness(Point(-20,-20));
		m_cameras[camId].getGui()->getPlayerDarkness()->setAnimFrames(0,0);
	}
}

void ShroomGame::updateCameraFadeBlack(int camId)
{
	//animation updates on the gui
	for(int k = 0; k < m_guiObjects[camId].size(); ++k)
	{
		if(m_guiObjects[camId][k].m_image.getStartFrame() != m_guiObjects[camId][k].m_obj->getFirstFrame() ||
			m_guiObjects[camId][k].m_image.getEndFrame() != m_guiObjects[camId][k].m_obj->getLastFrame())
		{
			m_guiObjects[camId][k].recalculateAnim();
		}
		m_guiObjects[camId][k].m_image.update(frameTime);
	}

	bool loadNewImages = false;
	Gridspace* newGridspace = m_cameras[camId].getGridspace();
	
	if(newGridspace->getBiome() != BiomeType::CAVE && m_cameras[camId].getGui()->getBiome() == BiomeType::CAVE)
	{
		//if we're in the cave and starting to fade
		if(m_guiObjects[camId][1].m_image.getCurrentFrame() == m_guiObjects[camId][1].m_image.getStartFrame())
		{
			loadNewImages = true; //trigger a switch of the current background
		}
	}
	else
	{
		//if we're anywhere else and halfway through the fade
		if(m_guiObjects[camId][1].m_image.getCurrentFrame() == m_guiObjects[camId][1].m_image.getEndFrame()/2)
			loadNewImages = true; //trigger a switch of the current background
	}
	
	//Assumes fade tiles to begin at 2nd index on the list
	if(loadNewImages)
	{
		newGridspace->addPlayer(m_players[camId].getId());
		m_cameras[camId].getGui()->setBiome(newGridspace->getBiome());
		m_screenObjects[camId].clear(); //clear all the old objects

		//reserve the first slot in the render list for the player
		Image tempImage;
		TextureManager tempTex;
		ScreenObject player(&(m_players[camId]), tempImage, tempTex, Layer::PLAYER);
		m_screenObjects[camId].push_back(player);

		//add vector slots for all the other objects
		std::vector<GameObject*> gridObjs = m_cameras[camId].getGridspace()->getAllObjects();
		for(int i = 0; i < gridObjs.size(); ++i)
		{
			TexInfo objTex = gridObjs[i]->getTexture();
			Image objImage;
			TextureManager objTexMan;
			ScreenObject screenObj(gridObjs[i], objImage, objTexMan, objTex.m_camLayer);
			m_screenObjects[camId].push_back(screenObj);
		}
		for(int i = 0; i < CameraType::NUM_CAMERAS; ++i)
		{
			if(camId != i)
			{
				if(m_cameras[camId].getGridspace()->hasPlayer(m_players[i].getId()))
				{
					//add ourselves to their render list
					addSecondaryPlayerToCam(i,camId);
					reInitializeTextures(i);
					//add them to our render list.
					addSecondaryPlayerToCam(camId,i);
				}
			}
		}

		//set the new background
		TexInfo Bkg = newGridspace->getTexture();
		m_cameraTextures[camId].onLostDevice();
		m_cameraTextures[camId].initialize(graphics, Bkg.m_resource);
		m_cameraBkgs[camId].initialize(graphics, Bkg.m_frameWidth, Bkg.m_frameHeight, 
				Bkg.m_frameColumns, &(m_cameraTextures[camId]));
		m_cameraBkgs[camId].setX(m_cameras[camId].getX());
		m_cameraBkgs[camId].setY(m_cameras[camId].getY());
		
		reInitializeTextures(camId);

		if(newGridspace->getBiome() == BiomeType::CAVE)
		{
			m_cameras[camId].setState(Camera::NORMAL);
		}
		else
		{
			m_cameras[camId].getGui()->getPlayerDarkness()->setAnimFrames(0,0);
		}
		
	}

	//if we've finished fading, go back to normal
	if(m_guiObjects[camId][1].m_image.getCurrentFrame() == m_guiObjects[camId][1].m_image.getEndFrame() - 1)
	{
		m_cameras[camId].getGui()->endFade();
		m_cameras[camId].setState(Camera::NORMAL);
		m_cameras[camId].getGui()->setBiome(newGridspace->getBiome());
	}
}

//=============================================================================
// Render game items
//=============================================================================
void ShroomGame::render()
{
	graphics->spriteBegin();                // begin drawing sprites

	switch(m_gameState)
	{
	case GameState::START: renderStartState();
		break;
	case GameState::MAIN: renderMainState();
		break;
	case GameState::END: renderEndState();
		break;
	default:break;
	}

    graphics->spriteEnd();                  // end drawing sprites

	if(m_gameState == GameState::MAIN)
	{
		char numStr[30]; 
		Point pos;
	
		//display the current time remaining
		int time =  TOTAL_TIME - (std::time(0) - m_startTime);
		if(time <= 0)
			gotoEndState();
		int minutes = time/60;
		int seconds = time % 60;
		//_itoa(time, numStr,10);
	
		if(minutes < 10)
			strcpy(numStr,"0");
		else
			strcpy(numStr,"");
		_itoa(minutes,numStr + strlen(numStr),10);
		if(seconds < 10)
			strcat_s(numStr, ":0");
		else
			strcat_s(numStr,":");
		_itoa(seconds,numStr + strlen(numStr),10);
		pos = Point(485,380);
		graphics->DrawTextString(pos.m_x,pos.m_y,numStr);

		//draw all the numbers for score and mushroom counts
		for(int i = 0; i < m_cameras.size(); ++i)
		{
			int numEdible = m_players[i].getNumMushrooms(MushroomType::EDIBLE);
			int numPoison = m_players[i].getNumMushrooms(MushroomType::POISONOUS);
			int numPsycho = m_players[i].getNumMushrooms(MushroomType::PSYCHOACTIVE);
			int numGlowing = m_players[i].getNumMushrooms(MushroomType::GLOWING);
			int numLittle = m_players[i].getNumMushrooms(MushroomType::LITTLE_BROWN);
			int score = m_players[i].getScore();


			_itoa(numEdible, numStr,10);
			pos = gridToScreenCoords(9,23,i);
			graphics->DrawTextString(pos.m_x,pos.m_y-3,numStr);

			_itoa(numPoison, numStr,10);
			pos = gridToScreenCoords(13,23,i);
			graphics->DrawTextString(pos.m_x,pos.m_y-3,numStr);

			_itoa(numPsycho, numStr,10);
			pos = gridToScreenCoords(17,23,i);
			graphics->DrawTextString(pos.m_x,pos.m_y-3,numStr);

			_itoa(numGlowing, numStr,10);
			pos = gridToScreenCoords(21,23,i);
			graphics->DrawTextString(pos.m_x,pos.m_y-3,numStr);

			_itoa(numLittle, numStr,10);
			pos = gridToScreenCoords(25,23,i);
			graphics->DrawTextString(pos.m_x,pos.m_y-3,numStr);

			_itoa(score, numStr, 10);
			pos = gridToScreenCoords(12,0,i);
			graphics->DrawTextString(pos.m_x, pos.m_y-1, numStr);
		}
	}

	else if(m_gameState == GameState::END)
	{
		int winner = -1;
		int max = -1;
		char numStr[30]; 
		for(int i = 0; i < m_players.size(); ++i)
		{
			strcpy(numStr,"Player ");
			_itoa(i+1,numStr + strlen(numStr),10);
			strcat_s(numStr, ": ");
			int score = m_players[i].getScore();
			if(score > max)
			{
				max = score;
				winner = i;
			}
			_itoa(score,numStr + strlen(numStr),10);
			graphics->DrawTextString(400,200 + (30*i),numStr);
		}

		strcpy(numStr,"Player ");
		_itoa(winner+1,numStr + strlen(numStr),10);
		strcat_s(numStr, " Wins!");
		graphics->DrawTextString(400,100,numStr);
	}
	
}

void ShroomGame::renderStartState()
{
	m_splashScreen.draw();
}

void ShroomGame::renderMainState()
{
	//draw anything on the layer 'hidden' behind the background tiles
	for(int i = 0; i < m_hiddenSpaces.size(); ++i)
	{
		m_hiddenSpaces[i].m_oldBkg.draw();
		for(int k = 0; k < m_hiddenSpaces[i].m_oldObjs.size(); ++k)
		{
			m_hiddenSpaces[i].m_oldObjs[k].m_image.draw();
		}
	}

	//draw transitioning backgrounds - necessary because there are 4 screens
	for(int i = 0; i < m_cameraBkgs.size(); ++i)
	{
		if(m_cameras[i].getState() == Camera::NEW_GRIDSPACE)
			m_cameraBkgs[i].draw();
	}

	//draw transitioning game objects
	for(int i = 0; i < m_cameras.size(); ++i)
	{
		if(m_cameras[i].getState() == Camera::NEW_GRIDSPACE)
		{
			for(int k = 0; k < m_screenObjects[i].size(); ++k)
			{
				if(m_screenObjects[i][k].m_layer == Layer::BACKGROUND)
					m_screenObjects[i][k].m_image.draw();
			}
			for(int k = 0; k < m_screenObjects[i].size(); ++k)
			{
				if(m_screenObjects[i][k].m_layer == Layer::COLLIDABLE)
					m_screenObjects[i][k].m_image.draw();
			}
			for(int k = 0; k < m_screenObjects[i].size(); ++k)
			{
				if(m_screenObjects[i][k].m_layer == Layer::PLAYER)
					m_screenObjects[i][k].m_image.draw();
			}
			for(int k = 0; k < m_screenObjects[i].size(); ++k)
			{
				if(m_screenObjects[i][k].m_layer == Layer::TOP)
					m_screenObjects[i][k].m_image.draw();
			}
		}
	}

	//draw the backgrounds
	for(int i = 0; i < m_cameraBkgs.size(); ++i)
	{
		if(m_cameras[i].getState() != Camera::NEW_GRIDSPACE)
			m_cameraBkgs[i].draw();
	}


	//draw all the game objects
	for(int i = 0; i < m_cameras.size(); ++i)
	{
		if(m_cameras[i].getState() != Camera::NEW_GRIDSPACE)
		{
			for(int k = 0; k < m_screenObjects[i].size(); ++k)
			{
				if(m_screenObjects[i][k].m_layer == Layer::BACKGROUND)
					m_screenObjects[i][k].m_image.draw();
			}
			for(int k = 0; k < m_screenObjects[i].size(); ++k)
			{
				if(m_screenObjects[i][k].m_layer == Layer::COLLIDABLE)
					m_screenObjects[i][k].m_image.draw();
			}
			for(int k = 0; k < m_screenObjects[i].size(); ++k)
			{
				if(m_screenObjects[i][k].m_layer == Layer::PLAYER)
					m_screenObjects[i][k].m_image.draw();
			}
			for(int k = 0; k < m_screenObjects[i].size(); ++k)
			{
				if(m_screenObjects[i][k].m_layer == Layer::TOP)
					m_screenObjects[i][k].m_image.draw();
			}
		}
	}
	
	m_cameraDivider.draw();
	for(int i = 0; i < m_cameras.size(); ++i)
	{
		for(int k = 0; k < m_guiObjects[i].size(); ++k)
		{
			m_guiObjects[i][k].m_image.draw();
		}
	}

	
}

void ShroomGame::renderEndState()
{
	m_splashScreen.draw();
	m_endPlayer.draw();
}

//=============================================================================
void ShroomGame::setGridToCamera(int camId, BiomeType biomeId, int gridId, CardinalDirection dir)
{
	for(int i = 0; i < m_cameras.size(); ++i)
	{
		if(i != camId && m_cameras[i].getState() != Camera::NORMAL)
			return;
	}

	//grab the 'old' background, set it to be underneath the new one
	if(dir != CardinalDirection::NUM_DIRECTIONS) //if this isn't the very first gridspace on the camera
	{
		TexInfo oldTex = m_cameras[camId].getGridspace()->getTexture();
		//m_hiddenSpaces[camId].m_oldTexManager.onLostDevice(); //apparently calling this more than once in a row causes explosions...
		m_hiddenSpaces[camId].m_oldTexManager.initialize(graphics, oldTex.m_resource);
		m_hiddenSpaces[camId].m_oldBkg.initialize(graphics, oldTex.m_frameWidth, oldTex.m_frameHeight, 
			oldTex.m_frameColumns, &(m_hiddenSpaces[camId].m_oldTexManager));

		//set the old render list to the 'sliding away' render list, sans the player
		m_hiddenSpaces[camId].m_oldObjs.clear();
		for(int i = 0; i < m_screenObjects[camId].size(); ++i)
		{
			GameObject* obj = m_screenObjects[camId][i].m_obj;
			TexInfo objTex = obj->getTexture();
			Image objImage;
			TextureManager objTexMan;
			ScreenObject screenObj(obj, objImage, objTexMan, objTex.m_camLayer);
			m_hiddenSpaces[camId].m_oldObjs.push_back(screenObj);
		}
		reInitializeHiddens(camId);

		//set the position of the 'old' backgrounds
		m_hiddenSpaces[camId].m_oldBkg.setY(m_cameraBkgs[camId].getY());
		m_hiddenSpaces[camId].m_oldBkg.setX(m_cameraBkgs[camId].getX());

		//remove the playerFrom old gridspace
		for(int i = 0; i < CameraType::NUM_CAMERAS; ++i)
		{
			if(camId != i)
			{
				//if the player is present on any other screens
				if(m_cameras[i].getGridspace()->hasPlayer(m_players[camId].getId()))
				{
					for(int k = 1; k < m_screenObjects[i].size(); ++k)
					{
						//find the player in the render list of the other screens
						if(m_screenObjects[i][k].m_layer == Layer::PLAYER)
						{
							if(m_screenObjects[i][k].m_obj->getPosition().m_x == m_players[camId].getPosition().m_x &&
								m_screenObjects[i][k].m_obj->getPosition().m_y == m_players[camId].getPosition().m_y)
							{
								std::vector<ScreenObject>::iterator itr = m_screenObjects[i].begin(); 
								itr += k;
								m_screenObjects[i].erase(itr); //remove
								reInitializeTextures(i);
							}
						}
					}
				}
			}
		}
		//tell the gridspace that the player is no longer there
		m_cameras[camId].getGridspace()->removePlayer(m_players[camId].getId());
		toggleLantern(false, camId);
	}

	//get the new image info, add it to the camera
	Gridspace* newGridspace = theMap.getGridspace(biomeId, gridId);
	if(newGridspace->getNumPlayers() == 0)
	{
		newGridspace->cleanUpDeadObjects();
		newGridspace->checkSpawnedMushrooms(); //fill in 
	}
	TexInfo Bkg = newGridspace->getTexture();
	m_cameraTextures[camId].onLostDevice();
	m_cameraTextures[camId].initialize(graphics, Bkg.m_resource);
	m_cameraBkgs[camId].initialize(graphics, Bkg.m_frameWidth, Bkg.m_frameHeight, 
			Bkg.m_frameColumns, &(m_cameraTextures[camId]));
	m_cameraBkgs[camId].setX(m_cameras[camId].getX());
	m_cameraBkgs[camId].setY(m_cameras[camId].getY());

	//get the new gridspace, set it to the camera
	m_cameras[camId].setGridspace(theMap.getGridspace(biomeId, gridId));
	m_cameras[camId].getGridspace()->addPlayer(m_players[camId].getId());
	m_screenObjects[camId].clear(); //clear all the old objects

	//reserve the first slot in the render list for the player
	Image tempImage;
	TextureManager tempTex;
	ScreenObject player(&(m_players[camId]), tempImage, tempTex, Layer::PLAYER);
	m_screenObjects[camId].push_back(player);

	//add vector slots for all the other objects
	std::vector<GameObject*> gridObjs = newGridspace->getAllObjects();
	for(int i = 0; i < gridObjs.size(); ++i)
	{
		TexInfo objTex = gridObjs[i]->getTexture();
		Image objImage;
		TextureManager objTexMan;
		ScreenObject screenObj(gridObjs[i], objImage, objTexMan, objTex.m_camLayer);
		m_screenObjects[camId].push_back(screenObj);
	}

	//if there is another player in the new space
	if(dir != CardinalDirection::NUM_DIRECTIONS) //do this check because other gridspaces know they have players but may not have render lists yet
	{
		for(int i = 0; i < CameraType::NUM_CAMERAS; ++i)
		{
			if(camId != i)
			{
				if(m_cameras[camId].getGridspace()->hasPlayer(m_players[i].getId()))
				{
					//add ourselves to their render list
					addSecondaryPlayerToCam(i,camId);
					reInitializeTextures(i);
					//add them to our render list.
					addSecondaryPlayerToCam(camId,i);
				}
			}
		}
	}
	reInitializeTextures(camId);
	
	
	//Move all the new objects off screen for animation, depending on which way they're moving
	int playerX = m_players[camId].getPosition().m_x;
	int playerY = m_players[camId].getPosition().m_y;
	switch(dir)
	{
		case CardinalDirection::NORTH:
			//move the player object to the opposite side of the screen
			m_players[camId].setPosition(playerX, m_cameras[camId].getY() + m_cameras[camId].getHeight() - (TILE_SIZE*2));
			m_screenObjects[camId][0].m_image.setY(m_cameras[camId].getY() - (TILE_SIZE*2));
			m_cameraBkgs[camId].setY(m_cameras[camId].getY() - m_cameras[camId].getHeight()); //move our current background all the way off screen
			for(int k = 1; k < m_screenObjects[camId].size(); ++k) //move all the other objects with the background
			{
				int y = gridToScreenCoords(m_screenObjects[camId][k].m_obj->getPosition(), camId).m_y;
				y -= m_cameras[camId].getHeight();
				m_screenObjects[camId][k].m_image.setY(y);
			}
			break;
		case CardinalDirection::EAST:
			m_players[camId].setPosition(m_cameras[camId].getX() + (TILE_SIZE + 1), playerY);
			m_screenObjects[camId][0].m_image.setX(m_cameras[camId].getX() +m_cameras[camId].getWidth() - (TILE_SIZE + 1));
			m_cameraBkgs[camId].setX(m_cameras[camId].getX() + m_cameras[camId].getWidth() - TILE_SIZE);
			for(int k = 1; k < m_screenObjects[camId].size(); ++k)
			{
				int x = gridToScreenCoords(m_screenObjects[camId][k].m_obj->getPosition(), camId).m_x;
				x += m_cameras[camId].getWidth();
				m_screenObjects[camId][k].m_image.setX(x);
			}
			break;
		case CardinalDirection::SOUTH:
			m_players[camId].setPosition(playerX, m_cameras[camId].getY() + (TILE_SIZE + 1));
			m_screenObjects[camId][0].m_image.setY(m_cameras[camId].getY() + m_cameras[camId].getHeight() - (TILE_SIZE + 1));
			m_cameraBkgs[camId].setY(m_cameras[camId].getY() + m_cameras[camId].getHeight() - TILE_SIZE);
			for(int k = 1; k < m_screenObjects[camId].size(); ++k)
			{
				int y = gridToScreenCoords(m_screenObjects[camId][k].m_obj->getPosition(), camId).m_y;
				y += m_cameras[camId].getHeight();
				m_screenObjects[camId][k].m_image.setY(y);
			}
			break;
		case CardinalDirection::WEST:
			m_players[camId].setPosition(m_cameras[camId].getX() + m_cameras[camId].getWidth() - (TILE_SIZE*2), playerY);
			m_screenObjects[camId][0].m_image.setX(m_cameras[camId].getX() - (TILE_SIZE*2));
			m_cameraBkgs[camId].setX(m_cameras[camId].getX() - m_cameras[camId].getWidth());
			for(int k = 1; k < m_screenObjects[camId].size(); ++k)
			{
				int x = gridToScreenCoords(m_screenObjects[camId][k].m_obj->getPosition(), camId).m_x;
				x -= m_cameras[camId].getWidth();
				m_screenObjects[camId][k].m_image.setX(x);
			}
			break;
		default: break;
	}
}

void ShroomGame::setBiomeToCamera(int camId, BiomeType biomeId, int gridId, CardinalDirection dir)
{
	Gridspace* newGridspace = theMap.getGridspace(biomeId, gridId);
	if(newGridspace->getNumPlayers() == 0)
	{
		newGridspace->cleanUpDeadObjects();
		newGridspace->checkSpawnedMushrooms();
	}

	m_cameras[camId].getGui()->beginFade();
	toggleLantern(false, camId);
	
	if(m_cameras[camId].getGridspace()->getBiome() == BiomeType::SWAMP && biomeId != BiomeType::SWAMP)
		toggleBugSpray(false, camId);
	else if(m_cameras[camId].getGridspace()->getBiome() != BiomeType::SWAMP && biomeId == BiomeType::SWAMP)
		toggleBugSpray(false, camId);

	//remove the playerFrom old gridspace
	for(int i = 0; i < CameraType::NUM_CAMERAS; ++i)
	{
		if(camId != i)
		{
			//if the player is present on any other screens
			if(m_cameras[i].getGridspace()->hasPlayer(m_players[camId].getId()))
			{
				for(int k = 1; k < m_screenObjects[i].size(); ++k)
				{
					//find the player in the render list of the other screens
					if(m_screenObjects[i][k].m_layer == Layer::PLAYER)
					{
						if(m_screenObjects[i][k].m_obj->getPosition().m_x == m_players[camId].getPosition().m_x &&
							m_screenObjects[i][k].m_obj->getPosition().m_y == m_players[camId].getPosition().m_y)
						{
							std::vector<ScreenObject>::iterator itr = m_screenObjects[i].begin(); 
							itr += k;
							m_screenObjects[i].erase(itr); //remove
							reInitializeTextures(i);
						}
					}
				}
			}
		}
	}

	//tell the gridspace that the player is no longer there
	m_cameras[camId].getGridspace()->removePlayer(m_players[camId].getId());

	//move the player object to the opposite side of the screen
	int playerX = m_players[camId].getPosition().m_x;
	int playerY = m_players[camId].getPosition().m_y;
	switch(dir)
	{
		case CardinalDirection::NORTH:
			m_players[camId].setPosition(playerX, m_cameras[camId].getY() + m_cameras[camId].getHeight() - TILE_SIZE*2);
			break;
		case CardinalDirection::EAST:
			m_players[camId].setPosition(m_cameras[camId].getX() + TILE_SIZE*2, playerY);
			break;
		case CardinalDirection::SOUTH:
			m_players[camId].setPosition(playerX, m_cameras[camId].getY() + TILE_SIZE*2);
			break;
		case CardinalDirection::WEST:
			m_players[camId].setPosition(m_cameras[camId].getX() + m_cameras[camId].getWidth() - TILE_SIZE*2, playerY);
			break;
		default: break;
	}
	
	//get the new gridspace, set it to the camera
	m_cameras[camId].setGridspace(theMap.getGridspace(biomeId, gridId));
	

	//Play the background song for the biome if it's the top left player that switched biomes
	if ( camId == 0 )
	{
		soundVault.playBackgroundSong( biomeId );
	}
}

void ShroomGame::reInitializeTextures(int camId)
{
	//initialize the player texture
	TexInfo pTex = m_players[camId].getTexture();
	m_screenObjects[camId][0].m_texManager.initialize(graphics, pTex.m_resource);
	m_screenObjects[camId][0].m_image.initialize(graphics, pTex.m_frameWidth, pTex.m_frameHeight, pTex.m_frameColumns, &(m_screenObjects[camId][0].m_texManager));
	m_screenObjects[camId][0].m_image.setX(m_screenObjects[camId][0].m_obj->getPosition().m_x);
	m_screenObjects[camId][0].m_image.setY(m_screenObjects[camId][0].m_obj->getPosition().m_y);
	m_screenObjects[camId][0].m_image.setFrameDelay(FRAME_DELAY);
	m_screenObjects[camId][0].m_image.setLoop(true);

	//initialize all the other textures
	//Note: ASSUMES THAT THE PLAYER IS AT THE 1st slot in m_screenObjects
	for(int i = 1; i < m_screenObjects[camId].size(); i++)
	{
		TexInfo objTex = m_screenObjects[camId][i].m_obj->getTexture();
		m_screenObjects[camId][i].m_texManager.initialize(graphics, objTex.m_resource);
		m_screenObjects[camId][i].m_image.initialize(graphics, objTex.m_frameWidth, objTex.m_frameHeight, objTex.m_frameColumns, &(m_screenObjects[camId][i].m_texManager));
		Point pos = m_screenObjects[camId][i].m_obj->getPosition();
		pos = gridToScreenCoords(pos, camId);
		m_screenObjects[camId][i].m_image.setX(pos.m_x);
		m_screenObjects[camId][i].m_image.setY(pos.m_y);
		m_screenObjects[camId][i].m_image.setFrameDelay(FRAME_DELAY);
		m_screenObjects[camId][i].m_image.setLoop(objTex.m_loops);
		m_screenObjects[camId][i].m_image.setFrames(m_screenObjects[camId][i].m_obj->getFirstFrame(), m_screenObjects[camId][i].m_obj->getLastFrame());
		m_screenObjects[camId][i].m_image.setCurrentFrame(m_screenObjects[camId][i].m_image.getStartFrame());
	}
}

//same as above, no need for player special case
void ShroomGame::reInitializeGui(int camId)
{
	for(int i = 0; i < m_guiObjects[camId].size(); i++)
	{
		TexInfo objTex = m_guiObjects[camId][i].m_obj->getTexture();
		m_guiObjects[camId][i].m_texManager.initialize(graphics, objTex.m_resource);
		m_guiObjects[camId][i].m_image.initialize(graphics, objTex.m_frameWidth, objTex.m_frameHeight, objTex.m_frameColumns, &(m_guiObjects[camId][i].m_texManager));
		Point pos = m_guiObjects[camId][i].m_obj->getPosition();
		if( i != 0)
			pos = gridToScreenCoords(pos, camId);
		m_guiObjects[camId][i].m_image.setX(pos.m_x);
		m_guiObjects[camId][i].m_image.setY(pos.m_y);
		m_guiObjects[camId][i].m_image.setFrameDelay(GUI_FRAME_DELAY);
		m_guiObjects[camId][i].m_image.setLoop(objTex.m_loops);
		m_guiObjects[camId][i].m_image.setFrames(m_guiObjects[camId][i].m_obj->getFirstFrame(), m_guiObjects[camId][i].m_obj->getLastFrame());
		m_guiObjects[camId][i].m_image.setCurrentFrame(m_guiObjects[camId][i].m_image.getStartFrame());
	}
}

//same as above, but deals with transition tiles
void ShroomGame::reInitializeHiddens(int camId)
{
	for(int i = 0; i < m_hiddenSpaces[camId].m_oldObjs.size(); i++)
	{
		TexInfo objTex = m_hiddenSpaces[camId].m_oldObjs[i].m_obj->getTexture();
		m_hiddenSpaces[camId].m_oldObjs[i].m_texManager.initialize(graphics, objTex.m_resource);
		m_hiddenSpaces[camId].m_oldObjs[i].m_image.initialize(graphics, objTex.m_frameWidth, objTex.m_frameHeight, objTex.m_frameColumns, &(m_hiddenSpaces[camId].m_oldObjs[i].m_texManager));
		Point pos = m_hiddenSpaces[camId].m_oldObjs[i].m_obj->getPosition();
		pos = gridToScreenCoords(pos, camId);
		m_hiddenSpaces[camId].m_oldObjs[i].m_image.setX(pos.m_x);
		m_hiddenSpaces[camId].m_oldObjs[i].m_image.setY(pos.m_y);
		m_hiddenSpaces[camId].m_oldObjs[i].m_image.setFrameDelay(FRAME_DELAY);
		m_hiddenSpaces[camId].m_oldObjs[i].m_image.setLoop(objTex.m_loops);
		m_hiddenSpaces[camId].m_oldObjs[i].m_image.setFrames(m_hiddenSpaces[camId].m_oldObjs[i].m_obj->getFirstFrame(), m_hiddenSpaces[camId].m_oldObjs[i].m_obj->getLastFrame());
		m_hiddenSpaces[camId].m_oldObjs[i].m_image.setCurrentFrame(m_hiddenSpaces[camId].m_oldObjs[i].m_image.getStartFrame());
	}
}

void ShroomGame::addSecondaryPlayerToCam(int camId, int playerId)
{
	//add ourselves to their render list
	Image ourImage;
	TextureManager ourTex;
	ScreenObject us(&(m_players[playerId]), ourImage, ourTex, Layer::PLAYER);
	m_screenObjects[camId].push_back(us);

	//initialize our texture
	int index = m_screenObjects[camId].size() - 1;
	TexInfo ourTexInfo = m_screenObjects[camId][index].m_obj->getTexture();
	m_screenObjects[camId][index].m_texManager.initialize(graphics, ourTexInfo.m_resource);
	m_screenObjects[camId][index].m_image.initialize(graphics, ourTexInfo.m_frameWidth, ourTexInfo.m_frameHeight, 
		ourTexInfo.m_frameColumns, &(m_screenObjects[camId][index].m_texManager));
	Point origPos = screenToGridCoords(m_screenObjects[camId][index].m_obj->getPosition(), camId); //convert our coords to the coords on the other screen
	Point newPos = gridToScreenCoords(origPos, camId);
	m_screenObjects[camId][index].m_image.setX(newPos.m_x);
	m_screenObjects[camId][index].m_image.setY(newPos.m_y);
}

//=============================================================================
//check to see if the player has wandered into a new gridspace
void ShroomGame::checkForScreenChange(int camId)
{
	Gridspace* tempG = m_cameras[camId].getGridspace();

	//if the player is off screen
	if(m_players[camId].getPosition().m_x <= m_cameras[camId].getX() + TILE_SIZE)
	{
		//if there is a gridspace to go to
		if(tempG->hasConnectionAt(CardinalDirection::WEST))
		{
			//set the new gridspace to the camera, and start the animation
			Gridspace::GridConnection con = tempG->getConnectionAt(CardinalDirection::WEST);
			if(tempG->getBiome() == con.m_biome)
			{
				m_cameras[camId].setState(Camera::NEW_GRIDSPACE);
				setGridToCamera(camId, con.m_biome, con.m_toGridId, CardinalDirection::WEST);
			}
			else
			{
				m_cameras[camId].setState(Camera::FADE_BLACK);
				setBiomeToCamera(camId, con.m_biome, con.m_toGridId, CardinalDirection::WEST);
			}
			
		}
	}
	else if(m_players[camId].getPosition().m_y <= m_cameras[camId].getY() + TILE_SIZE)
	{
		if(tempG->hasConnectionAt(CardinalDirection::NORTH))
		{
			Gridspace::GridConnection con = tempG->getConnectionAt(CardinalDirection::NORTH);
			if(tempG->getBiome() == con.m_biome)
			{
				m_cameras[camId].setState(Camera::NEW_GRIDSPACE);
				setGridToCamera(camId, con.m_biome, con.m_toGridId, CardinalDirection::NORTH);
			}
			else
			{
				m_cameras[camId].setState(Camera::FADE_BLACK);
				setBiomeToCamera(camId, con.m_biome, con.m_toGridId, CardinalDirection::NORTH);
			}
			
		}
	}
	else if(m_players[camId].getPosition().m_x >= m_cameras[camId].getX() + m_cameras[camId].getWidth() - TILE_SIZE)
	{
		if(tempG->hasConnectionAt(CardinalDirection::EAST))
		{
			
			Gridspace::GridConnection con = tempG->getConnectionAt(CardinalDirection::EAST);
			if(tempG->getBiome() == con.m_biome)
			{
				m_cameras[camId].setState(Camera::NEW_GRIDSPACE);
				setGridToCamera(camId, con.m_biome, con.m_toGridId, CardinalDirection::EAST);
			}
			else
			{
				m_cameras[camId].setState(Camera::FADE_BLACK);
				setBiomeToCamera(camId, con.m_biome, con.m_toGridId, CardinalDirection::EAST);
			}
			
		}
	}
	else if(m_players[camId].getPosition().m_y >= m_cameras[camId].getY() + m_cameras[camId].getHeight() - TILE_SIZE)
	{
		if(tempG->hasConnectionAt(CardinalDirection::SOUTH))
		{
			
			Gridspace::GridConnection con = tempG->getConnectionAt(CardinalDirection::SOUTH);
			if(tempG->getBiome() == con.m_biome)
			{
				m_cameras[camId].setState(Camera::NEW_GRIDSPACE);
				setGridToCamera(camId, con.m_biome, con.m_toGridId, CardinalDirection::SOUTH);
			}
			else
			{
				m_cameras[camId].setState(Camera::FADE_BLACK);
				setBiomeToCamera(camId, con.m_biome, con.m_toGridId, CardinalDirection::SOUTH);
			}
			
		}
	}
}

//Convert screen coordinates to corresponding tile on the map
//note: needs to be updated to handle 4 cameras
//=========================================================================
Point ShroomGame::screenToGridCoords(int x, int y, int camId)
{
	Point gridPoint = INVALID_POS;
	switch(camId)
	{
	case CameraType::TOP_LEFT: 
		{
			gridPoint.m_x = x/TILE_SIZE;
			gridPoint.m_y = y/TILE_SIZE;
		}
		break;
	case CameraType::TOP_RIGHT:
		{
			gridPoint.m_x = (x/TILE_SIZE) - CAM_TILES_X;
			gridPoint.m_y = y/TILE_SIZE;
		}
		break;
	case CameraType::BOTTOM_LEFT:
		{
			gridPoint.m_x = x/TILE_SIZE;
			gridPoint.m_y = (y/TILE_SIZE) - CAM_TILES_Y;
		}
		break;
	case CameraType::BOTTOM_RIGHT:
		{
			gridPoint.m_x = (x/TILE_SIZE) - CAM_TILES_X;
			gridPoint.m_y = (y/TILE_SIZE) - CAM_TILES_Y;
		}
		break;
	default: break;
	}
	return gridPoint;
}

Point ShroomGame::screenToGridCoords(Point screenPoint, int camId)
{
	return screenToGridCoords(screenPoint.m_x, screenPoint.m_y, camId);
}

Point ShroomGame::gridToScreenCoords(int x, int y, int camId)
{
	Point screenPoint = INVALID_POS;
	switch(camId)
	{
	case CameraType::TOP_LEFT:
		{
			screenPoint.m_x = x*TILE_SIZE;
			screenPoint.m_y = y*TILE_SIZE;
		}
		break;
	case CameraType::TOP_RIGHT:
		{
			screenPoint.m_x = (x + CAM_TILES_X) * TILE_SIZE;
			screenPoint.m_y = y*TILE_SIZE;
		}
		break;
	case CameraType::BOTTOM_LEFT:
		{
			screenPoint.m_x = x * TILE_SIZE;
			screenPoint.m_y = (y + CAM_TILES_Y) * TILE_SIZE;
		}
		break;
	case CameraType::BOTTOM_RIGHT:
		{
			screenPoint.m_x = (x + CAM_TILES_X) * TILE_SIZE;
			screenPoint.m_y = (y + CAM_TILES_Y) * TILE_SIZE;
		}
		break;
	default:break;
	}
	
	return screenPoint;
}

Point ShroomGame::gridToScreenCoords(Point gridPoint, int camId)
{
	return gridToScreenCoords(gridPoint.m_x, gridPoint.m_y, camId);
}

//takes screen coordinates in pixels and returns which 
//player's camera they're in
int ShroomGame::getCameraLoc(int x, int y)
{
	if(x < 512 && y < 384)
		return 0;
	else if(y < 384)
		return 1;
	else if(x < 512)
		return 2;
	else
		return 3;

}
//=============================================================================


bool ShroomGame::canPlayerMove(int camId, CardinalDirection dir)
{
	//don't let the player fall off the edge where there is no more map
	//if(!m_cameras[camId].getGridspace()->hasConnectionAt(dir))
	//{	
		switch(dir) //for each direction that does not have a connecting gridspace
		{
		case CardinalDirection::WEST:
			{//are we off the edge?
			if(m_players[camId].getPosition().m_x <= m_cameras[camId].getX())
				return false;
			}
			break;
		case CardinalDirection::EAST:
			{
			if(m_players[camId].getPosition().m_x >= m_cameras[camId].getX() + m_cameras[camId].getWidth() - (m_players[camId].getWidth()*TILE_SIZE))
				return false;
			}
			break;
		case CardinalDirection::NORTH:
			{
			if(m_players[camId].getPosition().m_y <= m_cameras[camId].getY())
				return false;
			}
			break;
		case CardinalDirection::SOUTH:
			{
				if(m_players[camId].getPosition().m_y >= m_cameras[camId].getY() + m_cameras[camId].getHeight() - (m_players[camId].getHeight()*TILE_SIZE))
				return false;
			}
			break;
		default: break;
		}
	//}

	//check for running into objects
	int westX = m_players[camId].getPosition().m_x;
	int eastX = westX + m_players[camId].getWidth() * TILE_SIZE;
	int northY = m_players[camId].getPosition().m_y;
	int southY = northY + m_players[camId].getHeight() * TILE_SIZE;
	Point pGridPos = screenToGridCoords(westX, northY, camId);

	switch(dir)
	{
	case CardinalDirection::NORTH:
	{
		//check all the tiles we're about to move into
		for(int i = 0; i <= m_players[camId].getWidth(); ++i)
		{
			Point check(pGridPos.m_x + i, pGridPos.m_y - 1);
			//if there's a mushroom
			if(m_cameras[camId].getGridspace()->hasMushroom(check.m_x,check.m_y))
			{
				Point checkPixels = gridToScreenCoords(check.m_x, check.m_y, camId);
				if(northY - (checkPixels.m_y + TILE_SIZE) <= PLAYER_SPEED) 
				{
					if(eastX > checkPixels.m_x || westX < checkPixels.m_x + TILE_SIZE)
						addMushToPlayer(camId, check.m_x, check.m_y);//pick up the mushroom
				}
			}
			//if there's anything else collidable
			if(m_cameras[camId].getGridspace()->hasCollisionTile(check.m_x, check.m_y))
			{
				Point checkPixels = gridToScreenCoords(check.m_x, check.m_y, camId);
				if(northY - (checkPixels.m_y + TILE_SIZE) <= PLAYER_SPEED) 
				{
					if(eastX > checkPixels.m_x || westX < checkPixels.m_x + TILE_SIZE)
						return false;
				}
			}
			
		}
	}
	break;
	case CardinalDirection::SOUTH:
	{
		for(int i = 0; i <= m_players[camId].getWidth(); ++i)
		{
			Point check(pGridPos.m_x + i, pGridPos.m_y + m_players[camId].getHeight());
			if(m_cameras[camId].getGridspace()->hasMushroom(check.m_x, check.m_y))
			{
				Point checkPixels = gridToScreenCoords(check.m_x, check.m_y, camId);
				if(checkPixels.m_y - (southY) <= PLAYER_SPEED)
				{
					if(eastX > checkPixels.m_x || westX < checkPixels.m_x + TILE_SIZE)
						addMushToPlayer(camId, check.m_x, check.m_y);
				}
			}
			if(m_cameras[camId].getGridspace()->hasCollisionTile(check.m_x, check.m_y))
			{
				Point checkPixels = gridToScreenCoords(check.m_x, check.m_y, camId);
				if(checkPixels.m_y - (southY) <= PLAYER_SPEED)
				{
					if(eastX > checkPixels.m_x || westX < checkPixels.m_x + TILE_SIZE)
						return false;
				}
			}

			if(m_cameras[camId].getGridspace()->hasMushroom(check.m_x, check.m_y + 1))
			{
				Point checkPixels = gridToScreenCoords(check.m_x, check.m_y + 1, camId);
				if(checkPixels.m_y - (southY) <= PLAYER_SPEED)
				{
					if(eastX > checkPixels.m_x || westX < checkPixels.m_x + TILE_SIZE)
						addMushToPlayer(camId, check.m_x, check.m_y + 1);
				}
			}
			if(m_cameras[camId].getGridspace()->hasCollisionTile(check.m_x, check.m_y + 1))
			{
				Point checkPixels = gridToScreenCoords(check.m_x, check.m_y + 1, camId);
				if(checkPixels.m_y - (southY) <= PLAYER_SPEED)
				{
					if(eastX > checkPixels.m_x || westX < checkPixels.m_x + TILE_SIZE)
						return false;
				}
			}
			
		}
	}
	break;
	case CardinalDirection::EAST:
	{
		for(int i = 0; i <= m_players[camId].getHeight(); ++i)
		{
			Point check(pGridPos.m_x + m_players[camId].getWidth(), pGridPos.m_y + i);
			if(m_cameras[camId].getGridspace()->hasMushroom(check.m_x, check.m_y))
			{
				Point checkPixels = gridToScreenCoords(check.m_x, check.m_y, camId);
				if(checkPixels.m_x - (eastX) <= PLAYER_SPEED)
				{
					if(northY < checkPixels.m_y + TILE_SIZE || southY > checkPixels.m_y)
						addMushToPlayer(camId, check.m_x, check.m_y);
				}
			}
			if(m_cameras[camId].getGridspace()->hasCollisionTile(check.m_x, check.m_y))
			{
				Point checkPixels = gridToScreenCoords(check.m_x, check.m_y, camId);
				if(checkPixels.m_x - (eastX) <= PLAYER_SPEED)
				{
					if(northY < checkPixels.m_y + TILE_SIZE || southY > checkPixels.m_y)
						return false;
				}
			}

			if(m_cameras[camId].getGridspace()->hasMushroom(check.m_x + 1, check.m_y))
			{
				Point checkPixels = gridToScreenCoords(check.m_x + 1, check.m_y, camId);
				if(checkPixels.m_x - (eastX) <= PLAYER_SPEED)
				{
					if(northY < checkPixels.m_y + TILE_SIZE || southY > checkPixels.m_y)
						addMushToPlayer(camId, check.m_x + 1, check.m_y);
				}
			}
			if(m_cameras[camId].getGridspace()->hasCollisionTile(check.m_x + 1, check.m_y))
			{
				Point checkPixels = gridToScreenCoords(check.m_x + 1, check.m_y, camId);
				if(checkPixels.m_x - (eastX) <= PLAYER_SPEED)
				{
					if(northY < checkPixels.m_y + TILE_SIZE || southY > checkPixels.m_y)
						return false;
				}
			}

			
		}
	}
	break;
	case CardinalDirection::WEST:
	{
		for(int i = 0; i <= m_players[camId].getHeight(); ++i)
		{
			Point check(pGridPos.m_x - 1, pGridPos.m_y + i);
			if(m_cameras[camId].getGridspace()->hasMushroom(check.m_x, check.m_y))
			{
				Point checkPixels = gridToScreenCoords(check.m_x, check.m_y, camId);
				if(westX - (checkPixels.m_x + TILE_SIZE) <= 2)
				{
					if(northY < checkPixels.m_y + TILE_SIZE || southY > checkPixels.m_y)
						addMushToPlayer(camId, check.m_x, check.m_y);
				}
			}
			if(m_cameras[camId].getGridspace()->hasCollisionTile(check.m_x, check.m_y))
			{
				Point checkPixels = gridToScreenCoords(check.m_x, check.m_y, camId);
				if(westX - (checkPixels.m_x + TILE_SIZE) <= 2)
				{
					if(northY < checkPixels.m_y + TILE_SIZE || southY > checkPixels.m_y)
						return false;
				}
			}
		}
	}
	break;
	default:break;
	}
	
	return true;
}

void ShroomGame::playerUseKey(int camId)
{
	std::vector<Point> tiles = getTriggeredTile(camId); //all the tiles directly in the player's path
	bool playActionAnim = true;

	for(int i = 0; i < tiles.size(); ++i)
	{
		//Gridspace* curGrid = m_cameras[camId].getGridspace(); TODO - sub this in?
		if(m_cameras[camId].getGridspace()->hasChest(tiles[i].m_x, tiles[i].m_y))
		{
			ItemType PlayerType = m_players[camId].getItem();
			ItemType ChestType = m_cameras[camId].getGridspace()->getChest(tiles[i].m_x, tiles[i].m_y)->getItem();
			ItemType temp = m_cameras[camId].getGridspace()->getChest(tiles[i].m_x, tiles[i].m_y)->swapItem(m_players[camId].getItem());
			m_players[camId].setItem(temp);
			m_cameras[camId].getGui()->setItem(temp);
			//reInitializeGui(camId);
			playActionAnim = false;
		}

		//if we're touching an npc
		else if(m_cameras[camId].getGridspace()->hasNpc(tiles[i].m_x, tiles[i].m_y))
		{
			Npc* theNpc = m_cameras[camId].getGridspace()->getNpc(tiles[i].m_x, tiles[i].m_y);
			//if we have mushrooms for them
			if(m_players[camId].getNumMushrooms(theNpc->getMushroomType()) > 0)
			{
				theNpc->setToSuccessAnimation();
				m_players[camId].cashInMushrooms(theNpc->getMushroomType());
			}
			else
			{
				theNpc->setToFailureAnimation();
			}
			playActionAnim = false; //do not trigger the special animation
		}

		//if we're touching a mushroom
		else if(m_cameras[camId].getGridspace()->hasMushroom(tiles[i].m_x, tiles[i].m_y))
		{
			Mushroom* theMush = m_cameras[camId].getGridspace()->getMushroom(tiles[i].m_x, tiles[i].m_y);
			//if the mushroom is potentially buried and we have a shovel to dig it up
			if(theMush->getType() == MushroomType::EDIBLE && m_players[camId].getItem() == ItemType::SHOVEL)
				theMush->setPickable(true);
		}
		//if we're touching a cow
		else if(m_cameras[camId].getGridspace()->hasCow(tiles[i].m_x, tiles[i].m_y))
		{
			Cow* theCow = m_cameras[camId].getGridspace()->getCow(tiles[i].m_x, tiles[i].m_y);
			if(m_players[camId].getItem() == ItemType::COW_FEED)
				theCow->feed();
			else
				theCow->setToFailureAnimation();
		}
		//check if we're in the cave and trying to use a lantern
		else if(m_cameras[camId].getGridspace()->getBiome() == BiomeType::CAVE)
		{
			if(m_players[camId].getItem() == ItemType::LANTERN)
			{
				toggleLantern(true, camId);
			}
		}
		//check if we're in the swamp and trying to use bug spray
		else if(m_cameras[camId].getGridspace()->getBiome() == BiomeType::SWAMP)
		{
			if(m_players[camId].getItem() == ItemType::BUG_SPRAY)
				toggleBugSpray(true, camId);
		}
	}

	//only play the action flail if an item is being used (or attempted)
	if(playActionAnim)
	{
		m_players[camId].setToActionAnimation();
	}
}

//get all the adjacent tiles in the direction the player is facing
std::vector<Point> ShroomGame::getTriggeredTile(int playerId)
{
	std::vector<Point> tiles;
	CardinalDirection dir = m_players[playerId].getFacingDir();
	Point curPos = screenToGridCoords(m_players[playerId].getPosition(), playerId);

	switch(dir)
	{
	case CardinalDirection::NORTH:
		{
			//check each tile along the proper side of the player
			for(int i = 0; i < m_players[playerId].getWidth() +1; ++i)
			{
				int x = curPos.m_x + i;
				int y = curPos.m_y - 1;
				tiles.push_back(Point(x,y));
			}
		}
		break;
		case CardinalDirection::SOUTH:
		{
			for(int i = 0; i < m_players[playerId].getWidth() +1; ++i)
			{
				int x = curPos.m_x + i;
				int y = curPos.m_y + m_players[playerId].getHeight();
				tiles.push_back(Point(x,y));
				tiles.push_back(Point(x,y+1));
			}
		}
		break;
		case CardinalDirection::EAST:
		{
			for(int i = 0; i < m_players[playerId].getHeight() +1; ++i)
			{
				int x = curPos.m_x + m_players[playerId].getWidth();
				int y = curPos.m_y + i;
				tiles.push_back(Point(x,y));
				tiles.push_back(Point(x+1,y));
			}
		}
		break;
		case CardinalDirection::WEST:
		{
			for(int i = 0; i < m_players[playerId].getWidth() +1; ++i)
			{
				int x = curPos.m_x - 1;
				int y = curPos.m_y + i;
				tiles.push_back(Point(x,y));
			}
		}
		break;
	default: break;
	}

	return tiles;
}

//take a mushroom off a spawner and put it in the player's bag
void ShroomGame::addMushToPlayer(int camId, int mushX, int mushY)
{
	Gridspace* curGrid = m_cameras[camId].getGridspace();
	Mushroom* tempMush = curGrid->getMushroom(mushX, mushY);
	if(!tempMush->isPickable())
		return; //EARLY EXIT - don't allow non-pickables

	for(int k = 0; k < m_cameras.size(); ++k)
	{
		if(curGrid->getId() == m_cameras[k].getGridspace()->getId() && curGrid->getBiome() == m_cameras[k].getGridspace()->getBiome()
			&& m_cameras[k].getState() == Camera::NORMAL)
		{
			for(int i = 0; i < m_screenObjects[k].size(); ++i)
			{
				Point objPos = m_screenObjects[k][i].m_obj->getPosition();
				if(objPos.m_x == mushX && objPos.m_y == mushY) //find a texture on the render list that matches our position
				{
					std::vector<ScreenObject>::iterator itr = m_screenObjects[k].begin(); 
					itr += i;
					m_screenObjects[k].erase(itr); //remove it from the render list
					reInitializeTextures(k); //fix the pointers
					break;
				}
			}
		}
	}
	
	//get the mushroom object at our position, pull it off the gridspace
	Mushroom theMush = m_cameras[camId].getGridspace()->getAndRemoveMushroom(mushX, mushY);
	for(int i = 0; i < m_cameras[camId].getGridspace()->getNumMushSpawners(); ++i)
	{
		//find the spawner the mushroom came from
		MushroomSpawner* spawn = m_cameras[camId].getGridspace()->getMushroomSpawner(i);
		if(spawn->getPosition().m_x == mushX && spawn->getPosition().m_y == mushY)
		{
			spawn->removeMushroom(); //tell the spawner to reset its timer
		}
	}
	m_players[camId].addMushroom(theMush); //add the mushroom to the player's bag
}

void ShroomGame::toggleLantern(bool isUsing, int playerId)
{
	//if we even have a lantern
	if( m_players[playerId].getItem() == ItemType::LANTERN)
	{
		bool updateTex = false;
		//if we are first turning it off
		if(!isUsing && m_cameras[playerId].getGui()->isLanternOn())
		{
			if(m_cameras[playerId].getGridspace()->getBiome() == BiomeType::CAVE)
			{
				m_cameras[playerId].getGui()->toggleLantern(false);  //set back to small player filter
				updateTex = true;
			}
			else
				m_cameras[playerId].getGui()->getPlayerDarkness()->setAnimFrames(0,0); //completely turn off player filter
		}
		//if we are first turning it on
		else if(isUsing && !m_cameras[playerId].getGui()->isLanternOn() && m_cameras[playerId].getGridspace()->getBiome() == BiomeType::CAVE)
		{
			m_cameras[playerId].getGui()->toggleLantern(true); //set to large player filter
			updateTex = true;
		}
		if(updateTex) //reset the player filter to the proper texture. Assumes the player darkness sprite is first on the gui list
		{
			TexInfo objTex = m_guiObjects[playerId][0].m_obj->getTexture();
			m_guiObjects[playerId][0].m_texManager.initialize(graphics, objTex.m_resource);
			m_guiObjects[playerId][0].m_image.initialize(graphics, objTex.m_frameWidth, objTex.m_frameHeight, objTex.m_frameColumns, &(m_guiObjects[playerId][0].m_texManager));
			Point pos = m_guiObjects[playerId][0].m_obj->getPosition();
			m_guiObjects[playerId][0].m_image.setX(pos.m_x);
			m_guiObjects[playerId][0].m_image.setY(pos.m_y);
			m_guiObjects[playerId][0].m_image.setFrameDelay(GUI_FRAME_DELAY);
			m_guiObjects[playerId][0].m_image.setFrames(m_guiObjects[playerId][0].m_obj->getFirstFrame(), m_guiObjects[playerId][0].m_obj->getLastFrame());
			m_guiObjects[playerId][0].m_image.setCurrentFrame(m_guiObjects[playerId][0].m_image.getStartFrame());
		}
	}
}

void ShroomGame::toggleBugSpray(bool isOn, int playerId)
{
	//toggle the player speed according to bug spray use
	if( m_players[playerId].getItem() == ItemType::BUG_SPRAY)
	{
		if(isOn)
			m_players[playerId].setSpeedNormal();
		else if (m_cameras[playerId].getGridspace()->getBiome() == BiomeType::SWAMP)
			m_players[playerId].setSpeedSwamp();
	}
	else if(m_cameras[playerId].getGridspace()->getBiome() == BiomeType::SWAMP)
		m_players[playerId].setSpeedSwamp(); //default to swamp speed in the swamp
	else
		m_players[playerId].setSpeedNormal(); //default to regular speed everywhere else
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void ShroomGame::releaseAll()
{
    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void ShroomGame::resetAll()
{
    Game::resetAll();
    return;
}
