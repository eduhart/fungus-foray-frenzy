// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Game Engine Part 1
// Chapter 4 spacewar.cpp v1.0
// Spacewar is the class we create.

#include "spaceWar.h"

//=============================================================================
// Constructor
//=============================================================================
Spacewar::Spacewar()
{
	frameCount = 0;  // for movement demo
}

//=============================================================================
// Destructor
//=============================================================================
Spacewar::~Spacewar()
{
    releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Spacewar::initialize(HWND hwnd)
{
    Game::initialize(hwnd); // throws GameError

	// CH 5 Addition
	// background texture
    if (!nebulaTexture.initialize(graphics,NEBULA_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing nebula texture"));

    // planet texture
    if (!shipTexture.initialize(graphics,SHIP_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing ship texture"));

	// explosion texture
	if (!explosionTexture.initialize(graphics,EXPL_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion texture"));

    // nebula
    if (!nebula.initialize(graphics,0,0,0,&nebulaTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing nebula"));

    // ship
    if (!ship.initialize(graphics,0,0,0,&shipTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing ship"));

	// explosion
	// 64 x 64 images in 4 columns
    if (!explosion.initialize(graphics,64,64,4,&explosionTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing explosion"));

	// set up the animation values
	explosion.setFrames(0,13); // total of 14 frames, numbered 0 - 13
	explosion.setCurrentFrame(0);
	explosion.setFrameDelay(EXPL_ANIMATION_DELAY);
	explosion.setX(100.0f); // set location of explosion off to left side
	explosion.setY(100.0f);
	explosion.setLoop(true); // loop the explosion

    // place ship in center of screen
    ship.setX(GAME_WIDTH*0.5f  - ship.getWidth()*0.5f);
    ship.setY(GAME_HEIGHT*0.5f - ship.getHeight()*0.5f);

    return;
}

//=============================================================================
// Update all game items
//=============================================================================
void Spacewar::update()
{
	// first logic - increment frame count, if 30 move the ship up one pixel
	frameCount++;

	if(frameCount == 30)
	{
		frameCount = 0;
		ship.setY(ship.getY()-1);
	}

	// if the right arrow button is down, rotate the ship clockwise
	if(input->isKeyDown(SHIP_RIGHT_KEY))
	{
		ship.setDegrees(ship.getDegrees() + frameTime * ROTATION_RATE);
	}

	// left button rotates counter clockwise
	if(input->isKeyDown(SHIP_LEFT_KEY))
	{
		ship.setDegrees(ship.getDegrees() + frameTime * -ROTATION_RATE);
	}

	// update the frameTime value for the explosion animation
	explosion.update(frameTime);
}


//=============================================================================
// Render game items
//=============================================================================
void Spacewar::render()
{
	// CH 5 Addition
	graphics->spriteBegin();                // begin drawing sprites

    nebula.draw();                          // add the background to the scene
    ship.draw();                            // add the ship to the scene
	explosion.draw();						// add the explosion animation

    graphics->spriteEnd();                  // end drawing sprites

}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Spacewar::releaseAll()
{
	// CH 5 Addition
	shipTexture.onLostDevice();
    nebulaTexture.onLostDevice();

    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Spacewar::resetAll()
{
	// CH 5 Addition
	nebulaTexture.onResetDevice();
    shipTexture.onResetDevice();

    Game::resetAll();
    return;
}
