// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 4 spacewar.h v1.0

#ifndef _SPACEWAR_H             // prevent multiple definitions if this 
#define _SPACEWAR_H             // ..file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "game.h"

// CH 5 Additions
#include "textureManager.h"
#include "image.h"


//=============================================================================
// Create game class
//=============================================================================
class Spacewar : public Game
{
private:
    // variables
	// CH 5 Additions
	TextureManager nebulaTexture;		// nebula texture
    TextureManager shipTexture;			// ship texture
	TextureManager explosionTexture;	// explosion images as a sprite sheet
    Image   ship;						// ship image
    Image   nebula;						// nebula image
	Image	explosion;					// single explosion

	// added by kjb to make the ship move one pxel every 30 frames
	int frameCount;

public:
    // Constructor
    Spacewar();

    // Destructor
    virtual ~Spacewar();

    // Initialize the game
    void initialize(HWND hwnd);
    void update();     
    void render();      
    void releaseAll();
    void resetAll();
};

#endif
