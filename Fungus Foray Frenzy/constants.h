// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 4 constants.h v1.0

#ifndef _CONSTANTS_H            // prevent multiple definitions if this 
#define _CONSTANTS_H            // ..file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <XInput.h>
#include <WindowsX.h>

//-----------------------------------------------
// Useful macros
//-----------------------------------------------
// Safely delete pointer referenced item
#define SAFE_DELETE(ptr)       { if (ptr) { delete (ptr); (ptr)=NULL; } }
// Safely release pointer referenced item
#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr)=NULL; } }
// Safely delete pointer referenced array
#define SAFE_DELETE_ARRAY(ptr) { if(ptr) { delete [](ptr); (ptr)=NULL; } }

// CH 5 Additions
// Safely call onLostDevice
#define SAFE_ON_LOST_DEVICE(ptr)    { if(ptr) { ptr->onLostDevice(); } }
// Safely call onResetDevice
#define SAFE_ON_RESET_DEVICE(ptr)   { if(ptr) { ptr->onResetDevice(); } }


// define transparency color
#define TRANSCOLOR  SETCOLOR_ARGB(0,255,0,255)  // transparent color (magenta)

//-----------------------------------------------
//                  Constants
//-----------------------------------------------
// CH 5 Additions - Textures

// window
const char CLASS_NAME[] = "Shroom Hunt";
const char GAME_TITLE[] = "Shroom Hunt";
const bool FULLSCREEN = false;              // windowed or fullscreen
const UINT GAME_WIDTH =  1024 /*512*/;               // width of game in pixels
const UINT GAME_HEIGHT = 768 /*500*/;               // height of game in pixels
 
// game
const double PI = 3.14159265;
const float FRAME_RATE  = 60.0f;               // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;             // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;   // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time used in calculations

const float EXPL_ANIMATION_DELAY = 0.1f;    // time between frames of explosion animation

const float ROTATION_RATE = 10.0f;         // degrees per second

// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR ESC_KEY      = VK_ESCAPE;   // escape key
const UCHAR ALT_KEY      = VK_MENU;     // Alt key
const UCHAR ENTER_KEY    = VK_RETURN;   // Enter key

// copied from textbook code to activate arrow keys
const UCHAR SHIP_LEFT_KEY    = VK_LEFT;     // left arrow
const UCHAR SHIP_RIGHT_KEY   = VK_RIGHT;    // right arrow
const UCHAR SHIP_UP_KEY      = VK_UP;       // up arrow
const UCHAR SHIP_DOWN_KEY    = VK_DOWN;     // down arrow


const UCHAR LEFT_KEY    = VK_LEFT;     // left arrow
const UCHAR RIGHT_KEY   = VK_RIGHT;    // right arrow
const UCHAR UP_KEY      = VK_UP;       // up arrow
const UCHAR DOWN_KEY    = VK_DOWN;     // down arrow
const UCHAR SPACE_KEY	= VK_SPACE;
const UCHAR A_KEY		= 0x065;
const UCHAR W_KEY		= 0x087;
const UCHAR S_KEY		= 0x083;
const UCHAR D_KEY		= 0x130;
const UCHAR E_KEY		= 0x069;


//Controller stuff

struct ControllerState
{
	DWORD				prev;
    XINPUT_STATE        state;
    XINPUT_VIBRATION    vibration;
    float               vibrateTimeLeft;    // mSec
    float               vibrateTimeRight;   // mSec
    bool                connected;
	float				LStick[2];
	bool				PrevDPadState[4];
	bool				PrevJoypadState[4];
	bool				PrevLStickState[2];
};

const DWORD GAMEPAD_THUMBSTICK_DEADZONE = (DWORD)(0.20f * 0X7FFF);    // default to 20% of range as deadzone
const DWORD GAMEPAD_TRIGGER_DEADZONE = 30;                      // trigger range 0-255
const DWORD MAX_CONTROLLERS = 4;                                // Maximum number of controllers supported by XInput

// Base values to represent joystick
const DWORD GAMEPAD_LSL			=-10;//=-1;
const DWORD GAMEPAD_LSR			=10;//=1;
const DWORD GAMEPAD_LSU			=20;//=1;
const DWORD GAMEPAD_LSD			=-20;//=-1;
const DWORD DEADZONE			=40;

// Bit corresponding to gamepad button in state.Gamepad.wButtons
const DWORD GAMEPAD_DPAD_UP        = 0x0001;
const DWORD GAMEPAD_DPAD_DOWN      = 0x0002;
const DWORD GAMEPAD_DPAD_LEFT      = 0x0004;
const DWORD GAMEPAD_DPAD_RIGHT     = 0x0008;
const DWORD GAMEPAD_START_BUTTON   = 0x0010;
const DWORD GAMEPAD_BACK_BUTTON    = 0x0020;
const DWORD GAMEPAD_LEFT_THUMB     = 0x0040;
const DWORD GAMEPAD_RIGHT_THUMB    = 0x0080;
const DWORD GAMEPAD_LEFT_SHOULDER  = 0x0100;
const DWORD GAMEPAD_RIGHT_SHOULDER = 0x0200;
const DWORD GAMEPAD_A              = 0x1000;
const DWORD GAMEPAD_B              = 0x2000;
const DWORD GAMEPAD_X              = 0x4000;
const DWORD GAMEPAD_Y              = 0x8000;

/*DWORD JOYPAD[4]={GAMEPAD_Y,GAMEPAD_X,GAMEPAD_Y,GAMEPAD_A};

DWORD SHOULDER[4]={GAMEPAD_RIGHT_SHOULDER,GAMEPAD_LEFT_SHOULDER,GAMEPAD_RIGHT_THUMB,GAMEPAD_LEFT_THUMB};

DWORD OPTIONS[2]={GAMEPAD_BACK_BUTTON,GAMEPAD_START_BUTTON};

DWORD DPAD[4]={GAMEPAD_DPAD_RIGHT,GAMEPAD_DPAD_LEFT,GAMEPAD_DPAD_DOWN,GAMEPAD_DPAD_UP};*/

#endif
