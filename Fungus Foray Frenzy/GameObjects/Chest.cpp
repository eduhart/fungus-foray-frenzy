#include "Chest.h"

//the different frames for the display bubbles
const static int SHOVEL_FRAME = 0;
const static int LANTERN_FRAME = 1;
const static int FEED_FRAME = 3;
const static int SPRAY_FRAME = 2;

Chest::Chest(void)
	: m_itemType(ItemType::NO_ITEM)
{
}


Chest::~Chest(void)
{
}

//take an item and spit out the one we had
ItemType Chest::swapItem(ItemType item)
{
	ItemType temp = m_itemType;
	setItem(item);
	return temp;
}

//set the display icon (via a different frame)
//depending on which item we have
void Chest::setItem(ItemType item)
{
	switch(item)
	{
	case ItemType::SHOVEL:
		setAnimFrames(SHOVEL_FRAME, SHOVEL_FRAME);
		break;
	case ItemType::LANTERN:
		setAnimFrames(LANTERN_FRAME, LANTERN_FRAME);
		break;
	case ItemType::COW_FEED:
		setAnimFrames(FEED_FRAME, FEED_FRAME);
		break;
	case ItemType::BUG_SPRAY:
		setAnimFrames(SPRAY_FRAME, SPRAY_FRAME);
		break;
	default: break;
	}
	
	setLooping(true);	

	//record our current item
	m_itemType = item;
	
}

void Chest::setToIdleAnimation()
{
	setItem(m_itemType);
	
}