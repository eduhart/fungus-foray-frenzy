#include "Player.h"
#include <math.h>
#include <ctime>

//Animation frames======================
static const int WALK_FRAMES = 3;
static const int ACTION_FRAMES = 1;

static const int WALK_DOWN_INDEX = 1;
static const int WALK_UP_INDEX = 6;
static const int WALK_LEFT_INDEX = 11;
static const int WALK_RIGHT_INDEX = 16;
static const int ACTION_DOWN_INDEX = 20;
static const int ACTION_UP_INDEX = 22;
static const int ACTION_LEFT_INDEX = 24;
static const int ACTION_RIGHT_INDEX = 26;
//=========================================


Player::Player(PlayerType id)
	: m_id(id)
	, m_score(0)
	, m_mushrooms()
	, m_currentItem(ItemType::NO_ITEM)
	, m_facingDir(CardinalDirection::SOUTH)
	, m_speed(PLAYER_SPEED)
{
	setWidth(1);
	setHeight(1);

	//randomly assign an item based on the system clock
	int randy = rand() + (std::time(0) % 22);
	randy = randy % (ItemType::NUM_ITEM_TYPES -1);
	switch(randy)
	{
	case 0: m_currentItem = ItemType::BUG_SPRAY;
		break;
	case 1: m_currentItem = ItemType::COW_FEED;
		break;
	case 2: m_currentItem = ItemType::LANTERN;
		break;
	case 3: m_currentItem = ItemType::SHOVEL;
		break;
	default: m_currentItem = ItemType::COW_FEED;
		break;
	}
	
	setLooping(true);
}


Player::~Player(void)
{
}

void Player::addMushroom(Mushroom theMushroom)
{
	m_mushrooms.push_back(theMushroom);
}

//get the total amount of mushrooms in the player's bag of a given type
int Player::getNumMushrooms(MushroomType type)
{
	if(type == MushroomType::NO_TYPE || type == MushroomType::NUM_MUSHROOM_TYPES)
		return m_mushrooms.size(); //EARLY EXIT

	int count = 0;
	for(int i = 0; i < m_mushrooms.size(); ++i)
	{
		if(m_mushrooms[i].getType() == type)
			count++;
	}
	return count;
}

//trade mushrooms of a given type for points
void Player::cashInMushrooms(MushroomType type)
{
	int mushCount = 0;
	int mushVal = 0;
	while(getNumMushrooms(type) > 0)
	{
		//find all the mushrooms of the right type
		for(int i = 0; i < m_mushrooms.size(); ++i)
		{
			if(m_mushrooms[i].getType() == type)
			{
				mushVal = m_mushrooms[i].getPointVal();
				//erase
				std::vector<Mushroom>::iterator itr = m_mushrooms.begin(); 
				itr += i;
				m_mushrooms.erase(itr);
				mushCount++;
				break;
			}
		}
	}

	//add to the score
	if(mushCount > 0)
	{
		m_score += (mushCount * mushVal);
	}

}

//tell the player which frames to use for walking
void Player::setToWalkAnimation(CardinalDirection dir)
{
	switch(dir)
	{
	case CardinalDirection::NORTH: 
		{
		setAnimFrames(WALK_UP_INDEX, WALK_UP_INDEX + WALK_FRAMES);
		m_facingDir = dir;
		}
		break;
	case CardinalDirection::SOUTH:
		{
		setAnimFrames(WALK_DOWN_INDEX, WALK_DOWN_INDEX + WALK_FRAMES);
		m_facingDir = dir;
		}
		break;
	case CardinalDirection::EAST:
		{
		setAnimFrames(WALK_RIGHT_INDEX, WALK_RIGHT_INDEX + WALK_FRAMES);
		m_facingDir = dir;
		}
		break;
	case CardinalDirection::WEST:
		{
		setAnimFrames(WALK_LEFT_INDEX, WALK_LEFT_INDEX + WALK_FRAMES);
		m_facingDir = dir;
		}
		break;
	default:break;
	}
}

//tell the player which frame to default to for idle
//based on the last arrow key pressed (store in m_facingDir)
void Player::setToIdleAnimation()
{
	switch(m_facingDir)
	{
	case CardinalDirection::NORTH:
		setAnimFrames(WALK_UP_INDEX - 1, WALK_UP_INDEX - 1);
		break;
	case CardinalDirection::SOUTH:
		setAnimFrames(WALK_DOWN_INDEX - 1, WALK_DOWN_INDEX - 1);
		break;
	case CardinalDirection::EAST:
		setAnimFrames(WALK_RIGHT_INDEX - 1, WALK_RIGHT_INDEX - 1);
		break;
	case CardinalDirection::WEST:
		setAnimFrames(WALK_LEFT_INDEX - 1, WALK_LEFT_INDEX - 1);
		break;
	default:break;
	}
	setLooping(true);
}

//tell the player which frames to use for the action
//also based on the last pressed arrow key (m_facingDir)
void Player::setToActionAnimation()
{
	switch(m_facingDir)
	{
	case CardinalDirection::NORTH:
		setAnimFrames(ACTION_UP_INDEX, ACTION_UP_INDEX + ACTION_FRAMES);
		break;
	case CardinalDirection::SOUTH:
		setAnimFrames(ACTION_DOWN_INDEX, ACTION_DOWN_INDEX + ACTION_FRAMES);
		break;
	case CardinalDirection::EAST:
		setAnimFrames(ACTION_RIGHT_INDEX, ACTION_RIGHT_INDEX + ACTION_FRAMES);
		break;
	case CardinalDirection::WEST:
		setAnimFrames(ACTION_LEFT_INDEX, ACTION_LEFT_INDEX + ACTION_FRAMES);
		break;
	default:break;
	}
	setLooping(false);
}

void Player::setSpeedNormal()
{
	m_speed = PLAYER_SPEED;
}

void Player::setSpeedSwamp()
{
	m_speed = PLAYER_SPEED_SWAMP;
}

void Player::BaseMoveSet()
{
	//base movement set
	/*
	moveSet.newAction(LEFT_KEY,"move left");
	moveSet.newAction(RIGHT_KEY,"move right");
	moveSet.newAction(UP_KEY,"move up");
	moveSet.newAction(DOWN_KEY,"move down");
	moveSet.newAction(SPACE_KEY,"use");
	*/
	
	moveSet.newAction(GAMEPAD_DPAD_LEFT,"move left");
	moveSet.newAction(GAMEPAD_DPAD_RIGHT,"move right");
	moveSet.newAction(GAMEPAD_DPAD_UP,"move up");
	moveSet.newAction(GAMEPAD_DPAD_DOWN,"move down");
	moveSet.newAction(GAMEPAD_A,"use");

}

void Player::takeAction(string action)
{
		/*if(action=="move left")
			setX(getX()-1);
		if(action=="move right")
			setX(getX()+1);
		if(action=="move up")
			setY(getY()-1);
		if(action=="move down")
			setY(getY()+1);
		if(action=="spin")
			setDegrees(getDegrees() + 90);*/
}



std::string Player::getName()
{
	return name;
}

void Player::setName(std::string nm)
{
	name=nm;
}

void Player::addAction(WPARAM key,std::string action)
{
	moveSet.newAction(key,action);
}