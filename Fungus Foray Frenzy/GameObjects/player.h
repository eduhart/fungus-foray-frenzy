#pragma once
#include "../Globals.h"

#include "gameobject.h"
#include "Mushroom.h"
#include "../Input Controller/ActionList.h"
#include "../constants.h"
#include <vector>


class Player : public GameObject
{
public:
	Player(PlayerType id);
	~Player(void);

	inline PlayerType getId() {return m_id;}
	inline CardinalDirection getFacingDir() {return m_facingDir;}
	inline void setItem(ItemType item) {m_currentItem = item;}
	inline ItemType getItem() {return m_currentItem;}
	inline int getScore() {return m_score;}
	inline void toggleItemUse(bool isUsing) {m_isUsingItem = isUsing;}
	inline bool isUsingItem() {return m_isUsingItem;}
	inline void setSpeed(int newSpeed) {m_speed = newSpeed;}
	inline int getSpeed() {return m_speed;}

	//variables
	std::string name;
	ActionList moveSet;

	//input controll relevant methods
	std::string getName();
	void setName(std::string nm);
	void addAction(WPARAM key,std::string action);
	void BaseMoveSet();
	void takeAction(string action);

	//mushroom related
	int getNumMushrooms(MushroomType type = MushroomType::NO_TYPE);
	void addMushroom(Mushroom theMushroom);
	void cashInMushrooms(MushroomType type);

	//animation related
	void setToWalkAnimation(CardinalDirection direction);
	void setToIdleAnimation();
	void setToActionAnimation();
	void setSpeedNormal();
	void setSpeedSwamp();

private:
	PlayerType m_id;

	int m_score;
	std::vector<Mushroom> m_mushrooms;

	ItemType m_currentItem;
	CardinalDirection m_facingDir;
	int m_speed;
	
	bool m_isUsingItem;

};
