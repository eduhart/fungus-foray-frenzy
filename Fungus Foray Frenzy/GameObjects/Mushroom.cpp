#include "Mushroom.h"

static const int PICKABLE_FRAME = 0;
static const int NON_PICKABLE_FRAME = 1;

Mushroom::Mushroom(void)
	: m_mushroomType(MushroomType::NO_TYPE)
	, m_pointValue(0)
	, m_isPickable(true)
{
	setHeight(1);
	setWidth(1);
}


Mushroom::~Mushroom(void)
{
}

void Mushroom::setType(MushroomType type)
{
	//default edibles to being 'buried'
	if(type == MushroomType::EDIBLE)
		setPickable(false);

	//default all mushrooms except browns to have 10 points
	if(type == MushroomType::LITTLE_BROWN)
		m_pointValue = 5;
	else
		m_pointValue = 10;

	m_mushroomType = type;
}

void Mushroom::setPickable(bool isPick)
{
	m_isPickable = isPick;

	//update the sprite to show a differences
	if(m_isPickable)
	{
		setAnimFrames(PICKABLE_FRAME, PICKABLE_FRAME);
	}
	else
	{
		setAnimFrames(NON_PICKABLE_FRAME, NON_PICKABLE_FRAME);
	}
	setLooping(true);
}

void Mushroom::setToIdleAnimation()
{
	setPickable(true);
}
