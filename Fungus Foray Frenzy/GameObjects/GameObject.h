#pragma once
#include "../Globals.h"

class GameObject
{
public:
	GameObject(void);
	~GameObject(void);

	//texture/animation getters and setters
	void setTexture(char* resource, int frameW = 0, int frameH = 0, int columns = 0, int layer = 0);
	inline void setTexture(TexInfo texInfo) {m_textureInfo = texInfo;}
	inline TexInfo getTexture() {return m_textureInfo;}
	inline void setAnimFrames(int start, int end) {m_startFrame = start; m_endFrame = end;}
	inline int getFirstFrame() {return m_startFrame;}
	inline int getLastFrame() {return m_endFrame;}
	inline void setLooping(bool loops) {m_textureInfo.m_loops = loops;}
	inline bool isLooping() {return m_textureInfo.m_loops;}
	inline void toggleExistance(bool exists) {m_exists = exists;}
	inline bool exists() {return m_exists;}
	virtual void setToIdleAnimation() = 0;
	
	//normal getters and setters
	inline void setPosition(Point pos) {m_position = pos;};
	inline void setPosition(int x, int y) {m_position.m_x = x; m_position.m_y = y;};
	inline Point getPosition() {return m_position;};
	inline void setWidth(int width) {m_width = width;};
	inline void setHeight(int height) {m_height = height;};
	inline int getWidth() {return m_width;};
	inline int getHeight() {return m_height;};

protected:
	Point m_position;
	
	int m_width;
	int m_height;
	bool m_exists;

	//texture shenanigans
	int m_startFrame;
	int m_endFrame;
	TexInfo m_textureInfo;
};

