#pragma once
#include "gameobject.h"
#include "../Globals.h"

class Npc : public GameObject
{
public:
	Npc(void);
	~Npc(void);
	inline void setMushroomType(MushroomType mushType) {m_takenMushroomType = mushType;};
	inline MushroomType getMushroomType() {return m_takenMushroomType;};

	void setToIdleAnimation();
	void setToSuccessAnimation();
	void setToFailureAnimation();

private:
	MushroomType m_takenMushroomType;
};
