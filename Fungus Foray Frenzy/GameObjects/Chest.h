#pragma once
#include "gameobject.h"
#include "../Globals.h"

class Chest : public GameObject
{
public:
	Chest(void);
	~Chest(void);

	void setItem(ItemType item);
	inline ItemType getItem() {return m_itemType;};
	ItemType swapItem(ItemType item);
	
	void setToIdleAnimation();

private:
	ItemType m_itemType;

};

