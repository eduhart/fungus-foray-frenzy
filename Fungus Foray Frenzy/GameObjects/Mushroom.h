#pragma once
#include "gameobject.h"
#include "../Globals.h"

class Mushroom : public GameObject
{
public:
	Mushroom(void);
	~Mushroom(void);

	inline int getPointVal() {return m_pointValue;};
	inline void setPointVal(int value) {m_pointValue = value;};

	inline MushroomType getType() {return m_mushroomType;};
	void setType(MushroomType type);

	inline bool isPickable() {return m_isPickable;};
	void setPickable(bool isPick);
	
	void setToIdleAnimation();
	
private:
	int m_pointValue;	
	MushroomType m_mushroomType;

	bool m_isPickable;
};

