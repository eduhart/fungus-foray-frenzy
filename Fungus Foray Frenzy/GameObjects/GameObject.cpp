#pragma once
#include "GameObject.h"


GameObject::GameObject(void)
	: m_position(Point(0,0))
	, m_width(1)
	, m_height(1)
	, m_startFrame(0)
	, m_endFrame(0)
	, m_textureInfo("")
	, m_exists(true)
{
}


GameObject::~GameObject(void)
{
}

void GameObject::setTexture(char* resource, int frameW, int frameH, int columns, int layers)
{
	TexInfo tex(resource, frameW, frameH, columns, layers);
	m_textureInfo = tex;
}