#include "MushroomSpawner.h"
#include <stdlib.h>

static const int WAIT_FRAMES = 100;

MushroomSpawner::MushroomSpawner(void)
	: m_hasMushroom(false)
	, m_canSpawn(true)
	, m_hasCow(false)
	, m_pointValue(0)
	, m_mushroomType(MushroomType::NO_TYPE)
	, m_position(INVALID_POS)
	, m_spawnChance(0.0)
	, m_lastRemoval(std::time(0))
	, m_textureInfo("")
{
	if(m_mushroomType == MushroomType::LITTLE_BROWN)
		m_pointValue = 5;
	else if(m_mushroomType != MushroomType::NO_TYPE)
		m_pointValue = 10;
}


MushroomSpawner::~MushroomSpawner(void)
{

}

void MushroomSpawner::setTexture(char* resource, int frameW, int frameH, int columns, int layers)
{
	TexInfo tex(resource, frameW, frameH, columns, layers);
	m_textureInfo = tex;
}


void MushroomSpawner::attemptToSpawn()
{
	if(!m_hasMushroom && m_canSpawn)
	{
		//for every 30-second chunk that has passed since a mushroom was last picked
		int seconds = (std::time(0) - m_lastRemoval) / 30;
		for(int i = 0; i <= seconds; ++i)
		{
			//pick a random number, if it falls within our "chance" range
			float randy = rand() % 100;
			if(randy < m_spawnChance)
			{
				m_hasMushroom = true; //we have a mushroom!
				break;
			}
		}
	}
}

//create a mushroom object with the properties stored here
Mushroom MushroomSpawner::makeMushroom()
{
	Mushroom newMush;
	newMush.setPointVal(m_pointValue);
	newMush.setType(m_mushroomType);
	newMush.setPosition(m_position);
	newMush.setTexture(m_textureInfo);

	return newMush;
}

//called when a player 'picks' the mushroom off the spawner
void MushroomSpawner::removeMushroom()
{
	m_hasMushroom = false;
	m_lastRemoval = std::time(0); //set to now
	if(m_hasCow)
		toggleSpawn(false);
}