#include "gui.h"


gui::gui(void)
{
	own_score=0;
	time_left=100000;
	held_mushrooms;
	held_item = NO_ITEM;
}


gui::~gui(void)
{
}

void gui::passGuiInfo(int player_score, float time, std::vector<Mushroom> mushrooms, ItemType currentItem)
{
	own_score=player_score;
	time_left=time;
	held_mushrooms=mushrooms;
	held_item = currentItem;
}

void gui::tick()
{
	time_left--;
}

void gui::increaseScore(int score)
{
	own_score = score;
}

void gui::getNewItem(ItemType newItem)
{
	held_item=newItem;
	//updates the picture being displayed
	switch( newItem )
	{
		case NO_ITEM:
			//image_current = image_non;
			break;
		case	SHOVEL:
			//image_current = image_shovel;
			break;
		case	BUG_SPRAY:
			//image_current = image_bug_spray;
			break;
		case	LANTERN:
			//image_current = image_lantern;
			break;
		case	COW_FEED:
			//image_current = image_cow_feed;
			break;
	}

}

void gui::updateMushrooms(std::vector<Mushroom> mushrooms)
{
	held_mushrooms = mushrooms;
}