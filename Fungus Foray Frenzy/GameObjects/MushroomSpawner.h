#pragma once
#include "Mushroom.h"
#include "../Globals.h"

#include <ctime>

class MushroomSpawner
{
public:
	MushroomSpawner(void);
	~MushroomSpawner(void);

	inline int getPointVal() {return m_pointValue;};
	inline void setPointVal(int value) {m_pointValue = value;};
	inline MushroomType getType() {return m_mushroomType;};
	inline void setType(MushroomType type) { m_mushroomType = type;};
	inline Point getPosition() {return m_position;};
	inline void setPosition(int x, int y) {m_position.m_x = x; m_position.m_y = y;};
	inline void setPosition(Point pos) {m_position = pos;};
	inline void setSpawnChance(int chance) {m_spawnChance = chance;};

	//spawning
	void attemptToSpawn();
	Mushroom makeMushroom();
	inline bool hasMushroom() {return m_hasMushroom;};
	inline bool canSpawn() {return m_canSpawn;};
	inline void toggleSpawn(bool canSpawn) {m_canSpawn = canSpawn;};
	inline void toggleCow(bool hasCow) {m_hasCow = hasCow;};
	inline bool hasCow() {return m_hasCow;};
	void removeMushroom();

	//texture stuff
	void setTexture(char* resource, int frameW = 0, int frameH = 0, int columns = 0, int layer = 1);
	inline void setTexture(TexInfo texInfo) {m_textureInfo = texInfo;};
	inline TexInfo getTexture() {return m_textureInfo;};

private:
	bool m_hasMushroom;
	bool m_canSpawn;
	bool m_hasCow;

	int m_pointValue;	
	MushroomType m_mushroomType;
	Point m_position;

	int m_spawnChance; //0->99
	std::time_t m_lastRemoval;

	TexInfo m_textureInfo;
};

