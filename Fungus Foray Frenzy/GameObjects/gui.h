#pragma once
#include "../Globals.h"
#include <vector>
#include "Mushroom.h"

class gui
{
public:
	gui(void);
	~gui(void);
	//a method to get all of the info that the gui needs at once
	void passGuiInfo(int player_score,float time, std::vector<Mushroom> mushrooms, ItemType currentItem);
	//have a second pass on the timer
	void tick();
	//add points to the total
	void increaseScore(int score);
	//for when the item type changes
	void getNewItem(ItemType newItem);
	//for when mushrooms are picked up or given away
	void updateMushrooms(std::vector<Mushroom> mushrooms);

private:
	int own_score;
	/*
	//for if we want players to be able to see the opponents scores during the game
	int p2_score 
	int	p3_score
	int p4_score
	*/

	//the time left in the game
	float time_left;

	//mushrooms the player currently has
	std::vector<Mushroom> held_mushrooms;

	//item the player is currently using
	ItemType held_item;

	//still need to load in the images for each item, as well as the images for the mushrooms and whatever else goes on the gui
	//image image_current;
	//image image_no_item
	//image image_shovel;
	//image image_bug_spray;
	//image image_lantern;
	//image image_cow_feed;
	//image image_mushroom_brown;
	//image image_mushroom_glowing;
	//image image_mushroom_psychoactive;
	//image image_mushroom_poisonous;
	//image image_mushroom_edible;
	
	//need to actually display everything on screen still. also still need to set up text font for everything.
};

