#pragma once
#include "gameobject.h"
#include "../GameObjects/MushroomSpawner.h"
#include "../Globals.h"

class Cow : public GameObject
{
public:
	Cow(void);
	~Cow(void);

	inline void setSpawnId(int id) {m_spawnerId = id;}
	inline int getSpawnId() {return m_spawnerId;}
	inline void setSpawner(MushroomSpawner* spawner) { m_spawner = spawner;}

	void feed();
	void setToIdleAnimation();
	void setToSuccessAnimation();
	void setToFailureAnimation();

private:
	int m_spawnerId;
	MushroomSpawner* m_spawner;
};

