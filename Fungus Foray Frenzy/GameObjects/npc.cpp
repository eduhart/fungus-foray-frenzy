#include "Npc.h"

const static int IDLE_INDEX = 0;
const static int SUCCESS_INDEX = 6;
const static int FAIL_INDEX = 12;
const static int FRAMES = 5;

Npc::Npc(void)
	: m_takenMushroomType(MushroomType::NO_TYPE)
{
	setToIdleAnimation();
}


Npc::~Npc(void)
{
}

void Npc::setToIdleAnimation()
{
	setAnimFrames(IDLE_INDEX, IDLE_INDEX + FRAMES);
	setLooping(true);
}

void Npc::setToSuccessAnimation()
{
	setAnimFrames(SUCCESS_INDEX, SUCCESS_INDEX + FRAMES);
	setLooping(false);
}

void Npc::setToFailureAnimation()
{
	setAnimFrames(FAIL_INDEX, FAIL_INDEX + FRAMES);
	setLooping(false);
}
