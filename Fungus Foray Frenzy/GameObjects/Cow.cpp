#include "Cow.h"

//animation frame indices
const static int IDLE_INDEX = 0;
const static int SUCCESS_INDEX = 6;
const static int FAIL_INDEX = 12;
const static int FRAMES = 5;


Cow::Cow(void)
	: m_spawnerId(0)
	, m_spawner()
{
	setToIdleAnimation();
}


Cow::~Cow(void)
{
}

//tell the connected mushroom spawner that it can spawn mushrooms
//set the cow to success animation
void Cow::feed()
{
	if(!m_spawner->canSpawn())
	{
		m_spawner->toggleSpawn(true);
		setToSuccessAnimation();
	}
}

void Cow::setToIdleAnimation()
{
	setAnimFrames(IDLE_INDEX, IDLE_INDEX + FRAMES);
	setLooping(true);
}

void Cow::setToSuccessAnimation()
{
	setAnimFrames(SUCCESS_INDEX, SUCCESS_INDEX + FRAMES);
	setLooping(false);
}

void Cow::setToFailureAnimation()
{
	setAnimFrames(FAIL_INDEX, FAIL_INDEX + FRAMES);
	setLooping(false);
}