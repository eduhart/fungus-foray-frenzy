#pragma once

#include "Globals.h"
#include "irrklang\irrKlang.h"
#include <stdio.h>
#include <string.h>

using namespace irrklang;

//Has static functions to play both background sounds and spontaneous sound effects.
//Background sounds will loop, sound effects will not.
//The 'initialize' function must be called before this class is usable.
//This isn't meant to be instantiated.
class SoundVault
{
	private:
		//Constants
		static const char* FOREST_THEME_PATH;
		static const char* SWAMP_THEME_PATH;
		static const char* CAVE_THEME_PATH;
		static const char* FARM_THEME_PATH;

		//Variables
		ISoundEngine* engine;

	public:
		//Construction/Destruction
		SoundVault();
		virtual ~SoundVault();

		//Methods
		void playBackgroundSong( BiomeType biome );
};