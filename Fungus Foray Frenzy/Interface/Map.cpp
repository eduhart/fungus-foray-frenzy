#include "Map.h"
#include <fstream>
#include <iostream>

#include "../GameObjects/BkgObject.h"
#include "../GameObjects/Chest.h"
#include "../GameObjects/npc.h"
#include "../GameObjects/MushroomSpawner.h"
#include "../GameObjects/Cow.h"

using namespace std;

Map::Map(int levelId)
	: m_levelId(levelId)
	, m_biomes()
{
}


Map::~Map(void)
{
}

void Map::load()
{
	getXmlAsString(); //pull in the file
	testLoad(); //parse the string into objects
	initObjects();
}

Gridspace* Map::getGridspace(BiomeType biome, int id)
{
	Biome* theBiome = getBiome(biome);
	return theBiome->getGridspace(id);
}

Biome* Map::getBiome(BiomeType id)
{
	Biome* theBiome;
	for(int i = 0; i < m_biomes.size(); ++i)
	{
		if(m_biomes[i].getId() == id)
			theBiome = &(m_biomes[i]);
	}
	return theBiome;
}

//pull the entire xml file into a vector of strings
void Map::getXmlAsString()
{
	//TODO: error checking
	ifstream bro("Design\\FinalMap.txt");
	string line;

	while(!bro.eof())
	{
		getline(bro, line);
		m_mapFileLines.push_back(line);
	}

	bro.close();
}

//load in all the specified map tiles and objects
void Map::testLoad()
{
	//the current object being built
	Biome curBiome;
	Gridspace curGridspace;
	Gridspace::GridConnection curConnection;

	BkgObject curBkgObject;
	Chest curItemChest;
	MushroomSpawner curMushroomSpawn;
	Npc curNpc;
	Cow curCow;

	TexInfo curTexture("");

	//whether or not we're building a given kind of object
	bool checkForBiomeVals = false;
	bool checkForGridVals = false;
	bool checkForConVals = false;

	bool checkForObjVals = false;
	bool checkForMushVals = false;
	bool checkForChestVals = false;
	bool checkForNpcVals = false;
	bool checkForCowVals = false;

	for(int i = 0; i < m_mapFileLines.size(); ++i)
	{
		//Biome specific ==============================================================================
		if(m_mapFileLines[i].find("<biome>") != std::string::npos)
			checkForBiomeVals = true;
		if(m_mapFileLines[i].find("<name>") != std::string::npos && checkForBiomeVals)
		{
			std::string name = extractXmlValue("<name>", i);
			if(name == "forest")
			{
				Biome tempB(BiomeType::FOREST);
				curBiome = tempB;
			}
			else if(name == "swamp")
			{
				Biome tempB(BiomeType::SWAMP);
				curBiome = tempB;
			}
			else if(name == "cave")
			{
				Biome tempB(BiomeType::CAVE);
				curBiome = tempB;
			}
			else if(name == "field")
			{
				Biome tempB(BiomeType::COW_FIELDS);
				curBiome = tempB;
			}
		}
		if(m_mapFileLines[i].find("</biome>") != std::string::npos) // end of a biome, add object to map
		{
			m_biomes.push_back(curBiome);
			checkForBiomeVals = false;
		}
		//==============================================================================================

		//Gridspace specific ===========================================================================
		if(m_mapFileLines[i].find("<gridspace>") != std::string::npos)
			checkForGridVals = true;
		if(m_mapFileLines[i].find("<idNum>") != std::string::npos && checkForGridVals)
		{
			std::string id = extractXmlValue("<idNum>", i);
			int idNum = atoi(id.c_str());
			Gridspace tempG(curBiome.getId(), idNum);
			curGridspace = tempG;
		}
		if(m_mapFileLines[i].find("<bkgSource>") != std::string::npos && checkForGridVals)
		{
			std::string resourceStr = "Art\\";
			switch(curBiome.getId())
			{
			case BiomeType::CAVE: resourceStr.append("cave\\");
				break;
			case BiomeType::FOREST: resourceStr.append("forest\\");
				break;
			case BiomeType::COW_FIELDS: resourceStr.append("farm\\");
				break;
			case BiomeType::SWAMP: resourceStr.append("swamp\\");
				break;
			default:break;
			}
			resourceStr.append(extractXmlValue("<bkgSource>", i));
			char* resource = new char[resourceStr.size() + 1];
			strcpy(resource, resourceStr.c_str());
			curGridspace.setTexture(resource);
		}
		if(m_mapFileLines[i].find("</gridspace>") != std::string::npos) //end of a gridspace, add to current biome
		{
			checkForGridVals = false;
			curBiome.addGridspace(curGridspace);
			curGridspace.resetToDefaults();
		}
		//==============================================================================================

		//Connection Specific===========================================================================
		if(m_mapFileLines[i].find("<connection>") != std::string::npos)
			checkForConVals = true;
		if(m_mapFileLines[i].find("<to>") != std::string::npos && checkForConVals)
		{
			std::string toStr = extractXmlValue("<to>", i);
			int to = atoi(toStr.c_str());
			curConnection.m_toGridId = to;
		}
		if(m_mapFileLines[i].find("<type>") != std::string::npos && checkForConVals)
		{
			std::string name = extractXmlValue("<type>", i);
			if(name == "north")
			{
				curConnection.m_fromDirection = CardinalDirection::NORTH;
				curConnection.m_toDirection = CardinalDirection::SOUTH;
			}
			else if(name == "south")
			{
				curConnection.m_fromDirection = CardinalDirection::SOUTH;
				curConnection.m_toDirection = CardinalDirection::NORTH;
			}
			else if(name == "east")
			{
				curConnection.m_fromDirection = CardinalDirection::EAST;
				curConnection.m_toDirection = CardinalDirection::WEST;	
			}
			else if(name == "west")
			{
				curConnection.m_fromDirection = CardinalDirection::WEST;
				curConnection.m_toDirection = CardinalDirection::EAST;
			}
		}
		if(m_mapFileLines[i].find("<biomeType>") != std::string::npos && checkForConVals)
		{
			std::string name = extractXmlValue("<biomeType>", i);
			if(name == "forest")
				curConnection.m_biome = BiomeType::FOREST;
			else if(name == "swamp")
				curConnection.m_biome = BiomeType::SWAMP;
			else if(name == "cave")
				curConnection.m_biome = BiomeType::CAVE;	
			else if(name == "field")
				curConnection.m_biome = BiomeType::COW_FIELDS;
		}
		if(m_mapFileLines[i].find("</connection>") != std::string::npos) //end of a connection, add to current gridspace
		{
			checkForConVals = false;
			curGridspace.addConnection(curConnection);
		}
		//==============================================================================================

		//BkgObject specific============================================================================
		if(m_mapFileLines[i].find("<bkgObject>") != std::string::npos)
			checkForObjVals = true;
		if(m_mapFileLines[i].find("<spriteSource>") != std::string::npos && checkForObjVals)
		{
			std::string resourceStr = "Art\\";
			switch(curBiome.getId())
			{
			case BiomeType::CAVE: resourceStr.append("cave\\");
				break;
			case BiomeType::FOREST: resourceStr.append("forest\\");
				break;
			case BiomeType::COW_FIELDS: resourceStr.append("farm\\");
				break;
			case BiomeType::SWAMP: resourceStr.append("swamp\\");
				break;
			default:break;
			}
			resourceStr.append(extractXmlValue("<spriteSource>", i));
			char* resource = new char[resourceStr.size()+1];
			strcpy(resource, resourceStr.c_str());
			curTexture.m_resource = resource;
		}
		if(m_mapFileLines[i].find("<layer>") != std::string::npos && checkForObjVals)
		{
			std::string layerStr = extractXmlValue("<layer>", i);
			int layer = atoi(layerStr.c_str());
			curTexture.m_camLayer = layer;
		}
		if(m_mapFileLines[i].find("<width>") != std::string::npos && checkForObjVals)
		{
			std::string wStr = extractXmlValue("<width>", i);
			int width = atoi(wStr.c_str());
			curTexture.m_frameWidth = width * TILE_SIZE;
			curBkgObject.setWidth(width);
		}
		if(m_mapFileLines[i].find("<height>") != std::string::npos && checkForObjVals)
		{
			std::string hStr = extractXmlValue("<height>", i);
			int height = atoi(hStr.c_str());
			curTexture.m_frameHeight = height * TILE_SIZE;
			curBkgObject.setHeight(height);
		}
		if(m_mapFileLines[i].find("<animCols>") != std::string::npos && checkForObjVals)
		{
			std::string animStr = extractXmlValue("<animCols>", i);
			int columns = atoi(animStr.c_str());
			curTexture.m_frameColumns = columns;
		}
		if(m_mapFileLines[i].find("<animFrames>") != std::string::npos && checkForObjVals)
		{
			std::string animStr = extractXmlValue("<animFrames>", i);
			int frames = atoi(animStr.c_str());
			curBkgObject.setAnimFrames(0,frames-1);
		}
		if(m_mapFileLines[i].find("<xCoord>") != std::string::npos && checkForObjVals)
		{
			std::string xStr = extractXmlValue("<xCoord>", i);
			int x = atoi(xStr.c_str());
			curBkgObject.setPosition(x, curBkgObject.getPosition().m_y);
		}
		if(m_mapFileLines[i].find("<yCoord>") != std::string::npos && checkForObjVals)
		{
			std::string yStr = extractXmlValue("<yCoord>", i);
			int y = atoi(yStr.c_str());
			curBkgObject.setPosition(curBkgObject.getPosition().m_x, y);
		}
		if(m_mapFileLines[i].find("</bkgObject>") != std::string::npos)
		{
			curTexture.m_loops = true;
			curBkgObject.setTexture(curTexture);
			curGridspace.addBkgObject(curBkgObject);
			checkForObjVals = false;
			curTexture.reset();
		}
		//==============================================================================================

		//NPC Specific==================================================================================
		if(m_mapFileLines[i].find("<npc>") != std::string::npos)
			checkForNpcVals = true;
		if(m_mapFileLines[i].find("<spriteSource>") != std::string::npos && checkForNpcVals)
		{
			std::string resourceStr = "Art\\";
			switch(curBiome.getId())
			{
			case BiomeType::CAVE: resourceStr.append("cave\\");
				break;
			case BiomeType::FOREST: resourceStr.append("forest\\");
				break;
			case BiomeType::COW_FIELDS: resourceStr.append("farm\\");
				break;
			case BiomeType::SWAMP: resourceStr.append("swamp\\");
				break;
			default:break;
			}
			resourceStr.append(extractXmlValue("<spriteSource>", i));
			char* resource = new char[resourceStr.size()+1];
			strcpy(resource, resourceStr.c_str());
			curTexture.m_resource = resource;
		}
		if(m_mapFileLines[i].find("<layer>") != std::string::npos && checkForNpcVals)
		{
			std::string layerStr = extractXmlValue("<layer>", i);
			int layer = atoi(layerStr.c_str());
			curTexture.m_camLayer = layer;
		}
		if(m_mapFileLines[i].find("<width>") != std::string::npos && checkForNpcVals)
		{
			std::string wStr = extractXmlValue("<width>", i);
			int width = atoi(wStr.c_str());
			curTexture.m_frameWidth = width * TILE_SIZE;
			curNpc.setWidth(width);
		}
		if(m_mapFileLines[i].find("<height>") != std::string::npos && checkForNpcVals)
		{
			std::string hStr = extractXmlValue("<height>", i);
			int height = atoi(hStr.c_str());
			curTexture.m_frameHeight = height * TILE_SIZE;
			curNpc.setHeight(height);
		}
		if(m_mapFileLines[i].find("<animCols>") != std::string::npos && checkForNpcVals)
		{
			std::string animStr = extractXmlValue("<animCols>", i);
			int columns = atoi(animStr.c_str());
			curTexture.m_frameColumns = columns;
		}
		if(m_mapFileLines[i].find("<xCoord>") != std::string::npos && checkForNpcVals)
		{
			std::string xStr = extractXmlValue("<xCoord>", i);
			int x = atoi(xStr.c_str());
			curNpc.setPosition(x, curNpc.getPosition().m_y);
		}
		if(m_mapFileLines[i].find("<yCoord>") != std::string::npos && checkForNpcVals)
		{
			std::string yStr = extractXmlValue("<yCoord>", i);
			int y = atoi(yStr.c_str());
			curNpc.setPosition(curNpc.getPosition().m_x, y);
		}
		if(m_mapFileLines[i].find("<desiredMush>") != std::string::npos && checkForNpcVals)
		{
			std::string mushStr = extractXmlValue("<desiredMush>", i);
			if(mushStr == "edible")
			{
				curNpc.setMushroomType(MushroomType::EDIBLE);
			}
			else if(mushStr == "glowing")
			{
				curNpc.setMushroomType(MushroomType::GLOWING);
			}
			else if(mushStr == "poisonous")
			{
				curNpc.setMushroomType(MushroomType::POISONOUS);
			}
			else if(mushStr == "psychoactive")
			{
				curNpc.setMushroomType(MushroomType::PSYCHOACTIVE);
			}
		}
		if(m_mapFileLines[i].find("</npc>") != std::string::npos)
		{
			curTexture.m_loops = true;
			curNpc.setTexture(curTexture);
			curGridspace.addNPC(curNpc);
			checkForNpcVals = false;
			curTexture.reset();
		}
		//==============================================================================================

		//Mushroom Spawner Specific=============================================================================
		if(m_mapFileLines[i].find("<mushroomSpawner>") != std::string::npos)
			checkForMushVals = true;
		if(m_mapFileLines[i].find("<spriteSource>") != std::string::npos && checkForMushVals)
		{
			std::string resourceStr = "Art\\";
			switch(curBiome.getId())
			{
			case BiomeType::CAVE: resourceStr.append("cave\\");
				break;
			case BiomeType::FOREST: resourceStr.append("forest\\");
				break;
			case BiomeType::COW_FIELDS: resourceStr.append("farm\\");
				break;
			case BiomeType::SWAMP: resourceStr.append("swamp\\");
				break;
			default:break;
			}
			resourceStr.append(extractXmlValue("<spriteSource>", i));
			char* resource = new char[resourceStr.size()+1];
			strcpy(resource, resourceStr.c_str());
			curTexture.m_resource = resource;
		}
		if(m_mapFileLines[i].find("<xCoord>") != std::string::npos && checkForMushVals)
		{
			std::string xStr = extractXmlValue("<xCoord>", i);
			int x = atoi(xStr.c_str());
			curMushroomSpawn.setPosition(x, curMushroomSpawn.getPosition().m_y);
		}
		if(m_mapFileLines[i].find("<yCoord>") != std::string::npos && checkForMushVals)
		{
			std::string yStr = extractXmlValue("<yCoord>", i);
			int y = atoi(yStr.c_str());
			curMushroomSpawn.setPosition(curMushroomSpawn.getPosition().m_x, y);
		}
		if(m_mapFileLines[i].find("<mushType>") != std::string::npos && checkForMushVals)
		{
			std::string mushStr = extractXmlValue("<mushType>", i);
			if(mushStr == "edible")
			{
				curMushroomSpawn.setType(MushroomType::EDIBLE);
			}
			else if(mushStr == "glowing")
			{
				curMushroomSpawn.setType(MushroomType::GLOWING);
			}
			else if(mushStr == "poisonous")
			{
				curMushroomSpawn.setType(MushroomType::POISONOUS);
			}
			else if(mushStr == "psychoactive")
			{
				curMushroomSpawn.setType(MushroomType::PSYCHOACTIVE);
			}
			else if(mushStr == "littleBrown")
			{
				curMushroomSpawn.setType(MushroomType::LITTLE_BROWN);
			}
		}
		if(m_mapFileLines[i].find("<spawnChance>") != std::string::npos && checkForMushVals)
		{
			std::string chanceStr = extractXmlValue("<spawnChance>", i);
			int chance = atoi(chanceStr.c_str());
			curMushroomSpawn.setSpawnChance(chance);
		}
		if(m_mapFileLines[i].find("</mushroomSpawner>") != std::string::npos)
		{
			curTexture.m_frameWidth = TILE_SIZE; //constant for all mushrooms
			curTexture.m_frameHeight = TILE_SIZE; //constant
			curTexture.m_camLayer = 1; //constant
			curTexture.m_frameColumns = 2; //constant
			curTexture.m_loops = true;
			curMushroomSpawn.setTexture(curTexture);
			curGridspace.addMushroomSpawner(curMushroomSpawn);
			checkForMushVals = false;
			curTexture.reset();
		}
		//==============================================================================================

		//Item Chest Specific===========================================================================
		if(m_mapFileLines[i].find("<itemChest>") != std::string::npos)
			checkForChestVals = true;
		if(m_mapFileLines[i].find("<spriteSource>") != std::string::npos && checkForChestVals)
		{
			/*std::string resourceStr = "Art\\";
			switch(curBiome.getId())
			{
			case BiomeType::CAVE: resourceStr.append("cave\\");
				break;
			case BiomeType::FOREST: resourceStr.append("forest\\");
				break;
			case BiomeType::COW_FIELDS: resourceStr.append("farm\\");
				break;
			case BiomeType::SWAMP: resourceStr.append("swamp\\");
				break;
			default:break;
			}
			resourceStr.append(extractXmlValue("<spriteSource>", i));
			char* resource = new char[resourceStr.size()+1];
			strcpy(resource, resourceStr.c_str());*/
			//curTexture.m_resource = "Art\\BackgroundSprites\\ItemChest";
		}
		if(m_mapFileLines[i].find("<layer>") != std::string::npos && checkForChestVals)
		{
			std::string layerStr = extractXmlValue("<layer>", i);
			int layer = atoi(layerStr.c_str());
			curTexture.m_camLayer = layer;
		}
		if(m_mapFileLines[i].find("<width>") != std::string::npos && checkForChestVals)
		{
			//std::string wStr = extractXmlValue("<width>", i);
			//int width = atoi(wStr.c_str());
			//curTexture.m_frameWidth = width * TILE_SIZE;
			//curItemChest.setWidth(width);
		}
		if(m_mapFileLines[i].find("<height>") != std::string::npos && checkForChestVals)
		{
			//std::string hStr = extractXmlValue("<height>", i);
			//int height = atoi(hStr.c_str());
			//curTexture.m_frameHeight = height * TILE_SIZE;
			//curItemChest.setHeight(height);
		}
		if(m_mapFileLines[i].find("<animCols>") != std::string::npos && checkForChestVals)
		{
			//std::string animStr = extractXmlValue("<animCols>", i);
			//int columns = atoi(animStr.c_str());
			//curTexture.m_frameColumns = columns;
		}
		if(m_mapFileLines[i].find("<xCoord>") != std::string::npos && checkForChestVals)
		{
			std::string xStr = extractXmlValue("<xCoord>", i);
			int x = atoi(xStr.c_str());
			curItemChest.setPosition(x, curItemChest.getPosition().m_y);
		}
		if(m_mapFileLines[i].find("<yCoord>") != std::string::npos && checkForChestVals)
		{
			std::string yStr = extractXmlValue("<yCoord>", i);
			int y = atoi(yStr.c_str());
			curItemChest.setPosition(curItemChest.getPosition().m_x, y);
		}
		if(m_mapFileLines[i].find("<initialItem>") != std::string::npos && checkForChestVals)
		{
			std::string itemStr = extractXmlValue("<initialItem>", i);
			if(itemStr == "shovel")
			{
				curItemChest.setItem(ItemType::SHOVEL);
			}
			else if(itemStr == "lantern")
			{
				curItemChest.setItem(ItemType::LANTERN);
			}
			else if(itemStr == "cowFeed")
			{
				curItemChest.setItem(ItemType::COW_FEED);
			}
			else if(itemStr == "bugSpray")
			{
				curItemChest.setItem(ItemType::BUG_SPRAY);
			}
		}
		if(m_mapFileLines[i].find("</itemChest>") != std::string::npos)
		{
			curTexture.m_resource = "Art\\BackgroundSprites\\ItemChest.png";
			curTexture.m_frameColumns = 4;
			curTexture.m_loops = true;
			curTexture.m_frameWidth = 2 * TILE_SIZE;
			curItemChest.setWidth(2);
			curItemChest.setTexture(curTexture);
			curTexture.m_frameHeight = 2 * TILE_SIZE;
			curItemChest.setHeight(2);
			curGridspace.addItemChest(curItemChest);
			checkForChestVals = false;
			curTexture.reset();
		}
		//==============================================================================================

		//Cow Specific==================================================================================
		if(m_mapFileLines[i].find("<cow>") != std::string::npos)
			checkForCowVals = true;
		if(m_mapFileLines[i].find("<spriteSource>") != std::string::npos && checkForCowVals)
		{
			/*std::string resourceStr = "Art\\";
			switch(curBiome.getId())
			{
			case BiomeType::CAVE: resourceStr.append("cave\\");
				break;
			case BiomeType::FOREST: resourceStr.append("forest\\");
				break;
			case BiomeType::COW_FIELDS: resourceStr.append("farm\\");
				break;
			case BiomeType::SWAMP: resourceStr.append("swamp\\");
				break;
			default:break;
			}
			resourceStr.append(extractXmlValue("<spriteSource>", i));
			char* resource = new char[resourceStr.size()+1];
			strcpy(resource, resourceStr.c_str());
			curTexture.m_resource = resource;*/
		}
		if(m_mapFileLines[i].find("<layer>") != std::string::npos && checkForCowVals)
		{
			std::string layerStr = extractXmlValue("<layer>", i);
			int layer = atoi(layerStr.c_str());
			curTexture.m_camLayer = layer;
		}
		if(m_mapFileLines[i].find("<width>") != std::string::npos && checkForCowVals)
		{
			std::string wStr = extractXmlValue("<width>", i);
			int width = atoi(wStr.c_str());
			curTexture.m_frameWidth = width * TILE_SIZE;
			curCow.setWidth(width);
		}
		if(m_mapFileLines[i].find("<height>") != std::string::npos && checkForCowVals)
		{
			std::string hStr = extractXmlValue("<height>", i);
			int height = atoi(hStr.c_str());
			curTexture.m_frameHeight = height * TILE_SIZE;
			curCow.setHeight(height);
		}
		if(m_mapFileLines[i].find("<animCols>") != std::string::npos && checkForCowVals)
		{
			//std::string animStr = extractXmlValue("<animCols>", i);
			//int columns = atoi(animStr.c_str());
			//curTexture.m_frameColumns = columns;
		}
		if(m_mapFileLines[i].find("<xCoord>") != std::string::npos && checkForCowVals)
		{
			std::string xStr = extractXmlValue("<xCoord>", i);
			int x = atoi(xStr.c_str());
			curCow.setPosition(x, curCow.getPosition().m_y);
		}
		if(m_mapFileLines[i].find("<yCoord>") != std::string::npos && checkForCowVals)
		{
			std::string yStr = extractXmlValue("<yCoord>", i);
			int y = atoi(yStr.c_str());
			curCow.setPosition(curCow.getPosition().m_x, y);
		}
		if(m_mapFileLines[i].find("<spawnerId>") != std::string::npos && checkForCowVals)
		{
			std::string idStr = extractXmlValue("<spawnerId>", i);
			int id = atoi(idStr.c_str());
			curCow.setSpawnId(id);
		}
		if(m_mapFileLines[i].find("</cow>") != std::string::npos)
		{
			curTexture.m_resource = "Art\\CharacterSprites\\cow.png";
			curTexture.m_loops = true;
			curTexture.m_frameColumns = 6;
			curTexture.m_frameWidth = 2 * TILE_SIZE;
			curCow.setWidth(2);
			curTexture.m_frameHeight = 2 * TILE_SIZE;
			curCow.setHeight(2);
			curCow.setTexture(curTexture);
			curGridspace.addCow(curCow);
			checkForCowVals = false;
			curTexture.reset();
		}
		//==============================================================================================

	}
}

//parse a line of xml to get the value between the tags
std::string Map::extractXmlValue(std::string tag, int lineIndex)
{
	int valIndex = m_mapFileLines[lineIndex].find(tag) + tag.length();
	std::string value = m_mapFileLines[lineIndex].substr(valIndex, m_mapFileLines[lineIndex].find("</") - valIndex);
	return value;
}

void Map::initObjects()
{
	for(int i = 0; i < m_biomes.size(); ++i)
	{
		for(int k = 0; k < m_biomes[i].getNumGridspaces(); ++k)
		{
			m_biomes[i].getGridspace(k)->initializeCows();
		}
	}
}