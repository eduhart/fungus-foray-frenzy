#include "Camera.h"


Camera::Camera(int id, int x, int y, int height, int width, Player* player)
	: m_idNum(id)
	, m_leftX(x)
	, m_topY(y)
	, m_height(height)
	, m_width(width)
	, m_player(player)
	, m_gui()
	, m_curState(CamState::NORMAL)
	, m_curGridspace()
	, m_layers()
{
}


Camera::~Camera(void)
{
}

void Camera::update()
{

}

