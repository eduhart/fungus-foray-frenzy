#pragma once
#include "../Globals.h"
#include "../GameObjects/GameObject.h"
#include "Gridspace.h"
#include "../GameObjects/player.h"
#include "GUI.h"
#include <vector>


class Camera
{
public:
	Camera(int id, int x, int y, int height, int width, Player* player);
	~Camera(void);

	//different render states
	enum CamState
	{
		NORMAL,			//Normal updating cycle with movement updates
		NEW_GRIDSPACE,	//Sliding the screen when changing to a new section
		FADE_BLACK,		//Fade when changing to a new biome

		NUM_STATES
	};

	void update();
	inline void setGridspace(Gridspace* newSpace) {m_curGridspace = newSpace;};
	inline int getX() {return m_leftX;};
	inline int getY() {return m_topY;};
	inline int getHeight() {return m_height;};
	inline int getWidth() {return m_width;};
	inline CamState getState() {return m_curState;};
	inline void setState(CamState state) {m_curState = state;};
	inline Gridspace* getGridspace() {return m_curGridspace;};
	inline Player* getPlayer() {return m_player;};
	inline GUI* getGui() {return &(m_gui);};

private:
	int m_idNum;
	int m_height;
	int m_width;
	int m_topY;
	int m_leftX;

	Player* m_player;
	GUI m_gui;

	CamState m_curState;

	Gridspace* m_curGridspace;
	std::vector<Layer> m_layers;
};
