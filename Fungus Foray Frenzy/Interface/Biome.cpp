#include "Biome.h"


Biome::Biome(void)
	: m_biomeId(BiomeType::FOREST)
	, m_gridspaces()
{
}

Biome::Biome(BiomeType id)
	: m_biomeId(id)
	, m_gridspaces()
{
}

Biome::~Biome(void)
{
}

Gridspace* Biome::getGridspace(int id)
{
	Gridspace* space;
	for(int i = 0; i < m_gridspaces.size(); ++i)
	{
		if(m_gridspaces[i].getId() == id)
			space = &(m_gridspaces[i]);
	}
	return space;
}

