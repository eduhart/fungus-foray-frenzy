#ifndef GRIDSPACE_H
#define GRIDSPACE_H

#include "../Globals.h"

#include "../GameObjects/GameObject.h"
#include "../GameObjects/Mushroom.h"
#include "../GameObjects/MushroomSpawner.h"
#include "../GameObjects/Chest.h"
#include "../GameObjects/npc.h"
#include "../GameObjects/BkgObject.h"
#include "../GameObjects/Cow.h"
#include <vector>


class Gridspace
{
public:
	Gridspace(void);
	Gridspace(BiomeType biome, int id);
	~Gridspace(void);

	struct GridConnection
	{
		BiomeType m_biome;
		int m_toGridId;
		CardinalDirection m_fromDirection;
		CardinalDirection m_toDirection;
		GridConnection(BiomeType biome, int id, CardinalDirection from, CardinalDirection to)
			: m_biome(biome)
			, m_toGridId(id)
			, m_fromDirection(from)
			, m_toDirection(to)
		{}
		GridConnection()
			: m_biome(BiomeType::FOREST)
			, m_toGridId(0)
			, m_fromDirection(CardinalDirection::NUM_DIRECTIONS)
			, m_toDirection(CardinalDirection::NUM_DIRECTIONS)
		{}
	};

	struct Tile //used only on layer1 for collisions
	{
		int m_x;
		int m_y;
		bool isOccupied;
		Tile(int x, int y, bool occupied)
			: m_x(x)
			, m_y(y)
			, isOccupied(occupied)
		{}

	};

	void resetToDefaults();

	//basic getters and setters
	void setTexture(char* resource, int frameW = 0, int frameH = 0, int columns = 0);
	inline void setTexture(TexInfo texInfo) {m_bkgTexture = texInfo;};
	inline TexInfo getTexture() {return m_bkgTexture;};
	inline int getId() {return m_idNum;};
	inline BiomeType getBiome() {return m_biomeId;};

	//adding objects
	void addNPC(Npc npcToAdd);
	void addMushroom(Mushroom mushToAdd);
	void addMushroomSpawner(MushroomSpawner spawnToAdd);
	void addItemChest(Chest chestToAdd);
	void addBkgObject(BkgObject objToAdd);
	void addCow(Cow cowToAdd);
	void addConnection(GridConnection connection);
	void addConnection(BiomeType biome, int id, CardinalDirection from, CardinalDirection to);
	void addPlayer(PlayerType id);
	void removePlayer(PlayerType id);

	//dealing with things on the collision layer
	void addCollisionTile(int x, int y);
	void addCollisionTile(GameObject* obj);
	void removeCollisionTile(int x, int y);
	void removeCollisionTile(GameObject* obj);
	bool hasCollisionTile(int x, int y);

	GridConnection getConnectionAt(CardinalDirection direction);
	bool hasConnectionAt(CardinalDirection direction);

	Npc* getNpc(int index);
	Npc* getNpc(int x, int y);
	inline int getNumNpcs() {return m_NPCs.size();};
	bool hasNpc(int x, int y);

	Mushroom* getMushroom(int index);
	Mushroom* getMushroom(int x, int y);
	Mushroom getAndRemoveMushroom(int index);
	Mushroom getAndRemoveMushroom(int x, int y);
	bool hasMushroom(int x, int y);
	inline int getNumMushrooms() {return m_mushrooms.size();};
	MushroomSpawner* getMushroomSpawner(int index);
	inline int getNumMushSpawners() {return m_mushSpawners.size();};

	Chest* getChest(int index);
	Chest* getChest(int x, int y);
	bool hasChest(int x, int y);
	inline int getNumItemChests() {return m_itemChests.size();};

	BkgObject* getBkgObject(int index);
	//BkgObject getBkgObject(int x, int y);
	inline int getNumBkgObjects() {return m_bkgObjects.size();};

	Cow* getCow(int index);
	Cow* getCow(int x, int y);
	bool hasCow(int x, int y);
	inline int getNumCows() {return m_cows.size();};

	bool hasPlayer(PlayerType id);
	inline int getNumPlayers() {return m_curPlayerIds.size();};

	std::vector<GameObject*> getAllObjects();
	void cleanUpDeadObjects();
	void checkSpawnedMushrooms();
	void initializeCows();

private:
	TexInfo m_bkgTexture;

	int m_idNum;
	BiomeType m_biomeId;

	std::vector<Npc> m_NPCs;
	std::vector<Mushroom> m_mushrooms;
	std::vector<MushroomSpawner> m_mushSpawners;
	std::vector<Chest> m_itemChests;
	std::vector<BkgObject> m_bkgObjects;
	std::vector<Cow> m_cows;
	std::vector<PlayerType> m_curPlayerIds;
	
	std::vector<GridConnection> m_connections;
	std::vector<std::vector<Tile>> m_tilesLayer1;
};

#endif
