#include "GUI.h"

//dimensional constants for all the icons ======
static const int MUSH_HEIGHT = 1;
static const int MUSH_WIDTH = 2;
static const int ITEM_HEIGHT = 2;
static const int ITEM_WIDTH = 4;
static const int SCORE_HEIGHT = 2;
static const int SCORE_WIDTH = 4;
static const int FADE_SIZE = 1;

static const Point ITEM_POS(16,0);
static const Point SCORE_POS(8,0);
static const int MUSH_X = 1;
static const int MUSH_Y = 23;
//==============================================

static const int LANTERN_SIZE = 3;
static const int NO_LANTERN = 1;
char* DARK_RES = "Art\\GUI\\PlayerDark.png";
char* LANTERN_RES = "Art\\GUI\\PlayerLantern.png";

GUI::GUI(void)
	: m_curBiome()
    , m_edibleSprite()
	, m_poisonousSprite()
	, m_psychoactiveSprite()
	, m_glowingSprite()
	, m_littleBrownSprite()
	, m_itemSprite()
	, m_scoreSprite()
	, m_fadeSprites()
	, m_isUsingLantern(false)
	, m_pPlayer()
{
	//resources
	char* EDIBLE_RES = "Art\\GUI\\edibleIcon.png";
	char* POISON_RES = "Art\\GUI\\poisonousIcon.png";
	char* GLOWING_RES = "Art\\GUI\\glowingIcon.png";
	char* PSYCHO_RES = "Art\\GUI\\psychoactiveIcon.png";
	char* LITTLEB_RES = "Art\\GUI\\littleBrownIcon.png";
	char* ITEM_RES = "Art\\GUI\\itemIcon.png";
	char* SCORE_RES = "Art\\GUI\\PlayerScore.png";
	char* FADE_RES = "Art\\GUI\\fadeTile.png";

	m_edibleSprite.setPosition(7, MUSH_Y);
	m_poisonousSprite.setPosition(11, MUSH_Y);
	m_psychoactiveSprite.setPosition(15, MUSH_Y);
	m_glowingSprite.setPosition(19, MUSH_Y);
	m_littleBrownSprite.setPosition(23, MUSH_Y);
	m_itemSprite.setPosition(ITEM_POS);
	m_scoreSprite.setPosition(SCORE_POS);
	m_playerDarkness.setPosition(0,0);

	m_edibleSprite.setHeight(MUSH_HEIGHT);
	m_edibleSprite.setWidth(MUSH_WIDTH);
	m_poisonousSprite.setWidth(MUSH_WIDTH);
	m_poisonousSprite.setHeight(MUSH_HEIGHT);
	m_psychoactiveSprite.setWidth(MUSH_WIDTH);
	m_psychoactiveSprite.setHeight(MUSH_HEIGHT);
	m_glowingSprite.setWidth(MUSH_WIDTH);
	m_glowingSprite.setHeight(MUSH_HEIGHT);
	m_littleBrownSprite.setWidth(MUSH_WIDTH);
	m_littleBrownSprite.setHeight(MUSH_HEIGHT);
	m_itemSprite.setWidth(ITEM_WIDTH);
	m_itemSprite.setHeight(ITEM_HEIGHT);
	m_scoreSprite.setHeight(SCORE_HEIGHT);
	m_scoreSprite.setWidth(SCORE_WIDTH);
	m_playerDarkness.setWidth(NO_LANTERN + 1);
	m_playerDarkness.setHeight(NO_LANTERN + 1);
	
	m_edibleSprite.setTexture(EDIBLE_RES);
	m_poisonousSprite.setTexture(POISON_RES);
	m_psychoactiveSprite.setTexture(PSYCHO_RES);
	m_glowingSprite.setTexture(GLOWING_RES);
	m_littleBrownSprite.setTexture(LITTLEB_RES);
	m_itemSprite.setTexture(ITEM_RES, ITEM_WIDTH * TILE_SIZE, ITEM_HEIGHT * TILE_SIZE, 1, 3);
	m_scoreSprite.setTexture(SCORE_RES);
	m_playerDarkness.setTexture(DARK_RES, (NO_LANTERN+3)*TILE_SIZE, (NO_LANTERN+3)*TILE_SIZE, 2, 3);

	m_itemSprite.setAnimFrames(0,0);
	m_playerDarkness.setAnimFrames(0,0);
	
	//push tiles to cover the screen for fades and cave darkness
	for(int i = 0; i < CAM_TILES_X; ++i)
	{
		std::vector<BkgObject> row;
		m_fadeSprites.push_back(row);
		for(int k = 0; k < CAM_TILES_Y; ++k)
		{
			BkgObject tempObj;
			tempObj.setPosition(i,k);
			tempObj.setHeight(FADE_SIZE);
			tempObj.setWidth(FADE_SIZE);
			tempObj.setTexture(FADE_RES, TILE_SIZE, TILE_SIZE, 11, 3);
			tempObj.setAnimFrames(0,0);
			m_fadeSprites[i].push_back(tempObj);
		}
	}

}

GUI::~GUI(void)
{
}

//set the item icon to the correct frame
void GUI::setItem(ItemType item)
{
	switch(item)
	{
	case ItemType::BUG_SPRAY:
		m_itemSprite.setAnimFrames(3,3);
		break;
	case ItemType::COW_FEED:
		m_itemSprite.setAnimFrames(2,2);
		break;
	case ItemType::LANTERN:
		m_itemSprite.setAnimFrames(1,1);
		break;
	case ItemType::SHOVEL:
		m_itemSprite.setAnimFrames(0,0);
		break;
	default: break;
	}
}

void GUI::beginFade()
{
	BiomeType fromWhichBiome = m_curBiome;
	for(int i = 0; i < m_fadeSprites.size(); ++i)
	{
		for(int k = 0; k < m_fadeSprites[i].size(); ++k)
		{
			if(fromWhichBiome != BiomeType::CAVE)
				m_fadeSprites[i][k].setAnimFrames(1,22); //do full fade in-out 
			else
				m_fadeSprites[i][k].setAnimFrames(11,22); //start at black for the cave
		}
	}
}

void GUI::endFade()
{
	for(int i = 0; i < m_fadeSprites.size(); ++i)
	{
		for(int k = 0; k < m_fadeSprites[i].size(); ++k)
		{
			m_fadeSprites[i][k].setAnimFrames(0,0); //set all tiles to transparent
		}
	}
	m_playerDarkness.setAnimFrames(0,0);
}

//get all the sprites
std::vector<GameObject*> GUI::getAllObjects()
{
	std::vector<GameObject*> objs;

	objs.push_back(&m_playerDarkness);
	for(int i = 0; i < m_fadeSprites.size(); ++i)
	{
		for(int k = 0; k < m_fadeSprites[i].size(); ++k)
		{
			objs.push_back(&(m_fadeSprites[i][k]));
		}
	}
	objs.push_back(&m_edibleSprite);
	objs.push_back(&m_poisonousSprite);
	objs.push_back(&m_psychoactiveSprite);
	objs.push_back(&m_glowingSprite);
	objs.push_back(&m_littleBrownSprite);
	objs.push_back(&m_itemSprite);
	objs.push_back(&m_scoreSprite);

	return objs;
}

std::vector<GameObject*> GUI::getFadeSprites()
{
	std::vector<GameObject*> objs;
	for(int i = 0; i < m_fadeSprites.size(); ++i)
	{
		for(int k = 0; k < m_fadeSprites[i].size(); ++k)
		{
			objs.push_back(&(m_fadeSprites[i][k]));
		}
	}
	return objs;
}

//set the character overla to be big or small
void GUI::toggleLantern(bool isOn)
{
	m_isUsingLantern = isOn;
	if(m_curBiome == BiomeType::CAVE)
	{
		if(isOn)
		{
			m_playerDarkness.setWidth(8);
			m_playerDarkness.setHeight(8);
			m_playerDarkness.setTexture(LANTERN_RES, 128, 128, 2, 3);
			m_playerDarkness.setAnimFrames(1,1);
		}
		else
		{
			m_playerDarkness.setWidth(NO_LANTERN + 1);
			m_playerDarkness.setHeight(NO_LANTERN + 1);
			m_playerDarkness.setTexture(DARK_RES, (NO_LANTERN+3)*TILE_SIZE, (NO_LANTERN+3)*TILE_SIZE, 2, 3);
			m_playerDarkness.setAnimFrames(1,1);
		}
	}
}

//turn darkness tiles on/off in regard to player's position and lantern use
void GUI::setDarkness(Point playerGridPos)
{
	if(m_curBiome == BiomeType::CAVE)
	{
		for(int i = 0; i < m_fadeSprites.size(); ++i)
		{
			for(int k = 0; k < m_fadeSprites[i].size(); ++k)
			{
				if(m_isUsingLantern) //big area transparent
				{
					m_fadeSprites[i][k].setAnimFrames(0,0);
					if(i < playerGridPos.m_x - (LANTERN_SIZE -1) || i > playerGridPos.m_x + LANTERN_SIZE)
						m_fadeSprites[i][k].setAnimFrames(10,10);
					if(k < playerGridPos.m_y - (LANTERN_SIZE -1) || k > playerGridPos.m_y + LANTERN_SIZE )
						m_fadeSprites[i][k].setAnimFrames(10,10);
				}
				else //small area transparent
				{
					m_fadeSprites[i][k].setAnimFrames(0,0);
					if(i < playerGridPos.m_x || i > playerGridPos.m_x + NO_LANTERN)
						m_fadeSprites[i][k].setAnimFrames(10,10);
					if(k < playerGridPos.m_y || k > playerGridPos.m_y + NO_LANTERN)
						m_fadeSprites[i][k].setAnimFrames(10,10);
					
				}
			}
		}
		if(m_isUsingLantern)
			m_playerDarkness.setPosition(m_pPlayer->getPosition().m_x - 56, m_pPlayer->getPosition().m_y - 56);
		else
			m_playerDarkness.setPosition(m_pPlayer->getPosition().m_x - 24, m_pPlayer->getPosition().m_y - 24);
		m_playerDarkness.setAnimFrames(1,1);
	}
	else
	{
		endFade();
	}
}

