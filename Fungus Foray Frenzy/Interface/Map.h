#pragma once
#include "../Globals.h"

#include "Biome.h"
#include "Gridspace.h"
#include <vector>
#include <string>

using namespace std;

class Map
{
public:
	Map(int levelId);
	~Map(void);

	void load();
	void testLoad();
	void getXmlAsString();
	void initObjects(); //initializes objects that need references to each other

	Gridspace* getGridspace(BiomeType biome, int id);
	Biome* getBiome(BiomeType id);

	std::string extractXmlValue(std::string tag, int lineIndex);

private:
	int m_levelId;
	std::vector<Biome> m_biomes;
	std::vector<std::string> m_mapFileLines;
};
