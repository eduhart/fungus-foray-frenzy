#pragma once
#include "../Globals.h"
#include "../GameObjects/GameObject.h"
#include "../GameObjects/BkgObject.h"
#include "../GameObjects/player.h"
#include <vector>

//each camera gets one of these
class GUI
{
public:
	GUI(void);
	~GUI(void);

	std::vector<GameObject*> getAllObjects();
	std::vector<GameObject*> getFadeSprites();
	
	void setItem(ItemType item);
	void beginFade();
	void endFade();
	void toggleLantern(bool isOn);
	void setDarkness(Point playerGridPos);

	inline void setPlayer(Player* thePlayer) {m_pPlayer = thePlayer;}
	inline void setBiome(BiomeType theBiome) {m_curBiome = theBiome;}
	inline BiomeType getBiome() {return m_curBiome;}
	inline BkgObject* getPlayerDarkness() {return &m_playerDarkness;}
	inline bool isLanternOn() {return m_isUsingLantern;}

private:
	BiomeType m_curBiome;

	//icons for the number of each mushroom owned 
	BkgObject m_edibleSprite;
	BkgObject m_poisonousSprite;
	BkgObject m_psychoactiveSprite;
	BkgObject m_glowingSprite;
	BkgObject m_littleBrownSprite;

	//icons for player stats
	BkgObject m_itemSprite;
	BkgObject m_scoreSprite;
	
	//black filters
	std::vector<std::vector<BkgObject>> m_fadeSprites;
	BkgObject m_caveSprite;
	BkgObject m_playerDarkness;
	bool m_isUsingLantern;
	Player* m_pPlayer;
};
