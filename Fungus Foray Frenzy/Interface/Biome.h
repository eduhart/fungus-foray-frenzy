#pragma once
#include "../Globals.h"

#include "Gridspace.h"
#include <vector>


class Biome
{
public:
	Biome(void);
	Biome(BiomeType id);
	~Biome(void);

	inline BiomeType getId() {return m_biomeId;};
	inline void addGridspace(Gridspace space) {m_gridspaces.push_back(space);};

	Gridspace* getGridspace(int id);
	inline int getNumGridspaces() {return m_gridspaces.size();}

private:

	BiomeType m_biomeId;
	std::vector<Gridspace> m_gridspaces;

};
