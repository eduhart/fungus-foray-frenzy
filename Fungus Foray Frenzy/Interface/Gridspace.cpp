#include "Gridspace.h"


Gridspace::Gridspace(void)
	: m_bkgTexture("")
	, m_idNum(0)
	, m_biomeId(BiomeType::FOREST)
	, m_NPCs()
	, m_mushrooms()
	, m_itemChests()
	, m_bkgObjects()
	, m_connections()
	, m_tilesLayer1()

{
	//fill the collision layer with empty tiles
	for(int i = 0; i < CAM_TILES_X; ++i)
	{
		std::vector<Tile> row;
		m_tilesLayer1.push_back(row);
		for(int k = 0; k < CAM_TILES_Y; ++k)
		{
			Tile tempTile(i,k,false);
			m_tilesLayer1[i].push_back(tempTile);
		}
	}
}

Gridspace::Gridspace(BiomeType biome, int id)
	: m_bkgTexture("")
	, m_idNum(id)
	, m_biomeId(biome)
	, m_NPCs()
	, m_mushrooms()
	, m_itemChests()
	, m_bkgObjects()
	, m_connections()
	, m_tilesLayer1()
{
	//fill the collision layer with empty tiles
	for(int i = 0; i < CAM_TILES_X; ++i)
	{
		std::vector<Tile> row;
		m_tilesLayer1.push_back(row);
		for(int k = 0; k < CAM_TILES_Y; ++k)
		{
			Tile tempTile(i,k,false);
			m_tilesLayer1[i].push_back(tempTile);
		}
	}
}


Gridspace::~Gridspace(void)
{
}

void Gridspace::resetToDefaults()
{
	m_NPCs.clear();
	m_connections.clear();
	m_mushrooms.clear();
	m_itemChests.clear();
	m_bkgObjects.clear();
	m_cows.clear();
	m_tilesLayer1.clear();

	setTexture("");
}

void Gridspace::addNPC(Npc npcToAdd)
{
	if(npcToAdd.getTexture().m_camLayer == 1)
		addCollisionTile(&(npcToAdd));
	m_NPCs.push_back(npcToAdd);
}

Npc* Gridspace::getNpc(int index)
{
	if(index < m_NPCs.size() && index >= 0)
		return &(m_NPCs[index]);
	return &(Npc());
}

//find an npc at a particular tile location
Npc* Gridspace::getNpc(int x, int y)
{
	Npc* theNpc;
	for(int i = 0; i < m_NPCs.size(); ++i)
	{
		for(int k = 0; k < m_NPCs[i].getWidth(); ++k)
		{
			for(int j = 0; j < m_NPCs[i].getHeight(); ++j)
			{
				if(m_NPCs[i].getPosition().m_x + k == x &&
					m_NPCs[i].getPosition().m_y + j == y)
				{
					theNpc = &(m_NPCs[i]);
				}
			}
		}
		
	}
	return theNpc;
}

bool Gridspace::hasNpc(int x, int y)
{
	for(int i = 0; i < m_NPCs.size(); ++i)
	{
		for(int k = 0; k < m_NPCs[i].getWidth(); ++k)
		{
			for(int j = 0; j < m_NPCs[i].getHeight(); ++j)
			{
				if(m_NPCs[i].getPosition().m_x + k == x &&
					m_NPCs[i].getPosition().m_y + j == y)
				{
					return true;
				}
			}
		}
	}
	return false;
}

void Gridspace::addMushroom(Mushroom mushToAdd)
{
	if(mushToAdd.getTexture().m_camLayer == 1)
		addCollisionTile(&(mushToAdd));
	m_mushrooms.push_back(mushToAdd);
}

Mushroom* Gridspace::getMushroom(int index)
{
	if(index < m_mushrooms.size() && index >= 0)
		return &(m_mushrooms[index]);
	return &(Mushroom());
}

void Gridspace::addMushroomSpawner(MushroomSpawner spawnToAdd)
{
	m_mushSpawners.push_back(spawnToAdd);
}

MushroomSpawner* Gridspace::getMushroomSpawner(int index)
{
	if(index < m_mushSpawners.size() && index >= 0)
		return &(m_mushSpawners[index]);
	return &(MushroomSpawner());
}

//find a mushroom at a particular tile location
bool Gridspace::hasMushroom(int x, int y)
{
	for(int i = 0; i < m_mushrooms.size(); ++i)
	{
		if(m_mushrooms[i].getPosition().m_x == x &&
			m_mushrooms[i].getPosition().m_y == y && m_mushrooms[i].exists())
		{
			return true;
		}
	}

	return false;
}

Mushroom* Gridspace::getMushroom(int x, int y)
{
	Mushroom* mush;
	for(int i = 0; i < m_mushrooms.size(); ++i)
	{
		if(m_mushrooms[i].getPosition().m_x == x &&
			m_mushrooms[i].getPosition().m_y == y)
		{
			mush = &(m_mushrooms[i]);
		}
	}
	return mush;
}

//find a mushroom at a particular location, AND pull it out
//of the object list
Mushroom Gridspace::getAndRemoveMushroom(int x, int y)
{
	Mushroom mush;
	for(int i = 0; i < m_mushrooms.size(); ++i)
	{
		if(m_mushrooms[i].getPosition().m_x == x &&
			m_mushrooms[i].getPosition().m_y == y)
		{
			mush = m_mushrooms[i];
			if(mush.getTexture().m_camLayer == 1)
				removeCollisionTile(x,y);
			m_mushrooms[i].toggleExistance(false);
			break;
		}
	}
	return mush;
}

void Gridspace::addItemChest(Chest chestToAdd)
{
	if(chestToAdd.getTexture().m_camLayer == 1)
		addCollisionTile(&(chestToAdd));
	m_itemChests.push_back(chestToAdd);
}

Chest* Gridspace::getChest(int index)
{
	if(index < m_itemChests.size() && index >= 0)
		return &(m_itemChests[index]);
	return &(Chest());
}

//find an item chest at a particular tile location
Chest* Gridspace::getChest(int x, int y)
{
	Chest* theChest;
	for(int i = 0; i < m_itemChests.size(); ++i)
	{
		for(int k = 0; k < m_itemChests[i].getWidth(); ++k)
		{
			for(int j = 0; j < m_itemChests[i].getHeight(); ++j)
			{
				if(m_itemChests[i].getPosition().m_x + k == x &&
					m_itemChests[i].getPosition().m_y + j == y)
				{
					theChest = &(m_itemChests[i]);
				}
			}
		}
		
	}
	return theChest;
}

bool Gridspace::hasChest(int x, int y)
{
	for(int i = 0; i < m_itemChests.size(); ++i)
	{
		for(int k = 0; k < m_itemChests[i].getWidth(); ++k)
		{
			for(int j = 0; j < m_itemChests[i].getHeight(); ++j)
			{
				if(m_itemChests[i].getPosition().m_x + k == x &&
					m_itemChests[i].getPosition().m_y + j == y && m_itemChests[i].exists())
				{
					return true;
				}
			}
		}
		
	}
	return false;

}

void Gridspace::initializeCows()
{
	for(int i = 0; i < m_cows.size(); ++i)
	{
		int spawnerId = m_cows[i].getSpawnId();
		if(spawnerId < getNumMushSpawners())
		{
			m_cows[i].setSpawner(&(m_mushSpawners[spawnerId]));
			m_mushSpawners[spawnerId].toggleCow(true);
			m_mushSpawners[spawnerId].toggleSpawn(false);
		}
	}
}

BkgObject* Gridspace::getBkgObject(int index)
{
	if(index < m_bkgObjects.size() && index >= 0)
		return &(m_bkgObjects[index]);
	return &(BkgObject());
}

void Gridspace::addBkgObject(BkgObject objToAdd)
{
	if(objToAdd.getTexture().m_camLayer == 1)
		addCollisionTile(&(objToAdd));
	m_bkgObjects.push_back(objToAdd);
}

void Gridspace::addCow(Cow cowToAdd)
{
	if(cowToAdd.getTexture().m_camLayer == 1)
		addCollisionTile(&(cowToAdd));
	m_cows.push_back(cowToAdd);
}

Cow* Gridspace::getCow(int index)
{
	if(index < m_cows.size() && index >= 0)
		return &(m_cows[index]);
	return &(Cow());
}

Cow* Gridspace::getCow(int x, int y)
{
	Cow* theCow;
	for(int i = 0; i < m_cows.size(); ++i)
	{
		for(int k = 0; k < m_cows[i].getWidth(); ++k)
		{
			for(int j = 0; j < m_cows[i].getHeight(); ++j)
			{
				if(m_cows[i].getPosition().m_x + k == x &&
					m_cows[i].getPosition().m_y + j == y)
				{
					theCow = &(m_cows[i]);
				}
			}
		}
		
	}
	return theCow;
}

bool Gridspace::hasCow(int x, int y )
{
	for(int i = 0; i < m_cows.size(); ++i)
	{
		for(int k = 0; k < m_cows[i].getWidth(); ++k)
		{
			for(int j = 0; j < m_cows[i].getHeight(); ++j)
			{
				if(m_cows[i].getPosition().m_x + k == x &&
					m_cows[i].getPosition().m_y + j == y && m_cows[i].exists())
				{
					return true;
				}
			}
		}
		
	}
	return false;
}

void Gridspace::addConnection(GridConnection connection)
{
	m_connections.push_back(connection);
}

void Gridspace::addConnection(BiomeType biome, int id, CardinalDirection from, CardinalDirection to)
{
	GridConnection connect(biome, id, from, to);

	if(m_connections.size() < CardinalDirection::NUM_DIRECTIONS) //no more than 4 connections
	{
		bool canAdd = true;
		for(int i = 0; i < m_connections.size(); ++i) 
		{
			if(m_connections[i].m_fromDirection == from) //prevent repeat direction additions
				canAdd = false;
		}

		if(canAdd)
			m_connections.push_back(connect);
	}
}

Gridspace::GridConnection Gridspace::getConnectionAt(CardinalDirection direction)
{
	for(int i = 0; i < m_connections.size(); ++i)
	{
		if(m_connections[i].m_fromDirection == direction)
			return m_connections[i];
	}

	GridConnection noSuchConnection;
	return noSuchConnection;
}

void Gridspace::addPlayer(PlayerType id)
{
	if(!hasPlayer(id))
	{
		m_curPlayerIds.push_back(id);
	}
}

void Gridspace::removePlayer(PlayerType id)
{
	for(int i = 0; i < m_curPlayerIds.size(); ++i)
	{
		if(m_curPlayerIds[i] == id)
		{
			std::vector<PlayerType>::iterator itr = m_curPlayerIds.begin(); 
			itr += i;
			m_curPlayerIds.erase(itr);
			break;
		}
	}
}


bool Gridspace::hasConnectionAt(CardinalDirection direction)
{
	for(int i = 0; i < m_connections.size(); ++i)
	{
		if(m_connections[i].m_fromDirection == direction)
			return true;
	}
	return false;
}

bool Gridspace::hasPlayer(PlayerType id)
{
	for(int i = 0; i < m_curPlayerIds.size(); ++i)
	{
		if(m_curPlayerIds[i] == id)
			return true;
	}
	return false;
}

//sets the texture information
void Gridspace::setTexture(char* resource, int frameW, int frameH, int columns)
{
	TexInfo tex(resource, frameW, frameH, columns);
	m_bkgTexture = tex;
}

//gets all the different types of objects in one general list
std::vector<GameObject*> Gridspace::getAllObjects()
{
	std::vector<GameObject*> objs;
	for(int i = 0; i < m_NPCs.size(); ++i)
	{
		objs.push_back(&(m_NPCs[i]));
	}
	for(int i = 0; i < m_mushrooms.size(); ++i)
	{
		if(m_mushrooms[i].exists())
			objs.push_back(&(m_mushrooms[i]));
	}
	for(int i = 0; i < m_itemChests.size(); ++i)
	{
		objs.push_back(&(m_itemChests[i]));
	}
	for(int i = 0; i < m_bkgObjects.size(); ++i)
	{
		objs.push_back(&(m_bkgObjects[i]));
	}
	for(int i = 0; i < m_cows.size(); ++i)
	{
		objs.push_back(&(m_cows[i]));
	}
	return objs;
}

void Gridspace::cleanUpDeadObjects()
{
	for(int i = 0; i < m_mushrooms.size(); ++i)
	{
		if(!m_mushrooms[i].exists())
		{
			std::vector<Mushroom>::iterator itr = m_mushrooms.begin(); 
			itr += i;
			m_mushrooms.erase(itr);
			i -= 2;
			//break;
		}
	}
}

void Gridspace::addCollisionTile(int x, int y)
{
	m_tilesLayer1[x][y].isOccupied = true;
}

void Gridspace::removeCollisionTile(int x, int y)
{
	m_tilesLayer1[x][y].isOccupied = false;
}

void Gridspace::addCollisionTile(GameObject* obj)
{
	int startX = obj->getPosition().m_x;
	int startY = obj->getPosition().m_y;
	for(int x = 0; x < obj->getWidth(); x++)
	{
		for(int y = 0; y < obj->getHeight(); y++)
		{
			addCollisionTile(startX + x, startY + y);
		}
	}
}

void Gridspace::removeCollisionTile(GameObject* obj)
{
	int startX = obj->getPosition().m_x;
	int startY = obj->getPosition().m_y;
	for(int x = 0; x < obj->getWidth(); x++)
	{
		for(int y = 0; y < obj->getHeight(); y++)
		{
			removeCollisionTile(startX + x, startY + y);
		}
	}
}

bool Gridspace::hasCollisionTile(int x, int y)
{
	if(x >= 0 && x < CAM_TILES_X && y >=0 && y < CAM_TILES_Y)
		return m_tilesLayer1[x][y].isOccupied;
	return false;
}

//loop through all mushroom spawners and see if there is a new mushroom
//if so, add it to the object list
//Note: This is ONLY called on a transition-in
void Gridspace::checkSpawnedMushrooms()
{
	m_mushrooms.clear();
	for(int i = 0; i < m_mushSpawners.size(); ++i)
	{
		m_mushSpawners[i].attemptToSpawn();
		if(m_mushSpawners[i].hasMushroom())
		{
			addMushroom(m_mushSpawners[i].makeMushroom());
		}
	}
}