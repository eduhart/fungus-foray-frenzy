#include "ActionNode.h"


ActionNode::ActionNode(void)
{
	info[0]="base character";
	info[1]="base action";

	next=NULL;
	prev=NULL;
}

ActionNode::ActionNode(string characterID,string action,ActionNode* nt,ActionNode* pv)
{
	info[0]=characterID;
	info[1]=action;

	next=nt;
	prev=pv;
}

ActionNode::ActionNode(string characterID,string action)
{
	info[0]=characterID;
	info[1]=action;

	next=NULL;
	prev=NULL;
}

ActionNode::~ActionNode(void)
{

}

void ActionNode::setNext(ActionNode* node)
{
	next=node;
}

void ActionNode::setPrev(ActionNode* node)
{
	prev=node;
}

void ActionNode::setInfo(string info1,string info2)
{
	info[0]=info1;
	info[1]=info2;
}

ActionNode* ActionNode::getNext()
{
	return next;
}

ActionNode* ActionNode::getPrev()
{
	return prev;
}

string* ActionNode::getInfo()
{
	return info;
}

void ActionNode::purgeNext()
{
	next=NULL;
}

void ActionNode::purgePrev()
{
	prev=NULL;
}