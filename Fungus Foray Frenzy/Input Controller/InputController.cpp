#include "InputController.h"


InputController::InputController(void)
{	
	checkControllers();
}


InputController::~InputController(void)
{
}

void InputController::processMessage(WPARAM msg)
{
	cout<<"Input : "<<msg<<endl;

	processMessage(msg,-1);
}

void InputController::processMessage(WPARAM msg,int player)
{
	int start=0;
	int end=4;

	if(player<4 && player>-1)
	{
		start=player;
		end=player+1;
	}

	for(int i=start;i<end;i++)
	{
		CharacterAction* action=moveSet[i].head;

		while(action!=NULL)
		{
			CharacterAction act=*action;
			//cout<<msg<<endl;

			if(act.getKey()==msg)
			{
				addToActionStack(i+1,act.getAction());
			}
			
			action=(*action).getNext();
		}
	}

}

string  InputController::findMessage(WPARAM msg,int player)
{
	int start=0;
	int end=4;

	if(player<4 && player>-1)
	{
		start=player;
		end=player+1;
	}

	for(int i=start;i<end;i++)
	{
		CharacterAction* action=moveSet[i].head;

		while(action!=NULL)
		{
			CharacterAction act=*action;
			//cout<<msg<<endl;

			if(act.getKey()==msg)
			{
				//addToActionStack(i+1,act.getAction());
				return act.getAction();
			}
			
			action=(*action).getNext();
		}
	}

	return "nothing";
}

void InputController::addtoMoveSet(int player,WPARAM key,string action)
{
	moveSet[player-1].newAction(key,action);
}

void InputController::display()
{
	cout<<endl;
	for(int i=0;i<4;i++)
	{
		cout<<endl<<"PLAYER "<<i<<" Move Set"<<endl;
		moveSet[i].display();
	}
	cout<<endl<<endl;
	cout<<"Action Stack"<<endl;
	actionStack.display();
}
void InputController::addToActionStack(int characterID,string action)
{
	stringstream val;

	val<<"player ";
	val<<characterID;
	string player = val.str();

	actionStack.addAction(player,action);
}
ActionNode* InputController::getNextAction()
{
	return actionStack.getNextAction();
}
void InputController::blankPreviousStates(int player)
{
	for(int i=0;i<4;i++)
	{
		controllers[player].PrevDPadState[i]=false;
		controllers[player].PrevJoypadState[i]=false;
	}
}
//=============================================================================
// Check for connected controllers
//=============================================================================
void InputController::checkControllers()
{
    DWORD result;
    for( DWORD i = 0; i <MAX_CONTROLLERS; i++)
    {
        result = XInputGetState(i, &controllers[i].state);

        if(result == ERROR_SUCCESS)
		{
			if(!controllers[i].connected)// || (controllers[i].LStick[0]<-3500 && controllers[i].LStick[1]<-3500))
			{
				setDefaultStickPos(i);
				blankPreviousStates(i);
			}
			//controllers[i].state.Gamepad;
            controllers[i].connected = true;
		}
        else
		{
            controllers[i].connected = false;
		}
    }
}
void InputController::setDefaultStickPos(int player)
{
	if(controllers[player].connected)
	{
	controllers[player].LStick[0]=controllers[player].state.Gamepad.sThumbLX;
	controllers[player].LStick[1]=controllers[player].state.Gamepad.sThumbLY;
	}

}
//=============================================================================
// Check state of controllers and create appropriate messages
//=============================================================================
void InputController::ControllerAction()
{
	for(int i=0;i<MAX_CONTROLLERS;i++)
	{
		if(controllers[i].connected)
        {
			//get the wButton total from the controller
				DWORD buttons=controllers[i].state.Gamepad.wButtons;
				controllers[i].state.Gamepad;

			//Joystick
				if(!controllers[i].PrevLStickState[0])
				{
					cout<< " ";
				}
				if(!controllers[i].PrevLStickState[1])
				{
					cout<<" ";
				}
				/*
				if(controllers[i].state.Gamepad.sThumbLX<controllers[i].LStick[0])
					processMessage(GAMEPAD_DPAD_LEFT,i);
				if(controllers[i].state.Gamepad.sThumbLX>controllers[i].LStick[0])
					processMessage(GAMEPAD_DPAD_RIGHT,i);
				if(controllers[i].state.Gamepad.sThumbLY>controllers[i].LStick[1])
					processMessage(GAMEPAD_DPAD_UP,i);
				if(controllers[i].state.Gamepad.sThumbLY<controllers[i].LStick[1])
					processMessage(GAMEPAD_DPAD_DOWN,i);
				*/
				
					
			//continually test the total, subtracting amounts
			//and sending messages until the total is sero

			
				string msg;

				//Joypad
				if(buttons&GAMEPAD_Y)
				{
					//get the proper action (if one exists)
					msg=findMessage(GAMEPAD_Y,i);					
					
					if(msg!="nothing")
					{

						//add hold or press according to previous button state
						if(controllers[i].PrevJoypadState[0])
							msg+=" hold";
						else 
							msg+=" press";
						
						//add to the action stack
						addToActionStack(i+1,msg);						
					}	
				}
				

				if(buttons&GAMEPAD_X)
				{
					//get the proper action (if one exists)
					msg=findMessage(GAMEPAD_X,i);					
					
					if(msg!="nothing")
					{

						//add hold or press according to previous button state
						if(controllers[i].PrevJoypadState[1])
							msg+=" hold";
						else 
							msg+=" press";

						//add to the action stack
						addToActionStack(i+1,msg);						
					}	
				}				

				if(buttons&GAMEPAD_B)
				{
					//get the proper action (if one exists)
					msg=findMessage(GAMEPAD_B,i);					
					
					if(msg!="nothing")
					{

						//add hold or press according to previous button state
						if(controllers[i].PrevJoypadState[2])
							msg+=" hold";
						else 
							msg+=" press";

						//add to the action stack
						addToActionStack(i+1,msg);						
					}	
				}


				if(buttons & GAMEPAD_A)
				{
					//get the proper action (if one exists)
					msg=findMessage(GAMEPAD_A,i);					
					
					if(msg!="nothing")
					{

						//add hold or press according to previous button state
						if(controllers[i].PrevJoypadState[3])
							msg+=" hold";
						else 
							msg+=" press";

						//add to the action stack
						addToActionStack(i+1,msg);						
					}	
				}				
				
				


		//D pad
				if(buttons & GAMEPAD_DPAD_RIGHT)
				{
					//get the proper action (if one exists)
					msg=findMessage(GAMEPAD_DPAD_RIGHT,i);					
					
					if(msg!="nothing")
					{

						//add hold or press according to previous button state
						if(controllers[i].PrevDPadState[0])
							msg+=" hold";
						else 
							msg+=" press";

						//add to the action stack
						addToActionStack(i+1,msg);
					}	
				}

				if(buttons & GAMEPAD_DPAD_LEFT)
				{	
					//get the proper action (if one exists)
					msg=findMessage(GAMEPAD_DPAD_LEFT,i);					
					
					if(msg!="nothing")
					{

						//add hold or press according to previous button state
						if(controllers[i].PrevDPadState[1])
							msg+=" hold";
						else 
							msg+=" press";

						//add to the action stack
						addToActionStack(i+1,msg);						
					}	
				}

				if(buttons & GAMEPAD_DPAD_DOWN)
				{
					//get the proper action (if one exists)
					msg=findMessage(GAMEPAD_DPAD_DOWN,i);					
					
					if(msg!="nothing")
					{

						//add hold or press according to previous button state
						if(controllers[i].PrevDPadState[2])
							msg+=" hold";
						else 
							msg+=" press";

						//add to the action stack
						addToActionStack(i+1,msg);						
					}	
				}
				

				if(buttons & GAMEPAD_DPAD_UP)
				{
					//get the proper action (if one exists)
					msg=findMessage(GAMEPAD_DPAD_UP,i);					
					
					if(msg!="nothing")
					{

						//add hold or press according to previous button state
						if(controllers[i].PrevDPadState[3])
							msg+=" hold";
						else 
							msg+=" press";

						//add to the action stack
						addToActionStack(i+1,msg);						
					}	
				}

				//document button presses
					//Joypad
					controllers[i].PrevJoypadState[0]=	buttons&GAMEPAD_Y;
					controllers[i].PrevJoypadState[1]=	buttons&GAMEPAD_X;
					controllers[i].PrevJoypadState[2]=	buttons&GAMEPAD_B;
					controllers[i].PrevJoypadState[3]=	buttons&GAMEPAD_A;

					//DPad
					controllers[i].PrevDPadState[0]=	buttons&GAMEPAD_DPAD_RIGHT;
					controllers[i].PrevDPadState[1]=	buttons&GAMEPAD_DPAD_LEFT;
					controllers[i].PrevDPadState[2]=	buttons&GAMEPAD_DPAD_DOWN;
					controllers[i].PrevDPadState[3]=	buttons&GAMEPAD_DPAD_UP;
				
					//Lstick
					controllers[i].PrevLStickState[0]= controllers[i].state.Gamepad.sThumbLX>(controllers[i].LStick[0]-DEADZONE) && controllers[i].state.Gamepad.sThumbLX<(controllers[i].LStick[0]+DEADZONE);
					controllers[i].PrevLStickState[1]= controllers[i].state.Gamepad.sThumbLY>(controllers[i].LStick[1]-DEADZONE) && controllers[i].state.Gamepad.sThumbLY<(controllers[i].LStick[1]+DEADZONE);
			

			
		}
	}
}
