#pragma once

#include <windows.h>
#include <iostream>

using namespace std;
class CharacterAction
{
public:
	//variables
		WPARAM key;
		string action;

		CharacterAction* next;
		CharacterAction* prev;

	CharacterAction(void);
	CharacterAction(WPARAM key,string act);
	CharacterAction(WPARAM key,string act,CharacterAction* nt, CharacterAction* pv);
	~CharacterAction(void);
	void setNext(CharacterAction* ca);
	void setPrev(CharacterAction* ca);
	void setAction(string act);
	string getAction();
	CharacterAction* getNext();
	CharacterAction* getPrev();
	void purgePrev();
	void purgeNext();
	void setKey(WPARAM ky);
	WPARAM getKey();
};

