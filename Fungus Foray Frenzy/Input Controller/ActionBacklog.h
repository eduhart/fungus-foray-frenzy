#pragma once

#include <Windows.h>
#include <iostream>
#include "ActionNode.h"
#include <string>

using namespace std;

class ActionBacklog
{
public:
	//variables
		ActionNode* head;
		ActionNode* tail;
		int numElements;

	ActionBacklog(void);
	~ActionBacklog(void);
	void addAction(string characterID,string action);
	ActionNode* getNextAction();
	void display();
};

