#pragma once
//#ifndef _GAME_H                 // prevent multiple definitions if this 
#define _GAME_H                 // ..file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <WindowsX.h>
#include <XInput.h>
#include <iostream>
#include "ActionList.h"
#include "ActionBacklog.h"
#include <string>
#include <sstream>
#include "../constants.h"

using namespace std;

class InputController
{
public:
	//gamepad relevant variables
		// Gamepad constants
			/*
			struct ControllerState
			{
				XINPUT_STATE        state;
				XINPUT_VIBRATION    vibration;
				float               vibrateTimeLeft;    // mSec
				float               vibrateTimeRight;   // mSec
				bool                connected;
			};


		// Bit corresponding to gamepad button in state.Gamepad.wButtons
			const DWORD GAMEPAD_DPAD_UP        = 0x0001;
			const DWORD GAMEPAD_DPAD_DOWN      = 0x0002;
			const DWORD GAMEPAD_DPAD_LEFT      = 0x0004;
			const DWORD GAMEPAD_DPAD_RIGHT     = 0x0008;
			const DWORD GAMEPAD_START_BUTTON   = 0x0010;
			const DWORD GAMEPAD_BACK_BUTTON    = 0x0020;
			const DWORD GAMEPAD_LEFT_THUMB     = 0x0040;
			const DWORD GAMEPAD_RIGHT_THUMB    = 0x0080;
			const DWORD GAMEPAD_LEFT_SHOULDER  = 0x0100;
			const DWORD GAMEPAD_RIGHT_SHOULDER = 0x0200;
			const DWORD GAMEPAD_A              = 0x1000;
			const DWORD GAMEPAD_B              = 0x2000;
			const DWORD GAMEPAD_X              = 0x4000;
			const DWORD GAMEPAD_Y              = 0x8000;
			*/


	//variables
		ActionList moveSet[4];
		ActionBacklog actionStack;
		ControllerState controllers[4];    // state of controllers


	InputController(void);
	~InputController(void);
	void processMessage(WPARAM msg);
	void processMessage(WPARAM msg,int player);
	string findMessage(WPARAM msg,int player);
	void addtoMoveSet(int player,WPARAM key,string action);
	void blankPreviousStates(int player);
	ActionNode* getNextAction();
	void display();
	void checkControllers();
	void readControllers();
	void ControllerAction();
private :
	void addToActionStack(int characterID,string action);
	void setDefaultStickPos(int player);

};

