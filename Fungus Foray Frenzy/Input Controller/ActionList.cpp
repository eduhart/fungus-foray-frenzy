#include "ActionList.h"


ActionList::ActionList(void)
{
	numElements=0;
	head=NULL;
	tail=NULL;
}


ActionList::~ActionList(void)
{
}

void ActionList::newAction(WPARAM key,string action)
{
	/*CharacterAction p,n;
	CharacterAction node(key,action);*/

	//if(head==NULL)
	if(numElements==0)
	{
		head=new CharacterAction(key,action);
		numElements++;
	}
	//else if(head!=NULL && tail==NULL)
	else if(numElements==1)
	{
		tail=new CharacterAction(key,action,NULL,head);
		(*head).setNext(tail);
		numElements++;
	}
	else
	{
		CharacterAction* prev=tail;
		tail=new CharacterAction(key,action,NULL,prev);
		(*prev).setNext(tail);	
		numElements++;
	}

}

void ActionList::display()
{
	cout<<"\n-------------------------"<<endl;
	if(head!=NULL)
	{
		CharacterAction* node=head;
		bool more=false;

		do
		{
			//make the halt flag false
				more=false;

			//get the pointer to the info array and use it to get the 
			//information
				string info=(*node).getAction();
				WPARAM key=(*node).getKey();
			//display the information
				cout<<"Key: "<<key<<" Action: "<<info<<endl;
			
			//change the halt flag if necessary	
				if((*node).getNext()!=NULL)
				{
					node=(*node).getNext();
					more=true;
				}
				else 
				{
					more=false;
				}
		}
		while(more);
	}
	//cout<<"\n^------------------------\n"<<endl;
	
}

/*CharacterAction* ActionList::getNextAction()
{
		//get head node
			CharacterAction* node=head;

	if(numElements>2)
	{
		//asign new head
			head=(*head).getNext();
			(*head).purgePrev();

		//deincrement element count
			numElements--;

		//return node
			return node;
	}
	else if(numElements==2)
	{
		//change head to tail
			head=tail;

		//nullify tail
			tail=NULL;

		//nullify connections
		(*head).purgePrev();
		(*head).purgeNext();

		//deincrement element count
			numElements--;

		//return node
			return node;
	}
	else if(numElements==1)
	{
		//destroy head
			head=NULL;

		//deincrement element count
			numElements--;

		//return node
			return node;
	}
	else return NULL;
}*/

