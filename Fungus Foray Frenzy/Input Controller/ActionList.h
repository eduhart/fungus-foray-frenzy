#pragma once

#include <windows.h>
#include "CharacterAction.h"
#include <string>

using namespace std;
class ActionList
{
public:
	//variables
		CharacterAction* head;
		CharacterAction* tail;
		int numElements;


	ActionList(void);
	~ActionList(void);
	void newAction(WPARAM key,string action);
	void setHead(CharacterAction act);
	void addNode(CharacterAction act);
	//CharacterAction* getNextAction();
	void addAction();
	void getNextAction();
	void display();

	
};

