#include "ActionBacklog.h"

ActionBacklog::ActionBacklog(void)
{
	head=NULL;
	tail=NULL;
	numElements=0;
}


ActionBacklog::~ActionBacklog(void)
{
}

void ActionBacklog::addAction(string characterID,string action)
{
	ActionNode p,n;
	ActionNode node(characterID,action);

	//if(head==NULL)
	if(numElements==0)
	{
		head=new ActionNode(characterID,action);
		numElements++;
	}
	//else if(head!=NULL && tail==NULL)
	else if(numElements==1)
	{
		tail=new ActionNode(characterID,action,NULL,head);
		(*head).setNext(tail);
		numElements++;
	}
	else
	{
		ActionNode* prev=tail;
		tail=new ActionNode(characterID,action,NULL,prev);
		(*prev).setNext(tail);	
		numElements++;
	}

}

ActionNode* ActionBacklog::getNextAction()
{
		//get head node
			ActionNode* node=head;

	if(numElements>2)
	{
		//asign new head
			head=(*head).getNext();
			(*head).purgePrev();

		//deincrement element count
			numElements--;

		//return node
			return node;
	}
	else if(numElements==2)
	{
		//change head to tail
			head=tail;

		//nullify tail
			tail=NULL;

		//nullify connections
		(*head).purgePrev();
		(*head).purgeNext();

		//deincrement element count
			numElements--;

		//return node
			return node;
	}
	else if(numElements==1)
	{
		//destroy head
			head=NULL;

		//deincrement element count
			numElements--;

		//return node
			return node;
	}
	else return NULL;
}

void ActionBacklog::display()
{
	cout<<"\n-------------------------"<<endl;
	if(head!=NULL)
	{
		ActionNode* node=head;
		bool more=false;

		do
		{
			//make the halt flag false
				more=false;

			//get the pointer to the info array and use it to get the 
			//information
				string* info=(*node).getInfo();
				string info1=*info;
				string info2=*(info+1);

			//display the information
				cout<<(info1)<<" "<<(info2)<<endl;
			
			//change the halt flag if necessary	
				if((*node).getNext()!=NULL)
				{
					node=(*node).getNext();
					more=true;
				}
				else 
				{
					more=false;
				}
		}
		while(more);
	}
	//cout<<"\n^------------------------\n"<<endl;
	
}