#include "CharacterAction.h"


CharacterAction::CharacterAction(void)
{
}

CharacterAction::CharacterAction(WPARAM ky,string act)
{
	key=ky;
	setAction(act);
	next=NULL;
	prev=NULL;
}

CharacterAction::CharacterAction(WPARAM ky,string act,CharacterAction* nt, CharacterAction* pv)
{
	key=ky;
	setAction(act);
	next=nt;
	prev=pv;
}

CharacterAction::~CharacterAction(void)
{

}

void CharacterAction::setAction(string act)
{
	action=act;
}

void CharacterAction::setNext(CharacterAction* ca)
{
	next=ca;
}

void CharacterAction::setPrev(CharacterAction* ca)
{
	prev=ca;
}

void CharacterAction::setKey(WPARAM ky)
{
	key=ky;
}

CharacterAction* CharacterAction::getNext()
{
	return next;
}

CharacterAction* CharacterAction::getPrev()
{
	return prev;
}

WPARAM CharacterAction::getKey()
{
	return key;
}

void CharacterAction::purgeNext()
{
	next=NULL;
}

void CharacterAction::purgePrev()
{
	prev=NULL;
}

string CharacterAction::getAction()
{
	return action;
}