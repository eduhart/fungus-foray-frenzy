#pragma once
#include <Windows.h>
#include <iostream>

using namespace std;
class ActionNode
{
public:
	//variables
		string info[2];
		ActionNode* next;
		ActionNode* prev;

	ActionNode(void);
	ActionNode(string characterID,string action,ActionNode* nt,ActionNode* pv);
	ActionNode(string characterID,string action);
	~ActionNode(void);
	void setNext(ActionNode* node);
	void setPrev(ActionNode* node);
	void setInfo(string info1, string info2);
	ActionNode* getNext();
	ActionNode* getPrev();
	string* getInfo();
	void purgeNext();
	void purgePrev();
};

