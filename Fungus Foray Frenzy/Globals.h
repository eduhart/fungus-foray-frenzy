#pragma once

enum PlayerType
{
	PLAYER_ZERO,
	PLAYER_ONE,
	PLAYER_TWO,
	PLAYER_THREE,

	NUM_PLAYERS
};

enum CameraType
{
	TOP_LEFT,
	TOP_RIGHT,
	BOTTOM_LEFT,
	BOTTOM_RIGHT,

	NUM_CAMERAS
};

enum ItemType
{
	NO_ITEM, 
	SHOVEL,
	BUG_SPRAY,
	LANTERN,
	COW_FEED,

	NUM_ITEM_TYPES

};

enum MushroomType
{
	NO_TYPE,
	PSYCHOACTIVE,
	EDIBLE,
	GLOWING, 
	POISONOUS,
	LITTLE_BROWN,

	NUM_MUSHROOM_TYPES
};

enum BiomeType
{
	FOREST,
	SWAMP,
	COW_FIELDS,
	CAVE,

	NUM_BIOMES
};

enum CardinalDirection
{
	NORTH,
	SOUTH,
	EAST,
	WEST,

	NUM_DIRECTIONS
};

enum Layer
{
	BACKGROUND,
	COLLIDABLE,
	PLAYER,
	TOP,

	NUM_LAYERS
};

enum GameState
{
	START,
	MAIN,
	END,
	
	NUM_STATES
};

struct TexInfo
{
	char* m_resource;
	int m_frameWidth;
	int m_frameHeight;
	int m_frameColumns;
	int m_camLayer;
	bool m_loops;
	TexInfo(char* resource, int frameW = 0, int frameH = 0, int columns = 0, int layer = 1, bool looping = false)
		: m_resource(resource)
		, m_frameWidth(frameW)
		, m_frameHeight(frameH)
		, m_frameColumns(columns)
		, m_camLayer(layer)
		, m_loops(looping)
	{}

	void reset()
	{
		m_resource = "";
		m_frameWidth = 0;
		m_frameHeight = 0;
		m_frameColumns = 0;
		m_camLayer = 0;
		m_loops = false;
	}
};

struct Point
{
	int m_x;
	int m_y;
	Point(int x, int y)
		: m_x(x)
		, m_y(y)
	{}
	Point()
		: m_x(0)
		, m_y(0)
	{}
};

static const Point INVALID_POS(-1,-1);
static const int TILE_SIZE = 16;
static const int CAM_TILES_X = 32;
static const int CAM_TILES_Y = 24;

static const int PLAYER_SPEED = 2;
static const int PLAYER_SPEED_SWAMP = 1;

static const float FRAME_DELAY = 0.4f;
static const float GUI_FRAME_DELAY = 0.1f;

static const int TOTAL_TIME = 600; //game length in seconds (10 minutes) 600s