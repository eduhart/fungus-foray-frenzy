#include "SoundVault.h"

//------Constants

const char* SoundVault::FOREST_THEME_PATH	= "Audio/Forest Theme.wav";
const char* SoundVault::SWAMP_THEME_PATH	= "Audio/Swamp Theme.wav";
const char* SoundVault::CAVE_THEME_PATH		= "Audio/Cave Theme.wav";
const char* SoundVault::FARM_THEME_PATH		= "Audio/Farm Theme.wav";

//------Construction/Destruction

SoundVault::SoundVault()
{
	//Using irrKlang engine for audio

	//Create the engine
	engine = createIrrKlangDevice();

	//Make sure it was successfully created
	if ( !engine )
	{
		printf( "Could not startup engine\n" );
	}
	else
	{
	}
}

SoundVault::~SoundVault()
{
}

void SoundVault::playBackgroundSong( BiomeType biome )
{
	char pathToPlay[100];

	//Figure out what biome it is
	switch( biome )
	{
		case BiomeType::FOREST:
			strcpy( pathToPlay, FOREST_THEME_PATH );
			break;
		case BiomeType::SWAMP:
			strcpy( pathToPlay, SWAMP_THEME_PATH );
			break;
		case BiomeType::CAVE:
			strcpy( pathToPlay, CAVE_THEME_PATH );
			break;
		case BiomeType::COW_FIELDS:
			strcpy( pathToPlay, FARM_THEME_PATH );
			break;
	}
	
	//Play the biome's theme song (if the pathToPlay isn't empty)
	if ( pathToPlay != "" )
	{
		engine->stopAllSounds();
		engine->play2D( pathToPlay, true );
	}
}